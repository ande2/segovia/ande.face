################################################################################
#	Programa		:	func_dele.4gl
#	Descripcion	:	elimina registros
#	Funciones	:  
#	
#	Parametros	
#		Recibe	:
#		Devuelve	:
#
#	SCCS id No	: %Z% %W%
#	Autor			: Erick Valdez
#	Fecha			: 
#	Path			: %P%
#
#	Control de cambios
#
#	Programador			Fecha			Descripcion de la modificacion
#
################################################################################
GLOBALS "admm0006_glob.4gl"

-- Funcion que prepara el cursor y las variables para modificar 
FUNCTION delete_init()

	-- Declaracion de variables
DEFINE 
	prepvar char(250)
 
	-- Se declara el cursor que selecciona el registro que estamos utilizando
DECLARE lockfunc CURSOR FOR
	SELECT * FROM mempr   
	WHERE mempr.empr_id = gr_empr.empr_id
   FOR UPDATE

	-- Le asignamos el valor a la variable de eliminacion
   LET prepvar = "DELETE FROM mempr",
                " WHERE CURRENT OF lockfunc"

	-- Preparamos la variable de eliminacion
   PREPARE delete_func FROM prepvar

END FUNCTION

-- Funci�n que elimina registros
FUNCTION delete_func()
DEFINE 
	mensaje  CHAR(50),
   cuantos SMALLINT

   LET int_flag = FALSE
   LET cuantos = 0
		
	SELECT COUNT (*) 
	INTO cuantos
	FROM mempr
	WHERE empr_id = gr_empr.empr_id

	IF cuantos > 0 THEN
		LET int_flag = TRUE
	END IF

   IF int_flag = TRUE   THEN
      CALL box_elifila() RETURNING int_flag

      IF int_flag THEN
         LET int_flag = FALSE

         BEGIN WORK

         	WHENEVER ERROR CONTINUE

            OPEN lockfunc 

            FETCH lockfunc INTO gr_empr.*

      		 WHENEVER ERROR STOP

            IF SQLCA.SQLCODE = NOTFOUND THEN
               ERROR "Este registro ya fue eliminado."
               ROLLBACK WORK
               RETURN int_flag
            ELSE
               IF SQLCA.SQLCODE < 0 THEN
                  ERROR "El registro no puede ser bloqueado para eliminaci�n."
                  ROLLBACK WORK
               ELSE
                  WHENEVER ERROR CONTINUE
                  EXECUTE delete_func
                  WHENEVER ERROR STOP

                  IF SQLCA.SQLCODE < 0 THEN
                     ERROR "El error n�mero ", SQLCA.SQLCODE USING
                     "-<<<<", " ha ocurrido."
                     ROLLBACK WORK
                  ELSE
                     COMMIT WORK
                     CLEAR FORM
							CALL box_valdato("El Registro ha sido Eliminado.")
                  END IF
                END IF
            END IF
     		 ELSE
         	LET mensaje = "Eliminaci�n Cancelada"
        	   CALL box_valdato(mensaje)
        		LET int_flag = TRUE
     		 END IF
		ELSE
			CALL box_valdato("El Registro no Puede ser Eliminado ya que se Utiliza en Otros Modulos")
			
			CLEAR FORM
	   END IF
   	RETURN int_flag
END FUNCTION
