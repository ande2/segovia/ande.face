################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : empr_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               empr_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "admm0006_glob.4gl"

FUNCTION empr_busca_init()
DEFINE
	select_stmt3			VARCHAR(1000)
	
LET select_stmt3 = "SELECT empr_id , ",
                  " empr_nit, ",
                  " empr_nom,",
                  " empr_dir1,",
                  " empr_dir2,",
                  " empr_idioma,",
                  " empr_codpos,",
                  " pais_nom,",
                  " dep_nom,",
                  " mun_nom,",
                  " empr_nomct,",
                  " empr_db,",
                  " est_id FROM mempr WHERE empr_id = ? "
PREPARE ex_stmt3 FROM select_stmt3

END FUNCTION

FUNCTION busca_empr()
DEFINE
	empr_query		CHAR(300),
	where_clause	CHAR(250),
	empr_cnt        SMALLINT,
   condicion        CHAR(300),
	empr_count		CHAR(300),
   mensaje          CHAR(100),
   respuesta        CHAR(6),
   lint_flag        SMALLINT
CLEAR FORM
--DISPLAY vempr_nomlog TO bmp1
CALL encabezado()
DISPLAY "Ingrese criterio de b�squeda y presione aceptar ",
        "para iniciar b�squeda.","" AT 22,8
DISPLAY "Solo aceptar para b�squeda general.","" AT 23,8

let gtit_enc="BUSQUEDA REGISTRO"
DISPLAY BY NAME gtit_enc                                                                                
LET int_flag = FALSE

CONSTRUCT BY NAME where_clause ON empr.empr_id ,
                              empr.empr_nit,
                              empr.empr_nom,
                              empr.empr_dir1,
                              empr.empr_dir2,
                              empr.empr_idioma,
                              empr.empr_codpos,
                              empr.pais_nom,
                              empr.dep_nom,
                              empr.mun_nom,
                              empr.empr_nomct,
                              empr.empr_db,
                              empr.est_id
 
	BEFORE CONSTRUCT
	CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
   	CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
	CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

    --ON KEY (CONTROL - W)
        --CALL showhelp (2)
			
END CONSTRUCT

IF int_flag = TRUE THEN
	LET int_flag = FALSE
	CLEAR FORM
	CALL encabezado()
	ERROR "B�squeda Abortada."
	RETURN FALSE, 0
END IF	 

LET where_report = where_clause
LET empr_query = "SELECT empr_id FROM mempr WHERE ", where_clause CLIPPED,
                " ORDER BY 1 " 

LET empr_count = "SELECT count(*) FROM mempr WHERE ", where_clause CLIPPED

PREPARE ex_stmt FROM empr_query

PREPARE ex_stmt2 FROM empr_count

DECLARE empr_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

OPEN empr_ptr

FETCH FIRST empr_ptr INTO gr_empr.*

IF SQLCA.SQLCODE = 100 THEN
	CALL box_valdato("No existen datos con estas condiciones.")
	MESSAGE "[Control-W] = Ayuda"
	CLOSE empr_ptr
	RETURN FALSE,0
ELSE
	DECLARE empr_all CURSOR FOR ex_stmt3	
	OPEN empr_all USING gr_empr.empr_id

	FETCH empr_all INTO gr_empr.*

	IF SQLCA.SQLCODE = NOTFOUND THEN
		CALL box_valdato("No existen datos con estas Condiciones.")
		CLOSE empr_ptr
		RETURN FALSE,0
	ELSE
		DECLARE contador_ptr CURSOR FOR ex_stmt2
		OPEN contador_ptr
		FETCH contador_ptr INTO empr_cnt
		CLOSE contador_ptr
		CALL empr_desp()
		MESSAGE "Existen ", empr_cnt USING "<<<<", " Articulos."
		SLEEP 2
		MESSAGE ""
		RETURN TRUE, empr_cnt
	END IF				
END IF				
END FUNCTION

FUNCTION fetch_cat(fetch_flag)
DEFINE
	fetch_flag		SMALLINT

FETCH RELATIVE fetch_flag empr_ptr INTO gr_empr.*

IF SQLCA.SQLCODE = NOTFOUND THEN
	IF fetch_flag = 1 THEN
		CALL box_valdato("Est� posicionado en el final de la lista.")
	ELSE
		CALL box_valdato("Est� posicionado en el principio de la lista.")
	END IF
ELSE
	OPEN empr_all USING gr_empr.empr_id

	FETCH empr_all INTO gr_empr.*
	IF SQLCA.SQLCODE = NOTFOUND THEN
		CLEAR FORM
		CALL encabezado()	
		DISPLAY BY NAME gr_empr.*
		CALL box_valdato("El registro fue eliminado desde la �ltima Consulta.")
	ELSE
		CALL empr_desp()
	END IF
	CLOSE empr_all
END IF
END FUNCTION

FUNCTION empr_desp()
SELECT a.empr_id ,
      a.empr_nit,
      a.empr_nom,
      a.empr_dir1,
      a.empr_dir2,
      a.empr_idioma,
      a.empr_codpos,
      a.pais_nom,
      a.dep_nom,
      a.mun_nom,
      a.empr_nomct,
      a.empr_db,
      a.est_id
 INTO gr_empr.*
FROM mempr a
WHERE a.empr_id= gr_empr.empr_id

   
DISPLAY BY NAME gr_empr.empr_id ,
                              gr_empr.empr_nit,
                              gr_empr.empr_nom,
                              gr_empr.empr_dir1,
                              gr_empr.empr_dir2,
                              gr_empr.empr_idioma,
                              gr_empr.empr_codpos,
                              gr_empr.pais_nom,
                              gr_empr.dep_nom,
                              gr_empr.mun_nom,
                              gr_empr.empr_nomct,
                              gr_empr.empr_db,
                              gr_empr.est_id
END FUNCTION

FUNCTION limpiar() 
WHENEVER ERROR CONTINUE
CLOSE empr_ptr
WHENEVER ERROR STOP
END FUNCTION
