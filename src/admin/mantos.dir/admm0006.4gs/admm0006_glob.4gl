################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# JMonterroso Mon Dec  8 13:57:44 CST 2003 Se agrego variable global de la BD 
################################################################################

SCHEMA "face"


GLOBALS
DEFINE
   g_sccs_var                 CHAR(70), # Variable para SCCS
   gr_empr,nr_empr,ur_empr    RECORD
      empr_id                 LIKE mempr.empr_id,
      empr_nit                LIKE mempr.empr_nit,
      empr_nom                LIKE mempr.empr_nom,
      empr_dir1               LIKE mempr.empr_dir1,
      empr_dir2               LIKE mempr.empr_dir2,
      empr_idioma             LIKE mempr.empr_idioma,
      empr_codpos             LIKE mempr.empr_codpos,
      pais_nom                LIKE mempr.pais_nom,
      dep_nom                 LIKE mempr.dep_nom,
      mun_nom                 LIKE mempr.mun_nom,
      empr_nomct              LIKE mempr.empr_nomct,
      empr_db                 LIKE mempr.empr_db,
      est_id                  LIKE mempr.est_id
   END RECORD,
   g_empr                     RECORD LIKE mempr.*,
   dbperm,
   dbname                     CHAR(40),
   vempr_nomlog               CHAR(15),
   usuario                    CHAR(20),
   hr                         DATE, 
   gtit_enc,   
   gtit_pie,
   nombre_departamento        VARCHAR(80),
   where_report               VARCHAR(1000)
END GLOBALS
