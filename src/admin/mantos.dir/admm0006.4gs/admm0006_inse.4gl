################################################################################
# Funcion     : %M%
# Descripcion : Modulo para ingreso de catalogo 
# Funciones   : empr_inse()
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : EAlvarezs
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

GLOBALS "admm0006_glob.4gl"

FUNCTION empr_inse()
DEFINE 
	err_msg        CHAR(80),
	w_cuenta		   INTEGER, -- Cuenta los registros que ya existen en mcat
	condicion	   CHAR(300),
	respuesta      char(06),
   lint_flag      SMALLINT ,
	lexiste        SMALLINT 


LET int_flag = FALSE

CLEAR FORM
CALL encabezado()

let gtit_enc="INGRESO REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

DISPLAY BY NAME gtit_enc                                                                                
INPUT BY NAME  gr_empr.empr_nit,
               gr_empr.empr_nom,
               gr_empr.empr_dir1,
               gr_empr.empr_dir2,
               gr_empr.empr_idioma,
               gr_empr.empr_codpos,
               gr_empr.pais_nom,
               gr_empr.dep_nom,
               gr_empr.mun_nom,
               gr_empr.empr_nomct,
               gr_empr.empr_db,
               gr_empr.est_id ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
 
	BEFORE INPUT
	CALL fgl_dialog_setkeylabel("ACCEPT","Grabar")
	CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")

    --ON KEY (CONTROL-W)
      --  CALL showhelp (1)
    
	ON KEY(CONTROL-B)


   AFTER FIELD empr_nom
      IF gr_empr.empr_nom IS NULL THEN
         CALL box_valdato("Debe ingresar el nombre de la empresa.")
         NEXT FIELD empr_nom
      ELSE
         LET lexiste = 0
         SELECT count(*)
           INTO lexiste
           FROM mempr
          WHERE mempr.empr_nom = gr_empr.empr_nom
         IF lexiste > 0 THEN
            CALL box_valdato("Existen datos con estas condiciones.")
            LET gr_empr.empr_nom = NULL
            NEXT FIELD empr_nom
         END IF
      END IF

   AFTER FIELD empr_nit
      IF gr_empr.empr_nit IS NULL THEN
         CALL box_valdato("Debe ingresar el numero de nit.")
         NEXT FIELD empr_nit
      END IF


	AFTER INPUT
		IF NOT int_flag THEN
			IF gr_empr.empr_nom IS null THEN
				NEXT FIELD empr_nom
			END IF

			IF gr_empr.empr_nit IS NULL THEN
				NEXT FIELD empr_nit
			ELSE

				LET gr_empr.empr_nom=upshift(gr_empr.empr_nom)
                		LET lexiste = 0
                		SELECT count(*)
                		INTO lexiste
                		FROM mempr
                		WHERE mempr.empr_nom = gr_empr.empr_nom
                		IF lexiste > 0 THEN
                        CALL box_valdato("Existen datos con estas condiciones.")
					          LET gr_empr.empr_nom = null
                         NEXT FIELD empr_nom 
                 		END IF
			END IF

			CALL box_gradato("Desea guardar la informaci�n.") 
			RETURNING respuesta
			CASE
				WHEN respuesta = "Si"
					LET int_flag = FALSE
				WHEN respuesta = "No"
					LET int_flag = TRUE
				WHEN respuesta = "Cancel"
					CONTINUE INPUT
			END CASE
		ELSE
			CALL box_inter() RETURNING int_flag
			IF int_flag = TRUE THEN 
				EXIT INPUT
			ELSE
				CONTINUE INPUT
			END IF
		END IF
END INPUT

IF int_flag = TRUE THEN
   LET int_flag = FALSE
   LET gr_empr.* = nr_empr.*
   CLEAR FORM
	CALL encabezado()
   CALL box_valdato("Ingreso de Datos Cancelado.")
    MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

   RETURN FALSE
END IF
LET gr_empr.empr_id = 0
BEGIN WORK
 
WHENEVER ERROR CONTINUE
--INSERT INTO mempr (VALUES(gr_empr.*)
INSERT INTO mempr (empr_id ,
empr_nit,
empr_nom,
empr_dir1,
empr_dir2,
empr_idioma,
empr_codpos,
pais_nom,
dep_nom,
mun_nom,
empr_nomct,
empr_db,
est_id)
VALUES(gr_empr.empr_id ,
               gr_empr.empr_nit,
               gr_empr.empr_nom,
               gr_empr.empr_dir1,
               gr_empr.empr_dir2,
               gr_empr.empr_idioma,
               gr_empr.empr_codpos,
               gr_empr.pais_nom,
               gr_empr.dep_nom,
               gr_empr.mun_nom,
               gr_empr.empr_nomct,
               gr_empr.empr_db,
               gr_empr.est_id)

WHENEVER ERROR STOP

IF SQLCA.SQLCODE < 0 THEN
	LET err_msg = err_get(SQLCA.SQLCODE)
   ROLLBACK WORK
   ERROR err_msg
   CALL errorlog(err_msg CLIPPED)
	RETURN FALSE
ELSE
   COMMIT WORK
	CALL empr_desp()
   CALL box_valdato("El c�digo ha sido grabado.")
  MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

	RETURN TRUE
END IF

END FUNCTION
