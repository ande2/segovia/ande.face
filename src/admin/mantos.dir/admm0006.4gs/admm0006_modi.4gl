###############################################################################
# Funcion     : %M%
# Descripcion : Modulo para la modificacion del catalogo 
# Funciones   : modi_init()
#               empr_modi()
#               empr_anul()
#               valida_modi()
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : ea
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# HREYES      Tue Sep 13 11:44:24 GMT 2005 se elimina usuario pro10eec
# JMonterroso Mon Dec  8 14:01:11 CST 2003 Se agrego la validacion de la BD
# JMonterroso Tue Oct 21 11:14:30 CDT 2003 Se agrego al usuario pro10eec el 
#                                 poder modificar la descripcion del articulo
################################################################################

GLOBALS "admm0006_glob.4gl"

FUNCTION modi_init()
DEFINE 
	prepvar VARCHAR(1000)
DISPLAY "gr_empr.empr_id ",gr_empr.empr_id
DECLARE lockempr CURSOR FOR
	SELECT   empr_id ,
            empr_nit,
            empr_nom,
            empr_dir1,
            empr_dir2,
            empr_idioma,
            empr_codpos,
            pais_nom,
            dep_nom,
            mun_nom,
            empr_nomct,
            empr_db,
            est_id
   FROM mempr
	WHERE mempr.empr_id = gr_empr.empr_id
	FOR UPDATE

LET prepvar = " UPDATE mempr " ,
               " SET    mempr.empr_nit    = ?, ",
               "        mempr.empr_nom    = ?, ",
               "        mempr.empr_dir1   = ?, ",
               "        mempr.empr_dir2   = ?, ",
               "        mempr.empr_idioma = ?, ",
               "        mempr.empr_codpos = ?, ",
               "        mempr.pais_nom    = ?, ",
               "        mempr.dep_nom     = ?, ",
               "        mempr.mun_nom     = ?, ",
               "        mempr.empr_nomct  = ?, ",
               "        mempr.empr_db     = ?, ",
               "        mempr.est_id      = ? ",
               " WHERE CURRENT OF lockempr"
WHENEVER ERROR CONTINUE
PREPARE upd_empr FROM prepvar
WHENEVER ERROR STOP
IF SQLCA.SQLCODE <> 0 THEN
   DISPLAY sqlerrmessage, " error"
   EXIT PROGRAM(1)
END IF
END FUNCTION

FUNCTION empr_modi()
DEFINE
	mensaje CHAR(100),
	respuesta CHAR(6),
	contador	SMALLINT,
	lexiste SMALLINT


let gtit_enc="MODIFICA REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

BEGIN WORK

OPEN lockempr

WHENEVER ERROR CONTINUE

FETCH lockempr INTO gr_empr.*

WHENEVER ERROR STOP
DISPLAY "-- ",SQLCA.sqlcode, " -- ",sqlerrmessage
IF SQLCA.SQLCODE < 0 OR SQLCA.SQLCODE = NOTFOUND THEN
	ROLLBACK WORK
	CALL box_valdato("No es posible bloquear el registro para Modificarlo.")
ELSE
	LET contador =  0

	LET ur_empr.* = gr_empr.*
	LET int_flag = FALSE

INPUT BY NAME  gr_empr.empr_nit,
               gr_empr.empr_nom,
               gr_empr.empr_dir1,
               gr_empr.empr_dir2,
               gr_empr.empr_idioma,
               gr_empr.empr_codpos,
               gr_empr.pais_nom,
               gr_empr.dep_nom,
               gr_empr.mun_nom,
               gr_empr.empr_nomct,
               gr_empr.empr_db,
               gr_empr.est_id ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
              
		BEFORE INPUT
			CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
			CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")
			CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	
		--ON KEY (CONTROL-W)
			--CALL showhelp (4)

   AFTER FIELD empr_nom
      IF gr_empr.empr_nom IS NULL THEN
         CALL box_valdato("Debe ingresar el nombre de la empresa.")
         NEXT FIELD empr_nom
      ELSE
         LET lexiste = 0
         SELECT count(*)
           INTO lexiste
           FROM mempr
          WHERE mempr.empr_nom = gr_empr.empr_nom
         IF lexiste > 1 THEN
            CALL box_valdato("Existen datos con estas condiciones.")
            LET gr_empr.empr_nom = NULL
            NEXT FIELD empr_nom
         END IF
      END IF

   AFTER FIELD empr_nit
      IF gr_empr.empr_nit IS NULL THEN
         CALL box_valdato("Debe ingresar el numero de nit.")
         NEXT FIELD empr_nit
      END IF


		AFTER INPUT
			IF NOT int_flag THEN
				IF gr_empr.empr_nom IS NULL THEN
					NEXT FIELD empr_nom
				END IF
				IF valida_modi() = TRUE THEN		
					CALL box_gradato("Desea guardar la información.") RETURNING respuesta
					IF respuesta = "Si" THEN
						LET int_flag = FALSE
						EXIT INPUT
					ELSE
			         IF respuesta = "No" THEN
							LET int_flag = TRUE
							EXIT INPUT
						ELSE
						   CONTINUE INPUT
				 		END IF
              	END IF
				ELSE	
					CALL box_valdato("No se efectuó ningun cambio.")
					LET int_flag = TRUE
				END IF
			ELSE
				CALL box_inter() RETURNING int_flag
				IF int_flag = TRUE THEN
					EXIT INPUT
				ELSE
					LET gr_empr.empr_nom   = get_fldbuf(empr_nom)
		 			CONTINUE INPUT
				END IF
			END IF
	END INPUT

	IF int_flag = TRUE THEN
		ROLLBACK WORK
		LET int_flag = FALSE
		LET gr_empr.* = ur_empr.*
		CALL empr_desp()
		CALL box_valdato("Modificación Cancelada." )
       MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

	ELSE
		WHENEVER ERROR CONTINUE
		EXECUTE upd_empr USING  gr_empr.empr_nit,
                              gr_empr.empr_nom,
                              gr_empr.empr_dir1,
                              gr_empr.empr_dir2,
                              gr_empr.empr_idioma,
                              gr_empr.empr_codpos,
                              gr_empr.pais_nom,
                              gr_empr.dep_nom,
                              gr_empr.mun_nom,
                              gr_empr.empr_nomct,
                              gr_empr.empr_db,
                              gr_empr.est_id
		WHENEVER ERROR STOP 	
		IF SQLCA.SQLCODE < 0 THEN
			ROLLBACK WORK
			LET gr_empr.* = ur_empr.*
			CALL empr_desp()
			LET mensaje = "El error numero ", SQLCA.SQLCODE USING "-<<<<", " ha ocurrido."
			CALL box_valdato(mensaje)
			RETURN FALSE
		ELSE
			COMMIT WORK
			CALL empr_desp()
			CALL box_valdato("Registro modificado.")
			RETURN TRUE
		END IF
	END IF
	RETURN FALSE
END IF
RETURN FALSE

END FUNCTION


FUNCTION valida_modi()
   IF gr_empr.empr_id = ur_empr.empr_id AND
      gr_empr.empr_nit = ur_empr.empr_nit AND
      gr_empr.empr_nom = ur_empr.empr_nom AND
      gr_empr.empr_dir1 = ur_empr.empr_dir1 AND
      gr_empr.empr_dir2 = ur_empr.empr_dir2 AND
      gr_empr.empr_idioma = ur_empr.empr_idioma AND
      gr_empr.empr_codpos = ur_empr.empr_codpos AND
      gr_empr.pais_nom = ur_empr.pais_nom AND
      gr_empr.dep_nom = ur_empr.dep_nom AND
      gr_empr.mun_nom = ur_empr.mun_nom AND
      gr_empr.empr_nomct = ur_empr.empr_nomct AND
      gr_empr.empr_db = ur_empr.empr_db AND
      gr_empr.est_id = ur_empr.est_id     THEN   
      RETURN FALSE
   END IF

   RETURN TRUE
END FUNCTION
