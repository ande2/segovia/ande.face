################################################################################
# Funcion     : %M%
# Descripcion : Modulo para ingreso de catalogo 
# Funciones   : empr_inse()
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : EAlvarezs
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

GLOBALS "admm0013_glob.4gl"

FUNCTION empr_inse()
DEFINE 
	err_msg        CHAR(80),
	w_cuenta		   INTEGER, -- Cuenta los registros que ya existen en mcat
	condicion	   CHAR(300),
	respuesta      char(06),
   lint_flag      SMALLINT ,
	lexiste        SMALLINT 


LET int_flag = FALSE

CLEAR FORM
CALL encabezado()

let gtit_enc="INGRESO REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

DISPLAY BY NAME gtit_enc    
INITIALIZE gr_esta.* TO NULL                                                                            
INPUT BY NAME  gr_esta.esta_cod,
               gr_esta.esta_nom,
               gr_esta.esta_dir1,
               gr_esta.esta_dir2,
               gr_esta.esta_codpos,
               gr_esta.pai_nom,
               gr_esta.dep_nom,
               gr_esta.mun_nom,
               gr_esta.esta_requestor,
               gr_esta.esta_userface,
               gr_esta.esta_dispo,
               gr_esta.esta_emailto,
               gr_esta.esta_emailcc
 ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
 
	BEFORE INPUT
	CALL fgl_dialog_setkeylabel("ACCEPT","Grabar")
	CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")

    --ON KEY (CONTROL-W)
      --  CALL showhelp (1)
    

   ON KEY(CONTROL-B)
   AFTER FIELD esta_nom
      IF gr_esta.esta_nom IS NULL THEN
         CALL box_valdato("Debe ingresar el nombre del establecimiento.")
         NEXT FIELD esta_nom
      ELSE
         --LET lexiste = 0
         --SELECT count(*)
           --INTO lexiste
           --FROM mesta
          --WHERE mesta.esta_nom = gr_esta.esta_nom
         --IF lexiste > 0 THEN
            --CALL box_valdato("Existen datos con estas condiciones.")
            --LET gr_esta.esta_nom = NULL
            --NEXT FIELD esta_nom
         --END IF
      END IF



	AFTER INPUT
		IF NOT int_flag THEN
			IF gr_esta.esta_nom IS null THEN
				NEXT FIELD esta_nom
			END IF
         --LET lexiste = 0
         --SELECT count(*)
         --INTO lexiste
         --FROM mesta
         --WHERE mesta.esta_nom = gr_esta.esta_nom
         --IF lexiste > 0 THEN
            --CALL box_valdato("Existen datos con estas condiciones.")
             --LET gr_esta.esta_nom = null
             --NEXT FIELD esta_nom 
         --END IF

         CALL box_gradato("Desea guardar la informaci�n.") 
         RETURNING respuesta
         CASE
            WHEN respuesta = "Si"
               LET int_flag = FALSE
            WHEN respuesta = "No"
               LET int_flag = TRUE
            WHEN respuesta = "Cancel"
               CONTINUE INPUT
         END CASE
		ELSE
			CALL box_inter() RETURNING int_flag
			IF int_flag = TRUE THEN 
				EXIT INPUT
			ELSE
				CONTINUE INPUT
			END IF
		END IF
END INPUT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      LET gr_esta.* = nr_esta.*
      CLEAR FORM
      CALL encabezado()
      CALL box_valdato("Ingreso de Datos Cancelado.")
       MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

      RETURN FALSE
   END IF
   
   BEGIN WORK
    
   WHENEVER ERROR CONTINUE
   INSERT 
      INTO    mempr (esta_cod,
                     esta_nom,
                     esta_dispo,
                     esta_dir1,
                     esta_dir2,
                     esta_codpos,
                     pai_nom,
                     dep_nom,
                     mun_nom,
                     esta_emailto,
                     esta_emailcc,
                     empr_id,
                     esta_requestor,
                     esta_userface)
      VALUES ( gr_esta.esta_cod,
               gr_esta.esta_nom,
               gr_esta.esta_dispo,
               gr_esta.esta_dir1,
               gr_esta.esta_dir2,
               gr_esta.esta_codpos,
               gr_esta.pai_nom,
               gr_esta.dep_nom,
               gr_esta.mun_nom,
               gr_esta.esta_emailto,
               gr_esta.esta_emailcc,
               id_empresa,
               gr_esta.esta_requestor,
               gr_esta.esta_userface
            )

   WHENEVER ERROR STOP

   IF SQLCA.SQLCODE < 0 THEN
      LET err_msg = err_get(SQLCA.SQLCODE)
      ROLLBACK WORK
      ERROR err_msg
      CALL errorlog(err_msg CLIPPED)
      RETURN FALSE
   ELSE
      COMMIT WORK
      CALL empr_desp()
      CALL box_valdato("El c�digo ha sido grabado.")
     MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

      RETURN TRUE
   END IF

END FUNCTION