###############################################################################
# Funcion     : %M%
# Descripcion : Modulo para la modificacion del catalogo 
# Funciones   : modi_init()
#               empr_modi()
#               empr_anul()
#               valida_modi()
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : ea
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# HREYES      Tue Sep 13 11:44:24 GMT 2005 se elimina usuario pro10eec
# JMonterroso Mon Dec  8 14:01:11 CST 2003 Se agrego la validacion de la BD
# JMonterroso Tue Oct 21 11:14:30 CDT 2003 Se agrego al usuario pro10eec el 
#                                 poder modificar la descripcion del articulo
################################################################################

GLOBALS "admm0013_glob.4gl"

FUNCTION modi_init()
DEFINE 
	prepvar VARCHAR(1000)
DECLARE lockempr CURSOR FOR
	SELECT   *
   FROM mesta
	WHERE mesta.empr_id = id_empresa
     AND   mesta.esta_id = gr_esta.esta_id
	FOR UPDATE

LET prepvar = " UPDATE mesta " ,
               " SET    mesta.esta_cod       = ?, ",
               "        mesta.esta_nom       = ?, ",
               "        mesta.esta_dispo     = ?, ",
               "        mesta.esta_dir1      = ?, ",
               "        mesta.esta_dir2      = ?, ",
               "        mesta.esta_codpos    = ?, ",
               "        mesta.pai_nom        = ?, ",
               "        mesta.dep_nom        = ?, ",
               "        mesta.mun_nom        = ?, ",
               "        mesta.esta_emailto   = ?, ",
               "        mesta.esta_emailcc   = ?, ",
               "        mesta.esta_requestor = ?, ",
               "        mesta.esta_userface  = ? ",
               " WHERE CURRENT OF lockempr"
WHENEVER ERROR CONTINUE
PREPARE upd_esta FROM prepvar
WHENEVER ERROR STOP
IF SQLCA.SQLCODE <> 0 THEN
   DISPLAY sqlerrmessage, " error"
   EXIT PROGRAM(1)
END IF
END FUNCTION

FUNCTION empr_modi()
DEFINE
	mensaje CHAR(100),
	respuesta CHAR(6),
	contador	SMALLINT,
	lexiste SMALLINT


let gtit_enc="MODIFICA REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

BEGIN WORK

OPEN lockempr

WHENEVER ERROR CONTINUE

FETCH lockempr INTO gr_esta.*

WHENEVER ERROR STOP
DISPLAY "-- ",SQLCA.sqlcode, " -- ",sqlerrmessage
IF SQLCA.SQLCODE < 0 OR SQLCA.SQLCODE = NOTFOUND THEN
	ROLLBACK WORK
	CALL box_valdato("No es posible bloquear el registro para Modificarlo.")
ELSE
	LET contador =  0

	LET ur_esta.* = gr_esta.*
	LET int_flag = FALSE

INPUT BY NAME  gr_esta.esta_cod,
               gr_esta.esta_nom,
               gr_esta.esta_dir1,
               gr_esta.esta_dir2,
               gr_esta.esta_codpos,
               gr_esta.pai_nom,
               gr_esta.dep_nom,
               gr_esta.mun_nom,
               gr_esta.esta_requestor,
               gr_esta.esta_userface,
               gr_esta.esta_dispo,
               gr_esta.esta_emailto,
               gr_esta.esta_emailcc ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
              
		BEFORE INPUT
			CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
			CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")
			CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	
		--ON KEY (CONTROL-W)
			--CALL showhelp (4)

   AFTER FIELD esta_nom
      IF gr_esta.esta_nom IS NULL THEN
         CALL box_valdato("Debe ingresar el nombre de la sucursal.")
         NEXT FIELD esta_nom
      ELSE
         --LET lexiste = 0
         --SELECT count(*)
           --INTO lexiste
           --FROM mesta
          --WHERE mesta.esta_nom = gr_esta.esta_nom
         --IF lexiste > 1 THEN
            --CALL box_valdato("Existen datos con estas condiciones.")
            --LET gr_esta.esta_nom = NULL
            --NEXT FIELD esta_nom
         --END IF
      END IF

		AFTER INPUT
			IF NOT int_flag THEN
				IF gr_esta.esta_nom IS NULL THEN
					NEXT FIELD esta_nom
				END IF
				IF valida_modi() = TRUE THEN		
					CALL box_gradato("Desea guardar la información.") RETURNING respuesta
					IF respuesta = "Si" THEN
						LET int_flag = FALSE
						EXIT INPUT
					ELSE
			         IF respuesta = "No" THEN
							LET int_flag = TRUE
							EXIT INPUT
						ELSE
						   CONTINUE INPUT
				 		END IF
              	END IF
				ELSE	
					CALL box_valdato("No se efectuó ningun cambio.")
					LET int_flag = TRUE
				END IF
			ELSE
				CALL box_inter() RETURNING int_flag
				IF int_flag = TRUE THEN
					EXIT INPUT
				ELSE
		 			CONTINUE INPUT
				END IF
			END IF
	END INPUT

	IF int_flag = TRUE THEN
		ROLLBACK WORK
		LET int_flag = FALSE
		LET gr_esta.* = ur_esta.*
		CALL empr_desp()
		CALL box_valdato("Modificación Cancelada." )
       MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

	ELSE
		WHENEVER ERROR CONTINUE
		EXECUTE  upd_esta 
         USING gr_esta.esta_cod,
               gr_esta.esta_nom,
               gr_esta.esta_dispo,
               gr_esta.esta_dir1,
               gr_esta.esta_dir2,
               gr_esta.esta_codpos,
               gr_esta.pai_nom,
               gr_esta.dep_nom,
               gr_esta.mun_nom,
               gr_esta.esta_emailto,
               gr_esta.esta_emailcc,
               gr_esta.esta_requestor,
               gr_esta.esta_userface
		WHENEVER ERROR STOP 	
		IF SQLCA.SQLCODE < 0 THEN
			ROLLBACK WORK
			LET gr_esta.* = ur_esta.*
			CALL empr_desp()
			LET mensaje = "El error numero ", SQLCA.SQLCODE USING "-<<<<", " ha ocurrido."
			CALL box_valdato(mensaje)
			RETURN FALSE
		ELSE
			COMMIT WORK
			CALL empr_desp()
			CALL box_valdato("Registro modificado.")
			RETURN TRUE
		END IF
	END IF
	RETURN FALSE
END IF
RETURN FALSE

END FUNCTION


FUNCTION valida_modi()
   IF gr_esta.esta_cod        = ur_esta.esta_cod AND
      gr_esta.esta_nom        = ur_esta.esta_nom AND
      gr_esta.esta_dispo      = ur_esta.esta_dispo AND
      gr_esta.esta_dir1       = ur_esta.esta_dir1 AND
      gr_esta.esta_dir2       = ur_esta.esta_dir2 AND
      gr_esta.esta_codpos     = ur_esta.esta_codpos AND
      gr_esta.pai_nom         = ur_esta.pai_nom AND
      gr_esta.dep_nom         = ur_esta.dep_nom AND
      gr_esta.mun_nom         = ur_esta.mun_nom AND
      gr_esta.esta_emailto    = ur_esta.esta_emailto AND
      gr_esta.esta_emailcc    = ur_esta.esta_emailcc AND
      gr_esta.esta_requestor  = ur_esta.esta_requestor AND
      gr_esta.esta_userface   = ur_esta.esta_userface
     THEN   
      RETURN FALSE
   END IF

   RETURN TRUE
END FUNCTION