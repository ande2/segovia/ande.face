################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : empr_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               empr_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "admm0014_glob.4gl"

FUNCTION empr_busca_init()
DEFINE
	select_stmt3			VARCHAR(1000)
	
   LET select_stmt3 = "SELECT *",
                     " FROM dres WHERE res_id = ?"
   PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION

FUNCTION busca_res()
DEFINE
	empr_query		CHAR(300),
	where_clause	CHAR(250),
	empr_cnt        SMALLINT,
   condicion        CHAR(300),
	empr_count		CHAR(300),
   mensaje          CHAR(100),
   respuesta        CHAR(6),
   lint_flag        SMALLINT
   
   CLEAR FORM
   CALL encabezado()

   let gtit_enc="BUSQUEDA REGISTRO"
   DISPLAY BY NAME gtit_enc                                                                                
   LET int_flag = FALSE

   CONSTRUCT 
      BY NAME     where_clause 
      ON          dres.esta_id,
                  dres.res_serie,
                  dres.tipdoc_id,
                  dres.res_femision,
                  dres.res_nautoriza,
                  dres.res_fecha,
                  dres.res_de,
                  dres.res_a
      BEFORE CONSTRUCT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

      --ON KEY (CONTROL - W)
        -- CALL showhelp (2)
			
   END CONSTRUCT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      CALL encabezado()
      ERROR "B�squeda Abortada."
      RETURN FALSE, 0
   END IF	 

   LET where_report = where_clause
   LET empr_query = "SELECT dres.res_id ",
                    " FROM dres, mesta ",
                    " WHERE mesta.empr_id = ", id_empresa, 
                    " AND   mesta.esta_id = dres.esta_id",
                    " AND ",   where_clause CLIPPED,
                    " ORDER BY 1 " 

   LET empr_count = "SELECT count(*) ",
                    " FROM dres, mesta",
                    " WHERE mesta.empr_id = ", id_empresa, 
                    " AND   mesta.esta_id = dres.esta_id ",
                     " AND ", where_clause CLIPPED
  -- DISPLAY 'db ', dbname

   DISPLAY empr_query
   
   PREPARE ex_stmt FROM empr_query

   PREPARE ex_stmt2 FROM empr_count

   DECLARE empr_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

   OPEN empr_ptr

   FETCH FIRST empr_ptr INTO gr_res.*

   IF SQLCA.SQLCODE = 100 THEN
      CALL box_valdato("No existen datos con estas condiciones.")
      MESSAGE "[Control-W] = Ayuda"
      CLOSE empr_ptr
      RETURN FALSE,0
   ELSE
      DECLARE empr_all CURSOR FOR ex_stmt3	
      OPEN empr_all USING gr_res.res_id

      FETCH empr_all INTO gr_res.*

      IF SQLCA.SQLCODE = NOTFOUND THEN
         CALL box_valdato("No existen datos con estas Condiciones.")
         CLOSE empr_ptr
         RETURN FALSE,0
      ELSE
         DECLARE contador_ptr CURSOR FOR ex_stmt2
         OPEN contador_ptr
         FETCH contador_ptr INTO empr_cnt
         CLOSE contador_ptr
         CALL empr_desp()
         MESSAGE "Existen ", empr_cnt USING "<<<<", " Articulos."
         RETURN TRUE, empr_cnt
      END IF				
   END IF				
END FUNCTION

FUNCTION fetch_cat(fetch_flag)
DEFINE
	fetch_flag		SMALLINT

FETCH RELATIVE fetch_flag empr_ptr INTO gr_res.*

IF SQLCA.SQLCODE = NOTFOUND THEN
	IF fetch_flag = 1 THEN
		CALL box_valdato("Est� posicionado en el final de la lista.")
	ELSE
		CALL box_valdato("Est� posicionado en el principio de la lista.")
	END IF
ELSE
	OPEN empr_all USING gr_res.res_id

	FETCH empr_all INTO gr_res.*
	IF SQLCA.SQLCODE = NOTFOUND THEN
		CLEAR FORM
		CALL encabezado()	
		DISPLAY BY NAME gr_res.*
		CALL box_valdato("El registro fue eliminado desde la �ltima Consulta.")
	ELSE
		CALL empr_desp()
	END IF
	CLOSE empr_all
END IF
END FUNCTION

FUNCTION empr_desp()
   SELECT   *
      INTO  gr_res.*
      FROM  dres a
      WHERE a.res_id= gr_res.res_id
   DISPLAY 
      BY NAME  gr_res.*
END FUNCTION

FUNCTION limpiar() 
   WHENEVER ERROR CONTINUE
   CLOSE empr_ptr
   WHENEVER ERROR STOP
END FUNCTION