################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# JMonterroso Mon Dec  8 13:57:44 CST 2003 Se agrego variable global de la BD 
################################################################################

SCHEMA "face"


GLOBALS
DEFINE
   g_sccs_var                 CHAR(70), # Variable para SCCS
   gr_res,nr_res,ur_res       RECORD LIKE dres.*,
   g_empr                     RECORD LIKE mempr.*,
   dbperm,
   dbname                     CHAR(40),
   vempr_nomlog               CHAR(15),
   usuario                    CHAR(20),
   hr                         DATE, 
   gtit_enc,   
   gtit_pie,
   id_empresa                 INTEGER,
   nombre_empresa             VARCHAR(80),
   nombre_departamento        VARCHAR(80),
   where_report               VARCHAR(1000)
END GLOBALS