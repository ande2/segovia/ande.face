################################################################################
# Funcion     : %M%
# Descripcion : Modulo para ingreso de catalogo 
# Funciones   : empr_inse()
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : EAlvarezs
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

GLOBALS "admm0014_glob.4gl"

FUNCTION empr_inse()
DEFINE 
	err_msg        CHAR(80),
	w_cuenta		   INTEGER, -- Cuenta los registros que ya existen en mcat
	condicion	   CHAR(300),
	respuesta      char(06),
   lint_flag      SMALLINT ,
	lexiste        SMALLINT 


LET int_flag = FALSE

CLEAR FORM
CALL encabezado()

let gtit_enc="INGRESO REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

DISPLAY BY NAME gtit_enc    
INITIALIZE gr_res.* TO NULL                                                                            
INPUT BY NAME  gr_res.esta_id,
               gr_res.res_serie,
               gr_res.tipdoc_id,
               gr_res.tipcon_id,
               gr_res.res_femision,
               gr_res.res_nautoriza,
               gr_res.res_fecha,
               gr_res.res_de,
               gr_res.res_a,
               gr_res.res_rangode,
               gr_res.res_rangoa
      WITHOUT DEFAULTS
 
	BEFORE INPUT
	CALL fgl_dialog_setkeylabel("ACCEPT","Grabar")
	CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")

    --ON KEY (CONTROL-W)
      --  CALL showhelp (1)
    
	ON KEY(CONTROL-B)


   AFTER FIELD res_serie
      IF gr_res.res_serie IS NULL THEN
         CALL box_valdato("Debe ingresar el numero de serie.")
         NEXT FIELD res_serie
{      ELSE
         LET lexiste = 0
         SELECT   COUNT(*)
            INTO lexiste
            FROM  dres a, mesta b
            WHERE a.res_serie =gr_res.res_serie
            AND   b.esta_id = a.esta_id
            AND   b.empr_id = id_empresa          
         IF lexiste > 0 THEN
            CALL box_valdato("Existen datos con estas condiciones.")
            LET gr_res.res_serie = NULL
            NEXT FIELD res_serie
         END IF}
      END IF

	AFTER INPUT
		IF NOT int_flag THEN
			IF gr_res.res_serie IS null THEN
				NEXT FIELD res_serie
			END IF
{         LET lexiste = 0
         SELECT   COUNT(*)
            INTO lexiste
            FROM  dres a, mesta b
            WHERE a.res_serie =gr_res.res_serie
            AND   b.esta_id = a.esta_id
            AND   b.empr_id = id_empresa 
            
         IF lexiste > 0 THEN
            CALL box_valdato("Existen datos con estas condiciones.")
             LET gr_res.res_serie = null
             NEXT FIELD res_serie
         END IF
}
         CALL box_gradato("Desea guardar la informaci�n.") 
         RETURNING respuesta
         CASE
            WHEN respuesta = "Si"
               LET int_flag = FALSE
            WHEN respuesta = "No"
               LET int_flag = TRUE
            WHEN respuesta = "Cancel"
               CONTINUE INPUT
         END CASE
		ELSE
			CALL box_inter() RETURNING int_flag
			IF int_flag = TRUE THEN 
				EXIT INPUT
			ELSE
				CONTINUE INPUT
			END IF
		END IF
END INPUT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      LET gr_res.* = nr_res.*
      CLEAR FORM
      CALL encabezado()
      CALL box_valdato("Ingreso de Datos Cancelado.")
       MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)
      RETURN FALSE
   END IF
   
   BEGIN WORK
    
   WHENEVER ERROR CONTINUE
   INSERT 
      INTO   dres (  res_serie,
                     res_femision,
                     res_nautoriza,
                     res_fecha,
                     res_de,
                     res_a,
                     res_rangode,
                     res_rangoa,
                     esta_id,
                     tipdoc_id,
                     tipcon_id)
      VALUES ( gr_res.res_serie,
               gr_res.res_femision,
               gr_res.res_nautoriza,
               gr_res.res_fecha,
               gr_res.res_de,
               gr_res.res_a,
               gr_res.res_rangode,
               gr_res.res_rangoa,
               gr_res.esta_id,
               gr_res.tipdoc_id,
               gr_res.tipcon_id)
   WHENEVER ERROR STOP

   IF SQLCA.SQLCODE < 0 THEN
      LET err_msg = err_get(SQLCA.SQLCODE)
      ROLLBACK WORK
      ERROR err_msg
      CALL errorlog(err_msg CLIPPED)
      RETURN FALSE
   ELSE
      COMMIT WORK
      CALL empr_desp()
      CALL box_valdato("El c�digo ha sido grabado.")
      MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)
      RETURN TRUE
   END IF

END FUNCTION