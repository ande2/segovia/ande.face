################################################################################
# Funcion     : %M%
# Descripcion : Modulo principal para ESTADOS GENERALES
# Funciones   : main
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
# SCCS ID No  : %Z% %W%
# Autor       : SCIDeas
# Fecha       : %H% %T%
# Path        : %P%
# Control de cambios
# Programador     Fecha                    Descripcion de la modificacion
# 
################################################################################


GLOBALS "admm0014_glob.4gl"


MAIN
	DEFINE
		n_param SMALLINT

	DEFER INTERRUPT

	OPTIONS
	INPUT WRAP,
	HELP FILE "admm0014_help.iem",
	HELP KEY CONTROL-W,
	COMMENT	LINE OFF,
	PROMPT	LINE LAST - 2,
	MESSAGE	LINE LAST - 1,
	ERROR	LINE LAST
                                                                                
	LET n_param = num_args()
	IF n_param = 0 THEN
		RETURN
	ELSE
		LET dbname = arg_val(1)
	END IF
   LET dbperm = dbname
   LET dbname = "face"
   DATABASE dbname
   SELECT   a.empr_id, a.empr_nom      
      INTO  id_empresa, nombre_empresa
      FROM  face:mempr a
      WHERE a.empr_db = dbperm
   CALL busca_empresa(dbperm) RETURNING g_empr.*
   CALL empr_init(dbname)
	CALL startlog("admm0014.log")
	CALL empr_init(dbname)
	CALL modi_init()
	CALL empr_busca_init()
	CALL empr_menu()

	close window screen
	LET g_sccs_var = "%W% created on %E%"
END MAIN


FUNCTION empr_init(dbname)
	DEFINE
		nombre_departamento, 
      nombre_resesa        CHAR(50),
		cmd                  CHAR(40),
		ip_usu,        
      dbname               CHAR(40),
		fecha_actual         DATE,
		dir_destino          CHAR(100),
      x_condicion          VARCHAR(200)
                                                                                
	LET dir_destino = fgl_getenv("PWD")
	LET dir_destino = dir_destino CLIPPED, "/empr_log.bmp"

	INITIALIZE nr_res.* TO NULL
	LET gr_res.* = nr_res.*
	LET usuario = fgl_getenv("LOGNAME")
	LET fecha_actual = today
	LET hr =fecha_actual
                                                                                
   SELECT	face:mempr.empr_id, face:mempr.empr_nom
		INTO	id_empresa, nombre_empresa
		FROM	face:mempr
		WHERE	face:mempr.empr_db = dbname

	OPEN FORM empr_form FROM "admm0014_form"

	DISPLAY FORM empr_form
	CLEAR FORM
	MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)
	CALL encabezado()
	CALL fgl_settitle("RESOLUCIONES")

   --LET x_condicion = "SELECT a.esta_id, LPAD(a.esta_id,3,'0')  || ' - ' || trim(a.esta_nom) ",
   LET x_condicion = "SELECT a.esta_id, a.esta_id  || ' - ' || trim(a.esta_nom) ",
                      " FROM  face:mesta a ",
                      " WHERE a.empr_id = ", id_empresa,
                      " ORDER BY a.esta_nom"
   CALL combo_din2("esta_id", x_condicion)

   LET x_condicion = "SELECT a.tipdoc_id, trim(a.tipdoc_cod) || ' - ' || trim(a.tipdoc_nom) ",
                      " FROM  face:mtipdoc a ",
                      " ORDER BY a.tipdoc_nom"
   CALL combo_din2("tipdoc_id", x_condicion)

   LET x_condicion = "SELECT a.tipcon_id, trim(a.tipcon_nom) ",
                   " FROM  face:mtipcon a ",
                   " ORDER BY a.tipcon_nom"
   CALL combo_din2("tipcon_id", x_condicion)
END FUNCTION


FUNCTION empr_menu()
	DEFINE 
		cuantos		SMALLINT,
		usuario CHAR(8),
		vopciones CHAR(255), --cadena de ceros y unos para control de permisos
		ord_opc SMALLINT,    --orden de las opciones en el menu
		cnt     SMALLINT,    --contdor
		tol ARRAY[13] OF RECORD
			opcion   CHAR(15),
			acc      CHAR(15),
			desc_opc CHAR(60),
			des_cod  SMALLINT,
			imagen   CHAR(20),
			linkprop INTEGER
		END RECORD,
		cpo ARRAY[13] OF RECORD
			opcion   CHAR(15),
			desc_opc CHAR(60),
			des_cod  SMALLINT,
			imagen   CHAR(20),
			linkprop INTEGER
		END RECORD,
		aui om.DOMnode,
		tb om.DomNode,
		tbi om.DomNode,
		tbs om.DomNode,
		tm  om.DomNode,
		tmg om.DomNode,
		tmi om.DomNode,
		tms om.DomNode,
		f  om.DomNode

	LET cnt = 1
	LET ord_opc = NULL

	-- captura nombre de usuario
	LET usuario = fgl_getenv("LOGNAME")

	CALL men_opc(usuario,"admm0014", dbperm ) RETURNING vopciones

	-- carga opciones de menu
	DECLARE opciones_menu CURSOR FOR
      SELECT	b.prog_ord, c.des_desc_md, 
               b.prop_desc, c.des_cod, c.des_desc_ct
			FROM	face:mprog a, face:dprogopc b, face:sd_des c
			WHERE	a.prog_nom = "admm0014"
			AND	b.prog_id = a.prog_id
			AND	c.des_tipo = 22
			AND	c.des_cod = b.des_cod
			ORDER BY
					b.prog_ord

 	LET cnt = 1
	FOREACH opciones_menu INTO ord_opc,cpo[cnt].*
		LET cnt = cnt + 1
	END FOREACH

	FREE opciones_menu
	FOR cnt = 1 TO LENGTH(vopciones)
		LET tol[cnt].acc     =DOWNSHIFT(cpo[cnt].opcion CLIPPED)
		LET tol[cnt].opcion  =cpo[cnt].opcion CLIPPED
		LET tol[cnt].desc_opc = cpo[cnt].desc_opc CLIPPED
		LET tol[cnt].des_cod  = cpo[cnt].des_cod
		LET tol[cnt].imagen   = DOWNSHIFT(cpo[cnt].imagen CLIPPED)
		LET tol[cnt].linkprop = cpo[cnt].linkprop
	END FOR

	LET aui=ui.Interface.getRootNode()
	LET tb =aui.createChild("ToolBar")
	LET tbi=createToolBarItem(tb,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --buscar 
	LET tbi=createToolBarItem(tb,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) --anterior
	LET tbi=createToolBarItem(tb,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) --proximo 
	LET tbs=createToolBarSeparator(tb)
	LET tbi=createToolBarItem(tb,tol[4].acc,tol[4].opcion,tol[4].desc_opc,tol[4].imagen) --ingreso 
	LET tbi=createToolBarItem(tb,tol[5].acc,tol[5].opcion,tol[5].desc_opc,tol[5].imagen) --modificar 
	LET tbi=createToolBarItem(tb,tol[6].acc,tol[6].opcion,tol[6].desc_opc,tol[6].imagen) -- anular
	LET tbs=createToolBarSeparator(tb)
	LET tbi= createToolBarItem(tb,tol[7].acc,tol[7].opcion,tol[7].desc_opc,tol[7].imagen) --impresion
   LET tbs=createToolBarSeparator(tb)
   LET tbi= createToolBarItem(tb,tol[9].acc,tol[9].opcion,tol[9].desc_opc,tol[9].imagen) --ayuda
	LET tbi= createToolBarItem(tb,tol[8].acc,tol[8].opcion,tol[8].desc_opc,tol[8].imagen) --salir
    

	MENU "OPCIONES"  
      BEFORE MENU
         CALL combo_din2("est_id","SELECT est_id,est_desc FROM mest ") 
         LET cuantos = 0

         -- despliega opciones a las que tiene acceso el usuario
         HIDE OPTION ALL
         FOR cnt = 1 TO LENGTH(vopciones)
            IF vopciones[cnt] = 1 AND
               (tol[cnt].des_cod = 1 OR
               tol[cnt].des_cod = 6 OR
               tol[cnt].des_cod = 9 OR 
               tol[cnt].des_cod = 7) THEN
               SHOW OPTION tol[cnt].opcion
               LET cuantos = cuantos + 1
            END IF
         END FOR
         IF cuantos = 0 THEN
            CALL box_error("Lo siento, no tiene acceso a este programa")
            EXIT MENU
         END IF

         
--opcion ayuda
   COMMAND KEY("B") tol[9].opcion tol[9].desc_opc
        --COMMAND KEY (CONTROL - W)
            CALL showhelp (99)
            
	-- opcion busqueda
   COMMAND KEY("B") tol[1].opcion tol[1].desc_opc
      HELP 2
      INITIALIZE gr_res.* to NULL
      CALL busca_res() RETURNING int_flag, cuantos
      IF int_flag = TRUE THEN
         LET int_flag = FALSE
         IF cuantos > 1 THEN

            -- despliega opciones a las que tiene acceso el usuario
            HIDE OPTION ALL
            FOR cnt = 1 TO LENGTH(vopciones)
               IF vopciones[cnt] = 1 THEN
                  SHOW OPTION tol[cnt].opcion
                  IF tol[cnt].des_cod = 2 THEN
                     NEXT OPTION tol[cnt].opcion
                  END IF
               END IF
            END FOR
         ELSE

            -- despliega opciones a las que tiene acceso el usuario
            HIDE OPTION ALL
            FOR cnt = 1 TO LENGTH(vopciones)
               IF vopciones[cnt] = 1 AND
                  (tol[cnt].des_cod<> 2 OR
                  tol[cnt].des_cod <> 3) THEN
                  SHOW OPTION tol[cnt].opcion
                  IF tol[cnt].des_cod = 2 THEN
                     NEXT OPTION tol[cnt].opcion
                  END IF
               END IF
            END FOR
         END IF
      ELSE

         -- despliega opciones a las que tiene acceso el usuario
         HIDE OPTION ALL
         FOR cnt = 1 TO LENGTH(vopciones)
            IF vopciones[cnt] = 1 AND
               (tol[cnt].des_cod = 1 OR
               tol[cnt].des_cod = 6 OR
               tol[cnt].des_cod = 9 OR 
               tol[cnt].des_cod = 9) THEN
               SHOW OPTION tol[cnt].opcion
            END IF
         END FOR
      END IF

   -- opcion anterior
	COMMAND KEY ("A") tol[2].opcion tol[2].desc_opc
		HELP 7
		CALL fetch_cat(-1)

	-- opcion Siguiente
	COMMAND KEY ("S") tol[3].opcion tol[3].desc_opc
		HELP 6
		CALL fetch_cat(1)


	-- opcion modificar
	COMMAND KEY("O") tol[5].opcion tol[5].desc_opc
		HELP 4
      CALL modi_init()      
		CALL empr_modi() RETURNING int_flag
		IF int_flag = TRUE THEN
			LET int_flag = FALSE
			CALL limpiar()

	-- despliega opciones a las que tiene acceso el usuario
         HIDE OPTION ALL
         FOR cnt = 1 TO LENGTH(vopciones)
         	IF vopciones[cnt] = 1 AND
            	(tol[cnt].des_cod = 1 OR
            	tol[cnt].des_cod = 6 OR
            	tol[cnt].des_cod = 9 OR 
               tol[cnt].des_cod = 7) THEN
            	SHOW OPTION tol[cnt].opcion
         	END IF
         END FOR
		END IF

	-- opcion ingreso
	COMMAND KEY ("I") tol[4].opcion tol[4].desc_opc
		HELP 1
		CALL empr_inse() RETURNING INT_FLAG
		IF int_flag = TRUE THEN
			LET int_flag = FALSE
			CALL limpiar()

	-- despliega opciones a las que tiene acceso el usuario
	HIDE OPTION ALL
         FOR cnt = 1 TO LENGTH(vopciones)
             IF vopciones[cnt] = 1 AND
                (tol[cnt].des_cod = 1 OR
                 tol[cnt].des_cod = 6 OR
                 tol[cnt].des_cod = 9 OR 
                 tol[cnt].des_cod = 7) THEN
                 SHOW OPTION tol[cnt].opcion
              END IF
         END FOR
		END IF

      -- opcion anular, pendiente
   COMMAND  key("E") tol[6].opcion tol[6].desc_opc
		HELP 8
      --CALL PrintReport ()

     
-- opcion imprimir
   COMMAND  key("R") tol[7].opcion tol[7].desc_opc
		HELP 8
      CALL PrintReport1 ()        

      
-- opcion salir
	COMMAND key("Q") tol[8].opcion tol[8].desc_opc
		HELP 8
		EXIT MENU
	END MENU
END FUNCTION


FUNCTION encabezado()

END FUNCTION


FUNCTION usu_destino()
	DEFINE
		ip_usu CHAR(20),
		i SMALLINT
 
		LET ip_usu = fgl_getenv("FGLSERVER")

		FOR i = 1 TO 20
			IF ip_usu[i] = ":" THEN
				EXIT FOR
			END IF
		END FOR
                                                                                
		LET ip_usu = ip_usu[1,i-1] CLIPPED
                                                                                
		RETURN ip_usu
END FUNCTION

FUNCTION PrintReport1()
   DEFINE myHandler om.SaxDocumentHandler
   DEFINE r_dres RECORD LIKE dres.*

   LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
   
   START REPORT reporte1  
   DECLARE rcur1 CURSOR FOR 
      SELECT * FROM dres
      ORDER BY res_serie
      
   FOREACH rcur1 INTO r_dres.*
      OUTPUT TO REPORT reporte1(r_dres.*)
   END FOREACH 

   FINISH REPORT reporte1  

   CALL fgl_report_stopGraphicalCompatibilityMode()
END FUNCTION 

REPORT reporte1(l_dres)
DEFINE l_dres RECORD LIKE dres.*

FORMAT 
   PAGE HEADER
      PRINT COLUMN 10, "LONAS SEGOVIA, S.A."
      PRINT COLUMN 10, "Reporte de Resoluciones"
      SKIP 1 LINE
      PRINT COLUMN 10, "================================================================================"
      PRINT COLUMN 10, "No. de Aut",
            COLUMN 30, "Serie",
            COLUMN 39, "Rango inicial",
            COLUMN 54, "Rago final"
      PRINT COLUMN 10, "================================================================================"
   ON EVERY ROW 
      PRINT COLUMN 10, l_dres.res_nautoriza, 
            COLUMN 32, l_dres.res_serie, 
            COLUMN 39, l_dres.res_rangode USING "#,###,##&", 
            COLUMN 54, l_dres.res_rangoa  USING "#,###,##&"
END REPORT
