###############################################################################
# Funcion     : %M%
# Descripcion : Modulo para la modificacion del catalogo 
# Funciones   : modi_init()
#               empr_modi()
#               empr_anul()
#               valida_modi()
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : ea
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# HREYES      Tue Sep 13 11:44:24 GMT 2005 se elimina usuario pro10eec
# JMonterroso Mon Dec  8 14:01:11 CST 2003 Se agrego la validacion de la BD
# JMonterroso Tue Oct 21 11:14:30 CDT 2003 Se agrego al usuario pro10eec el 
#                                 poder modificar la descripcion del articulo
################################################################################

GLOBALS "admm0014_glob.4gl"

FUNCTION modi_init()
DEFINE 
	prepvar VARCHAR(1000)
DECLARE lockempr CURSOR FOR
	SELECT   *
   FROM dres
	WHERE dres.res_id = gr_res.res_id
	FOR UPDATE

LET prepvar = " UPDATE  dres " ,
               " SET    dres.res_serie       = ?, ",
               "        dres.res_femision    = ?, ",
               "        dres.res_nautoriza   = ?, ",
               "        dres.res_fecha       = ?, ",
               "        dres.res_de          = ?, ",
               "        dres.res_a           = ?, ",
               "        dres.res_rangode     = ?, ",
               "        dres.res_rangoa      = ?, ",
               "        dres.esta_id         = ?, ",
               "        dres.tipdoc_id       = ?,  ",
               "        dres.tipcon_id       = ?  ",
               " WHERE CURRENT OF lockempr"
WHENEVER ERROR CONTINUE
PREPARE upd_res FROM prepvar
WHENEVER ERROR STOP
IF SQLCA.SQLCODE <> 0 THEN
   DISPLAY sqlerrmessage, " error"
   EXIT PROGRAM(1)
END IF
END FUNCTION

FUNCTION empr_modi()
DEFINE
	mensaje CHAR(100),
	respuesta CHAR(6),
	contador	SMALLINT,
	lexiste SMALLINT


let gtit_enc="MODIFICA REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

BEGIN WORK

OPEN lockempr

WHENEVER ERROR CONTINUE

FETCH lockempr INTO gr_res.*

WHENEVER ERROR STOP
DISPLAY "-- ",SQLCA.sqlcode, " -- ",sqlerrmessage
IF SQLCA.SQLCODE < 0 OR SQLCA.SQLCODE = NOTFOUND THEN
	ROLLBACK WORK
	CALL box_valdato("No es posible bloquear el registro para Modificarlo.")
ELSE
	LET contador =  0

	LET ur_res.* = gr_res.*
	LET int_flag = FALSE

INPUT BY NAME  gr_res.esta_id,
               gr_res.res_serie,
               gr_res.tipdoc_id,
               gr_res.tipcon_id,
               gr_res.res_femision,
               gr_res.res_nautoriza,
               gr_res.res_fecha,
               gr_res.res_de,
               gr_res.res_a, 
               gr_res.res_rangode,
               gr_res.res_rangoa 
      ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
              
		BEFORE INPUT
			CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
			CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")
			CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	
		--ON KEY (CONTROL-W)
			-- CALL showhelp (4)

   AFTER FIELD res_serie
      IF gr_res.res_serie IS NULL THEN
         CALL box_valdato("Debe ingresar el numero de serie.")
         NEXT FIELD res_serie
      END IF

		AFTER INPUT
			IF NOT int_flag THEN
				IF gr_res.res_serie IS NULL THEN
					NEXT FIELD res_serie
				END IF
				IF valida_modi() = TRUE THEN		
					CALL box_gradato("Desea guardar la información.") RETURNING respuesta
					IF respuesta = "Si" THEN
						LET int_flag = FALSE
						EXIT INPUT
					ELSE
			         IF respuesta = "No" THEN
							LET int_flag = TRUE
							EXIT INPUT
						ELSE
						   CONTINUE INPUT
				 		END IF
              	END IF
				ELSE	
					CALL box_valdato("No se efectuó ningun cambio.")
					LET int_flag = TRUE
				END IF
			ELSE
				CALL box_inter() RETURNING int_flag
				IF int_flag = TRUE THEN
					EXIT INPUT
				ELSE
		 			CONTINUE INPUT
				END IF
			END IF
	END INPUT

	IF int_flag = TRUE THEN
		ROLLBACK WORK
		LET int_flag = FALSE
		LET gr_res.* = ur_res.*
		CALL empr_desp()
		CALL box_valdato("Modificación Cancelada." )
       MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

	ELSE
		WHENEVER ERROR CONTINUE
		EXECUTE  upd_res 
         USING gr_res.res_serie,
               gr_res.res_femision,
               gr_res.res_nautoriza,
               gr_res.res_fecha,
               gr_res.res_de,
               gr_res.res_a,
               gr_res.res_rangode,
               gr_res.res_rangoa,
               gr_res.esta_id,
               gr_res.tipdoc_id,
               gr_res.tipcon_id
		WHENEVER ERROR STOP 	
		IF SQLCA.SQLCODE < 0 THEN
			ROLLBACK WORK
			LET gr_res.* = ur_res.*
			CALL empr_desp()
			LET mensaje = "El error numero ", SQLCA.SQLCODE USING "-<<<<", " ha ocurrido."
			CALL box_valdato(mensaje)
			RETURN FALSE
		ELSE
			COMMIT WORK
			CALL empr_desp()
			CALL box_valdato("Registro modificado.")
			RETURN TRUE
		END IF
	END IF
	RETURN FALSE
END IF
RETURN FALSE

END FUNCTION


FUNCTION valida_modi()
   IF gr_res.res_id 		   = ur_res.res_id 	      AND
      gr_res.res_serie 	   = ur_res.res_serie 	   AND
      gr_res.res_femision 	= ur_res.res_femision 	AND
      gr_res.res_nautoriza = ur_res.res_nautoriza 	AND
      gr_res.res_fecha 	   = ur_res.res_fecha 	   AND
      gr_res.res_de 		   = ur_res.res_de 	      AND
      gr_res.res_a 		   = ur_res.res_a 		   AND
      gr_res.res_rangode 	= ur_res.res_rangode 	AND
      gr_res.res_rangoa 	= ur_res.res_rangoa 		AND
      gr_res.esta_id 		= ur_res.esta_id 	      AND
      gr_res.tipdoc_id 	   = ur_res.tipdoc_id 	   AND
      gr_res.tipcon_id 	   = ur_res.tipcon_id 	   

     THEN   
      RETURN FALSE
   END IF

   RETURN TRUE
END FUNCTION