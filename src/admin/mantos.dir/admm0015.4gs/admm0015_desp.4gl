################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : empr_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               empr_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "admm0015_glob.4gl"

FUNCTION empr_busca_init()
DEFINE
	select_stmt3			VARCHAR(1000)
	
   LET select_stmt3 = "SELECT *",
                     " FROM mtipdoc WHERE  tipdoc_id = ?"
   PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION

FUNCTION busca_tipdoc()
DEFINE
	empr_query		CHAR(300),
	where_clause	CHAR(250),
	empr_cnt        SMALLINT,
   condicion        CHAR(300),
	empr_count		CHAR(300),
   mensaje          CHAR(100),
   resputipdoc        CHAR(6),
   lint_flag        SMALLINT
   
   CLEAR FORM
   CALL encabezado()

   let gtit_enc="BUSQUEDA REGISTRO"
   DISPLAY BY NAME gtit_enc                                                                                
   LET int_flag = FALSE

   CONSTRUCT 
      BY NAME     where_clause 
      ON          mtipdoc.tipdoc_cod,
                  mtipdoc.tipdoc_nom

      BEFORE CONSTRUCT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

      --ON KEY (CONTROL - W)
         --CALL showhelp (2)
			
   END CONSTRUCT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      CALL encabezado()
      ERROR "B�squeda Abortada."
      RETURN FALSE, 0
   END IF	 

   LET where_report = where_clause
   LET empr_query = "SELECT tipdoc_id FROM mtipdoc WHERE ",  where_clause CLIPPED,
                   " ORDER BY 1 " 

   LET empr_count = "SELECT count(*) FROM mtipdoc WHERE ",  where_clause CLIPPED

   PREPARE ex_stmt FROM empr_query

   PREPARE ex_stmt2 FROM empr_count

   DECLARE empr_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

   OPEN empr_ptr

   FETCH FIRST empr_ptr INTO gr_tipdoc.*

   IF SQLCA.SQLCODE = 100 THEN
      CALL box_valdato("No existen datos con tipdocs condiciones.")
      MESSAGE "[Control-W] = Ayuda"
      CLOSE empr_ptr
      RETURN FALSE,0
   ELSE
      DECLARE empr_all CURSOR FOR ex_stmt3	
      OPEN empr_all USING gr_tipdoc.tipdoc_id

      FETCH empr_all INTO gr_tipdoc.*

      IF SQLCA.SQLCODE = NOTFOUND THEN
         CALL box_valdato("No existen datos con tipdocs Condiciones.")
         CLOSE empr_ptr
         RETURN FALSE,0
      ELSE
         DECLARE contador_ptr CURSOR FOR ex_stmt2
         OPEN contador_ptr
         FETCH contador_ptr INTO empr_cnt
         CLOSE contador_ptr
         CALL empr_desp()
         MESSAGE "Existen ", empr_cnt USING "<<<<", " Tipos."
         RETURN TRUE, empr_cnt
      END IF				
   END IF				
END FUNCTION

FUNCTION fetch_cat(fetch_flag)
DEFINE
	fetch_flag		SMALLINT

FETCH RELATIVE fetch_flag empr_ptr INTO gr_tipdoc.*

IF SQLCA.SQLCODE = NOTFOUND THEN
	IF fetch_flag = 1 THEN
		CALL box_valdato("Est� posicionado en el final de la lista.")
	ELSE
		CALL box_valdato("Est� posicionado en el principio de la lista.")
	END IF
ELSE
	OPEN empr_all USING gr_tipdoc.tipdoc_id

	FETCH empr_all INTO gr_tipdoc.*
	IF SQLCA.SQLCODE = NOTFOUND THEN
		CLEAR FORM
		CALL encabezado()	
		DISPLAY BY NAME gr_tipdoc.*
		CALL box_valdato("El registro fue eliminado desde la �ltima Consulta.")
	ELSE
		CALL empr_desp()
	END IF
	CLOSE empr_all
END IF
END FUNCTION

FUNCTION empr_desp()
   SELECT   *
      INTO  gr_tipdoc.*
      FROM  mtipdoc a
      WHERE a.tipdoc_id= gr_tipdoc.tipdoc_id
   DISPLAY 
      BY NAME  gr_tipdoc.tipdoc_id,
               gr_tipdoc.tipdoc_cod,
               gr_tipdoc.tipdoc_nom
END FUNCTION

FUNCTION limpiar() 
   WHENEVER ERROR CONTINUE
   CLOSE empr_ptr
   WHENEVER ERROR STOP
END FUNCTION