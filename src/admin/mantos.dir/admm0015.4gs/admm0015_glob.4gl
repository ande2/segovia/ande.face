SCHEMA  "face"


GLOBALS
DEFINE
   g_sccs_var                 CHAR(70), # Variable para SCCS
   gr_tipdoc,nr_tipdoc,ur_tipdoc    RECORD LIKE mtipdoc.*,
   g_empr                     RECORD LIKE mempr.*,
   dbperm,
   dbname                     CHAR(40),
   vempr_nomlog               CHAR(15),
   usuario                    CHAR(20),
   hr                         DATE, 
   gtit_enc,   
   gtit_pie,
   id_empresa                 INTEGER,
   nombre_empresa             VARCHAR(80),
   nombre_departamento        VARCHAR(80),
   where_report               VARCHAR(1000)
END GLOBALS