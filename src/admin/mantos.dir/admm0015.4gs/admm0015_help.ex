     �   d �  ^ N  6 b  � ?  � @  (  �  i d�  		 e�  
� f  � g �  � h  �
Con la presente opci�n usted podr� hacer el ingreso de un nuevo c�digo de 
cat�logo.

Debe tomar en cuenta que se utilizar� en todos los documentos relacionados
con art�culos en el almac�n.


	          Screen = Siguiente p�gina   Resume = Salir 

 
Con la presente opci�n usted podra realizar la b�squeda de uno o varios
c�digos de cat�logo que hayan sido ingresadas al sistema, para lo cual podr�
ingresar una serie de criterios o patrones de b�squeda que le permitir�n la  
localizaci�n del c�digo de su inter�s.

Debe tomar en cuenta que los c�digos del cat�logo resultantes de las     
b�squedas podr� eliminarlos, modificarlos o simplemente consultarlos. 


	          Screen = Siguiente p�gina   Resume = Salir 

 
Con la presente opci�n usted podr� anular un c�digo de cat�logo que haya    
seleccionado de la b�squeda previa.

Debe tomar en cuenta que para poder anular un c�digo de cat�logo, no tiene
que estar siendo utilizado el c�digo del mismo en ning�n otro documento del 
sistema.


	          Screen = Siguiente p�gina   Resume = Salir 

 
Con la presente opci�n usted podr� modificar la descripci�n de un c�digo de 
cat�logo que haya seleccionado de la b�squeda previa.

Debe tomar en cuenta que para poder modificar un c�digo de cat�logo, no tiene
que estar siendo utilizado el c�digo del mismo en ning�n otro documento del 
sistema.

	          Screen = Siguiente p�gina   Resume = Salir 

 
Al presionar la presente opci�n usted podr� visualizar el pr�ximo registro o
documento resultante de la b�squeda realizada en el sistema. Si usted ya
llego al final de la b�squeda, el sistema le indicar� que ya no hay mas
registros o documentos que consultar.


	          Screen = Siguiente p�gina   Resume = Salir 

 
Al presionar la presente opci�n usted podr� visualizar el registro o documento
anterior resultante de la b�squeda realizada en el sistema. Si usted ya
lleg� al final de la b�squeda, el sistema le indicar� que ya no hay mas 
registros o documentos que consultar.

	          Screen = Siguiente p�gina   Resume = Salir 

 
Al presionar la presente opci�n usted podr� salir del men� de mantenimiento de
c�digos de cat�logo.


	          Screen = Siguiente p�gina   Resume = Salir 

 
Al posicionarse en este campo coloque el c�digo de la familia que corresponda
o considere es el que esta buscando o ingresando como nuevo.  

Si no conoce el c�digo deje el campo en blanco y presione Aceptar o Esc para 
buscar todos los c�digos que existan.

Si esta ingresando un nuevo c�digo de cat�logo y no sabe los c�digos 
respectivos de familias, oprimiento CONTROL-B o BUSCAR puede averiguar que 
c�digos de familia existen.

	          Screen = Siguiente p�gina   Resume = Salir 

 
Al posicionarse en este campo coloque el c�digo del tipo  que correponda o
considere es el que esta buscando o ingresando como nuevo.

Si no conoce el c�digo deje el campo en blanco y presione Aceptar o Esc para 
buscar todos los c�digos que existan.

Si en caso esta ingresando un nuevo c�digo de cat�logo y no sabe los c�digos 
respectivos de tipos, oprimiento CONTROL-B o BUSCAR puede averiguar que 
c�digos de tipos existen.

	          Screen = Siguiente p�gina   Resume = Salir 

 
Al posicionarse en este campo coloque el c�digo del tipo  que correponda o
considere es el que esta buscando

Si no conoce el c�digo deje el campo en blanco y presione Aceptar o Esc para 
buscar todos los c�digos que existan.

	          Screen = Siguiente p�gina   Resume = Salir 

 
Al posicionarse en este campo coloque la descripci�n del c�digo del catalogo
que este ingresando o que se esta buscando. 
 

	          Screen = Siguiente p�gina   Resume = Salir 

 

Al posicionarse en este campo coloque el c�digo de la uniad de medida que 
correponda o considere es el que esta buscando o ingresando como nuevo.

Si no conoce el c�digo deje el campo en blanco y presione Aceptar o Esc para 
buscar todos los c�digos que existan.

Si en caso esta ingresando un nuevo c�digo de cat�logo y no sabe los c�digos 
respectivos de unidades de medida, oprimiento CONTROL-B o BUSCAR puede 
averiguar que c�digos de unidades de medida existen.

	          Screen = Siguiente p�gina   Resume = Salir 

 