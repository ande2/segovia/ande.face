################################################################################
# Funcion     : %M%
# Descripcion : Modulo para ingreso de catalogo 
# Funciones   : empr_inse()
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : EAlvarezs
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

GLOBALS "admm0015_glob.4gl"

FUNCTION empr_inse()
DEFINE 
	err_msg        CHAR(80),
	w_cuenta		   INTEGER, -- Cuenta los registros que ya existen en mcat
	condicion	   CHAR(300),
	resputipdoc      char(06),
   lint_flag      SMALLINT ,
	lexiste        SMALLINT 


LET int_flag = FALSE

CLEAR FORM
CALL encabezado()

let gtit_enc="INGRESO REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

DISPLAY BY NAME gtit_enc    
INITIALIZE gr_tipdoc.* TO NULL                                                                            
INPUT BY NAME  gr_tipdoc.tipdoc_cod,
               gr_tipdoc.tipdoc_nom
 ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
 
	BEFORE INPUT
	CALL fgl_dialog_setkeylabel("ACCEPT","Grabar")
	CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")

    --ON KEY (CONTROL-W)
      --  CALL showhelp (1)
    
	ON KEY(CONTROL-B)


   AFTER FIELD tipdoc_nom
      IF gr_tipdoc.tipdoc_nom IS NULL THEN
         CALL box_valdato("Debe ingresar el nombre del tipdocblecimiento.")
         NEXT FIELD tipdoc_nom
      ELSE
         LET lexiste = 0
         SELECT count(*)
           INTO lexiste
           FROM mtipdoc
          WHERE mtipdoc.tipdoc_nom = gr_tipdoc.tipdoc_nom
         IF lexiste > 0 THEN
            CALL box_valdato("Existen datos con esas condiciones.")
            LET gr_tipdoc.tipdoc_nom = NULL
            NEXT FIELD tipdoc_nom
         END IF
      END IF



	AFTER INPUT
		IF NOT int_flag THEN
			IF gr_tipdoc.tipdoc_nom IS null THEN
				NEXT FIELD tipdoc_nom
			END IF
         LET lexiste = 0
         SELECT count(*)
         INTO lexiste
         FROM mtipdoc
         WHERE mtipdoc.tipdoc_nom = gr_tipdoc.tipdoc_nom
         IF lexiste > 0 THEN
            CALL box_valdato("Existen datos con tipdocs condiciones.")
             LET gr_tipdoc.tipdoc_nom = null
             NEXT FIELD tipdoc_nom 
         END IF

         CALL box_gradato("Desea guardar la informaci�n.") 
         RETURNING resputipdoc
         CASE
            WHEN resputipdoc = "Si"
               LET int_flag = FALSE
            WHEN resputipdoc = "No"
               LET int_flag = TRUE
            WHEN resputipdoc = "Cancel"
               CONTINUE INPUT
         END CASE
		ELSE
			CALL box_inter() RETURNING int_flag
			IF int_flag = TRUE THEN 
				EXIT INPUT
			ELSE
				CONTINUE INPUT
			END IF
		END IF
END INPUT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      LET gr_tipdoc.* = nr_tipdoc.*
      CLEAR FORM
      CALL encabezado()
      CALL box_valdato("Ingreso de Datos Cancelado.")
       MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

      RETURN FALSE
   END IF
   
   BEGIN WORK
    
   WHENEVER ERROR CONTINUE
   INSERT 
      INTO    mtipdoc (tipdoc_cod,
                     tipdoc_nom
                     )
      VALUES ( gr_tipdoc.tipdoc_cod,
               gr_tipdoc.tipdoc_nom
            )

   WHENEVER ERROR STOP

   IF SQLCA.SQLCODE < 0 THEN
      LET err_msg = err_get(SQLCA.SQLCODE)
      ROLLBACK WORK
      ERROR err_msg
      CALL errorlog(err_msg CLIPPED)
      RETURN FALSE
   ELSE
      COMMIT WORK
      CALL empr_desp()
      CALL box_valdato("El c�digo ha sido grabado.")
     MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

      RETURN TRUE
   END IF

END FUNCTION