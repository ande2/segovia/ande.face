GLOBALS "admm0015_glob.4gl"

FUNCTION modi_init()
DEFINE 
	prepvar VARCHAR(1000)
DECLARE lockempr CURSOR FOR
	SELECT   *
   FROM mtipdoc
	WHERE mtipdoc.tipdoc_id = gr_tipdoc.tipdoc_id
	FOR UPDATE

LET prepvar = " UPDATE mtipdoc " ,
               " SET    mtipdoc.tipdoc_cod       = ?, ",
               "        mtipdoc.tipdoc_nom       = ? ",
               " WHERE CURRENT OF lockempr"
WHENEVER ERROR CONTINUE
PREPARE upd_tipdoc FROM prepvar
WHENEVER ERROR STOP
IF SQLCA.SQLCODE <> 0 THEN
   DISPLAY sqlerrmessage, " error"
   EXIT PROGRAM(1)
END IF
END FUNCTION

FUNCTION empr_modi()
DEFINE
	mensaje CHAR(100),
	resputipdoc CHAR(6),
	contador	SMALLINT,
	lexiste SMALLINT


let gtit_enc="MODIFICA REGISTRO"
MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

BEGIN WORK

OPEN lockempr

WHENEVER ERROR CONTINUE

FETCH lockempr INTO gr_tipdoc.*

WHENEVER ERROR STOP
DISPLAY "-- ",SQLCA.sqlcode, " -- ",sqlerrmessage
IF SQLCA.SQLCODE < 0 OR SQLCA.SQLCODE = NOTFOUND THEN
	ROLLBACK WORK
	CALL box_valdato("No es posible bloquear el registro para Modificarlo.")
ELSE
	LET contador =  0

	LET ur_tipdoc.* = gr_tipdoc.*
	LET int_flag = FALSE

INPUT BY NAME  gr_tipdoc.tipdoc_cod,
               gr_tipdoc.tipdoc_nom
               ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
              
		BEFORE INPUT
			CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
			CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")
			CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
	
		ON KEY (CONTROL-W)
			CALL showhelp (4)

   AFTER FIELD tipdoc_nom
      IF gr_tipdoc.tipdoc_nom IS NULL THEN
         CALL box_valdato("Debe ingresar el nombre de la sucursal.")
         NEXT FIELD tipdoc_nom
      ELSE
         LET lexiste = 0
         SELECT count(*)
           INTO lexiste
           FROM mtipdoc
          WHERE mtipdoc.tipdoc_nom = gr_tipdoc.tipdoc_nom
         IF lexiste > 1 THEN
            CALL box_valdato("Existen datos con tipdocs condiciones.")
            LET gr_tipdoc.tipdoc_nom = NULL
            NEXT FIELD tipdoc_nom
         END IF
      END IF

		AFTER INPUT
			IF NOT int_flag THEN
				IF gr_tipdoc.tipdoc_nom IS NULL THEN
					NEXT FIELD tipdoc_nom
				END IF
				IF valida_modi() = TRUE THEN		
					CALL box_gradato("Desea guardar la información.") RETURNING resputipdoc
					IF resputipdoc = "Si" THEN
						LET int_flag = FALSE
						EXIT INPUT
					ELSE
			         IF resputipdoc = "No" THEN
							LET int_flag = TRUE
							EXIT INPUT
						ELSE
						   CONTINUE INPUT
				 		END IF
              	END IF
				ELSE	
					CALL box_valdato("No se efectuó ningun cambio.")
					LET int_flag = TRUE
				END IF
			ELSE
				CALL box_inter() RETURNING int_flag
				IF int_flag = TRUE THEN
					EXIT INPUT
				ELSE
		 			CONTINUE INPUT
				END IF
			END IF
	END INPUT

	IF int_flag = TRUE THEN
		ROLLBACK WORK
		LET int_flag = FALSE
		LET gr_tipdoc.* = ur_tipdoc.*
		CALL empr_desp()
		CALL box_valdato("Modificación Cancelada." )
       MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

	ELSE
		WHENEVER ERROR CONTINUE
		EXECUTE  upd_tipdoc 
         USING gr_tipdoc.tipdoc_cod,
               gr_tipdoc.tipdoc_nom
		WHENEVER ERROR STOP 	
		IF SQLCA.SQLCODE < 0 THEN
			ROLLBACK WORK
			LET gr_tipdoc.* = ur_tipdoc.*
			CALL empr_desp()
			LET mensaje = "El error numero ", SQLCA.SQLCODE USING "-<<<<", " ha ocurrido."
			CALL box_valdato(mensaje)
			RETURN FALSE
		ELSE
			COMMIT WORK
			CALL empr_desp()
			CALL box_valdato("Registro modificado.")
			RETURN TRUE
		END IF
	END IF
	RETURN FALSE
END IF
RETURN FALSE

END FUNCTION


FUNCTION valida_modi()
   IF gr_tipdoc.tipdoc_cod        = ur_tipdoc.tipdoc_cod AND
      gr_tipdoc.tipdoc_nom        = ur_tipdoc.tipdoc_nom 
     THEN   
      RETURN FALSE
   END IF

   RETURN TRUE
END FUNCTION