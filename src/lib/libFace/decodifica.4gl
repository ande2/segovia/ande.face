IMPORT xml

FUNCTION parse(file)
   DEFINE FILE       STRING 
   DEFINE FileD      STRING
   DEFINE arc_auxc   STRING
   DEFINE arc_auxd   STRING
   DEFINE comando    STRING
   DEFINE texto      TEXT
   DEFINE i, j, x, y INTEGER
   DEFINE idx        INTEGER
   DEFINE lFirma     STRING
   LET lFirma = '' 
   LET arc_auxc = "fac", FGL_GETPID() USING "&&&&&&",".xml"
   LET arc_auxd = "fad", FGL_GETPID() USING "&&&&&&",".xml"
   LOCATE texto IN MEMORY
   TRY
      LET texto = FILE
      CALL texto.WriteFile ( arc_auxc )
      LET comando ="fglpass -dec64 ", arc_auxc CLIPPED, " > ", arc_auxd  
      RUN comando
      CALL texto.ReadFile ( arc_auxd )
      LET fileD = texto
   CATCH
      DISPLAY "ERROR en conversion de XML"
      RETURN NULL
   END TRY

   TRY
      LET x = 0
      LET y = 0
      FOR idx = 1 TO filed.getLength()
         IF filed.subString( idx, idx + 14 ) = "<SignatureValue" THEN
            LET i = idx + 14
            FOR j = i TO filed.getLength()
               IF filed.subString(j, j ) = ">" THEN
                  LET x = j
                  EXIT FOR
               END IF
            END FOR
            FOR j = x + 1 TO filed.getLength()
               IF filed.subString(j, j ) = "<" THEN
                  LET y = j
                  EXIT FOR
               END IF
            END FOR
            EXIT FOR
         END IF
      END FOR
      LET lfirma = filed.subString(x+1,y-1)
   CATCH
      MESSAGE "ERROR al obtener la firma electronica"
      LET lfirma = ''
   END TRY
   RETURN lFirma
END FUNCTION