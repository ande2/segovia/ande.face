

DATABASE face

DEFINE
   rDet                                DYNAMIC ARRAY OF RECORD LIKE ddoc.*,
   rD                                  RECORD LIKE ddoc.*,
   rEnc RECORD
      --Asignación
      doc_serie                        LIKE mdoc.doc_serie, 
      doc_num                          LIKE mdoc.doc_num, 
      doc_femision                     LIKE mdoc.doc_femision, 
      res_nautoriza                    LIKE dres.res_nautoriza, 
      res_fecha                        LIKE dres.res_fecha, 
      res_de                           LIKE dres.res_de, 
      res_a                            LIKE dres.res_a,
      --Procesamiento
      esta_emailfrom                   LIKE mesta.esta_emailto, 
      esta_emailto                     LIKE mesta.esta_emailto, 
      esta_emailcc                     LIKE mesta.esta_emailcc, 
      cli_formato                      LIKE mcli.cli_formato,   
      --Encabezado
      tipdoc_cod                       LIKE mtipdoc.tipdoc_nom, 
      doc_estado                       VARCHAR(20), 
      doc_moneda                       CHAR(3), 
      doc_tipocambio                   DEC(10,5), 
      cli_regimenisr                   VARCHAR(20), 
      doc_identificator                LIKE mdoc.doc_id,
      --Datos del vendedor
      empr_nit                         LIKE mempr.empr_nit, 
      empr_nom                         LIKE mempr.empr_nom, 
      empr_idioma                      LIKE mempr.empr_idioma, 
      esta_nom                         LIKE mesta.esta_nom, 
      esta_cod                         LIKE mesta.esta_cod, 
      esta_dispo                       LIKE mesta.esta_dispo,
      esta_dir1                        LIKE mesta.esta_dir1, 
      esta_dir2                        LIKE mesta.esta_dir2, 
      ven_mun_nom                      LIKE mesta.mun_nom, 
      ven_dep_nom                      LIKE mesta.dep_nom, 
      ven_pai_nom                      LIKE mesta.pai_nom, 
      esta_codpos                      LIKE mesta.esta_codpos,
      --Comprador
      cli_nit                          LIKE mcli.cli_nit, 
      cli_nomcom                       LIKE mcli.cli_nomcom, 
      cli_idioma                       LIKE mcli.cli_idioma, 
      cli_dir1                         LIKE mcli.cli_dir1, 
      cli_dir2                         LIKE mcli.cli_dir2, 
      cli_mun_nom                      LIKE mcli.mun_nom, 
      cli_dep_nom                      LIKE mcli.dep_nom, 
      cli_pai_nom                     LIKE mcli.pais_nom, 
      cli_codpos                       LIKE mcli.cli_codpos,
      --Totales
      doc_subtotal_sin_descto          LIKE mdoc.doc_subtotal_sin_descto, 
      doc_suma_desctos                 LIKE mdoc.doc_suma_desctos, 
      doc_desctob_base                  LIKE mdoc.doc_desctob_base, 
      doc_desctob_tasa                  LIKE mdoc.doc_desctob_tasa, 
      doc_desctob_monto                 LIKE mdoc.doc_desctob_monto, 
      doc_desctos_base                  LIKE mdoc.doc_desctos_base, 
      doc_desctos_tasa                  LIKE mdoc.doc_desctos_tasa, 
      doc_desctos_monto                 LIKE mdoc.doc_desctos_monto, 
      doc_subtotal_con_descto          LIKE mdoc.doc_subtotal_con_descto, 
      doc_total_impuestos              LIKE mdoc.doc_total_impuestos, 
      doc_ing_neto_gravado             LIKE mdoc.doc_ing_neto_gravado, 
      doc_total_iva                    LIKE mdoc.doc_total_iva, 
      doc_impuesto_tipo                LIKE mdoc.doc_impuesto_tipo, 
      doc_impuesto_base                LIKE mdoc.doc_impuesto_base, 
      doc_impuesto_tasa                LIKE mdoc.doc_impuesto_tasa, 
      doc_impuesto_monto               LIKE mdoc.doc_impuesto_monto
   END RECORD,
   lautorizacion              CHAR(200),
   lrequestor                 CHAR(40),
   lEntity                    CHAR(12),
   lUserFace                  CHAR(20),
	lDispositivo				   SMALLINT,
	lTipoActivo					   CHAR(10),
   QueryBuffer                VARCHAR(1000),
   lcount                     SMALLINT,
   lorigen                    CHAR(3),
   id_empresa                 SMALLINT,
   wstatus 			            INTEGER,
   lerr_message               VARCHAR(255),
   strXML                     STRING




   
FUNCTION makeXML ( ldoc_id, laut )
CONSTANT lTasaIVA             = 0.12
DEFINE 
   ldoc_id                    INTEGER,
   laut                       SMALLINT,
   total_letras               VARCHAR(150),
   idx                        SMALLINT,
   lserie                     LIKE mdoc.doc_serie,
   ldocumento                 LIKE mdoc.doc_num,
   strXML                     STRING
      
      SELECT   origen
      INTO  lorigen
      FROM  face:mdoc_i
      WHERE doc_id = ldoc_id

      CASE lorigen
         WHEN 'REP'  LET id_empresa = 1
         WHEN 'CXC'  LET id_empresa = 2
         WHEN 'EQI'  LET id_empresa = 3
      END CASE
    
	--Buscando datos para la conexion
   SELECT   c.esta_requestor, d.empr_nit, c.esta_userface, c.esta_dispo, e.tipdoc_cod 
      INTO  lRequestor, lEntity, lUserFace, lDispositivo, lTipoActivo
      FROM  face:mdoc a, face:dres b, 
            face:mesta C, face:mempr d, 
            face:mtipdoc e 
      WHERE a.doc_id    = ldoc_id
      AND   b.res_id    = a.res_id 
      --AND   a.doc_num   BETWEEN b.res_rangode AND b.res_rangoa 
      AND   C.esta_id   = b.esta_id 
      AND   d.empr_id   = C.empr_id 
      AND   e.tipdoc_id = b.tipdoc_id   
   
   --Lee los datos del encabezado
   SELECT      --Asignacion 
               a.doc_serie, a.doc_num, a.doc_femision, b.res_nautoriza, b.res_fecha, b.res_de, b.res_a,
               --Procesamiento
               c.esta_emailto, ' ', c.esta_emailcc, 'PDF',
               --Encabezado
               e.tipdoc_cod, a.doc_est, a.doc_codmon , a.doc_tipocambio, 'PAGO_TRIMESTRAL', a.doc_id,
               --Datos del vendedor
               f.empr_nit, f.empr_nom, f.empr_idioma, c.esta_nom, c.esta_cod, 1,c.esta_dir1, c.esta_dir2, c.mun_nom, c.dep_nom, c.pai_nom, c.esta_codpos,
               --Comprador
               REPLACE(a.cli_nit,'-', ''), a.cli_nomcom, a.cli_idioma, TRIM(a.cli_dir1), TRIM(a.cli_dir2), TRIM(a.mun_nom), TRIM(a.dep_nom), TRIM(a.pai_nom), TRIM(a.cli_codpos), 
               --Totales
               a.doc_subtotal_sin_descto, a.doc_suma_desctos, 
               a.doc_desctob_base, a.doc_desctob_tasa, a.doc_desctob_monto, 
               a.doc_desctos_base, a.doc_desctos_tasa, a.doc_desctos_monto, 
               a.doc_subtotal_con_descto, a.doc_total_impuestos, a.doc_ing_neto_gravado, a.doc_total_iva, 
               a.doc_impuesto_tipo, a.doc_impuesto_base, a.doc_impuesto_tasa, a.doc_impuesto_monto
         INTO  rEnc.*
         FROM  face:mdoc a, face:dres b, 
               face:mesta c,
               face:mtipdoc e, face:mempr f
         WHERE a.doc_id          = ldoc_id
         AND   b.res_id          = a.res_id
         --AND   a.doc_num         BETWEEN b.res_rangode AND b.res_rangoa 
         AND   c.esta_id         = b.esta_id
         AND   e.tipdoc_id       = b.tipdoc_id
         AND   f.empr_id         = c.empr_id

         
   IF STATUS = NOTFOUND THEN
      DISPLAY "Factura no Existe, favor verifique"
      RETURN FALSE
   END IF


   --Revisar si el cliente es consumidor final
   IF rEnc.cli_nit = "C/F" THEN
      LET rEnc.cli_nit = 'CF'
   END IF
   
   --Cargando el detalle
   CALL rDet.clear()
   
   LET idx = 1
   
   DECLARE ListaDet CURSOR FOR
     SELECT    a.*                          
         FROM  face:ddoc a
         WHERE a.doc_id = ldoc_id
            
   FOREACH ListaDet INTO rD.*
      LET rDet[idx].* = rd.*
      LET idx = idx + 1
   END FOREACH

   --Armando el encabezado del documento
   LET strXML =   '<?xml version="1.0" encoding="UTF-8"?> \n',
                  '<FactDocGT \n',
                  ' xmlns="http://www.fact.com.mx/schema/gt"\n',
                  ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"\n',
                  ' xsi:schemaLocation="http://www.fact.com.mx/schema/gt http://www.mysuitemex.com/fact/schema/fx_2012_gt.xsd">\n'

   --Concatenando la version
   LET strXML = strXML, ' <Version>2</Version>\n'

   
   IF NOT laut THEN
      --Asignacion solicitada
      LET strXML = strXML, '<AsignacionSolicitada>\n'

      LET strXML = strXML, '<Serie>',
                           rEnc.doc_serie CLIPPED,
                           '</Serie>\n'

      LET strXML = strXML, '<NumeroDocumento>',
                           rEnc.doc_num USING "<<<<<<<<<<<<",
                           '</NumeroDocumento>\n'

      LET strXML = strXML, '<FechaEmision>',
                           DATE(rEnc.doc_femision)       USING "YYYY-MM-DD",
                           "T",
                           TIME(rEnc.doc_femision)       CLIPPED,
                           '</FechaEmision>\n'

      LET strXML = strXML, '<NumeroAutorizacion>',
                           rEnc.res_nautoriza         CLIPPED,
                           '</NumeroAutorizacion>\n'

      LET strXML = strXML, '<FechaResolucion>',
                           rEnc.res_fecha       USING "YYYY-MM-DD", 
                           '</FechaResolucion>\n'

      LET strXML = strXML, '<RangoInicialAutorizado>',
                           rEnc.res_de USING "<<<<<<<<<<",
                           '</RangoInicialAutorizado>\n'

      LET strXML = strXML, '<RangoFinalAutorizado>',
                           rEnc.res_a USING "<<<<<<<<<<",
                           '</RangoFinalAutorizado>\n'
                           
      LET strXML = strXML, '</AsignacionSolicitada>\n'
   END IF

   --Armando la seccion del procesamiento
   LET strXML = strXML, '<Procesamiento>\n'
   
   LET strXML = strXML, '<Dictionary name="email">\n'

   LET strXML = strXML, '<Entry k="from" v="', rEnc.esta_emailfrom CLIPPED, '" />\n'
   
   LET strXML = strXML, '<Entry k="to" v="', rEnc.esta_emailto CLIPPED, '" />\n'
   
   LET strXML = strXML, '<Entry k="cc" v="', rEnc.esta_emailcc CLIPPED, '" />\n'
   
   LET strXML = strXML, '<Entry k="formats" v="', rEnc.cli_formato CLIPPED, '" />\n'

   LET strXML = strXML, '</Dictionary>\n'
   
   LET strXML = strXML, '</Procesamiento>\n'

   
   --Armando el Encabezado
   LET strXML = strXML, '<Encabezado>\n'
   
   LET strXML = strXML, '<TipoActivo>',
                        lTipoActivo CLIPPED,
                        '</TipoActivo>\n'

	IF rEnc.doc_estado = "ANULADO" THEN
		LET strXML = strXML, '<RedefinirEstadoDeCopia>',
                             rEnc.doc_estado CLIPPED,
                             '</RedefinirEstadoDeCopia>\n'
          LET strXML = strXML, '<FechaCancelacion>',
                               DATE(rEnc.doc_femision)       USING "YYYY-MM-DD",
                               '</FechaCancelacion>\n'
	END IF

   LET strXML = strXML, '<CodigoDeMoneda>',
                        rEnc.doc_moneda CLIPPED,
                        '</CodigoDeMoneda>\n'	

   LET strXML = strXML, '<TipoDeCambio>',
                        rEnc.doc_tipocambio USING "<,<<&.&&&&",
                        '</TipoDeCambio>\n'

   LET strXML = strXML, '<InformacionDeRegimenIsr>',
                        rEnc.cli_regimenisr CLIPPED,
                        '</InformacionDeRegimenIsr>\n'

   LET strXML = strXML, '<ReferenciaInterna>',
                        rEnc.doc_identificator USING "<<<<<<<<<&",
                        '</ReferenciaInterna>\n'
   
   LET strXML = strXML, '</Encabezado>\n'

   --Datos del Vendedor
   LET strXML = strXML, '<Vendedor>\n'
   
   LET strXML = strXML, '<Nit>',
                        rEnc.empr_nit USING "&&&&&&&&&&&&", 
                        '</Nit>\n'

   LET strXML = strXML, '<NombreComercial>',
                        rEnc.empr_nom CLIPPED,
                        '</NombreComercial>\n'

   LET strXML = strXML, '<Idioma>',
                        rEnc.empr_idioma CLIPPED,
                        '</Idioma>\n'

   LET strXML = strXML, '<DireccionDeEmisionDeDocumento>\n'

   LET strXML = strXML, '<NombreDeEstablecimiento>',
                        rEnc.esta_nom CLIPPED,
                        '</NombreDeEstablecimiento>\n'
                        
   LET strXML = strXML, '<CodigoDeEstablecimiento>',
                        rEnc.esta_cod USING "&&&",
                        '</CodigoDeEstablecimiento>\n'

   LET strXML = strXML, '<DispositivoElectronico>',
                        rEnc.esta_dispo USING "&&&",
                        '</DispositivoElectronico>\n'

   LET strXML = strXML, '<Direccion1>',
                        rEnc.esta_dir1 CLIPPED,
                        '</Direccion1>\n'

   IF rEnc.esta_dir2 != "" THEN
      LET strXML = strXML, '<Direccion2>',
                           rEnc.esta_dir2 CLIPPED,
                           '</Direccion2>\n'
   END IF   

   LET strXML = strXML, '<Municipio>',
                        rEnc.ven_mun_nom CLIPPED,
                        '</Municipio>\n'

   IF rEnc.ven_dep_nom IS NOT NULL THEN
      LET strXML = strXML, '<Departamento>',
                           rEnc.ven_dep_nom CLIPPED,
                           '</Departamento>\n'
   END IF

   LET strXML = strXML, '<CodigoDePais>',
                        rEnc.ven_pai_nom CLIPPED,
                        '</CodigoDePais>\n'

   LET strXML = strXML, '<CodigoPostal>',
                        rEnc.esta_codpos USING "&&&&&",
                        '</CodigoPostal>\n'

   LET strXML = strXML, '</DireccionDeEmisionDeDocumento>\n'
   
   LET strXML = strXML, '</Vendedor>\n'

   --Datos del Comprador
   LET strXML = strXML, '<Comprador>\n'
   
   LET strXML = strXML, '<Nit>',
                        rEnc.cli_nit CLIPPED,
                        '</Nit>\n'

   LET rEnc.cli_nomcom = revisa_caracter ( rEnc.cli_nomcom )
   
   LET strXML = strXML, '<NombreComercial>',
                        rEnc.cli_nomcom CLIPPED,
                        '</NombreComercial>\n'

   LET strXML = strXML, '<Idioma>',
                        rEnc.cli_idioma CLIPPED,
                        '</Idioma>\n'

   LET strXML = strXML, '<DireccionComercial>\n'

   LET strXML = strXML, '<Direccion1>'
   IF rEnc.cli_dir1 IS NOT NULL THEN
      LET strXML = strXML, rEnc.cli_dir1 CLIPPED
   ELSE
      LET strXML = strXML, 'Guatemala'
   END IF
   LET strXML = strXML, '</Direccion1>\n'

   IF rEnc.cli_dir2 IS NOT NULL THEN
      LET strXML = strXML, '<Direccion2>',
                           rEnc.cli_dir2 CLIPPED,
                           '</Direccion2>\n'
   END IF
   
   LET strXML = strXML, '<Municipio>'
   IF rEnc.cli_mun_nom IS NOT NULL THEN
      LET strXML = strXML, rEnc.cli_mun_nom CLIPPED
   ELSE
      LET strXML = strXML, 'Guatemala'
   END IF
   LET strXML = strXML, '</Municipio>\n'

   LET strXML = strXML, '<Departamento>'
   IF rEnc.cli_dep_nom IS NOT NULL THEN
      LET strXML = strXML, rEnc.cli_dep_nom CLIPPED
   ELSE
      LET strXML = strXML, 'Guatemala'
   END IF
   LET strXML = strXML, '</Departamento>\n'
   
   LET strXML = strXML, '<CodigoDePais>',
                        rEnc.cli_pai_nom CLIPPED,
                        '</CodigoDePais>\n'

   IF rEnc.cli_codpos IS NOT NULL THEN
      LET strXML = strXML, '<CodigoPostal>',
                           rEnc.cli_codpos CLIPPED,
                           '</CodigoPostal>\n'
   END IF

   LET strXML = strXML, '</DireccionComercial>\n'
   
   LET strXML = strXML, '</Comprador>\n'

   
   --Detalles
   LET strXML = strXML, '<Detalles>\n'

   FOR idx = 1 TO rDet.getLength()

      IF rDet[idx].pro_nom IS NOT NULL THEN
         LET strXML = strXML, '<Detalle>\n'
         
         LET rDEt[idx].pro_nom = revisa_caracter ( rDEt[idx].pro_nom )
         
         LET strXML = strXML, '<Descripcion>',
                              rDet[idx].pro_nom CLIPPED,
                              '</Descripcion>\n'

         LET strXML = strXML, '<CodigoEAN>',
                              rDet[idx].pro_ean  CLIPPED,
                              '</CodigoEAN>\n'

         LET strXML = strXML, '<UnidadDeMedida>',
                              rDet[idx].pro_unimed CLIPPED,
                              '</UnidadDeMedida>\n'

         LET strXML = strXML, '<Cantidad>',
                              rDet[idx].pro_cantidad USING "<<<<<<<",
                              '</Cantidad>\n'

         LET strXML = strXML, '<ValorSinDR>\n'
         
         LET strXML = strXML, '<Precio>',
                              ( rDet[idx].pro_valor_sdesto_precio  /
                                rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                              '</Precio>\n'

         LET strXML = strXML, '<Monto>',
                              ( rDet[idx].pro_valor_sdescto_monto /
                                rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                              '</Monto>\n'

         LET strXML = strXML, '</ValorSinDR>\n'

         IF rDet[idx].pro_monto_desctob + rDet[idx].pro_monto_desctos > 0 THEN
         
            LET strXML = strXML, '<DescuentosYRecargos>\n'

            LET strXML = strXML, '<SumaDeDescuentos>',
                                 ( ( rDet[idx].pro_monto_desctob +  rDet[idx].pro_monto_desctos) / rEnc.doc_tipocambio )  USING "<<<<<<&.&&",
                                 '</SumaDeDescuentos>\n'
         END IF

         IF rDet[idx].pro_monto_desctob > 0 THEN
            LET strXML = strXML, '<DescuentoORecargo>\n'

            LET strXML = strXML, '<Operacion>',
                                 'DESCUENTO',
                                 '</Operacion>\n'

            LET strXML = strXML, '<Servicio>',
                                 'ALLOWANCE_GLOBAL',
                                 '</Servicio>\n'

            LET strXML = strXML, '<Base>',
                                 ( rDet[idx].pro_base_desctob  / rEnc.doc_tipocambio ) USING   "<<<<<<&.&&",
                                 '</Base>\n'

            LET strXML = strXML, '<Tasa>',
                                 rDet[idx].pro_tasa_desctob   USING "<<<<<<&.&&",
                                 '</Tasa>\n'

            LET strXML = strXML, '<Monto>',
                                 ( rDet[idx].pro_monto_desctob / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                                 '</Monto>\n'
                
            LET strXML = strXML, '</DescuentoORecargo>\n'
         END IF

         IF rDet[idx].pro_monto_desctos > 0 THEN
            LET strXML = strXML, '<DescuentoORecargo>\n'

            LET strXML = strXML, '<Operacion>',
                                 'DESCUENTO',
                                 '</Operacion>\n'

            LET strXML = strXML, '<Servicio>',
                                 'ALLOWANCE_GLOBAL',
                                 '</Servicio>\n'

            LET strXML = strXML, '<Base>',
                                 ( rDet[idx].pro_base_desctos / rEnc.doc_tipocambio ) USING   "<<<<<<&.&&",
                                 '</Base>\n'

            LET strXML = strXML, '<Tasa>',
                                 rDet[idx].pro_tasa_desctos  USING "<<<<<<&.&&",
                                 '</Tasa>\n'

            LET strXML = strXML, '<Monto>',
                                 ( rDet[idx].pro_monto_desctos / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                                 '</Monto>\n'
                
            LET strXML = strXML, '</DescuentoORecargo>\n'
         END IF

         
         IF rDet[idx].pro_monto_desctob + rDet[idx].pro_monto_desctos > 0 THEN
            LET strXML = strXML, '</DescuentosYRecargos>\n'
         END IF

      LET strXML = strXML, '<ValorConDR>\n'
      
      LET strXML = strXML, '<Precio>',
                           ( rDet[idx].pro_valor_cdescto_precio / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                           '</Precio>\n'

      LET strXML = strXML, '<Monto>',
                           ( rDet[idx].pro_valor_cdesto_monto / rEnc.doc_tipocambio)            USING "<<<<<<&.&&",
                           '</Monto>\n'

      LET strXML = strXML, '</ValorConDR>\n'

      IF rDet[idx].pro_imp_tasa = 0 THEN
         LET strXML = strXML, '<ProductoConIvaTasaCero>',
                              "true",
                              '</ProductoConIvaTasaCero>\n'
      END IF;

      LET strXML = strXML, '<Impuestos>\n'

      LET strXML = strXML, '<TotalDeImpuestos>',
                           ( rDet[idx].prod_total_imp / rEnc.doc_tipocambio)         USING  "<<<<<<&.&&",
                           '</TotalDeImpuestos>\n'

      LET strXML = strXML, '<IngresosNetosGravados>',
                           ( rDet[idx].pro_ing_neto_gravado / rEnc.doc_tipocambio)     USING "<<<<<<&.&&",
                           '</IngresosNetosGravados>\n'

      LET strXML = strXML, '<TotalDeIVA>',
                           ( rDet[idx].prod_total_iva / rEnc.doc_tipocambio)                 USING "<<<<<<&.&&",
                           '</TotalDeIVA>\n'

      LET strXML = strXML, '<Impuesto>\n'

      LET strXML = strXML, '<Tipo>',
                           'IVA',
                           '</Tipo>\n'

      LET strXML = strXML, '<Base>',
                           ( rDet[idx].pro_imp_base / rEnc.doc_tipocambio )  USING "<<<<<<&.&&",
                           '</Base>\n'

      LET strXML = strXML, '<Tasa>',
                           rDet[idx].pro_imp_tasa   USING "<<<<<<&.&&",
                           '</Tasa>\n'

      LET strXML = strXML, '<Monto>',
                           ( rDet[idx].pro_imp_monto / rEnc.doc_tipocambio)  USING  "<<<<<<&.&&",
                           '</Monto>\n'

      LET strXML = strXML, '</Impuesto>\n'

      LET strXML = strXML, '</Impuestos>\n'
      
      LET strXML = strXML, '<Categoria>',
                           rDet[idx].pro_tipo,
                           '</Categoria>\n'

      LET strXML = strXML, '</Detalle>\n'
      
   END IF
      
   END FOR
   
   LET strXML = strXML, '</Detalles>\n'


   
   --Totales
   LET strXML = strXML, '<Totales>\n'

   LET strXML = strXML, '<SubTotalSinDR>',
                        ( ( sumarDetalle( 'valorSinDRMonto' ) )/ rEnc.doc_tipocambio)  USING "<<<<<<&.&&",
                        '</SubTotalSinDR>\n'

                        
   IF SumarDetalle( "sumaDeDescuentos" ) > 0 THEN
      LET strXML = strXML, '<DescuentosYRecargos>\n'

      LET strXML = strXML, '<SumaDeDescuentos>',
                           ( ( sumarDetalle ( "sumaDeDescuentos" ) ) / rEnc.doc_tipocambio )  USING "<<<<<<&.&&",
                           '</SumaDeDescuentos>\n'
   END IF

   IF sumarDetalle ( "descuentobMonto") > 0 THEN
      LET strXML = strXML, '<DescuentoORecargo>\n'

      LET strXML = strXML, '<Operacion>',
                           'DESCUENTO',
                           '</Operacion>\n'

      LET strXML = strXML, '<Servicio>',
                           'ALLOWANCE_GLOBAL',
                           '</Servicio>\n'

      LET strXML = strXML, '<Base>',
                           ( ( sumarDetalle ( "descuentobBase" ) ) / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                           '</Base>\n'

      LET strXML = strXML, '<Tasa>',
                           rDet[1].pro_tasa_desctob       USING "<<<<<<&.&&",
                           '</Tasa>\n'

      LET strXML = strXML, '<Monto>',
                           ( ( sumarDetalle ( "descuentobMonto" ) ) / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                           '</Monto>\n'
          
      LET strXML = strXML, '</DescuentoORecargo>\n'
   END IF

   IF sumarDetalle ( "descuentosMonto") > 0 THEN
      LET strXML = strXML, '<DescuentoORecargo>\n'

      LET strXML = strXML, '<Operacion>',
                           'DESCUENTO',
                           '</Operacion>\n'

      LET strXML = strXML, '<Servicio>',
                           'ALLOWANCE_GLOBAL',
                           '</Servicio>\n'

      LET strXML = strXML, '<Base>',
                           ( ( sumarDetalle ( "descuentosBase" ) ) / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                           '</Base>\n'

      LET strXML = strXML, '<Tasa>',
                           rDet[1].pro_tasa_desctos       USING "<<<<<<&.&&",
                           '</Tasa>\n'

      LET strXML = strXML, '<Monto>',
                           ( ( sumarDetalle ( "descuentosMonto" ) ) / rEnc.doc_tipocambio )  USING "<<<<<<&.&&",
                           '</Monto>\n'
          
      LET strXML = strXML, '</DescuentoORecargo>\n'
   END IF

   IF SumarDetalle( "sumaDeDescuentos" ) > 0 THEN
      LET strXML = strXML, '</DescuentosYRecargos>\n'
   END IF
   
   LET strXML = strXML, '<SubTotalConDR>',
                        ( ( sumarDetalle ( "valorConDRMonto" ) ) / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                        '</SubTotalConDR>\n'

   LET strXML = strXML, '<Impuestos>\n'
   
   LET strXML = strXML, '<TotalDeImpuestos>',
                        ( ( sumarDetalle ( "totalDeImpuestos" ) ) / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                        '</TotalDeImpuestos>\n'

   LET strXML = strXML, '<IngresosNetosGravados>',
                        ( ( sumarDetalle ( "ingresosNetosGravados" ) ) / rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                        '</IngresosNetosGravados>\n'

   LET strXML = strXML, '<TotalDeIVA>',
                        ( ( sumarDetalle ( "totalIVA" ) ) / rEnc.doc_tipocambio ) USING "<<<<<<&.&&", 
                        '</TotalDeIVA>\n'

   LET strXML = strXML, '<Impuesto>\n'

   LET strXML = strXML, '<Tipo>',
                        rEnc.doc_impuesto_tipo CLIPPED,
                        '</Tipo>\n'

   LET strXML = strXML, '<Base>',
                        ( ( sumarDetalle ( "impuestoBase" ) ) / rEnc.doc_tipocambio )  USING "<<<<<<&.&&",
                        '</Base>\n'

   LET strXML = strXML, '<Tasa>',
                        rEnc.doc_impuesto_tasa  USING "<<<<<<&.&&",
                        '</Tasa>\n'

   LET strXML = strXML, '<Monto>',
                        ( ( sumarDetalle ( "totalIVA" ) )/ rEnc.doc_tipocambio ) USING "<<<<<<&.&&",
                        '</Monto>\n'

   LET strXML = strXML, '</Impuesto>\n'

   LET strXML = strXML, '</Impuestos>\n'

   LET strXML = strXML, '<Total>',
                        ( ( rEnc.doc_subtotal_con_descto + rEnc.doc_impuesto_monto) / rEnc.doc_tipocambio )   USING "<<<<<<&.&&",
                        '</Total>\n'

   LET total_letras = num_letra ( ( rEnc.doc_subtotal_con_descto  + rEnc.doc_impuesto_monto ) / rEnc.doc_tipocambio )
   
   LET strXML = strXML, '<TotalLetras>',
                        total_letras CLIPPED,
                        '</TotalLetras>\n'

   LET strXML = strXML, '</Totales>\n'


{	IF lMontoCertificado > 0 THEN
		LET strXML = strXML, '<TextosDePie>'

		LET strXML = strXML, '<Texto>',
									"DESC. CERTIFICADO DE REGALO Q.", 
									lMontoCertificado USING "<<<,<<&.&&",
									'</Texto>'

		LET strXML = strXML, '</TextosDePie>'
	END IF}

   LET strXML = strXML, '</FactDocGT>'
   RETURN strXML
END FUNCTION


FUNCTION sumarDetalle ( lItem  )
DEFINE
   lItem       STRING,
   lMonto      DEC(12,2),
   idx         INTEGER
   LET lMonto = 0
   FOR idx = 1 TO rDet.getLength()
      CASE lItem
         WHEN "valorSinDRMonto"        LET lMonto = lMonto + rDet[idx].pro_valor_sdescto_monto
         WHEN "sumaDeDescuentos"       LET lMonto = lMonto + rDet[idx].pro_monto_desctob + rDet[idx].pro_monto_desctos
         WHEN "descuentobBase"         LET lMonto = lMonto + rDet[idx].pro_base_desctob
         WHEN "descuentobMonto"        LET lMonto = lMonto + rDet[idx].pro_monto_desctob
         WHEN "descuentosBase"         LET lMonto = lMonto + rDet[idx].pro_base_desctos
         WHEN "descuentosMonto"        LET lMonto = lMonto + rDet[idx].pro_monto_desctos
         WHEN "valorConDRMonto"        LET lMonto = lMonto + rDet[idx].pro_valor_cdesto_monto
         WHEN "totalDeImpuestos"       LET lMonto = lMonto + rDet[idx].prod_total_imp
         WHEN "ingresosNetosGravados"  LET lMonto = lMonto + rDet[idx].pro_ing_neto_gravado
         WHEN "totalIVA"               LET lMonto = lMonto + rDet[idx].prod_total_iva
         WHEN "impuestoBase"           LET lMonto = lMonto + rDet[idx].pro_imp_base
         WHEN "impuestoMonto"          LET lMonto = lMonto + rDet[idx].pro_imp_monto
      END CASE
   END FOR
   RETURN lMonto
END FUNCTION
