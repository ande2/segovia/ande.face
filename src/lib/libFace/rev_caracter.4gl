FUNCTION revisa_caracter ( Dato )
DEFINE i SMALLINT
DEFINE Dato CHAR(100)
DEFINE retorna VARCHAR(100)
LET Retorna = ""
FOR i = 1 TO LENGTH(Dato)
   IF Dato[i] = "&" THEN
      LET Retorna = Retorna, '&amp;'
   ELSE
      LET Retorna = Retorna, Dato[i]
   END IF
END FOR
RETURN Retorna
END FUNCTION