##############################################################################
#	Modulo      :%M%
#	Descripcion :Cajas de pregunta y mensajes globales
#	Funciones   :box_inter    Funcion para confirmacion de interrupcion de prog.
#	             box_elifila  Funcion para eliminar una fila de un vector
#               box_valdato  Funcion para validar datos en programas
#               box_actdato  Funcion para validar si actualiza o no datos
#	SCCS Id No. :%Z% %W%
#	Autor       :Giovanni Yanes
#	Fecha       :%H% %T%
#	Path        :%P%
###############################################################################
#Funcion global para confirmacion de interrupcion de programa

FUNCTION box_inter()
DEFINE
	respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmaci�n de Interrupci�n","Est� seguro de querer cancelar el proceso?","No","Yes|No","question",1)
	RETURNING respuesta

	IF respuesta = "No" THEN
		LET int_flag = FALSE
	ELSE
		LET int_flag = TRUE
	END IF

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN int_flag

END FUNCTION

FUNCTION box_elifila()
DEFINE
	respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion para eliminacion","Esta seguro de querer eliminar el registro?","No","Yes|No","question",1)
	RETURNING respuesta

	IF respuesta = "No" THEN
		LET int_flag = FALSE
	ELSE
		LET int_flag = TRUE
	END IF

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN int_flag

END FUNCTION

FUNCTION box_valdato(mensaje)
DEFINE
	mensaje CHAR(100)
   --# CALL fgl_init4js()
	--# CALL fgl_winmessage("Advertencia",mensaje,"exclamation")
   
   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
END FUNCTION

FUNCTION box_actfila()
DEFINE
	respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion para actualizar","Esta seguro de querer actualizar el registro?","No","Yes|No","question",1)
	RETURNING respuesta

	IF respuesta = "No" THEN
		LET int_flag = FALSE
	ELSE
		LET int_flag = TRUE
	END IF

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN int_flag

END FUNCTION

FUNCTION box_gradato(mensaje)
DEFINE
    mensaje,respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion del sistema" ,mensaje,"Cancel","Yes|No|Cancel","question",1)
	RETURNING respuesta

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN respuesta

END FUNCTION

FUNCTION box_prompt(mensaje)
	DEFINE mensaje,respuesta CHAR(10)

	--#CALL fgl_init4js()
	--#CALL fgl_winprompt(20, 2, "mensaje", "" ,50,0) RETURNING respuesta

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN respuesta
	
END FUNCTION

FUNCTION box_error(mensaje)
DEFINE mensaje CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winmessage("ATENCION",mensaje,"stop")
	
   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN
END FUNCTION

FUNCTION box_anula()
DEFINE
	respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion para Anular","Esta seguro de querer anular el registro?","No","Yes|No","question",1)
	RETURNING respuesta

	IF respuesta = "No" THEN
		LET int_flag = FALSE
	ELSE
		LET int_flag = TRUE
	END IF

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN int_flag

END FUNCTION


FUNCTION box_pregunta(mensaje)
DEFINE
    mensaje,respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion del sistema" ,mensaje,"No","Yes|No","question",1)
	RETURNING respuesta

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN respuesta

END FUNCTION

FUNCTION box_deshabilita()
DEFINE
	respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion para deshabilitar","Esta seguro de querer deshabilitar el registro?","No","Yes|No","question",1)
	RETURNING respuesta

	IF respuesta = "No" THEN
		LET int_flag = FALSE
	ELSE
		LET int_flag = TRUE
	END IF

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN int_flag

END FUNCTION

FUNCTION box_habilita()
DEFINE
	respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion para habilitar","Esta seguro de querer habilitar el registro?","No","Yes|No","question",1)
	RETURNING respuesta

	IF respuesta = "No" THEN
		LET int_flag = FALSE
	ELSE
		LET int_flag = TRUE
	END IF

   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
	RETURN int_flag

END FUNCTION

FUNCTION box_confirma(mensaje)
DEFINE
    mensaje,respuesta CHAR(100)

	--#CALL fgl_init4js()
	--#CALL fgl_winquestion("Confirmacion del sistema" ,mensaje,"No","Yes|No","question",1)
	RETURNING respuesta

   IF respuesta = "No" THEN
      LET int_flag = FALSE
   ELSE
      LET int_flag = TRUE
   END IF
 
   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
   RETURN int_flag

END FUNCTION

FUNCTION box_informa(mensaje)
DEFINE
	mensaje CHAR(100)
   --# CALL fgl_init4js()
	--# CALL fgl_winmessage("Advertencia",mensaje,"info")
   --IF fgl_getuitype() <> "JAVA" THEN
	   --DISPLAY ""
   --END IF
END FUNCTION

FUNCTION box_mensaje01(mensaje)
DEFINE
    mensaje   VARCHAR(500,1),
    respuesta CHAR(3)

   --#CALL fgl_init4js()
   --#CALL fgl_winquestion("Confirmacion del sistema" ,mensaje,"No","Yes|No","question",1)
   RETURNING respuesta

   --IF fgl_getuitype() <> "JAVA" THEN
      --DISPLAY ""
   --END IF

   RETURN respuesta

END FUNCTION
