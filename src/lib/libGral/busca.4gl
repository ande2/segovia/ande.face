##############################################################################
#  Funcion: %M%
#  Descripcion: funcion para buscar valores especificos en las tablas 
#  Funciones: busca_depto()
#
#  Parametros
#  recibidos:
#  Parametros
#  devueltos:
#
#  SCCS Id No.: %Z% %W%
#  Autor: Erick Valdez
#  Fecha: %H% %T%
#  Path: %P%
#
#  Control de cambios
#
#  Programador       Fecha                Descripcion de la modificacion
#
############################################################################## 
DATABASE face
FUNCTION busca_depto(w_user)
DEFINE
	w_user 	CHAR(20),
	ini_user	CHAR(3),
	w_depto	CHAR(50)

	LET ini_user = w_user[1,3]

	-- Busca el departamento
	SELECT a.des_desc_lg
	INTO w_depto
	FROM sd_des a
	WHERE a.des_tipo = 40
	AND a.des_desc_ct = ini_user

	RETURN w_depto

END FUNCTION

FUNCTION busca_empresa(datab)
DEFINE datab VARCHAR(100), 
       lEmpr RECORD LIKE mempr.*

--WHENEVER ERROR CONTINUE
   SELECT a.*
     INTO lEmpr.*
     FROM face:mempr a
    WHERE a.empr_db = datab
--WHENEVER ERROR STOP
--DISCONNECT CURRENT
--DATABASE datab
RETURN lEmpr.*
END FUNCTION