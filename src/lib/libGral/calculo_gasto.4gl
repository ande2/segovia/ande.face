################################################################################
# Funcion     : %M%
# Descripcion : Funciones de uso general en la generacion de reportes
# Funciones   : calculo_gasto(vlinkfor, vproc_id, vfor_canuni, 
#               vcosto_util,vpro_plu, gsueenpla,gcod_emp)
#               calcula_gtoplanta()
#               selecciona_pesokil(reg_peso)
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : AGarcia
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# AGarcia  Tue Jan 20 15:35:54 CST 2009 Se agrega mantenimiento de productos de
#                                       maquila
# AGarcia  Fri Oct  5 18:30:41 CST 2007 Se corrige insercion de datos cuando
#                                       el vprog = 1
# AGarcia  Mon Jul 23 17:33:04 CST 2007 Se elimina funcion selecciona_pesokil,
#                     funcion temporal_planta
# AGarcia   Tue Jun 12 14:25:21 CST 2007 Se agrega funcion selecciona_pesokil()
# AGarcia   Mon May 28 16:46:54 CST 2007 Se corrige descripcion Hora Hombre por
#                                        Hora maquina
# costo_util = 1 Representa el costo actual del producto (ubicado en la tabla
#                mart, campo: art_cosact)
# costo_util = 2 Representa el costo historico ubicado en tabla 
#                (mcubmatpri campo = cmp_cosunifin)
# costo_util = 3 Representa el costo histórico ubicado en la tabla (mart campo=
#                art_cosderep)                
# vprog          Identifica el reporte que se esta generando.
################################################################################

FUNCTION calculo_gasto(vlinkfor, vproc_id, vfor_canuni, vcosto_util,vpro_plu, vpro_pesokil, gsueenpla,gcod_emp,mcorr,vempaque,vprog,vpro_plu_1)
DEFINE
	vprog       SMALLINT,        -- 1 rep.costos reales y 0  los otros reportes.
	vlinkfor    INTEGER,         -- link de formulacion
	vproc_id    INTEGER,         -- proceso
	vproc_id_t  INTEGER,         -- proceso auxiliar
   vfor_canuni DECIMAL(10,3),   -- cantidad
	vcosto_util SMALLINT,        -- Costo a utilizar(descripcion en encabezado)
	vpro_plu    INTEGER,   -- variales de color y producto
   gsueenpla   SMALLINT,	        --
	vpro_pesokil DECIMAL(12,6),  -- peso en kilos segun empresa
	gcod_emp    INTEGER,         -- codigo de la empresa
	mcorr       SMALLINT,        -- linea correlativa
	vempaque    SMALLINT,        -- si tiene material de empaque sera > 0
   vpro_id, vcol_id INTEGER,
	vpro_plu_1  INTEGER,
--variables para el calculo de la mano de obra
   reg_maquinas RECORD
		linkfor  INTEGER,
		proc_id  INTEGER,
		linkmaq  INTEGER,
		mol_id   SMALLINT,
		for_tiereqhor  DATETIME hour to minute,
		mc_tip	SMALLINT,
		mc_num   INTEGER		
	END RECORD,
	reg_empleados RECORD
		pla_id 	INTEGER,
		emp_id 	SMALLINT,
		est_id   INTEGER,
		ptb_id	INTEGER,
		emp_nom	CHAR(40),
		emp_gru	CHAR(1),
		emp_devext	SMALLINT,
		emp_valsue	DECIMAL(18,2),
		cue_id		INTEGER,
		emp_tipmo	SMALLINT
	END RECORD,
   reg_presta RECORD
		mob_id	INTEGER,
		mob_desc CHAR(30),
		mob_coef DECIMAL(15,6),
		est_id	INTEGER,
		mob_inchorext	SMALLINT,
		mob_calcenuv   CHAR(1),
		mob_utiforesp  SMALLINT,
		mob_codforesp  SMALLINT,
		mob_desforesp  CHAR(255),
		mob_monmaxcal  DECIMAL(18,2)
	END RECORD,

--variables de tiempo
   vhora CHAR(5),
   vminutos SMALLINT,
   vhoras, vtminutos DECIMAL(5,2),
   vhoras_req DECIMAL(10,3),           --horas requeridas en la OP

--variables para depreciaciones
   costo_dep, total_dep DECIMAL(18,6),
   val_diadep SMALLINT,
   cant_dep DECIMAL(18,6),
	desc_maq_dep CHAR(70),

--variables para energia electrica
   consumo_ee DECIMAL(10,6),
   valor_ee, costo_ee, total_ee DECIMAL(18,6),
	tot_costo_ee DECIMAL(18,6),

--variables para combustibles
   consumo_com INTEGER,
   valor_com, costo_com, total_com DECIMAL(18,6),
	vfam_id, vtip_id, vcat_cor INTEGER,
	vcat_desc, vumd_desc CHAR(30),

--variables para mantenimientos
	consumo_maquila DECIMAL(18,6),
   consumo_mant DECIMAL(18,6),
   costo_mant, total_mant DECIMAL(18,6),
	cant_man  DECIMAL(18,6),
	val_depmen DECIMAL(18,6),
	desc_maq_man CHAR(70),

--variables para mano de obra
   cant_manos DECIMAL(18,6),
   costo_manos DECIMAL(18,6),
   costo_directo DECIMAL(18,6),
   total_mano DECIMAL(18,6),
   vproceso SMALLINT, 
	desc_maq_manob CHAR(70),
	desc_maq CHAR(70),
   empleados_directos INTEGER,
	total_mano_indirecta DECIMAL(18,6),
	costo__mano_indirecta DECIMAL(18,6),
   total_mano_directa DECIMAL(18,6),
   costo_mano_indirecta DECIMAL(18,6),
   costo_indirecto DECIMAL(18,6),

			
-- variables para gasto por planta y gastos generales
	vgas_otrpro_pla DECIMAL(18,6),
	vgas_otrpro_tot DECIMAL(18,6),
	factor_planta   DECIMAL(18,6),
	factor_generales  DECIMAL(18,6),
	gastos_planta     DECIMAL(18,6),
	gastos_generales  DECIMAL(18,6),
   vpla_id     INTEGER,
   vcanpro_planta DECIMAL(18,6),
   vcanpro_total  DECIMAl(18,6),
   vmes SMALLINT,
   vmes_ant SMALLINT,
   vanio_ant SMALLINT,
   i, vanio SMALLINT,
	gastos_otrpro_pla DECIMAL(18,6),
	gastos_canpro_planta DECIMAL(18,6),
   gastos_otrpro_tot DECIMAL(18,6),
	gastos_canpro_total DECIMAL(18,6),
   vfor_cantmat, vart_cosact DECIMAL(18,6)

--inicializacion de variables de tiempo
LET i          = 0
LET vhoras     = 0
LET vminutos   = 0
LET vtminutos  = 0
LET vhoras_req = 0

-- Inicializa variables de gastos de fabricacion
LET costo_ee  = 0
LET costo_com = 0
LET total_ee  = 0
LET total_com = 0
LET vgas_otrpro_pla   = 0
LET vgas_otrpro_tot   = 0
LET factor_planta     = 0 
LET factor_generales  = 0 
LET gastos_planta     = 0
LET gastos_generales  = 0
LET vcanpro_planta    = 0
LET vcanpro_total     = 0
LET gastos_otrpro_pla = 0
LET gastos_canpro_planta = 0
LET gastos_otrpro_tot    = 0
LET gastos_canpro_total  = 0
LET vfor_cantmat = 0
LET vart_cosact  = 0
LET tot_costo_ee = 0
LET costo_dep = 0
LET vpla_id   = 0
--selecciona informacion de las maquinas utilizadas segun la formulacion
INITIALIZE reg_maquinas.* TO NULL
INITIALIZE reg_presta.* TO NULL
INITIALIZE reg_empleados.* TO NULL

DECLARE cursor_maq CURSOR FOR
   SELECT dformaq.*
   FROM dformaq
   WHERE dformaq.linkfor = vlinkfor

FOREACH cursor_maq INTO reg_maquinas.*

-- Inicializa variables de las maquinas
   LET consumo_ee = 0
   LET val_depmen = 0
   LET val_diadep = 0
   LET consumo_maquila = 0
   LET consumo_mant = 0
   LET desc_maq  =  NULL
   LET costo_dep  = 0
   LET cant_dep   = 0
   LET total_dep  = 0
   LET costo_mant = 0
	LET cant_man   = 0
	LET total_mant = 0
	LET cant_manos = 0
	LET total_mano = 0
	LET costo_manos = 0
	LET costo_directo = 0
	LET empleados_directos = 0
	LET total_mano_indirecta = 0
	LET costo_mano_indirecta  = 0
	LET total_mano_directa   = 0
	LET total_mano_indirecta = 0
	LET costo_indirecto      = 0

--separa las horas y los minutos del tiempo requerido por maquina
   LET vhora = reg_maquinas.for_tiereqhor
   LET vhoras = vhora[1,2]
   LET vminutos = vhora[4,5]

--traslado los minutos a horas
   IF vminutos <> 0 THEN
      LET vtminutos = vminutos / 60
      LET vhoras = vhoras + vtminutos
   END IF

	LET vhoras_req = vhoras

--selecciona informacion de consumos por maquinas
   SELECT mmaq.maq_conkilhor, mmaq.maq_valdepmen, mmaq.maq_candiadep,
          mmaq.maq_valsosrep, mmaq.maq_nom
   INTO consumo_ee, val_depmen, val_diadep, consumo_mant, desc_maq
   FROM mmaq
   WHERE mmaq.proc_id = reg_maquinas.proc_id
   AND mmaq.linkmaq = reg_maquinas.linkmaq
   AND mmaq.maq_candiadep > 0
   UNION
   SELECT mmaq.maq_conkilhor,mmaq.maq_valdepmen,
          mmaq.maq_candiadep, mmaq.maq_valsosrep, mmaq.maq_nom
   INTO consumo_ee, val_depmen, val_diadep, consumo_mant, desc_maq
   FROM mmaq, drelmaqproc
   WHERE drelmaqproc.proc_id = reg_maquinas.proc_id
   AND drelmaqproc.linkmaq = reg_maquinas.linkmaq
   AND mmaq.linkmaq = drelmaqproc.linkmaq
   AND mmaq.maq_candiadep > 0

	if sqlca.sqlcode <> 0 then
		display "400",sqlca.sqlcode
	end if

-- Selecciona pro_id
	SELECT dpro.pro_id, dpro.col_id
	INTO vpro_id, vcol_id
	FROM dpro
	WHERE dpro.pro_plu = vpro_plu
-- Verifica  si el producto es de maquila
	
	SELECT  mcapprod.cpr_cosunimaqui
	INTO  consumo_maquila
	FROM  mcapprod
	WHERE mcapprod.pro_id = vpro_id
	AND   mcapprod.cpr_cosunimaqui > 0
	IF consumo_maquila IS NULL THEN
		LET consumo_maquila = 0
	END IF
	IF consumo_maquila > 0 THEN
		LET consumo_mant = consumo_maquila
	END IF 

--selecciona el valor de la energia electrica
   SELECT mtipcos.tpcos_cosuni
   INTO valor_ee
   FROM mtipcos
   WHERE mtipcos.tpcos_id = 1
   AND mtipcos.est_id = 1

-- calculo de energia electrica
   LET costo_ee = ((consumo_ee * vhoras_req)/vfor_canuni) 
	LET tot_costo_ee = tot_costo_ee + costo_ee
   LET total_ee = total_ee + (costo_ee * valor_ee)

	IF total_ee   IS NULL THEN 
   	LET total_ee   = 0 
	END IF

-- calculo de depreciaciones 
   LET costo_dep =(val_depmen /(val_diadep * 24))
	LET cant_dep = vhoras_req/vfor_canuni


	LET total_dep = ((costo_dep * vhoras_req)/vfor_canuni)
   LET vproceso = vproc_id + 1  
	
	IF total_dep   IS NULL THEN 
   	LET total_dep  = 0 
	END IF
	LET desc_maq_dep = "DEPRECIACIONES ", desc_maq

-- Se inserta en tabla temporal y se coloca el valor de la familia como 666666 
-- porque no necesito este valor en el reporte y por el orden que requiero en el
-- despliegue del reporte
	IF vprog = 0 THEN
		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","666666" ,"1","", desc_maq_dep ,"HORA MAQUINA",cant_dep, costo_dep, total_dep, 0,0,0, vpro_plu, 0,0, vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)
	ELSE
		IF vprog = 2 THEN
   		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION" ,"","","", "DEPRECIACIONES","",cant_dep, costo_dep, total_dep, 0,0,0,vpro_plu,0,0,vpro_pesokil,mcorr, 0, vpro_id, vcol_id, " ", vpro_plu_1)
		ELSE
   		INSERT INTO temporal_for values(7,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION" ,"","","", "DEPRECIACIONES","",cant_dep, costo_dep, total_dep, 0,0,0,vpro_plu,0,0,vpro_pesokil,mcorr, 0, vpro_id, vcol_id, " ", vpro_plu_1)
		END IF
	END IF 


--Calculo de gastos de mantenimiento
	IF consumo_maquila  >  0 THEN
		LET costo_mant = consumo_mant
	ELSE
   	LET costo_mant = (consumo_mant/(val_diadep * 24))
	END IF 
	LET total_mant = ((costo_mant * vhoras_req)/vfor_canuni)
	LET cant_man  = vhoras_req/vfor_canuni
	
	IF total_mant IS NULL THEN
		LET total_mant= 0	
	END IF 
	IF consumo_maquila > 0 THEN
		LET total_mant = costo_mant
		LET cant_man = 1 
		LET desc_maq_man = "COSTO ", desc_maq
	ELSE
		LET desc_maq_man = "MANTENIMIENTO ", desc_maq
	END IF 

-- Se inserta en tabla temporal y se coloca el valor de la familia como 999999 
-- porque no necesito este valor en el reporte y por el orden que requiero en el
-- despliegue del reporte

	IF vprog = 0 THEN
		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","999999","1","", desc_maq_man, "HORA MAQUINA ",cant_man, costo_mant, total_mant, 0,0,0,vpro_plu,0,0,vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)
	ELSE
		IF vprog = 2 THEN
   		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION" ,"","","", "MANTENIMIENTO", "", cant_man, costo_mant, total_mant, 0,0,0,vpro_plu,0,0,vpro_pesokil, mcorr,0,vpro_id, vcol_id, " ", vpro_plu_1)
		ELSE
   		INSERT INTO temporal_for values(8,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION" ,"","","", "MANTENIMIENTO", "", cant_man, costo_mant, total_mant, 0,0,0,vpro_plu,0,0,vpro_pesokil, mcorr,0,vpro_id, vcol_id, " ", vpro_plu_1)
		END IF 
	END IF 

-- Calcula  mano de obra
		
	IF gcod_emp <> 916 THEN
   	SELECT NVL(SUM((c_ahorro:empleados.sueldo_act /30)/8),0), memp.pla_id 
   	INTO costo_directo, vpla_id
   	FROM mrelempmaq, memp, c_ahorro:empleados, c_ahorro:mccemp
   	WHERE mrelempmaq.linkmaq = reg_maquinas.linkmaq
   	AND memp.emp_id = mrelempmaq.emp_id
   	AND memp.est_id = 13
   	AND c_ahorro:empleados.ibm = memp.emp_id
   	AND c_ahorro:empleados.cod_emp = gcod_emp
		AND c_ahorro:empleados.cod_emp= c_ahorro:mccemp.cod_emp
		AND c_ahorro:empleados.ibm = c_ahorro:mccemp.ibm
		AND c_ahorro:mccemp.cue_det= 43
		GROUP BY 2

		SELECT COUNT( memp.emp_id)
		INTO empleados_directos
		FROM c_ahorro:empleados, memp, c_ahorro:mccemp
		WHERE  c_ahorro:empleados.cod_emp = gcod_emp
		AND   c_ahorro:empleados.ibm = memp.emp_id
		AND   memp.pla_id = vpla_id
		AND   memp.est_id = 13
		AND   c_ahorro:empleados.ibm = c_ahorro:mccemp.ibm
		AND   c_ahorro:empleados.cod_emp = c_ahorro:mccemp.cod_emp
		AND   c_ahorro:mccemp.cue_det = 43
		
		SELECT SUM(c_ahorro:empleados.sueldo_act)
		INTO  total_mano_indirecta
		FROM  c_ahorro:empleados, memp, c_ahorro:mccemp
		WHERE c_ahorro:empleados.cod_emp = gcod_emp
		AND   c_ahorro:empleados.ibm = memp.emp_id
		AND   memp.pla_id = vpla_id
		AND   memp.est_id = 13
		AND   c_ahorro:empleados.ibm = c_ahorro:mccemp.ibm
		AND   c_ahorro:empleados.cod_emp = c_ahorro:mccemp.cod_emp
		AND   c_ahorro:mccemp.cue_det = 41

	ELSE
		
   	SELECT NVL(SUM((memp.emp_valsue/30)/8),0), memp.pla_id 
   	INTO costo_directo, vpla_id
   	FROM mrelempmaq, memp, mcue
   	WHERE mrelempmaq.linkmaq = reg_maquinas.linkmaq
   	AND memp.emp_id = mrelempmaq.emp_id
	   AND mcue.cue_id = memp.cue_id
   	AND memp.est_id = 13
		AND mcue.cue_det= 43
		GROUP BY 2

		SELECT COUNT(memp.emp_id)
		INTO empleados_directos
		FROM memp, mcue
		WHERE memp.pla_id = vpla_id
		AND   mcue.cue_id = memp.cue_id
		AND   memp.est_id = 13
		AND   mcue.cue_det = 43
		
		SELECT SUM(memp.emp_valsue)
		INTO  total_mano_indirecta
		FROM  memp, mcue
		WHERE memp.pla_id = vpla_id
		AND   mcue.cue_id = memp.cue_id
		AND   memp.est_id = 13
		AND   mcue.cue_det = 41

	END IF

   LET  costo_indirecto = (total_mano_indirecta/(empleados_directos*(44*4)))

	IF costo_indirecto IS NULL THEN
		LET costo_indirecto = 0
	END IF 

	IF costo_directo IS NULL THEN
		LET costo_directo = 0
	END IF 
			
	LET  cant_manos  = vhoras_req/vfor_canuni
	LET  total_mano_directa = (costo_directo * cant_manos) 
	LET  total_mano_indirecta = costo_indirecto * cant_manos

	IF total_mano_indirecta IS NULL THEN
		LET total_mano_indirecta = 0
	END IF 

	IF total_mano_directa IS NULL THEN
		LET total_mano_directa = 0
	END IF 

	IF vprog = 1 OR vprog = 2 THEN
  		LET costo_manos = costo_directo + costo_indirecto
  		LET total_mano = total_mano_directa + total_mano_indirecta
	END IF	
	
	IF total_mano IS NULL THEN
		LET total_mano= 0	
	END IF 

	IF costo_manos IS NULL THEN
		LET costo_manos = 0
	END IF 

-- Se inserta en tabla temporal y se coloca el valor de la familia como 888888 
-- porque no necesito este valor en el reporte y por el orden que requiero en el
-- despliegue del reporte
	IF vprog = 0 THEN
		IF costo_directo > 0 THEN
			LET desc_maq_manob = "MANO DE OBRA DIRECTA  ", desc_maq
			INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","888888","","", desc_maq_manob, "HORA MAQUINA",cant_manos,costo_directo , total_mano_directa, 0,0,0,vpro_plu,0,0,vpro_pesokil,mcorr,0,0,0, " ", vpro_plu_1)
		END IF 
		IF costo_indirecto > 0 THEN
			LET desc_maq_manob = "MANO DE OBRA INDIRECTA  ", desc_maq
			INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","888888","","", desc_maq_manob, "HORA MAQUINA",cant_manos,costo_indirecto , total_mano_indirecta, 0,0,0,vpro_plu,0,0,vpro_pesokil,mcorr,0,0,0, " ", vpro_plu_1)
		END IF 

	ELSE
		IF vprog = 2 THEN
   		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION" ,"","","", "MANO DE OBRA", "",cant_manos,costo_manos , total_mano, 0,0,0,vpro_plu, 0,0, vpro_pesokil,mcorr,0,vpro_id , vcol_id," ",vpro_plu_1)
		ELSE
   		INSERT INTO temporal_for values(5,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION" ,"","","", "MANO DE OBRA", "",cant_manos,costo_manos , total_mano, 0,0,0,vpro_plu, 0,0, vpro_pesokil,mcorr,0,vpro_id , vcol_id," ", vpro_plu_1)
		END IF 
	END IF 

END FOREACH

FREE cursor_maq

IF vempaque > 0 THEN
-- Calculo de los gasto generales y por planta
-- Solo se calcularan estos gastos si dentro de la formulación no tiene,
-- familias = 3104 

	SELECT NVL(mlinpro.pla_id, 0)
	INTO vpla_id
	FROM dpro,msub, mlinpro
	WHERE dpro.sub_id = msub.sub_id
	AND msub.lnp_id = mlinpro.lnp_id
	AND dpro.pro_plu =  vpro_plu

	SELECT sum(gastos_fabricacion.gastos_canpro), sum(gastos_otros)
	INTO  gastos_canpro_planta, gastos_otrpro_pla
	FROM gastos_fabricacion
	WHERE gastos_fabricacion.planta = vpla_id
	AND   gastos_fabricacion.correlativo = 1

	LET factor_planta =   gastos_otrpro_pla/ gastos_canpro_planta
	LET gastos_planta =  factor_planta * vpro_pesokil

	SELECT sum(gastos_fabricacion.gastos_canpro), sum(gastos_otros)
	INTO  gastos_canpro_total, gastos_otrpro_tot
	FROM gastos_fabricacion
	WHERE gastos_fabricacion.planta = 0
	AND gastos_fabricacion.correlativo = 2

	LET factor_generales = gastos_otrpro_tot/gastos_canpro_total
	LET gastos_generales = factor_generales * vpro_pesokil
END IF 


LET vproceso = vproc_id + 1

-- Selecciona el gasto del combustible

IF vcosto_util = 1 THEN
	DECLARE cursor_gcom CURSOR FOR
		SELECT dfor.proc_id
		FROM mfor, dfor,mcat
		WHERE mfor.pro_plu = vpro_plu 
		AND mfor.est_id = 1 
		AND mfor.linkfor = dfor.linkfor 
		AND dfor.cat_plu = mcat.cat_plu 
		AND mcat.fam_id IN (3202)
		GROUP BY 1
	FOREACH cursor_gcom INTO vproc_id_t
		DECLARE cursor_util1 CURSOR FOR
			SELECT sum(((dfor.for_cantmat/mfor.for_canuni) * mart.art_cosact)),
      	(dfor.for_cantmat/mfor.for_canuni), mart.art_cosact, mcat.fam_id,
         mcat.tip_id, mcat.cat_cor, mcat.cat_desc, munimed.umd_desc
			FROM mfor, dfor,mcat, munimed, mart 
			WHERE mfor.pro_plu = vpro_plu 
			AND dfor.linkfor = mfor.linkfor 
			AND dfor.proc_id = vproc_id_t
			AND mfor.est_id = 1 
			AND mfor.linkfor = dfor.linkfor 
			AND dfor.cat_plu = mcat.cat_plu 
			AND mart.cat_plu = mcat.cat_plu 
			AND mcat.umd_id = munimed.umd_id 
			AND mcat.fam_id = 3202
			GROUP BY 2,3,4,5,6,7,8
		FOREACH cursor_util1 into  total_com, vfor_cantmat, vart_cosact, vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc

			IF vprog = 0  THEN
				INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc, vfor_cantmat,vart_cosact, total_com, 0,0,0, vpro_plu, 0,0, vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)
			ELSE
				IF vprog = 2 THEN
					INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc, vfor_cantmat,vart_cosact, total_com, 0,0,0, vpro_plu, 0,0, vpro_pesokil,mcorr,0,vpro_id,vcol_id," ", vpro_plu_1)
				ELSE
         		INSERT INTO temporal_for values(4,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor,vcat_desc,"HORA MAQUINA",vfor_cantmat, vart_cosact, total_com, 0,0,0,vpro_plu,0,0, vpro_pesokil, mcorr,0,vpro_id, vcol_id, " ", vpro_plu_1)
				END IF 
			END IF 
		END FOREACH 
		FREE cursor_util1
 	END FOREACH
	FREE cursor_gcom
END IF 

IF vcosto_util = 2 THEN
	DECLARE cursor_gcom1 CURSOR FOR
		SELECT dfor.proc_id
		FROM mfor, dfor,mcat
		WHERE mfor.pro_plu = vpro_plu 
		AND mfor.est_id = 1 
		AND mfor.linkfor = dfor.linkfor 
		AND dfor.cat_plu = mcat.cat_plu 
		AND mcat.fam_id IN (3202)
		GROUP BY 1
	FOREACH cursor_gcom1 INTO vproc_id_t
		DECLARE cursor_util2 CURSOR FOR
			SELECT sum(((dfor.for_cantmat/mfor.for_canuni) * mcubmatpri.cmp_cosunifin)),
       	(dfor.for_cantmat/mfor.for_canuni), mcubmatpri.cmp_cosunifin,
			mcat.fam_id, mcat.tip_id, mcat.cat_cor, mcat.cat_desc, munimed.umd_desc
			FROM mfor, dfor,mcat, munimed, mcubmatpri 
			WHERE mfor.pro_plu = vpro_plu 
			AND dfor.linkfor = mfor.linkfor 
			AND dfor.proc_id = vproc_id_t
			AND mfor.est_id = 1 
			AND mfor.linkfor = dfor.linkfor 
			AND dfor.cat_plu = mcat.cat_plu 
			AND mcubmatpri.cat_plu = mcat.cat_plu 
			AND mcubmatpri.cmp_mes = vmes_ant
			AND mcubmatpri.cmp_anio = vanio_ant
			AND mcat.umd_id = munimed.umd_id 
			AND mcat.fam_id = 3202
			GROUP BY 2,3,4,5,6,7,8
		FOREACH cursor_util2 INTO total_com, vfor_cantmat, vart_cosact, vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc
			IF vprog = 0  THEN
   			INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc, vfor_cantmat,vart_cosact, total_com, 0,0,0,vpro_plu,0,0,vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)
			ELSE
				IF vprog = 2 THEN
   				INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc, vfor_cantmat,vart_cosact, total_com, 0,0,0,vpro_plu,0,0,vpro_pesokil,mcorr,0,vpro_id,vcol_id," ", vpro_plu_1)
	
				ELSE
         		INSERT INTO temporal_for values(4,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor,vcat_desc,"HORA MAQUINA",vfor_cantmat, vart_cosact, total_com, 0,0,0,vpro_plu,0,0, vpro_pesokil, mcorr,0,vpro_id, vcol_id, " ", vpro_plu_1)
				END IF 	
			END IF 
		END FOREACH
		FREE cursor_util2
	END FOREACH
	FREE cursor_gcom1
END IF 


IF vcosto_util = 3 THEN
	DECLARE cursor_gcom2 CURSOR FOR
		SELECT dfor.proc_id
		FROM mfor, dfor,mcat
		WHERE mfor.pro_plu = vpro_plu 
		AND mfor.est_id = 1 
		AND mfor.linkfor = dfor.linkfor 
		AND dfor.cat_plu = mcat.cat_plu 
		AND mcat.fam_id IN (3202)
		GROUP BY 1
	FOREACH cursor_gcom2 INTO vproc_id_t
		DECLARE cursor_util3 CURSOR FOR
			SELECT sum(((dfor.for_cantmat/mfor.for_canuni) * mart.art_cosderep)),
          (dfor.for_cantmat/mfor.for_canuni), mart.art_cosderep, 
			mcat.fam_id, mcat.tip_id, mcat.cat_cor, mcat.cat_desc, munimed.umd_desc
			FROM mfor, dfor,mcat, munimed, mart 
			WHERE mfor.pro_plu = vpro_plu 
			AND dfor.linkfor = mfor.linkfor 
			AND dfor.proc_id = vproc_id_t
			AND mfor.est_id = 1 
			AND mfor.linkfor = dfor.linkfor 
			AND dfor.cat_plu = mcat.cat_plu 
			AND mart.cat_plu = mcat.cat_plu 
			AND mcat.umd_id = munimed.umd_id 
			AND mcat.fam_id = 3202
			GROUP BY 2,3,4,5,6,7,8
		FOREACH cursor_util3 INTO total_com, vfor_cantmat, vart_cosact, vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc
		IF vprog = 0 THEN
   		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc, vfor_cantmat,vart_cosact, total_com, 0,0,0,vpro_plu, 0,0, vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)
		ELSE
			IF vprog = 2 THEN
   			INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor, vcat_desc, vumd_desc, vfor_cantmat,vart_cosact, total_com, 0,0,0,vpro_plu, 0,0, vpro_pesokil,mcorr,0,vpro_id,vcol_id," ", vpro_plu_1)
			ELSE
        		INSERT INTO temporal_for values(4,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION",vfam_id, vtip_id, vcat_cor,vcat_desc,"HORA MAQUINA",vfor_cantmat, vart_cosact, total_com, 0,0,0,vpro_plu,0,0, vpro_pesokil, mcorr,0,vpro_id, vcol_id, " ", vpro_plu_1)
			END IF
		END IF 
		END FOREACH
		FREE cursor_util3
	END FOREACH
	FREE cursor_gcom2
END IF 

IF total_com IS NULL THEN
	LET total_com = 0
END IF
-- si el producto es de maquila no se deben registrar los gastos de fabricacion
-- y gastos de planta ( IF consumo_maquila <= 0 si desplegara los gastos)

IF vprog = 0 THEN
	INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","777777","1","", "ENERGIA ELECTRICA", "KW.",tot_costo_ee,valor_ee , total_ee, 0,0,0,vpro_plu,0,0, vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)


	IF consumo_maquila <= 0 THEN
		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","999999","2","", "GASTOS DE FABRICACION DE PLANTA", " ",vpro_pesokil, factor_planta, gastos_planta, 0,0,0, vpro_plu,0,0, vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)

		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","999999","3","", "GASTOS DE FABRICACION GENERALES", " ",vpro_pesokil, factor_generales,gastos_generales, 0,0,0,vpro_plu,0,0, vpro_pesokil,mcorr,0,0,0," ", vpro_plu_1)
	END IF 

ELSE
	IF vprog = 2 THEN
		UPDATE temporal_for
		SET temporal_for.proc_id = vproceso
		WHERE temporal_for.orden = 3

		INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","","","", "ENERGIA ELECTRICA", "KW.",tot_costo_ee,valor_ee , total_ee, 0,0,0,vpro_plu,0,0, vpro_pesokil,mcorr,0,vpro_id,vcol_id," ", vpro_plu_1)

		IF consumo_maquila <= 0 THEN
			INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","","","", "GASTOS DE FABRICACION DE PLANTA", " ",vpro_pesokil, factor_planta, gastos_planta, 0,0,0, vpro_plu,0,0, vpro_pesokil,mcorr,0,vpro_id,vcol_id," ", vpro_plu_1)

			INSERT INTO temporal_for values(3,vlinkfor,0,vproceso,"GASTOS DE FABRICACION","","","", "GASTOS DE FABRICACION GENERALES", " ",vpro_pesokil, factor_generales,gastos_generales, 0,0,0,vpro_plu,0,0, vpro_pesokil,mcorr,0,vpro_id,vcol_id," ", vpro_plu_1)
		END IF 
	ELSE
   	UPDATE temporal_for
		SET temporal_for.proc_id = vproceso
		WHERE temporal_for.orden > 2

		INSERT INTO temporal_for values(3,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION","","","", "ENERGIA ELECTRICA", "KW.",tot_costo_ee,valor_ee , total_ee, 0,0,0,vpro_plu,0,0, vpro_pesokil,mcorr,0,vpro_id,vcol_id," ", vpro_plu_1)

		IF consumo_maquila <= 0 THEN
			INSERT INTO temporal_for values(9,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION","",""," ", "GASTOS DE FABRICACION DE PLANTA", " ",vpro_pesokil, factor_planta, gastos_planta, 0,0,0, vpro_plu,0,0, vpro_pesokil,mcorr,0,vpro_id, vcol_id," ", vpro_plu_1)
			INSERT INTO temporal_for values(6,vlinkfor,vcosto_util,vproceso,"GASTOS DE FABRICACION","","","", "GASTOS DE FABRICACION GENERALES", " ",vpro_pesokil, factor_generales,gastos_generales, 0,0,0,vpro_plu,0,0, vpro_pesokil,mcorr,0,vpro_id, vcol_id," ", vpro_plu_1)
		END IF 
	END IF

END IF 

END FUNCTION 
