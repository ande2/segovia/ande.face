################################################################################
# Funcion      : %M%
# Descripcion  : Funcion para consulta y diagrama de procesos 
# Funciones    : qry_initd()
#                dgr_proc(docp_id,pro_plu,linkfor,pla_id)
#                diagrama(proc_desc,fila,cuadro,fila_ap,cuadro_ap,reg.*)
#                dsp_detalle(docp_id,pro_plu,fila,cuadro)
#                det_mat(desc_docto,docto,proc_id)
#                det_cn(docp_id,pro_plu)
# Parametros
# Recibidos    :docp_id,pro_plu,linkfor,pla_id
# Paramentros
# Devueltos    :
#
# SCCS id No   : %Z% %W%
# Autor        : Giovanni Yanes
# Fecha        : %H% %T%
# Path         : %P%
# Llamar :CALL dgr_proc(vdocp_id,vpro_plu,vlinkfor,vpla_id)
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

FUNCTION qry_initd()
DEFINE 
   sqlsmnt VARCHAR(1500,1),
   vemp_id SMALLINT

   LET sqlsmnt = 
   "SELECT 'Recibido del proceso ' || tmp_arbol.proc_desc,' ',' ', ",
   "SUM(tmp_dreppro.rpp_canbue),' ' FROM dtmp_arbol,tmp_arbol,tmp_dreppro ",
   "WHERE dtmp_arbol.proc_id = ?",
   " AND   tmp_arbol.proc_id = dtmp_arbol.proc_pred",
   " AND   tmp_dreppro.proc_id = tmp_arbol.proc_id GROUP BY 1,2,3"

   PREPARE ex_sqlsmnt2 FROM sqlsmnt

   DECLARE cur_procant CURSOR FOR ex_sqlsmnt2

   LET sqlsmnt = 
   "SELECT tmp_saldev.tip_id,CASE WHEN tmp_saldev.tip_id = 2 THEN 'Salida ' ",
   "WHEN tmp_saldev.tip_id = 5 THEN 'Devolución' ",
   "WHEN tmp_saldev.tip_id = 6 THEN 'Material recibido por reclasificacion de materiales' ",
   "WHEN tmp_saldev.tip_id = 7 THEN 'Material enviado por reclasificacion de materiales' END tipo, ",
   "tmp_saldev.docm_fec,tmp_saldev.docm_idtip,",
   "CASE WHEN (tmp_saldev.tip_id = 2 OR tmp_saldev.tip_id = 6) THEN SUM(tmp_saldev.sdv_canuni) END salidas,",
   "CASE WHEN (tmp_saldev.tip_id = 5 OR tmp_saldev.tip_id = 7) THEN SUM(tmp_saldev.sdv_canuni) END devoluc ",
   --"CASE WHEN (tmp_saldev.tip_id = 2 OR tmp_saldev.tip_id = 6) THEN SUM(DECODE(mcat.umd_id,1,tmp_saldev.sdv_canuni,(tmp_saldev.sdv_canuni*mart.art_pesokil))) END salidas,",
   --"CASE WHEN (tmp_saldev.tip_id = 5 OR tmp_saldev.tip_id = 7) THEN SUM(DECODE(mcat.umd_id,1,tmp_saldev.sdv_canuni,(tmp_saldev.sdv_canuni*mart.art_pesokil))) END devoluc ",
   "FROM tmp_saldev,mcat,mart ",
   "WHERE tmp_saldev.est_id = 7 ",
   "AND tmp_saldev.proc_id = ?",
   "AND mcat.cat_plu = tmp_saldev.cat_plu ",
   "AND mcat.fam_id IN (3101,3104,9999) ",
   "AND mart.cat_plu = mcat.cat_plu ",
   "GROUP BY 1,2,3,4 ORDER BY 3,4 "

   PREPARE ex_sqlsmnt FROM sqlsmnt

   DECLARE cur_saldev CURSOR FOR ex_sqlsmnt

   LET sqlsmnt = 
   "SELECT 'Rep. producción (Producto bueno) ',tmp_dreppro.rpp_fec, ",
   "tmp_dreppro.rpp_num,' ',SUM(tmp_dreppro.rpp_canbue) ",
   "FROM tmp_dreppro WHERE tmp_dreppro.proc_id = ?",
   " AND tmp_dreppro.rpp_canbue > 0",
   " GROUP BY 1,2,3,4",
   " UNION ALL ",
   "SELECT 'Rep. producción (Producto malo) ',tmp_dreppro.rpp_fec, ",
   "tmp_dreppro.rpp_num,' ',SUM(tmp_dreppro.rpp_canmal) ",
   "FROM tmp_dreppro WHERE tmp_dreppro.proc_id = ?",
   " AND tmp_dreppro.rpp_canmal > 0",
   " GROUP BY 1,2,3,4"

SELECT mdatgen.dg_codemp
INTO vemp_id
FROM mdatgen

   LET sqlsmnt = sqlsmnt clipped,
   " UNION ALL ",
   "SELECT 'Rep. producción (control de calidad) ',tmp_dreppro.rpp_fec, ",
   "tmp_dreppro.rpp_num,' ',SUM(tmp_dreppro.rpp_canconcal) ",
   "FROM tmp_dreppro WHERE tmp_dreppro.proc_id = ?",
   " AND tmp_dreppro.rpp_canconcal > 0",
   " GROUP BY 1,2,3,4",
   " UNION ALL",
   " SELECT 'Rep. producción (desperdicio del proceso) ',tmp_dreppro.rpp_fec, ",
   "tmp_dreppro.rpp_num,' ',SUM(tmp_dreppro.rpp_cfacesproc) ",
   "FROM tmp_dreppro WHERE tmp_dreppro.proc_id = ?",
   " AND tmp_dreppro.rpp_cfacesproc > 0",
   " GROUP BY 1,2,3,4",
   " UNION ALL",
   " SELECT 'Rep. producción (reproceso post. del proceso) ',tmp_dreppro.rpp_fec, ",
   "tmp_dreppro.rpp_num,' ',SUM(tmp_dreppro.rpp_repposproc) ",
   "FROM tmp_dreppro WHERE tmp_dreppro.proc_id = ?",
   " AND tmp_dreppro.rpp_repposproc > 0",
   " GROUP BY 1,2,3,4"

   LET sqlsmnt = sqlsmnt clipped, " ORDER BY 3,2,1"

   DISPLAY "SQLSMNT: ",sqlsmnt CLIPPED
   PREPARE ex_sqlsmnt3 FROM sqlsmnt

   DECLARE cur_reppro CURSOR FOR ex_sqlsmnt3

   LET sqlsmnt = 
   "SELECT mcat.fam_id,mcat.tip_id,mcat.cat_cor,mcat.cat_desc, ",
   "tmp_saldev.sdv_canuni FROM tmp_saldev,mcat WHERE tmp_saldev.docm_idtip = ?",
   " AND   tmp_saldev.proc_id = ?",
   " AND   mcat.cat_plu = tmp_saldev.cat_plu ORDER BY 1,2,3 "

   PREPARE ex_sqlsmnt4 FROM sqlsmnt

   DECLARE cur_detmat CURSOR FOR ex_sqlsmnt4

   LET sqlsmnt = 
   "SELECT mcat.fam_id,mcat.tip_id,mcat.cat_cor,SUM(tmp_mconnetmat.cnm_matplainiu),",
   "SUM(tmp_mconnetmat.cnm_salalmu),SUM(tmp_mconnetmat.cnm_devalmu),",
   "SUM(tmp_mconnetmat.cnm_matplafinu), ",
   "SUM(tmp_mconnetmat.cnm_matplainiu+tmp_mconnetmat.cnm_salalmu-",
   "tmp_mconnetmat.cnm_devalmu-tmp_mconnetmat.cnm_matplafinu)",
   " FROM tmp_mconnetmat,mcat WHERE tmp_mconnetmat.docp_id = ?",
   " AND tmp_mconnetmat.pro_plu = ?",
   " AND tmp_mconnetmat.cnm_mes = ?",
   " AND tmp_mconnetmat.cnm_anio = ?",
   " AND mcat.cat_plu = tmp_mconnetmat.cat_plu",
   " GROUP BY 1,2,3 ORDER BY 1,2,3"

   PREPARE ex_sqlsmnt5 FROM sqlsmnt 

   DECLARE cur_neto1 CURSOR FOR ex_sqlsmnt5

   LET sqlsmnt = 
   "SELECT mcat.fam_id,mcat.tip_id,mcat.cat_cor,0,",
   "SUM(tmp_mconnetmat.cnm_salalmu),SUM(tmp_mconnetmat.cnm_devalmu),",
   "0,SUM(tmp_mconnetmat.cnm_salalmu-tmp_mconnetmat.cnm_devalmu)",
   " FROM tmp_mconnetmat,mcat WHERE tmp_mconnetmat.docp_id = ?",
   " AND tmp_mconnetmat.pro_plu = ?",
   " AND mcat.cat_plu = tmp_mconnetmat.cat_plu",
   " GROUP BY 1,2,3 ORDER BY 1,2,3"

   PREPARE ex_sqlsmnt6 FROM sqlsmnt 

   DECLARE cur_neto2 CURSOR FOR ex_sqlsmnt6

END FUNCTION

FUNCTION dgr_proc(vdocp_id,vpro_plu,vlinkfor,vpla_id)
DEFINE
  ret SMALLINT,
  vproc_id,vdocp_id,vpro_plu,vlinkfor,vpla_id INTEGER,
  vtip_id,vdocp_num,vpro_id,vcol_id INTEGER,
  vpro_desc CHAR(100),
  vcol_desc CHAR(50),
  vpla_desc,vtip_desc CHAR(30),
  vpeso_kil DECIMAL(12,6),
  vniv_peso SMALLINT,
  vest_desc,vumd_desc CHAR(20),
  max_pagina,vpagina SMALLINT


OPEN WINDOW forma1 AT 1,1 WITH FORM "diagrama" ATTRIBUTES(FORM LINE 1)
DISPLAY FORM forma1
CALL fgl_init4js()
CALL drawinit()
CALL drawselect("draw")
--TITULOS
CALL drawtext(945,865,"NOMBRE DEL")  RETURNING ret
CALL drawtext(915,865,"PROCESO") RETURNING ret
CALL drawrectangle(795,730,125,260) RETURNING ret
CALL drawtext(885,865,"RECIBIDO BUENO   ")  RETURNING ret
CALL drawtext(855,865,"PRODUCIDO BUENO  ")  RETURNING ret
CALL drawtext(825,865,"PRODUCIDO MALO   ")  RETURNING ret
CALL drawtext(795,865,"SALDO DEL PROCESO")  RETURNING ret


SELECT mdocpro.tip_id,mdocpro.docp_num,mest.est_desc
INTO vtip_id,vdocp_num,vest_desc
FROM mdocpro,mest
WHERE mdocpro.docp_id = vdocp_id
AND   mest.est_id = mdocpro.est_id

SELECT dpro.pro_id,dpro.col_id,mpro.pro_desc,mcol.col_desc,munimed.umd_desc
INTO vpro_id,vcol_id,vpro_desc,vcol_desc,vumd_desc
FROM dpro,mpro,mcol,munimed
WHERE dpro.pro_plu = vpro_plu
AND   mpro.pro_id = dpro.pro_id
AND   mcol.col_id = dpro.col_id
AND   munimed.umd_id = mpro.umd_id

LET vpro_desc = vpro_desc CLIPPED," ",vcol_desc CLIPPED

CASE vtip_id
   WHEN 6
      LET vtip_desc = "ORDEN DE PRODUCCION"
   WHEN 7
      LET vtip_desc = "ORDEN DE SERVICIO"
   WHEN 8
      LET vtip_desc = "PLAN DE PRODUCCION"
END CASE

LET vpeso_kil = 0
LET vniv_peso = NULL
SELECT mdatgen.dg_nivregpeso
INTO vniv_peso
FROM mdatgen

CASE vniv_peso
   WHEN 1 SELECT mpro.pro_pesokil
          INTO vpeso_kil
          FROM mpro
          WHERE mpro.pro_id = vpro_id

   WHEN 2 SELECT dpro.pro_pesokil
          INTO vpeso_kil
          FROM dpro
          WHERE dpro.pro_plu = vpro_plu

   WHEN 3 SELECT MAX(dordpro.pro_pesokil)
          INTO vpeso_kil
          FROM dordpro
          WHERE dordpro.docp_id = vdocp_id
          AND  dordpro.pro_plu = vpro_plu
END CASE


CALL drawfillcolor("black")
CALL drawline(920,315,000,410) RETURNING ret
CALL drawline(795,315,000,410) RETURNING ret
CALL drawline(795,315,130,000) RETURNING ret
CALL drawline(795,725,130,000) RETURNING ret

DISPLAY "DOCTO.: " AT 1,3 ATTRIBUTE(BLUE)
DISPLAY vtip_desc CLIPPED AT 1,11 ATTRIBUTE(MAGENTA)
DISPLAY "No.: " AT 1,31 ATTRIBUTE(BLUE)
DISPLAY vdocp_num USING "#####" AT 1,36 ATTRIBUTE(MAGENTA)
DISPLAY "ESTADO:" AT 1,42 ATTRIBUTE(BLUE)
DISPLAY vest_desc CLIPPED AT 1,50 ATTRIBUTE(MAGENTA)
DISPLAY "PRODUCTO: " AT 2,3 ATTRIBUTE(BLUE)
DISPLAY vpro_id USING "<<<<<<<","-",vcol_id USING "<<<<<" 
        AT 2,13 ATTRIBUTE(MAGENTA)
DISPLAY vpro_desc[1,30] CLIPPED AT 3,27 ATTRIBUTE(MAGENTA)
DISPLAY vpro_desc[31,60] CLIPPED AT 4,27 ATTRIBUTE(MAGENTA)
DISPLAY vpro_desc[61,90] CLIPPED AT 5,27 ATTRIBUTE(MAGENTA)
DISPLAY "U.MEDIDA:" AT 2,26 ATTRIBUTE(BLUE)
DISPLAY vumd_desc CLIPPED AT 2,36 ATTRIBUTE(MAGENTA)
DISPLAY "PESO:" AT 2,44 ATTRIBUTE(BLUE)
DISPLAY "A PRODUCIR:" AT 3,3 ATTRIBUTE(BLUE)
DISPLAY "ENT.BODEGA:" AT 4,3 ATTRIBUTE(BLUE)
DISPLAY "MATERIALES:" AT 5,3 ATTRIBUTE(BLUE)
DISPLAY vpeso_kil USING "<<<<&.&&&&&&" AT 2,50 ATTRIBUTE(MAGENTA)

LET vpagina = 1

CALL dsp_proc(vdocp_id,vpro_plu,vlinkfor,vtip_id,vpeso_kil)

SELECT max(tmp_arbol.pagina)
INTO max_pagina
FROM tmp_arbol

CALL qry_initd()

MENU ""
 BEFORE MENU
  HIDE OPTION ALL
  CALL fgl_dialog_setkeylabel("CONTROL-P","Salir")
  CALL fgl_dialog_setkeylabel("CONTROL-E","Consumo Neto")
  --CALL fgl_dialog_setkeylabel("CONTROL-U","<<Mas proc")
  --CALL fgl_dialog_setkeylabel("CONTROL-V","Mas proc>>")

  IF max_pagina > vpagina THEN
     CALL fgl_dialog_setkeylabel("CONTROL-U","<<Mas proc")
     CALL fgl_dialog_setkeylabel("CONTROL-V","")
  ELSE
     CALL fgl_dialog_setkeylabel("CONTROL-U","")
     CALL fgl_dialog_setkeylabel("CONTROL-V","")
  END IF
 COMMAND "SALIR "
   EXIT MENU
 COMMAND KEY (control-u)
   IF vpagina = max_pagina THEN
      CALL box_error("No hay mas procesos hacia la izquierda")
   ELSE
      LET vpagina = vpagina + 1
      CALL drawclear()
      CALL fetch_diagrama(vpagina) 
      IF vpagina = max_pagina THEN
         CALL fgl_dialog_setkeylabel("CONTROL-U","")
      ELSE
         CALL fgl_dialog_setkeylabel("CONTROL-U","<<Mas proc")
      END IF
      IF vpagina = 1 THEN
         CALL fgl_dialog_setkeylabel("CONTROL-V","")
      ELSE
         CALL fgl_dialog_setkeylabel("CONTROL-V","Mas proc>>")
      END IF
   END IF

 COMMAND KEY (control-v)
   IF vpagina = 1 THEN
      CALL box_error("No hay mas procesos hacia la derecha")
   ELSE
      LET vpagina = vpagina - 1
      CALL drawclear()
      CALL fetch_diagrama(vpagina) 
      IF vpagina = max_pagina THEN
         CALL fgl_dialog_setkeylabel("CONTROL-U","")
      ELSE
         CALL fgl_dialog_setkeylabel("CONTROL-U","<<Mas proc")
      END IF
      IF vpagina = 1 THEN
         CALL fgl_dialog_setkeylabel("CONTROL-V","")
      ELSE
         CALL fgl_dialog_setkeylabel("CONTROL-V","Mas proc>>")
      END IF
   END IF

 COMMAND KEY (control-e)
  CALL det_cn(vdocp_id,vpro_plu)
 COMMAND KEY (control-p)
  CALL elimina_tablascv()
  EXIT MENU
 COMMAND KEY (f1)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,1,1)
 COMMAND KEY (f2)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,1,2)
 COMMAND KEY (f3)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,1,3)
 COMMAND KEY (f4)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,1,4)
 COMMAND KEY (f5)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,1,5)
 COMMAND KEY (f6)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,2,1)
 COMMAND KEY (f7)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,2,2)
 COMMAND KEY (f8)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,2,3)
 COMMAND KEY (f9)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,2,4)
 COMMAND KEY (f10)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,2,5)
 COMMAND KEY (f11)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,3,1)
 COMMAND KEY (f12)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,3,2)
 COMMAND KEY (f13)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,3,3)
 COMMAND KEY (f14)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,3,4)
 COMMAND KEY (f15)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,3,5)
 COMMAND KEY (f16)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,4,1)
 COMMAND KEY (f17)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,4,2)
 COMMAND KEY (f18)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,4,3)
 COMMAND KEY (f19)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,4,4)
 COMMAND KEY (f20)
  CALL dsp_detalle(vdocp_id,vpro_plu,vpagina,4,5)

END MENU

CLOSE WINDOW forma1

END FUNCTION

FUNCTION diagrama(vproc_desc,vfila,vcuadro,vfila_ap,vcuadro_ap,reg)
DEFINE
vproc_desc CHAR(50),
vfila,vcuadro,vfila_ap,vcuadro_ap,ret SMALLINT,
reg RECORD
   saldo_ini DECIMAL(15,3),
   can_bue   DECIMAL(15,3),
   can_mal   DECIMAL(15,3),
   saldo_fin DECIMAL(15,3)
END RECORD,
vtexto1,vtexto2,vtexto3,vtexto4 CHAR(12),
vproc_tit1,vproc_tit2 CHAR(9)

LET vtexto1 = reg.saldo_ini USING "(#####&.&&&)"
LET vtexto2 = reg.can_bue   USING "(#####&.&&&)"
LET vtexto3 = reg.can_mal   USING "(#####&.&&&)"
LET vtexto4 = reg.saldo_fin USING "(#####&.&&&)"

IF LENGTH(vproc_desc) > 9 THEN
   LET vproc_tit1 = vproc_desc[1,9]
   LET vproc_tit2 = vproc_desc[10,18]
ELSE
   LET vproc_tit1 = NULL
   LET vproc_tit2 = vproc_desc
END IF


##########################################################################
IF vfila = 1 THEN

IF vcuadro = 5 THEN
CALL drawtext(750,080,vproc_tit1)  RETURNING ret
CALL drawtext(720,080,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--lina recta
CALL drawline(660,152,000,53) RETURNING ret
CALL drawline(665,195,-10,15) RETURNING ret
CALL drawline(650,195,15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(600,5,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f5")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(690,080,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(690,070,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f5")
IF reg.can_bue < 0 THEN
   CALL drawtext(660,080,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(660,070,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f5")
IF reg.can_mal < 0 THEN
   CALL drawtext(630,080,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(630,070,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f5")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(600,080,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(600,070,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f5")
END IF
IF vcuadro = 4 THEN
CALL drawtext(750,280,vproc_tit1)  RETURNING ret
CALL drawtext(720,280,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--linea recta
CALL drawline(660,352,000,53) RETURNING ret
CALL drawline(665,395,-10,15) RETURNING ret
CALL drawline(650,395,15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(600,205,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f4")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(690,280,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(690,270,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f4")
IF reg.can_bue < 0 THEN
   CALL drawtext(660,280,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(660,270,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f4")
IF reg.can_mal < 0 THEN
   CALL drawtext(630,280,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(630,270,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f4")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(600,280,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(600,270,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f4")
END IF
IF vcuadro = 3 THEN
CALL drawtext(750,480,vproc_tit1)  RETURNING ret
CALL drawtext(720,480,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--lina recta
CALL drawline(660,552,000,53) RETURNING ret
CALL drawline(665,595,-10,15) RETURNING ret
CALL drawline(650,595,15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(600,405,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f3")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(690,480,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(690,470,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f3")
IF reg.can_bue < 0 THEN
   CALL drawtext(660,480,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(660,470,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f3")
IF reg.can_mal < 0 THEN
   CALL drawtext(630,480,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(630,470,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f3")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(600,480,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(600,470,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f3")
END IF
IF vcuadro = 2 THEN
CALL drawtext(750,680,vproc_tit1)  RETURNING ret
CALL drawtext(720,680,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--lina recta
CALL drawline(660,752,000,53) RETURNING ret
CALL drawline(665,795,-10,15) RETURNING ret
CALL drawline(650,795,15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(600,605,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f2")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(690,680,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(690,670,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f2")
IF reg.can_bue < 0 THEN
   CALL drawtext(660,680,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(660,670,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f2")
IF reg.can_mal < 0 THEN
   CALL drawtext(630,680,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(630,670,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f2")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(600,680,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(600,670,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f2")
END IF
IF vcuadro = 1 THEN
CALL drawtext(750,880,vproc_tit1)  RETURNING ret
CALL drawtext(720,880,vproc_tit2)  RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(600,805,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f1")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(690,880,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(690,870,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f1")
IF reg.can_bue < 0 THEN
   CALL drawtext(660,880,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(660,870,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f1")
IF reg.can_mal < 0 THEN
   CALL drawtext(630,880,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(630,870,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f1")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(600,880,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(600,870,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f1")
END IF
END IF
##########################################################################
IF vfila = 2 THEN
IF vcuadro = 5 THEN
   CALL drawtext(550,080,vproc_tit1)  RETURNING ret
   CALL drawtext(520,080,vproc_tit2)  RETURNING ret
   CALL drawfillcolor("black")

   IF vfila_ap = vfila THEN
      --lina recta
      CALL drawline(460,152,000,53) RETURNING ret
      CALL drawline(465,195,-10,15) RETURNING ret
      CALL drawline(450,195,15,15) RETURNING ret
   ELSE
      --lina diagonal
      CALL drawline(460,152,140,85) RETURNING ret
      CALL drawline(590,215,10,25) RETURNING ret
      CALL drawline(600,235,-15,15) RETURNING ret
   END IF
   CALL drawfillcolor("gray")
   CALL drawrectangle(400,5,125,147) RETURNING ret
   CALL drawbuttonleft(ret,"f10")
   IF reg.saldo_ini < 0 THEN
      CALL drawtext(490,080,vtexto1)  RETURNING ret
   ELSE
      CALL drawtext(490,070,vtexto1)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f10")
   IF reg.can_bue < 0 THEN
      CALL drawtext(460,080,vtexto2)  RETURNING ret
   ELSE
      CALL drawtext(460,070,vtexto2)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f10")
   IF reg.can_mal < 0 THEN
      CALL drawtext(430,080,vtexto3)  RETURNING ret
   ELSE
      CALL drawtext(430,070,vtexto3)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f10")
   IF reg.saldo_fin < 0 THEN
      CALL drawtext(400,080,vtexto4)  RETURNING ret
   ELSE
      CALL drawtext(400,070,vtexto4)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f10")
END IF
IF vcuadro = 4 THEN
   CALL drawtext(550,280,vproc_tit1)  RETURNING ret
   CALL drawtext(520,280,vproc_tit2)  RETURNING ret
   CALL drawfillcolor("black")
   IF vfila_ap = vfila THEN
      --linea recta
      CALL drawline(460,352,000,53) RETURNING ret
      CALL drawline(465,395,-10,15) RETURNING ret
      CALL drawline(450,395,15,15) RETURNING ret
   ELSE
      --linea diagonal
      CALL drawline(460,352,140,85) RETURNING ret
      CALL drawline(590,415,10,25) RETURNING ret
      CALL drawline(600,435,-15,15) RETURNING ret
   END IF
   CALL drawfillcolor("gray")
   CALL drawrectangle(400,205,125,147) RETURNING ret
   CALL drawbuttonleft(ret,"f9")
   IF reg.saldo_ini < 0 THEN
      CALL drawtext(490,280,vtexto1)  RETURNING ret
   ELSE
      CALL drawtext(490,270,vtexto1)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f9")
   IF reg.can_bue < 0 THEN
      CALL drawtext(460,280,vtexto2)  RETURNING ret
   ELSE
      CALL drawtext(460,270,vtexto2)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f9")
   IF reg.can_mal < 0 THEN
      CALL drawtext(430,280,vtexto3)  RETURNING ret
   ELSE
      CALL drawtext(430,270,vtexto3)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f9")
   IF reg.saldo_fin < 0 THEN
      CALL drawtext(400,280,vtexto4)  RETURNING ret
   ELSE
      CALL drawtext(400,270,vtexto4)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f9")
END IF
IF vcuadro = 3 THEN
   CALL drawtext(550,480,vproc_tit1)  RETURNING ret
   CALL drawtext(520,480,vproc_tit2)  RETURNING ret
   CALL drawfillcolor("black")
   IF vfila_ap = vfila THEN
      --lina recta
      CALL drawline(460,552,000,53) RETURNING ret
      CALL drawline(465,595,-10,15) RETURNING ret
      CALL drawline(450,595,15,15) RETURNING ret
   ELSE
      --linea diagonal
      CALL drawline(460,552,140,85) RETURNING ret
      CALL drawline(590,615,10,25) RETURNING ret
      CALL drawline(600,635,-15,15) RETURNING ret
   END IF
   CALL drawfillcolor("gray")
   CALL drawrectangle(400,405,125,147) RETURNING ret
   CALL drawbuttonleft(ret,"f8")
   IF reg.saldo_ini < 0 THEN
      CALL drawtext(490,480,vtexto1)  RETURNING ret
   ELSE
      CALL drawtext(490,470,vtexto1)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f8")
   IF reg.can_bue < 0 THEN
      CALL drawtext(460,480,vtexto2)  RETURNING ret
   ELSE
      CALL drawtext(460,470,vtexto2)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f8")
   IF reg.can_mal < 0 THEN
      CALL drawtext(430,480,vtexto3)  RETURNING ret
   ELSE
      CALL drawtext(430,470,vtexto3)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f8")
   IF reg.saldo_fin < 0 THEN
      CALL drawtext(400,480,vtexto4)  RETURNING ret
   ELSE
      CALL drawtext(400,470,vtexto4)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f8")
END IF
IF vcuadro = 2 THEN
   CALL drawtext(550,680,vproc_tit1)  RETURNING ret
   CALL drawtext(520,680,vproc_tit2)  RETURNING ret
   CALL drawfillcolor("black")
   IF vfila_ap = vfila THEN
      --lina recta
      CALL drawline(460,752,000,53) RETURNING ret
      CALL drawline(465,795,-10,15) RETURNING ret
      CALL drawline(450,795,15,15) RETURNING ret
   ELSE
      --linea diagonal
      CALL drawline(460,752,140,85) RETURNING ret
      CALL drawline(590,815,10,25) RETURNING ret
      CALL drawline(600,835,-15,15) RETURNING ret
      CALL drawfillcolor("gray")
   END IF
   CALL drawrectangle(400,605,125,147) RETURNING ret
   CALL drawbuttonleft(ret,"f7")
   IF reg.saldo_ini < 0 THEN
      CALL drawtext(490,680,vtexto1)  RETURNING ret
   ELSE
      CALL drawtext(490,670,vtexto1)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f7")
   IF reg.can_bue < 0 THEN
      CALL drawtext(460,680,vtexto2)  RETURNING ret
   ELSE
      CALL drawtext(460,670,vtexto2)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f7")
   IF reg.can_mal < 0 THEN
      CALL drawtext(430,680,vtexto3)  RETURNING ret
   ELSE
      CALL drawtext(430,670,vtexto3)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f7")
   IF reg.saldo_fin < 0 THEN
      CALL drawtext(400,680,vtexto4)  RETURNING ret
   ELSE
      CALL drawtext(400,670,vtexto4)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f7")
END IF
IF vcuadro = 1 THEN
   CALL drawtext(550,880,vproc_tit1)  RETURNING ret
   CALL drawtext(520,880,vproc_tit2)  RETURNING ret
   CALL drawfillcolor("gray")
   CALL drawrectangle(400,805,125,147) RETURNING ret
   CALL drawbuttonleft(ret,"f6")
   IF reg.saldo_ini < 0 THEN
      CALL drawtext(490,880,vtexto1)  RETURNING ret
   ELSE
      CALL drawtext(490,870,vtexto1)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f6")
   IF reg.can_bue < 0 THEN
      CALL drawtext(460,880,vtexto2)  RETURNING ret
   ELSE
      CALL drawtext(460,870,vtexto2)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f6")
   IF reg.can_bue < 0 THEN
      CALL drawtext(430,880,vtexto3)  RETURNING ret
   ELSE
      CALL drawtext(430,870,vtexto3)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f6")
   IF reg.saldo_fin < 0 THEN
      CALL drawtext(400,880,vtexto4)  RETURNING ret
   ELSE
      CALL drawtext(400,870,vtexto4)  RETURNING ret
   END IF
   CALL drawbuttonleft(ret,"f6")
END IF
END IF
##########################################################################
IF vfila = 3 THEN
IF vcuadro = 5 THEN
CALL drawtext(350,080,vproc_tit1)  RETURNING ret
CALL drawtext(320,080,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--lina recta
CALL drawline(260,152,000,53) RETURNING ret
CALL drawline(265,195,-10,15) RETURNING ret
CALL drawline(250,195,15,15) RETURNING ret
--lina diagonal
CALL drawline(260,152,140,85) RETURNING ret
CALL drawline(390,215,10,25) RETURNING ret
CALL drawline(400,235,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(260,152,340,85) RETURNING ret
CALL drawline(590,215,10,25) RETURNING ret
CALL drawline(600,235,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(200,5,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f15")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(290,080,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(290,070,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f15")
IF reg.can_bue < 0 THEN
   CALL drawtext(260,080,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(260,070,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f15")
IF reg.can_mal < 0 THEN
   CALL drawtext(230,080,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(230,070,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f15")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(200,080,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(200,070,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f15")
END IF
IF vcuadro = 4 THEN
CALL drawtext(350,280,vproc_tit1)  RETURNING ret
CALL drawtext(320,280,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--linea recta
CALL drawline(260,352,000,53) RETURNING ret
CALL drawline(265,395,-10,15) RETURNING ret
CALL drawline(250,395,15,15) RETURNING ret
--linea diagonal
CALL drawline(260,352,140,85) RETURNING ret
CALL drawline(390,415,10,25) RETURNING ret
CALL drawline(400,435,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(260,352,340,85) RETURNING ret
CALL drawline(590,415,10,25) RETURNING ret
CALL drawline(600,435,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(200,205,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f14")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(290,280,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(290,270,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f14")
IF reg.can_bue < 0 THEN
   CALL drawtext(260,280,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(260,270,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f14")
IF reg.can_mal < 0 THEN
   CALL drawtext(230,280,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(230,270,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f14")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(200,280,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(200,270,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f14")
END IF
IF vcuadro = 3 THEN
CALL drawtext(350,480,vproc_tit1)  RETURNING ret
CALL drawtext(320,480,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
CALL drawline(260,552,000,53) RETURNING ret
CALL drawline(265,595,-10,15) RETURNING ret
CALL drawline(250,595,15,15) RETURNING ret
--linea diagonal
CALL drawline(260,552,140,85) RETURNING ret
CALL drawline(390,615,10,25) RETURNING ret
CALL drawline(400,635,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(260,552,340,85) RETURNING ret
CALL drawline(590,615,10,25) RETURNING ret
CALL drawline(600,635,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(200,405,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f13")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(290,480,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(290,470,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f13")
IF reg.can_bue < 0 THEN
   CALL drawtext(260,480,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(260,470,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f13")
IF reg.can_mal < 0 THEN
   CALL drawtext(230,480,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(230,470,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f13")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(200,480,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(200,470,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f13")
END IF
IF vcuadro = 2 THEN
CALL drawtext(350,680,vproc_tit1)  RETURNING ret
CALL drawtext(320,680,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
CALL drawline(260,752,000,53) RETURNING ret
CALL drawline(265,795,-10,15) RETURNING ret
CALL drawline(250,795,15,15) RETURNING ret
--linea diagonal
CALL drawline(260,752,140,85) RETURNING ret
CALL drawline(390,815,10,25) RETURNING ret
CALL drawline(400,835,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(260,752,340,85) RETURNING ret
CALL drawline(390,815,10,25) RETURNING ret
CALL drawline(400,835,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(200,605,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f12")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(290,680,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(290,670,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f12")
IF reg.can_bue < 0 THEN
   CALL drawtext(260,680,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(260,670,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f12")
IF reg.can_bue < 0 THEN
   CALL drawtext(230,680,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(230,670,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f12")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(200,680,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(200,670,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f12")
END IF
IF vcuadro = 1 THEN
CALL drawtext(350,880,vproc_tit1)  RETURNING ret
CALL drawtext(320,880,vproc_tit2)  RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(200,805,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f11")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(290,880,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(290,870,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f11")
IF reg.can_bue < 0 THEN
   CALL drawtext(260,880,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(260,870,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f11")
IF reg.can_mal < 0 THEN
   CALL drawtext(230,880,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(230,870,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f11")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(200,880,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(200,870,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f11")
END IF
END IF
##########################################################################
IF vfila = 4 THEN
IF vcuadro = 5 THEN
CALL drawtext(150,080,vproc_tit1)  RETURNING ret
CALL drawtext(120,080,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--lina recta
CALL drawline(60,152,000,53) RETURNING ret
CALL drawline(65,195,-10,15) RETURNING ret
CALL drawline(50,195,15,15) RETURNING ret
--lina diagonal
CALL drawline(60,152,140,85) RETURNING ret
CALL drawline(190,215,10,25) RETURNING ret
CALL drawline(200,235,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(60,152,340,85) RETURNING ret
CALL drawline(390,215,10,25) RETURNING ret
CALL drawline(400,235,-15,15) RETURNING ret
--linea diagonal 3
CALL drawline(60,152,540,85) RETURNING ret
CALL drawline(590,215,10,25) RETURNING ret
CALL drawline(600,235,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawtext(055,080,vproc_tit1)  RETURNING ret
CALL drawtext(025,080,vproc_tit2)  RETURNING ret
CALL drawrectangle(005,005,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f20")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(090,080,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(090,070,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f20")
IF reg.can_bue < 0 THEN
   CALL drawtext(060,080,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(060,070,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f20")
IF reg.can_mal < 0 THEN
   CALL drawtext(030,080,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(030,070,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f20")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(000,080,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(000,070,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f20")
END IF
IF vcuadro = 4 THEN
CALL drawtext(150,280,vproc_tit1)  RETURNING ret
CALL drawtext(120,280,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
--linea recta
CALL drawline(60,352,000,53) RETURNING ret
CALL drawline(65,395,-10,15) RETURNING ret
CALL drawline(50,395,15,15) RETURNING ret
--linea diagonal
CALL drawline(60,352,140,85) RETURNING ret
CALL drawline(190,415,10,25) RETURNING ret
CALL drawline(200,435,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(60,352,340,85) RETURNING ret
CALL drawline(390,415,10,25) RETURNING ret
CALL drawline(400,435,-15,15) RETURNING ret
--linea diagonal 3
CALL drawline(60,352,540,85) RETURNING ret
CALL drawline(590,415,10,25) RETURNING ret
CALL drawline(600,435,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(005,205,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f19")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(095,280,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(095,270,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f19")
IF reg.can_bue < 0 THEN
   CALL drawtext(065,280,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(065,270,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f19")
IF reg.can_mal < 0 THEN
   CALL drawtext(035,280,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(035,270,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f19")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(005,280,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(005,270,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f19")
END IF
IF vcuadro = 3 THEN
CALL drawtext(150,480,vproc_tit1)  RETURNING ret
CALL drawtext(120,480,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
CALL drawline(60,552,000,53) RETURNING ret
CALL drawline(65,595,-10,15) RETURNING ret
CALL drawline(50,595,15,15) RETURNING ret
--linea diagonal
CALL drawline(60,552,140,85) RETURNING ret
CALL drawline(190,615,10,25) RETURNING ret
CALL drawline(200,635,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(60,552,340,85) RETURNING ret
CALL drawline(390,615,10,25) RETURNING ret
CALL drawline(400,635,-15,15) RETURNING ret
--linea diagonal 3
CALL drawline(60,552,540,85) RETURNING ret
CALL drawline(590,615,10,25) RETURNING ret
CALL drawline(600,635,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(005,405,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f18")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(095,480,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(095,470,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f18")
IF reg.can_bue < 0 THEN
   CALL drawtext(065,480,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(065,470,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f18")
IF reg.can_mal < 0 THEN
   CALL drawtext(035,480,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(035,470,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f18")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(005,480,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(005,470,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f18")
END IF
IF vcuadro = 2 THEN
CALL drawtext(150,680,vproc_tit1)  RETURNING ret
CALL drawtext(120,680,vproc_tit2)  RETURNING ret
CALL drawfillcolor("black")
CALL drawline(60,752,000,53) RETURNING ret
CALL drawline(65,795,-10,15) RETURNING ret
CALL drawline(50,795,15,15) RETURNING ret
--linea diagonal 1
CALL drawline(60,752,140,85) RETURNING ret
CALL drawline(190,815,10,25) RETURNING ret
CALL drawline(200,835,-15,15) RETURNING ret
--linea diagonal 2
CALL drawline(60,752,340,85) RETURNING ret
CALL drawline(390,815,10,25) RETURNING ret
CALL drawline(400,835,-15,15) RETURNING ret
--linea diagonal 3
CALL drawline(60,752,540,85) RETURNING ret
CALL drawline(590,815,10,25) RETURNING ret
CALL drawline(600,835,-15,15) RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(005,605,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f17")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(095,680,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(095,670,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f17")
IF reg.can_bue < 0 THEN
   CALL drawtext(065,680,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(065,670,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f17")
IF reg.can_mal < 0 THEN
   CALL drawtext(035,680,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(035,670,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f17")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(005,680,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(005,670,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f17")
END IF
IF vcuadro = 1 THEN
CALL drawtext(150,880,vproc_tit1)  RETURNING ret
CALL drawtext(120,880,vproc_tit2)  RETURNING ret
CALL drawfillcolor("gray")
CALL drawrectangle(005,805,125,147) RETURNING ret
CALL drawbuttonleft(ret,"f16")
IF reg.saldo_ini < 0 THEN
   CALL drawtext(095,880,vtexto1)  RETURNING ret
ELSE
   CALL drawtext(095,870,vtexto1)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f16")
IF reg.can_bue < 0 THEN
   CALL drawtext(065,880,vtexto2)  RETURNING ret
ELSE
   CALL drawtext(065,870,vtexto2)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f16")
IF reg.can_mal < 0 THEN
   CALL drawtext(035,880,vtexto3)  RETURNING ret
ELSE
   CALL drawtext(035,870,vtexto3)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f16")
IF reg.saldo_fin < 0 THEN
   CALL drawtext(005,880,vtexto4)  RETURNING ret
ELSE
   CALL drawtext(005,870,vtexto4)  RETURNING ret
END IF
CALL drawbuttonleft(ret,"f16")
END IF
END IF

END FUNCTION

FUNCTION dsp_detalle(vdocp_id,vpro_plu,vpagina,vfila,vcuadro)
DEFINE
   vpagina,vfila,vcuadro,vtipo SMALLINT,
   vdocp_id,vpro_plu,vproc_id INTEGER,
   vproc_desc CHAR(30),
   ga_detp1 ARRAY[120] OF RECORD 
      det_desc CHAR(40),
      det_fecha DATE,
      det_numero INTEGER,
      det_ingreso DECIMAL(15,3),
      det_egreso DECIMAL (15,3)
   END RECORD,
   na_detp1 RECORD 
      det_desc CHAR(40),
      det_fecha DATE,
      det_numero INTEGER,
      det_ingreso DECIMAL(15,3),
      det_egreso DECIMAL (15,3)
   END RECORD,
   cnt SMALLINT,
   vdet_salfin DECIMAL(15,3),
   vdesc_facpro CHAR(15),
   pos_row SMALLINT,
   vemp_id SMALLINT

INITIALIZE na_detp1 TO NULL

--inicializa variables
FOR cnt = 1 TO 120 
   LET ga_detp1[cnt].* = na_detp1.*
END FOR

OPEN WINDOW forma2 AT 7,1 WITH FORM "det_proc" ATTRIBUTES(FORM LINE 1)

SELECT tmp_arbol.proc_id,tmp_arbol.proc_desc,tmp_arbol.det_salfin
INTO vproc_id,vproc_desc,vdet_salfin
FROM tmp_arbol
WHERE tmp_arbol.pagina = vpagina
AND   tmp_arbol.fila = vfila
AND   tmp_arbol.cuadro = vcuadro

DISPLAY BY NAME vproc_id,vproc_desc,vdet_salfin ATTRIBUTE(MAGENTA)

SELECT tmp_dforfacpro.*
FROM tmp_dforfacpro
WHERE tmp_dforfacpro.proc_id = vproc_id
AND   tmp_dforfacpro.for_facpro = 4

IF SQLCA.SQLCODE = NOTFOUND THEN
   LET vdesc_facpro = "UNIDAD"
ELSE
   LET vdesc_facpro = "KILO"
END IF

DISPLAY BY NAME vdesc_facpro ATTRIBUTE(MAGENTA)

LET cnt = 1

OPEN cur_procant USING vproc_id

FOREACH cur_procant INTO ga_detp1[cnt].*
   LET cnt = cnt + 1
END FOREACH

OPEN cur_saldev USING vproc_id

FOREACH cur_saldev INTO vtipo,ga_detp1[cnt].*
   LET cnt = cnt + 1
END FOREACH

SELECT mdatgen.dg_codemp
INTO vemp_id
FROM mdatgen

OPEN cur_reppro USING vproc_id,vproc_id,vproc_id,vproc_id,vproc_id

FOREACH cur_reppro INTO ga_detp1[cnt].*
   LET cnt = cnt + 1
END FOREACH

CALL set_count(cnt-1)

LET pos_row = cnt - 1

DISPLAY ARRAY ga_detp1 TO sa_det1.*
   BEFORE DISPLAY
      LET cnt = arr_curr()
      CALL fgl_dialog_setkeylabel("INTERRUPT","")
      CALL fgl_dialog_setkeylabel("ACCEPT","Salir")

      IF ga_detp1[cnt].det_desc = "Salida" OR
         ga_detp1[cnt].det_desc = "Devolución" THEN
         CALL fgl_dialog_setkeylabel("CONTROL-V","Det.materiales")
      ELSE
         CALL fgl_dialog_setkeylabel("CONTROL-V","")
      END IF

   BEFORE ROW
      LET cnt = arr_curr()
      IF ga_detp1[cnt].det_desc = "Salida" OR
         ga_detp1[cnt].det_desc = "Devolución" THEN
         CALL fgl_dialog_setkeylabel("CONTROL-V","Det.materiales")
      ELSE
         CALL fgl_dialog_setkeylabel("CONTROL-V","")
      END IF

   ON KEY(CONTROL-V)
      LET cnt = arr_curr()
      CALL det_mat(ga_detp1[cnt].det_desc,ga_detp1[cnt].det_numero,vproc_id)
      CALL set_count(pos_row)
      
END DISPLAY

CLOSE WINDOW forma2

END FUNCTION


FUNCTION det_mat(vdesc_docto,vdocto,vproc_id)
DEFINE
   vdocto,vproc_id INTEGER,
   vdesc_docto CHAR(40),
   cnt SMALLINT,
   ga_detm1 ARRAY[20] OF RECORD
      fam_id INTEGER,
      tip_id INTEGER,
      cat_cor INTEGER,
      cat_desc CHAR(50),
      sdv_canuni DECIMAL(15,3)
   END RECORD

OPEN WINDOW forma3 AT 10,4 WITH FORM "det_mat" ATTRIBUTES(FORM LINE 1,BORDER) 

DISPLAY BY NAME vdesc_docto,vdocto

OPEN cur_detmat USING vdocto,vproc_id

LET cnt = 1

FOREACH cur_detmat INTO ga_detm1[cnt].*
   LET cnt = cnt + 1
END FOREACH

CALL set_count(cnt-1)

DISPLAY ARRAY ga_detm1 TO sa_detm1.*
   BEFORE DISPLAY
      CALL fgl_dialog_setkeylabel("INTERRUPT","")
      CALL fgl_dialog_setkeylabel("ACCEPT","Salir")

END DISPLAY

CLOSE WINDOW forma3
END FUNCTION

FUNCTION det_cn(vdocp_id,vpro_plu)
DEFINE
 vdocp_id,vpro_plu INTEGER,
 cnt,vmes,vanio SMALLINT,
 n_mes CHAR(15),
 ga_cn1 ARRAY[50] OF RECORD
    fam_id INTEGER,
    tip_id INTEGER,
    cat_cor INTEGER,
    cn_mpini DECIMAL(15,3),
    cn_sal DECIMAL(15,3),
    cn_dev DECIMAL(15,3),
    cn_mpfin DECIMAL(15,3),
    cn_neto DECIMAL(15,3)
 END RECORD,
 vestado SMALLINT,
 vcat_desc CHAR(50),
 vumd_desc CHAR(40)

OPEN WINDOW forma4 AT 7,4 WITH FORM "det_connet" ATTRIBUTES(FORM LINE 1,BORDER) 
LET vestado = 1

WHILE vestado = 1

   INPUT BY NAME vmes,vanio
   BEFORE INPUT
      CALL fgl_dialog_setkeylabel("INTERRUPT","")
      CALL fgl_dialog_setkeylabel("ACCEPT","Consultar")
      CALL fgl_dialog_setkeylabel("CONTROL-V","Salir")

    ON KEY (CONTROL-V)
      LET vestado = 0
      EXIT INPUT

   AFTER FIELD vmes

     IF vmes IS NULL OR
        (vmes < 0 AND vmes > 12) 	then
        CALL box_error("Debe ingresar un mes valido")
        NEXT FIELD vmes
     END IF

     IF vmes > 0 THEN
        SELECT sd_des.des_desc_lg
        INTO n_mes
        FROM sd_des
        WHERE sd_des.des_tipo = 4
        AND   sd_des.des_cod = vmes
     ELSE
        LET n_mes = "TODOS LOS MESES"
     END IF

     DISPLAY BY NAME n_mes
     IF vmes IS NULL OR
        (vmes < 0 AND vmes > 12) 	then
        CALL box_error("Debe ingresar un mes valido")
        NEXT FIELD vmes
     END IF

     IF vmes > 0 THEN
        SELECT sd_des.des_desc_lg
        INTO n_mes
        FROM sd_des
        WHERE sd_des.des_tipo = 4
        AND   sd_des.des_cod = vmes
     ELSE
        LET n_mes = "TODOS LOS MESES"
     END IF

     DISPLAY BY NAME n_mes

   AFTER INPUT

   END INPUT

IF vestado = 1 THEN
   LET cnt = 1

   IF vmes = 0 AND vanio = 0 THEN
      OPEN cur_neto2 USING vdocp_id,vpro_plu
      FOREACH cur_neto2 INTO ga_cn1[cnt].*
         LET cnt = cnt + 1
      END FOREACH
   ELSE
      OPEN cur_neto1 USING vdocp_id,vpro_plu,vmes,vanio
      FOREACH cur_neto1 INTO ga_cn1[cnt].*
         LET cnt = cnt + 1
      END FOREACH
   END IF

   CALL set_count(cnt-1)
   DISPLAY ARRAY ga_cn1 TO sa_cn1.*
   BEFORE DISPLAY
      CALL fgl_dialog_setkeylabel("INTERRUPT","")
      CALL fgl_dialog_setkeylabel("ACCEPT","Salir")
      CALL fgl_dialog_setkeylabel("CONTROL-V","Otra consulta")


   ON KEY(CONTROL-B)
      LET vestado = 1 
      EXIT DISPLAY

   BEFORE ROW
     LET cnt = arr_curr()
     SELECT mcat.cat_desc,munimed.umd_desc
     INTO vcat_desc,vumd_desc
     FROM mcat,munimed
     WHERE mcat.fam_id = ga_cn1[cnt].fam_id
     AND   mcat.tip_id = ga_cn1[cnt].tip_id
     AND   mcat.cat_cor = ga_cn1[cnt].cat_cor
     AND   munimed.umd_id = mcat.umd_id

     DISPLAY vcat_desc TO cat_desc 
     DISPLAY vumd_desc TO umd_desc

   AFTER DISPLAY
      LET vestado = 0

   END DISPLAY

END IF

END WHILE

CLOSE WINDOW forma4


END FUNCTION


FUNCTION fetch_diagrama(vpagina)
DEFINE
   vpagina SMALLINT,
   ret SMALLINT,
   reg RECORD
      saldo_ini DECIMAL(15,3),
      can_bue   DECIMAL(15,3),
      can_mal   DECIMAL(15,3),
      saldo_fin DECIMAL(15,3)
   END RECORD,
   reg2 RECORD
      proc_id INTEGER,
      proc_desc CHAR(50),
      pagina SMALLINT,
      fila SMALLINT,
      cuadro SMALLINT,
      mapeado SMALLINT,
      pagina_ap SMALLINT,
      fila_ap SMALLINT,
      cuadro_ap SMALLINT ,
      es_primero CHAR(1),
      det_salini DECIMAL(15,3),
      det_canbue DECIMAL(15,3),
      det_canmal DECIMAL(15,3),
      det_salfin DECIMAL(15,3)
   END RECORD

   --TITULOS
   CALL drawtext(945,865,"NOMBRE DEL")  RETURNING ret
   CALL drawtext(915,865,"PROCESO") RETURNING ret
   CALL drawfillcolor("white")
   CALL drawrectangle(795,730,125,260) RETURNING ret
   CALL drawtext(885,865,"RECIBIDO BUENO   ")  RETURNING ret
   CALL drawtext(855,865,"PRODUCIDO BUENO  ")  RETURNING ret
   CALL drawtext(825,865,"PRODUCIDO MALO   ")  RETURNING ret
   CALL drawtext(795,865,"SALDO DEL PROCESO")  RETURNING ret

   CALL drawfillcolor("black")
   CALL drawline(920,315,000,410) RETURNING ret
   CALL drawline(795,315,000,410) RETURNING ret
   CALL drawline(795,315,130,000) RETURNING ret
   CALL drawline(795,725,130,000) RETURNING ret

   IF vpagina > 1 THEN
      --lina recta
      CALL drawline(660,952,000,43) RETURNING ret
      CALL drawline(665,985,-10,15) RETURNING ret
      CALL drawline(650,985,15,15) RETURNING ret
   END IF

   DECLARE cur_fetch CURSOR FOR
   SELECT tmp_arbol.*
   FROM tmp_arbol
   WHERE tmp_arbol.pagina = vpagina

   FOREACH cur_fetch INTO reg2.*
      LET reg.saldo_ini = reg2.det_salini
      LET reg.can_bue = reg2.det_canbue
      LET reg.can_mal = reg2.det_canmal
      LET reg.saldo_fin = reg2.det_salfin

      CALL diagrama(reg2.proc_desc,reg2.fila,reg2.cuadro,reg2.fila_ap,reg2.cuadro_ap,reg.*)
   END FOREACH



END FUNCTION