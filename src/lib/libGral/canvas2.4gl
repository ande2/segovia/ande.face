################################################################################
# Funcion      : %M%
# Descripcion  : Funcion para consulta y diagrama de procesos
# Funciones    : dsp_proc(docp_id,pro_plu,linkfor,tip_id,peso_kil)
#                elimina_tablascv()
# Parametros
# Recibidos    :docp_id,pro_plu,linkfor,tip_id,peso_kil
# Paramentros
# Devueltos    :
#
# SCCS id No   : %Z% %W%
# Autor        : Giovanni Yanes
# Fecha        : %H% %T%
# Path         : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
###############################################################################

FUNCTION dsp_proc(vdocp_id,vpro_plu,vlinkfor,vtip_id,vpeso_kil)
DEFINE
   vcambia_factor SMALLINT,
   saldo_adoc DECIMAL(15,3),
   vtmp_proc,vlinkfor INTEGER,
   vpeso_kil DECIMAL(12,6),
   vtip_id,vpro_plu,vdocp_id,vproc_pred,vproceso,vproc_final INTEGER,
   cnt0,cnt0_old,cuenta_datos,vanio,vmes,control_mapeado,cnt,cnt2,cnt3,cnt_old,cnt2_old SMALLINT,
   reg RECORD
      saldo_ini DECIMAL(15,3),
      can_bue   DECIMAL(15,3),
      can_mal   DECIMAL(15,3),
      saldo_fin DECIMAL(15,3)
   END RECORD,
   reg2 RECORD
      proc_id INTEGER,
      proc_desc CHAR(50),
      pagina SMALLINT,
      fila SMALLINT,
      cuadro SMALLINT,
      mapeado SMALLINT,
      pagina_ap SMALLINT,
      fila_ap SMALLINT,
      cuadro_ap SMALLINT ,
      es_primero CHAR(1),
      det_salini DECIMAL(15,3),
      det_canbue DECIMAL(15,3),
      det_canmal DECIMAL(15,3),
      det_salfin DECIMAL(15,3)
   END RECORD,
   reg3 RECORD
      can_desproc DECIMAL(15,3),
      can_repposproc DECIMAL(15,3)
   END RECORD,
   vproc_desc CHAR(50),
   vcanbue,vcan_salini,vcan_mat,vcan_sol,vcan_bod DECIMAL(15,3),
   sqlstm VARCHAR(1000,1),
   vemp_id SMALLINT
   

INITIALIZE reg.* TO NULL

--crea tablas temporales
CREATE TEMP TABLE tmp_arbol
(
 proc_id INTEGER,
 proc_desc CHAR(50),
 pagina SMALLINT,
 fila SMALLINT,
 cuadro SMALLINT,
 mapeado SMALLINT,
 pagina_ap SMALLINT,
 fila_ap SMALLINT,
 cuadro_ap SMALLINT,
 es_primero CHAR(1),
 det_salini DECIMAL(15,3),
 det_canbue DECIMAL(15,3),
 det_canmal DECIMAL(15,3),
 det_salfin DECIMAL(15,3)
) WITH NO LOG

CREATE TEMP TABLE dtmp_arbol
(
  proc_id INTEGER,
  proc_pred INTEGER
) WITH NO LOG

CREATE TEMP TABLE tmp_saldev
(
 est_id INTEGER,
 tip_id SMALLINT,
 docm_idtip INTEGER,
 docm_fec DATE,
 cat_plu INTEGER,
 proc_id INTEGER,
 sdv_canuni DECIMAL(18,3)
) WITH NO LOG

--Carga tablas temporales
--Salidas y devoluciones de almacen
INSERT INTO tmp_saldev
SELECT mdocm.est_id,mdocm.tip_id,mdocm.docm_idtip,
DATE(mdocm.docm_fecaut) docm_fec,dsaldevproc.cat_plu,dsaldevproc.proc_id,
dsaldevproc.sdv_canuni
FROM mdocm,dsaldevproc
WHERE dsaldevproc.docp_id = vdocp_id
AND   dsaldevproc.pro_plu = vpro_plu
AND   mdocm.docm_id = dsaldevproc.docm_id

--Reclasificacion de consumo de materiales (DESTINO)
INSERT INTO tmp_saldev
SELECT mreasiconmat.est_id,6 tip_id,mreasiconmat.racm_num docm_idtip,
DATE(mreasiconmat.racm_fecdoc) docm_fec,dreasicmori.cat_plu,dreasicmdes.proc_id,
dreasicmdes.racm_cfaces sdv_canuni
FROM mreasiconmat,dreasicmori,dreasicmdes
WHERE mreasiconmat.est_id IN (1,7)
AND dreasicmori.linkracm = mreasiconmat.linkracm
AND dreasicmdes.linkracm = dreasicmori.linkracm
AND dreasicmdes.racm_corr = dreasicmori.racm_corr
AND dreasicmdes.docp_id = vdocp_id
AND dreasicmdes.pro_plu = vpro_plu

--Reclasificacion de consumo de materiales (ORIGEN)
INSERT INTO tmp_saldev
SELECT mreasiconmat.est_id,7 tip_id,mreasiconmat.racm_num docm_idtip,
DATE(mreasiconmat.racm_fecdoc) docm_fec,dreasicmori.cat_plu,dreasicmori.proc_id,
dreasicmori.racm_canori sdv_canuni
FROM mreasiconmat,dreasicmori
WHERE mreasiconmat.est_id IN (1,7)
AND dreasicmori.linkracm = mreasiconmat.linkracm
AND dreasicmori.docp_id = vdocp_id
AND dreasicmori.pro_plu = vpro_plu

SET ISOLATION TO DIRTY READ
SELECT mreppro.rpp_num,mreppro.rpp_fec,dreppro.*
FROM mreppro,dreppro
WHERE mreppro.est_id = 1
AND  dreppro.linkrepp = mreppro.linkrepp
AND  dreppro.docp_id= vdocp_id
AND  dreppro.pro_plu= vpro_plu
INTO TEMP tmp_dreppro WITH NO LOG

SELECT mconnetmat.*
FROM mconnetmat
WHERE mconnetmat.docp_id = vdocp_id
AND   mconnetmat.pro_plu = vpro_plu
INTO TEMP tmp_mconnetmat WITH NO LOG

SELECT adoccospro.*
FROM adoccospro
WHERE adoccospro.docp_id = vdocp_id
AND   adoccospro.pro_plu = vpro_plu
INTO TEMP tmp_adoccospro WITH NO LOG

SELECT dforfacpro.*
FROM dforfacpro
WHERE dforfacpro.linkfor = vlinkfor
INTO TEMP tmp_dforfacpro WITH NO LOG

SELECT dprocpred.*
FROM dprocpred
WHERE dprocpred.linkfor = vlinkfor
INTO TEMP tmp_dprocpred WITH NO LOG

SELECT dprocform.*,mproc.proc_desc
FROM dprocform,mproc
WHERE dprocform.linkfor = vlinkfor
AND   mproc.proc_id = dprocform.proc_id
INTO TEMP tmp_dprocform WITH NO LOG


WHENEVER ERROR CONTINUE
--Determina el ultimo proceso
SELECT tmp_dprocform.proc_id,tmp_dprocform.proc_desc
INTO vproc_final,vproc_desc
FROM tmp_dprocform
WHERE tmp_dprocform.linkfor = vlinkfor
AND tmp_dprocform.proc_id NOT IN (SELECT tmp_dprocpred.for_procpred 
                              FROM tmp_dprocpred
                              WHERE tmp_dprocpred.linkfor = tmp_dprocform.linkfor)
WHENEVER ERROR STOP

IF SQLCA.SQLCODE = -284 THEN
   CALL box_error("Existe mas de un proceso final para la formulacion de este producto")
   RETURN
END IF

INSERT INTO tmp_arbol
VALUES(vproc_final,vproc_desc,1,1,1,0,0,0,0,"N",0,0,0,0)
--proceso,fila,cuadro,ya esta mapeado

LET cnt2 = 1

WHILE TRUE
   DECLARE cur_map CURSOR FOR
   SELECT tmp_arbol.pagina,tmp_arbol.fila,tmp_arbol.cuadro,tmp_arbol.proc_id
   FROM tmp_arbol
   WHERE tmp_arbol.mapeado = 0 

   FOREACH cur_map INTO cnt0,cnt,cnt2,vproc_pred
      LET cnt3 = 0
      LET cnt0_old = cnt0
      LET cnt_old = cnt
      LET cnt2_old = cnt2
      DECLARE cur_proc CURSOR FOR 
      SELECT tmp_dprocpred.for_procpred
      FROM tmp_dprocpred  
      WHERE tmp_dprocpred.linkfor = vlinkfor
      AND   tmp_dprocpred.proc_id = vproc_pred

      FOREACH cur_proc INTO vproceso

        IF vproceso > 0 THEN
           SELECT tmp_dprocform.proc_desc
           INTO vproc_desc
           FROM tmp_dprocform
           WHERE tmp_dprocform.linkfor = vlinkfor
           AND   tmp_dprocform.proc_id = vproceso

           IF cnt3 = 0 THEN
              LET cnt2 = cnt2 + 1
           END IF

           IF cnt2 > 5 THEN
              LET cnt0 = cnt0 + 1
              LET cnt2 = 1
           END IF

           INSERT INTO tmp_arbol
           VALUES(vproceso,vproc_desc,cnt0,cnt,cnt2,0,cnt0_old,cnt_old,cnt2_old,"N",0,0,0,0)
        ELSE
           UPDATE tmp_arbol
           SET es_primero = "S" 
           WHERE tmp_arbol.proc_id = vproc_pred
        END IF

        INSERT INTO dtmp_arbol
        VALUES(vproc_pred,vproceso)
     
        LET cnt = cnt + 1
        LET cnt3 = cnt3 + 1
      END FOREACH

      UPDATE tmp_arbol
      SET tmp_arbol.mapeado = 1 
      where tmp_arbol.proc_id = vproc_pred 

   END FOREACH

   LET control_mapeado = NULL

   SELECT COUNT(*)
   INTO control_mapeado
   FROM tmp_arbol
   WHERE tmp_arbol.mapeado = 0

   IF control_mapeado = 0 OR control_mapeado IS NULL THEN
      EXIT WHILE
   END IF 

END WHILE

DECLARE cur_desp CURSOR FOR
SELECT tmp_arbol.*
FROM tmp_arbol

FOREACH cur_desp INTO reg2.*

LET reg.saldo_ini = 0
LET vcan_salini = 0
LET vcambia_factor = 0

SELECT *
FROM dtmp_arbol
WHERE dtmp_arbol.proc_id = reg2.proc_id
AND   dtmp_arbol.proc_pred = 0

IF SQLCA.SQLCODE = NOTFOUND THEN

SELECT *
FROM tmp_dforfacpro
WHERE tmp_dforfacpro.linkfor = vlinkfor
AND   tmp_dforfacpro.proc_id = reg2.proc_id
AND   tmp_dforfacpro.for_facpro = 4

IF SQLCA.SQLCODE = NOTFOUND THEN
   LET cuenta_datos = 0
   SELECT COUNT(*)
   INTO cuenta_datos 
   FROM dtmp_arbol,tmp_dforfacpro
   WHERE dtmp_arbol.proc_id = reg2.proc_id
   AND  tmp_dforfacpro.proc_id = dtmp_arbol.proc_pred
   AND  tmp_dforfacpro.for_facpro = 4

   IF cuenta_datos > 0 THEN
      LET vcambia_factor = 1
      DECLARE cur_salini CURSOR FOR
      SELECT tmp_dreppro.proc_id,sum(tmp_dreppro.rpp_canbue)
      INTO reg.saldo_ini
      FROM dtmp_arbol,tmp_dreppro
      WHERE dtmp_arbol.proc_id = reg2.proc_id
      AND tmp_dreppro.proc_id = dtmp_arbol.proc_pred
      GROUP BY 1

      FOREACH cur_salini INTO vtmp_proc,vcanbue
         SELECT *
         FROM tmp_dforfacpro
         WHERE tmp_dforfacpro.linkfor = vlinkfor
         AND   tmp_dforfacpro.proc_id = vtmp_proc
         AND  tmp_dforfacpro.for_facpro = 4
       
         IF SQLCA.SQLCODE = NOTFOUND THEN
            LET reg.saldo_ini = reg.saldo_ini + vcanbue
         ELSE
            IF vpeso_kil > 0 THEN
               LET reg.saldo_ini = reg.saldo_ini + (vcanbue/vpeso_kil)
            ELSE
               LET reg.saldo_ini = reg.saldo_ini + vcanbue
            END IF 
         END IF

      END FOREACH

   ELSE
      SELECT SUM(tmp_dreppro.rpp_canbue)  
      INTO reg.saldo_ini
      FROM dtmp_arbol,tmp_dreppro
      WHERE dtmp_arbol.proc_id = reg2.proc_id
      AND tmp_dreppro.proc_id = dtmp_arbol.proc_pred
      IF reg.saldo_ini IS NULL THEN
         LET reg.saldo_ini = 0
      END IF
   END IF

ELSE
   SELECT SUM(tmp_dreppro.rpp_canbue)
   INTO reg.saldo_ini
   FROM dtmp_arbol,tmp_arbol,tmp_dreppro
   WHERE dtmp_arbol.proc_id = reg2.proc_id
   AND tmp_arbol.proc_id = dtmp_arbol.proc_pred
   AND tmp_dreppro.proc_id = tmp_arbol.proc_id

   IF reg.saldo_ini IS NULL THEN
      LET reg.saldo_ini = 0
   END IF

   LET vcan_salini = 0
   LET sqlstm = 
   "SELECT SUM(CASE WHEN mcat.umd_id = 1 THEN ",
   "(tmp_mconnetmat.cnm_salalmu-tmp_mconnetmat.cnm_devalmu) ",
   "ELSE ",
   "((tmp_mconnetmat.cnm_salalmu-tmp_mconnetmat.cnm_devalmu)*DECODE(NVL(mart.art_pesokil,0),0,1,mart.art_pesokil)) ",
   "END) ",
   "FROM tmp_mconnetmat,mcat,mart ",
   "WHERE tmp_mconnetmat.proc_id = ",reg2.proc_id,
   " AND   mcat.cat_plu = tmp_mconnetmat.cat_plu",
   " AND   mcat.fam_id IN (3101,3104,9999)",
   " AND   mart.cat_plu = mcat.cat_plu"

   PREPARE ex_sqlstm FROM sqlstm
   EXECUTE ex_sqlstm INTO vcan_salini

   DISPLAY "1:VCAN_SALINI ",vcan_salini

   IF vcan_salini IS NULL THEN
      LET vcan_salini = 0
   END IF

END IF
ELSE
   LET vcan_salini = 0
   LET sqlstm =
   "SELECT SUM(CASE WHEN mcat.umd_id = 1 THEN",
   " (tmp_mconnetmat.cnm_salalmu-tmp_mconnetmat.cnm_devalmu)",
   " ELSE",
   " ((tmp_mconnetmat.cnm_salalmu-tmp_mconnetmat.cnm_devalmu)*DECODE(NVL(mart.art_pesokil,0),0,1,mart.art_pesokil))",
   " END)",
   " FROM tmp_mconnetmat,mcat,mart",
   " WHERE tmp_mconnetmat.proc_id = ",reg2.proc_id,
   " AND   mcat.cat_plu = tmp_mconnetmat.cat_plu",
   " AND   mcat.fam_id IN (3101,3104,9999)",
   " AND   mart.cat_plu = mcat.cat_plu"

   PREPARE ex_sqlstm2 FROM sqlstm
   EXECUTE ex_sqlstm2 INTO vcan_salini

   IF vcan_salini IS NULL THEN
      LET vcan_salini = 0
   END IF

   SELECT *
   FROM tmp_dforfacpro 
   WHERE tmp_dforfacpro.linkfor = vlinkfor
   AND   tmp_dforfacpro.proc_id = reg2.proc_id
   AND   tmp_dforfacpro.for_facpro = 4

   IF SQLCA.SQLCODE = NOTFOUND THEN
      IF vpeso_kil > 0 THEN
         LET vcan_salini = vcan_salini / vpeso_kil
      END IF
   END IF

END IF

LET reg.saldo_ini = reg.saldo_ini + vcan_salini
SELECT mdatgen.dg_codemp
INTO vemp_id
FROM mdatgen

DISPLAY "VEMP_ID: ",vemp_id

SELECT SUM(tmp_dreppro.rpp_canbue),SUM(tmp_dreppro.rpp_canmal+NVL(tmp_dreppro.rpp_canconcal,0))
INTO reg.can_bue,reg.can_mal
FROM tmp_dreppro
WHERE tmp_dreppro.docp_id = vdocp_id
AND   tmp_dreppro.pro_plu = vpro_plu
AND   tmp_dreppro.proc_id = reg2.proc_id

DISPLAY "VDOCP_ID: ",vdocp_id," VPRO_PLU: ",vpro_plu," PROCESO: ",reg2.proc_id," CAN_MAL: ",reg.can_mal," CAN_BUE: ",reg.can_bue

IF reg.can_bue IS NULL THEN
   LET reg.can_bue = 0
END IF
IF reg.can_mal IS NULL THEN
   LET reg.can_mal = 0
END IF

SELECT mdatgen.dg_codemp
INTO vemp_id
FROM mdatgen

LET reg3.can_desproc = 0
LET reg3.can_repposproc = 0

SELECT *
FROM tmp_dforfacpro
WHERE tmp_dforfacpro.linkfor = vlinkfor
AND   tmp_dforfacpro.proc_id = reg2.proc_id
AND   tmp_dforfacpro.for_facpro = 4 
IF SQLCA.SQLCODE <> NOTFOUND THEN --Si factor de produccion es KG buenos, suma a la produccion mala el desperdicio y merma post. del proceso
	SELECT SUM(tmp_dreppro.rpp_cfacesproc),SUM(tmp_dreppro.rpp_repposproc)
	INTO reg3.can_desproc,reg3.can_repposproc
	FROM tmp_dreppro
	WHERE tmp_dreppro.docp_id = vdocp_id
	AND   tmp_dreppro.pro_plu = vpro_plu		
	AND   tmp_dreppro.proc_id = reg2.proc_id

	IF reg3.can_desproc IS NULL THEN
		LET reg3.can_desproc = 0
	END IF
	IF reg3.can_repposproc IS NULL THEN
		LET reg3.can_repposproc = 0
	END IF

	LET reg.can_mal = reg.can_mal + reg3.can_desproc + reg3.can_repposproc

ELSE --Si factor de produccion no son KG buenos, pero el predecesor si lo es, suma la la prod. mala el desperdicio y merma post. del proceso
	LET cuenta_datos = 0
  	SELECT COUNT(*)
  	INTO cuenta_datos 
  	FROM dtmp_arbol,tmp_dforfacpro
  	WHERE dtmp_arbol.proc_id = reg2.proc_id
  	AND  tmp_dforfacpro.proc_id = dtmp_arbol.proc_pred
  	AND  tmp_dforfacpro.for_facpro = 4

	IF cuenta_datos > 0 THEN
		SELECT SUM(tmp_dreppro.rpp_cfacesproc),SUM(tmp_dreppro.rpp_repposproc)
		INTO reg3.can_desproc,reg3.can_repposproc
		FROM tmp_dreppro
		WHERE tmp_dreppro.docp_id = vdocp_id
		AND   tmp_dreppro.pro_plu = vpro_plu		
		AND   tmp_dreppro.proc_id = reg2.proc_id
		IF reg3.can_desproc IS NULL THEN
			LET reg3.can_desproc = 0
		END IF
		IF reg3.can_repposproc IS NULL THEN
			LET reg3.can_repposproc = 0
		END IF

		IF vpeso_kil > 0 THEN
        	LET reg3.can_desproc = reg3.can_desproc/vpeso_kil
        	LET reg3.can_repposproc = reg3.can_repposproc/vpeso_kil
		END IF

		LET reg.can_mal = reg.can_mal + reg3.can_desproc + reg3.can_repposproc
	END IF
END IF

IF reg2.es_primero = "S" THEN
   SELECT *
   FROM tmp_dforfacpro
   WHERE tmp_dforfacpro.linkfor = vlinkfor
   AND   tmp_dforfacpro.proc_id = reg2.proc_id
   AND   tmp_dforfacpro.for_facpro = 4 
   IF SQLCA.SQLCODE = NOTFOUND THEN
      LET reg.saldo_fin = 0
   ELSE
      LET reg.saldo_fin = reg.saldo_ini - reg.can_bue - reg.can_mal
   END IF
ELSE
   LET reg.saldo_fin = reg.saldo_ini - reg.can_bue - reg.can_mal
END IF

IF vcambia_factor = 1 THEN
   LET vcambia_factor = 0
   SELECT (adoccospro.dcp_salini+adoccospro.dcp_caruni-adoccospro.dcp_abouni)
   INTO saldo_adoc
   FROM adoccospro
   WHERE adoccospro.docp_id = vdocp_id
   AND   adoccospro.pro_plu = vpro_plu
   AND   adoccospro.proc_id = reg2.proc_id
   AND   adoccospro.dcp_mes = MONTH(TODAY)
   AND   adoccospro.dcp_anio = YEAR(TODAY)

   IF reg.saldo_fin >= -0.009 AND reg.saldo_fin <= 0.009 THEN
      IF saldo_adoc <> reg.saldo_fin THEN
         LET reg.saldo_ini = reg.saldo_ini - reg.saldo_fin
         LET reg.saldo_fin = saldo_adoc
      END IF
   END IF
END IF

UPDATE tmp_arbol
SET det_salini = reg.saldo_ini,
    det_canbue = reg.can_bue,
    det_canmal = reg.can_mal,
    det_salfin = reg.saldo_fin
WHERE tmp_arbol.proc_id = reg2.proc_id

IF reg2.pagina = 1 THEN
CALL diagrama(reg2.proc_desc,reg2.fila,reg2.cuadro,reg2.fila_ap,reg2.cuadro_ap,reg.*)
END IF

END FOREACH

LET vcan_bod = 0

SELECT SUM(dentbod.etb_canpro)
INTO vcan_bod
FROM mentbod,dentbod
WHERE mentbod.est_id = 4
AND   dentbod.linkentb = mentbod.linkentb
AND   dentbod.etb_num = vdocp_id
AND   dentbod.pro_plu = vpro_plu

IF vcan_bod IS NULL THEN
   LET vcan_bod = 0
END IF

DISPLAY vcan_bod USING "###,##&.&&&" AT 4,14 ATTRIBUTE(MAGENTA)

LET vanio = NULL
LET vmes = NULL
SELECT MAX(tmp_adoccospro.dcp_anio) 
INTO vanio
FROM tmp_adoccospro
WHERE tmp_adoccospro.docp_id = vdocp_id
AND   tmp_adoccospro.pro_plu = vpro_plu

IF vanio IS NULL THEN
   LET vanio = YEAR(TODAY)
   LET vmes = MONTH(TODAY)
ELSE
   SELECT MAX(tmp_adoccospro.dcp_mes)
   INTO vmes
   FROM tmp_adoccospro 
   WHERE tmp_adoccospro.docp_id = vdocp_id
   AND   tmp_adoccospro.pro_plu = vpro_plu
   AND   tmp_adoccospro.dcp_anio = vanio
END IF

CASE (vtip_id)
   WHEN 6
   SELECT dordpro.op_canpro
   INTO vcan_sol
   FROM dordpro
   WHERE dordpro.docp_id = vdocp_id
   AND   dordpro.pro_plu = vpro_plu
   AND   dordpro.op_anio = vanio
   AND   dordpro.op_mes = vmes

   WHEN 7
   SELECT mordserv.ors_canuni
   INTO vcan_sol
   FROM mordserv
   WHERE mordserv.docp_num = vdocp_id
   AND  mordserv.pro_id = vpro_plu

   WHEN 8
   SELECT dplapro.ppr_canapro
   INTO vcan_sol
   FROM dplapro
   WHERE dplapro.docp_id = vdocp_id
   AND   dplapro.pro_plu = vpro_plu
   AND   dplapro.ppr_anio = vanio
   AND   dplapro.ppr_mes = vmes
END CASE

DISPLAY vcan_sol USING "###,##&.&&&" AT 3,14 ATTRIBUTE(MAGENTA)

LET vcan_mat = 0
LET sqlstm = 
"SELECT SUM(CASE WHEN mcat.umd_id = 1 THEN",
" (tmp_mconnetmat.cnm_salalmu-tmp_mconnetmat.cnm_devalmu)",
" ELSE",
" ((tmp_mconnetmat.cnm_salalmu-tmp_mconnetmat.cnm_devalmu)*DECODE(mart.art_pesokil,0,1,mart.art_pesokil))",
" END)",
" FROM tmp_mconnetmat,mcat,mart",
" WHERE mcat.cat_plu = tmp_mconnetmat.cat_plu",
" AND   mcat.fam_id IN (3101,3104,9999)",
" AND   mart.cat_plu = mcat.cat_plu"

PREPARE ex_sqlstm3 FROM sqlstm
EXECUTE ex_sqlstm3 INTO vcan_mat

IF vcan_mat IS NULL THEN
   LET vcan_mat = 0
END IF

IF vpeso_kil > 0 THEN
   LET vcan_mat = vcan_mat / vpeso_kil
END IF

DISPLAY vcan_mat USING "###,##&.&&&" AT 5,14 ATTRIBUTE(MAGENTA)

END FUNCTION

FUNCTION elimina_tablascv()
--Elimina tablas temporales
DROP TABLE dtmp_arbol
DROP TABLE tmp_arbol
DROP TABLE tmp_dreppro
DROP TABLE tmp_adoccospro
DROP TABLE tmp_dprocpred
DROP TABLE tmp_dprocform
DROP TABLE tmp_dforfacpro
DROP TABLE tmp_saldev
DROP TABLE tmp_mconnetmat

END FUNCTION