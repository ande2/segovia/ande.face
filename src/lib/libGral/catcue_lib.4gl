##############################################################################
#	Modulo      :%M%
#	Descripcion :funcion de cuentas contables
#	Funciones   :catcue_lib  Funcion para armar cuentas contables
#	SCCS Id No. :%Z% %W%
#	Autor       :Giovanni Yanes
#	Fecha       :%H% %T%
#	Path        :%P%
###############################################################################
DATABASE db0000
FUNCTION catcue_lib(vcue_id)
DEFINE
	vcue_id INTEGER,
	vcatcue char(30),
	err_msg CHAR(80),
	lprepare VARCHAR(1000),
	gr_cuenta RECORD 
		cue_id   LIKE mcue.cue_id,
		cue_may  like mcue.cue_may,
		cue_men  LIKE mcue.cue_men,
		cue_det  LIKE mcue.cue_det,
		cue_ref  LIKE mcue.cue_ref
	END RECORD 

   LET lprepare =" SELECT cue_id,LPAD(cue_may,3,'0'), LPAD(cue_men,4,'0'), LPAD(cue_det,3,'0'),",
                 " CASE ",
                 " WHEN cue_ref IS NOT NULL THEN  LPAD(cue_ref,4,'0') ",
   				  " ELSE ' ' ",
   				  " END ",
   				  " FROM mcue ",
   				  " WHERE mcue.est_id=1 AND mcue.cue_id =", vcue_id

	
 	WHENEVER ERROR CONTINUE
	PREPARE pre_cuenta  FROM lPREPARE 
   WHENEVER ERROR STOP
   IF (SQLCA.SQLCODE <> 0) THEN
		ERROR err_msg
		CALL errorlog(err_msg CLIPPED)          
		CALL box_error("!!! Existe un problema en la busqueda de la cuenta contable  -- contacte al programador !!!")
		LET vcatcue = NULL
		INITIALIZE gr_cuenta.* TO NULL	
   END IF
 	WHENEVER ERROR CONTINUE
	EXECUTE pre_cuenta  INTO  gr_cuenta.*
   WHENEVER ERROR STOP
	IF SQLCA.SQLCODE < 0 THEN
		ERROR err_msg
		CALL errorlog(err_msg CLIPPED)          
		CALL box_error("!!! Cuenta no encontrada !!!")
		LET vcatcue = NULL
		INITIALIZE gr_cuenta.* TO NULL	
	END IF
	RETURN gr_cuenta.*

END FUNCTION

