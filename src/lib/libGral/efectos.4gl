#globals "globales.4gl"
 
# NOTAS 
# - esta funcion debe emplearse con archivos que no contengan codigos ascii
# ya que estos haran que la pantalla se desconfigure Victor Espino.

# - esta funcion no puede funcionar dentro de ventanas ya que necesita emplear 
# la ventana completa.


function efectos(nomeje)
 define 
   nomeje char(50),
   nomeje2 char(50),
   usuario char(12),
   inicio, final integer,
   press char(1),

   totregs,
   ubi, 
   contlin1,
   contlin2,
   contcol integer,
   rg record
     orden integer,
     linea char(248)
   end record,
   part char(80),
   fecha char(10),
   coord char(40),
   cef,
   ef,
   t,
   l_masmenos smallint,
   cadenaeje char(100)
  

   let int_flag = 0

#   alter index iarchivox to cluster
#   alter index iarchivox to not cluster

   # programa hecho por victor alfonso espino gomez 
   options prompt line 2,
           error line 2,
           message line 2

   open window mensaje at 10,10 with 2 rows, 60 columns attribute(border)
   message "Espere un momento" attribute(reverse)

   let nomeje2 = nomeje clipped, "Z"

   let cadenaeje = ""
   let cadenaeje = "nl -ba ", nomeje clipped, " > ",  nomeje2 clipped
   #message cadenaeje
   run cadenaeje

   let cadenaeje = ""
   let cadenaeje = "cat ", nomeje2 clipped, " > ", nomeje clipped
   #message cadenaeje
   run cadenaeje

   # sacar el nombre del sistema 
   let usuario = fgl_getenv("LOGNAME") clipped
  
	delete from archivox
	where archivox.usr = usuario

   create temp table archivoy 
   (
    orden serial,
    linea char(248)
   ) with no log 

   load from nomeje delimiter "{"
     insert into archivoy(linea)
   #message nomeje

   # borrar archivos de texto para que puedan ser generados por otro
   # usuario

   let cadenaeje = ""
   let cadenaeje = "cat /dev/null>", nomeje2 clipped
   run cadenaeje
   #message cadenaeje

   let cadenaeje = ""
   let cadenaeje = "cat /dev/null>", nomeje clipped
   run cadenaeje
   #message cadenaeje

   # crear tablas temporales
   create temp table archivousr
   (
    usuario char(12)
   ) with no log
   insert into archivousr values (usuario)

   # trasladar datos a archivo temporal.
   # el begin work, lock table, commit work debe usarse con archivos logfile
#   begin work 
#     call verif()
#     lock table archivoy in exclusive mode
#     lock table archivousr in exclusive mode
#     lock table archivox in exclusive mode

      insert into archivox(usr, orden, linea)
        select archivousr.usuario, archivoy.orden, archivoy.linea[8,248] 
          from archivoy, archivousr
         where archivousr.usuario = usuario

#      unlock table archivox
#   commit work
   drop table archivousr
   drop table archivoy

   select count(*) into totregs from archivox
    where archivox.usr = usuario

   let contlin1 = 1
   let contlin2 = 22
   let contcol  = 1

   let press = "l"

   close window mensaje

   clear screen
   let ef = 0
   let l_masmenos = 0
   while press != "0" and press is not null 

     # victor espino producciones creativas

     let fecha = today using "dd-mm-yyyy"

     let coord="Lineas ", contlin1 using "<<<<<", "+", l_masmenos using "(<&)",
                     "-", contlin2 using "<<<<<", "+", l_masmenos using "(<&)",
                     " ", "Bloque ", contcol using "#", " ",
                     "+", ef using "(<&)" 

     display "                                                                                " at 1, 1 attribute(reverse)

     display "Usuario:" at 1,1
       attribute(reverse)
     display usuario at 1, 10
       attribute(reverse)
     display coord at 1, 30
       attribute(reverse)
     display fecha at 1,71
       attribute(reverse)

     # hecho por victor alfonso espino gomez
     # Mon Oct 14 16:47:44 CST 1996

     let ubi = 3

     if press != "1" and press != "3" and press != "4" and press != "6" then
        declare p_contl cursor for 
         select orden, linea from archivox
          where orden between (contlin1 + l_masmenos) and (contlin2 + l_masmenos)
            and usr = usuario
          order by orden
     end if

     foreach p_contl into rg.*
        if rg.linea[2,100] =     "                                                                                                   " then 
           let rg.linea[1,248] = "                                                                                                   "
        end if

        case (contcol)

          when 1 
            let part = rg.linea[1 + ef , 80 + ef ] 
            display part at ubi, 1

          when 2
            let part = rg.linea[81 + ef, 160 + ef]
            display part at ubi, 1

          when 3    
            let part = rg.linea[161 + ef, 240 + ef] 
            display part at ubi, 1
    
          when  4 
            error "Ya no puede ir mas a la derecha" 
              attribute(reverse)
            sleep 1
            let contcol = 3
            
            let part = rg.linea[161,240] 
            display part at ubi, 1

          when 0 
            error "Ya no puede ir mas a la izquierda" 
              attribute(reverse)
            sleep 1
            let contcol = 1

            let part = rg.linea[1,80] 
            display part at ubi, 1

        end case
           
        let ubi = ubi + 1

     end foreach
   
     for t = ubi to 24
       display "                                                                                " at t, 1
     end for

     let press = ""
     while press is null 
       prompt "Abajo=2b,+l Arriba=8,-l Izquierda=4b,1c Derecha=6b,3c Inicio=7 Fin=9 Salir=0 ->" attribute(reverse) for char press attribute(reverse)
     end while
     if int_flag then
        let press = "0"
     end if
     
     display "                                                                                " at 2, 1 attribute(reverse)
     if press = "2" then
        if contlin2 < totregs then  
           let contlin1 = contlin1 + 22
           let contlin2 = contlin2 + 22
           let l_masmenos = 0
        else
           error "Ya no puede ir mas adelante." 
             attribute(reverse)
           sleep 1 
        end if 
    end if
    if press = "+" then
       if (l_masmenos < 22) and ((contlin2 + l_masmenos) != totregs) then
          let l_masmenos = l_masmenos + 1 
       end if
    end if 

    if press = "8" then 
       if contlin1 > 1 then 
          let contlin1 = contlin1 - 22 
          let contlin2 = contlin2 - 22
          let l_masmenos = 0
       else 
          error "Ya no puede ir mas atras." 
            attribute(reverse)
          sleep 1
       end if
    end if
    if press = "-" then
       if (l_masmenos > -22) and ((contlin1 + l_masmenos) != 1) then
          let l_masmenos = l_masmenos - 1
       end if
    end if 
    if press = "4" then
       let contcol = contcol - 1
       let ef = 0 
    end if

    if press = "6" then
       let contcol = contcol + 1
       let ef = 0
    end if

    if press = "7" then
       let contlin1 = 1  
       let contlin2 = 22
    end if
   
    if press = "9" then
       if (totregs/22) <> round(totregs/22) then
          let contlin1 = (totregs / 22)
          let contlin1 = (contlin1 * 22) + 1
       else
          let contlin1 = (totregs / 22) 
          let contlin1 = ( (contlin1-1) * 22) + 1
       end if

       let contlin2 = (contlin1 + 21)
    end if

    if press = "1" and (ef > -80) then
       if contcol = 1 and ef = 0 then
          let cef = 0
       else
          let cef = 1
       end if
       let ef = ef - cef
    end if 

    if press = "3" and (ef < 80) then 
       if contcol = 3 and ef = 0 then
          let cef = 0
       else
          let cef = 1
       end if
       let ef = ef + cef
    end if

  end while
  message "Espere porfavor ......" attribute(reverse)

  # begin work, commit work, lock table, 
  # usese si en caso la base de datos tiene logfile
#  begin work
#  call verif()
#  lock table archivox in exclusive mode

    delete from archivox
      where archivox.usr = usuario
        and archivox.orden > 0

    update statistics for table archivox 

#    unlock table archivox
#  commit work

   clear screen
end function

FUNCTION round(valor)
  DEFINE valor       DECIMAL(11,3),
         valor2      DECIMAL(11,2),
         entero      INTEGER,
         uno_f       DECIMAL(11,3)

        LET entero = valor                       
        LET uno_f = (valor - entero) * 100         
        LET entero = uno_f                       
        LET uno_f = (uno_f - entero) * 10        
        LET entero = uno_f                       
        LET uno_f = uno_f / 1000                 
        LET valor2 = valor - uno_f                    
 
#Mon May 13 11:52:46 guatemala 1996 se cambio de 5 a 6
        IF entero >= 5                           
           THEN LET valor2 = valor2 + 0.01             
        END IF                                   
        RETURN valor2
END FUNCTION

function ubicar(f)
  define  
    f     char(13),
    ira   char(50),
    hogar char(50)
    
    let hogar = fgl_getenv("HOME")
    let ira   = hogar clipped, "/", f clipped

    return ira
end function

# esta funcion sirve para agregar los archivos con sufijo B al archivo base.
# para que todos los archivos generados por los programas queden dentro de uno
# solo
# Fri Oct 18 10:15:30 CST 1996

function unir(des)
  define 
    des   char(50),
    des2   char(50),
    long  integer,
    long2 integer,
    ejec  char(100)
   
    if des is null then
       error "archivo enviado incorrecto"
       sleep 1
       return
    end if
    let long = length(des) 
    let long2= (long - 1)

    let des2 = des[1,long2]
    
    let ejec = ""
    let ejec = "cat ", des clipped , " >> ", des2 clipped
    run ejec

    let ejec = ""
#    let ejec = "cat /dev/null>", des clipped
    let ejec = "rm ", des clipped
    run ejec

end function

function agrega_r(ar)

 define 
   ar char(13)

   let ar = ar clipped, "R"
   return ar

end function

# esta funcion sirve para condensar el tamano de la letra de la impresora
# la cual se sugiere que sea puesta al inicio de cada reporte para 
# evitar el enviar codigos ascii a los reportes
function condensar()
  define
    dbimprime  char(50),
    ejecuta    char(100)

  let dbimprime = fgl_getenv("DBPRINT")
  let ejecuta = dbimprime clipped,  " condensado"
  run ejecuta
end function


function verif()
  # verificar si estan usando el archivo x en otra computadora.

#  WHENEVER ERROR CONTINUE
  WHILE TRUE
    LOCK TABLE archivox IN SHARE MODE
    IF status = 0 THEN
       EXIT WHILE
    ELSE 
       MESSAGE "Se esta cargado la consulta a pantalla en otra terminal espere porfavor - ", status
    END IF
  END WHILE
  #WHENEVER ERROR STOP
end function
