{*********************** IDENTIFICATION DIVISION *******************************

 Empresa							: AREA DE EMPAQUES EMBALAJES Y COMPLEMENTOS
 Programa							: /u2/dbcomun/errores.4gl
 Descripcion					: Controla errores de diferente indole.
 Formas Utilizadas		: No usa
 Fecha de Elaboracion	: Noviembre, 1,998
 Elaborado por 				: H.C.M.
 Version							: 1:00
 Caracteristica				: Funcion

*******************************************************************************}

### RUTINA PARA ATRAPAR EL ERROR ###
GLOBALS  "glob_hcm.4gl"
  

FUNCTION errores()
	DEFINE
		enter		char(1),
		no_err	INTEGER

	LET no_err = status 

	options
		prompt line last,
		message line last


	case 
		when (no_err = -289 or no_err = -215 or no_err = -263 or 
					no_err = -242 or no_err = -244)
			call emite_l2(15,"la tabla esta siendo usada por otro usuario")
			prompt "Desea seguir esperando S/N " attribute(bold) for char enter
	
			if enter = "S" or enter = "s" then
				set lock mode to wait
				message "Momento Transaccion en espera" attribute(bold)
			else
				if bfacera then
					rollback work
				end if

				CALL mensaje01(no_err)

			end if

		otherwise
			if bfacera then
				rollback work
			end if
	
			CALL mensaje01(no_err)
	end case

	options
		prompt line first,
		message line first

END FUNCTION