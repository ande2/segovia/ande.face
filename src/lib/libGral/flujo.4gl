-- funcion para calcular el numero de semana del year.

FUNCTION num_semana_anio(vfecha)
DEFINE vfecha DATE,
       vanio_fecha SMALLINT,
       vfecha_lunes1 DATE,
       vdias_trans SMALLINT,
       vnum_semana DECIMAL(12,7),
       vnum_semana2 DECIMAL(12,7),
       vchar_num CHAR(13),
       vsemana SMALLINT,
       vdia_semana SMALLINT,
       i SMALLINT

   -- anio de la fecha de pago

   LET vanio_fecha = YEAR(vfecha)

   -- fecha del primer lunes del anio

   FOR i=1 TO 7

      LET vdia_semana = WEEKDAY(mdy(1,i,vanio_fecha))

      IF vdia_semana = 1 THEN
         LET vfecha_lunes1 = mdy(1,i,vanio_fecha)

         IF vfecha < vfecha_lunes1 THEN
            LET vfecha_lunes1 = mdy(1,i,vanio_fecha-1)
         END IF

         EXIT FOR
      END IF

   END FOR

   -- numero de dias transcurridos entre fechas

   LET vdias_trans = (vfecha - vfecha_lunes1)

   -- numero de dias por semana

   LET vnum_semana = (vdias_trans / 7)

   -- ajuste de dias para tomar en cuenta el dia actual

   LET vnum_semana2 = vnum_semana + 1

   -- ingresa el valor en un char para seleccionar las posiciones necesarias

   LET vchar_num = vnum_semana2

   -- selecciona unicamente el valor entero

   LET vsemana = vchar_num[1,5]

   RETURN vsemana

END FUNCTION


