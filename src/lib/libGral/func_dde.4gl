################################################################################
# Funcion     : %M%
# Descripcion : Funcion para establecer coneccion con EXCEL 
# Funciones   : macroe(libro,doc_type)
#               send_dde(libro,cells,vals)
#               connect(prog,libro)
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z%
# Autor       : 4js 
# Fecha       : %H%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# JMonterroso Tue Jan 30 13:48:04 GMT 2007 Se agrego un sleep de 15 segundos
#                                 cuando se trate de abrir un Excel 2000
# idener	 	  Thu Nov 30 10:00:00 GMT 2006 Se agrego funcion para mandar a 
#                                 Escribir a Word y dar formato en cada linea 
#                                 Ingresada, Como Negrita,Subrayado, etc...
# JMonterroso Wed Sep 28 19:58:06 GMT 2005 Se estandarizo para el envio de
#                                 variables segun la version del office
################################################################################

--Funcion que ejecuta una macro en EXCEL 
GLOBALS
	DEFINE verhe SMALLINT
END GLOBALS

FUNCTION macroe(libro,doc_type)
DEFINE 
	libro	CHAR(50),
	doc_type CHAR(30),
	cmd CHAR(100)

IF verhe = 1 THEN
	LET cmd = "run(\\\"PERSONAL.XLS!",doc_type CLIPPED,"\\\")" CLIPPED
	IF DDEExecute("EXCEL",libro,cmd) THEN 
	ELSE
		CALL box_valdato("No se pudo ejecutar la MACRO consulte a INFORMATICA")
	END IF
END IF

IF verhe = 2 THEN
	LET cmd = "run(\\\"PERSONAL.XLSB!",doc_type CLIPPED,"\\\")" CLIPPED
	IF DDEExecute("EXCEL",libro,cmd) THEN 
	ELSE
		CALL box_valdato("No se pudo ejecutar la MACRO consulte a INFORMATICA")
	END IF
END IF

END FUNCTION

--Funcion que envia parametros de despliegue a EXCEL 

FUNCTION send_dde(libro,cells,vals)
DEFINE 
	cells CHAR(100), -- rango de celdas
	vals CHAR(10000),  -- valor que se quiere escribir en una celda especifica
	libro CHAR(50)   -- nombre del libro que se esta usando

IF DDEpoke("EXCEL", libro, cells, vals) THEN
	RETURN true 
ELSE
	CALL box_valdato("No se envio informaci�n al archivo de EXCEL")
	RETURN false
END IF

END FUNCTION

--Funcion para realizar la coneccion a DDE 

FUNCTION connect(prog,libro)
DEFINE 
	libro CHAR(50),        -- libro que se esta usando
	prog CHAR(50),         -- nombre del programa que se usa Word, Excel, etc.
	runit CHAR(80),        -- path donde se encuentras los exe de Office
	ret SMALLINT,          -- bfacera que recibe el resultado de las funciones
	err_msg CHAR(100),     -- captura error
	vsigf, vsigc CHAR(1),  -- indica la sigla de la columna segun el idioma
   bfacera SMALLINT

LET ret = 0
LET vsigf = "L"
LET vsigc = "C"
LET bfacera = 0
LET verhe = 1

-- Intenta realizar la conexion con Excel

IF DDEConnect(prog,libro) then
	RETURN true, vsigf, vsigc
ELSE

-- Intenta abrir Excel XP Ingles/Win XP en Espanol

	LET runit = "C:\\\\Archivos de Programa\\\\Microsoft Office\\\\Office10\\\\",prog CLIPPED,".EXE" 
   LET ret = winexec( runit ) 
	LET vsigf = "L"
	LET vsigc = "C"

	IF DDEConnect(prog,libro) then
		LET bfacera = 1
	ELSE
		LET bfacera = 0
	END IF

	IF bfacera = 0 THEN

-- Intenta abrir Excel 2000 Ingles/Win XP en Ingles

	 	LET runit = "C:\\\\Program Files\\\\Microsoft Office\\\\Office\\\\",prog CLIPPED,".EXE" 
     	LET ret = winexec( runit ) 
		LET vsigf = "L"
		LET vsigc = "C"

		IF DDEConnect(prog,libro) then
			LET bfacera = 1
		ELSE
			LET bfacera = 0
		END IF

		IF bfacera = 0 THEN

-- Intenta abrir Excel 2000 Espanol/Win XP en Ingles

        	LET runit = "C:\\\\Archivos de Programa\\\\Microsoft Office\\\\Office\\\\",prog CLIPPED,".EXE" 
        	LET ret = winexec( runit ) 
			LET vsigf = "L"
			LET vsigc = "C"

			SLEEP 5
			IF DDEConnect(prog,libro) then
				SLEEP 15
				LET bfacera = 1
				RETURN TRUE, vsigf, vsigc
			ELSE
				LET bfacera = 0
			END IF

			IF bfacera = 0 THEN

-- Intenta abrir Excel 2003 Ingles/Win XP en Espanol

				LET runit = "C:\\\\Archivos de Programa\\\\Microsoft Office\\\\Office11\\\\",prog CLIPPED,".EXE" 
				LET ret = winexec( runit ) 
				LET vsigf = "F"
				LET vsigc = "C"

				IF DDEConnect(prog,libro) then
					LET bfacera = 1
				ELSE
					LET bfacera = 0
				END IF

######
				IF bfacera = 0 THEN

-- Intenta abrir Excel 2000 Espanol/Win XP en Ingles

				 	LET runit = "C:\\\\Program Files\\\\Microsoft Office\\\\Office11\\\\",prog CLIPPED,".EXE" 
    			 	LET ret = winexec( runit ) 
					LET vsigf = "F"
					LET vsigc = "C"

					IF DDEConnect(prog,libro) then
						LET bfacera = 1
					ELSE
						LET bfacera = 0
					END IF

					IF bfacera = 0 THEN
					-- Intenta abrir Excel 2003 Solo para maquina de Ada Lucy
				 		LET runit = "C:\\\\Office11\\\\",prog CLIPPED,".EXE" 
    			 		LET ret = winexec( runit ) 
						LET vsigf = "F"
						LET vsigc = "C"

						IF DDEConnect(prog,libro) then
							LET bfacera = 1
						ELSE
							LET bfacera = 0
						END IF
--  office 2007
			   		IF bfacera = 0 THEN
							LET runit = "C:\\\\Archivos de Programa\\\\Microsoft Office\\\\Office12\\\\",prog CLIPPED,".EXE" 
							LET ret = winexec( runit ) 
							LET vsigf = "F"
							LET vsigc = "C"
							LET verhe = 2

							IF DDEConnect(prog,libro) then
								LET bfacera = 1
							ELSE
								LET bfacera = 0
							END IF

			   			IF bfacera = 0 THEN
								LET runit = "C:\\\\Program Files\\\\Microsoft Office\\\\Office12\\\\",prog CLIPPED,".EXE" 
								LET ret = winexec( runit ) 
								LET vsigf = "F"
								LET vsigc = "C"
								LET verhe = 2

								IF DDEConnect(prog,libro) then
									LET bfacera = 1
								ELSE
									LET bfacera = 0
								END IF
							END IF

						END IF 
					END IF 
--
				END IF
#######
			END IF 
  	   END IF
  	END IF
END IF
   
SLEEP 2  -- Da tiempo para que EXCEL se termine de abrir, puede variar

-- Intenta realizar una segunda conexion con Excel

IF DDEConnect(prog,libro) then
	SLEEP 3
	RETURN TRUE, vsigf, vsigc
ELSE
	CALL DDEGeterror() RETURNING err_msg
	DISPLAY "ERR ", err_msg CLIPPED
	CALL box_valdato ("No se realiz� la conecci�n, consulte a INFORMATICA")
	RETURN FALSE, vsigf, vsigc
END IF

END FUNCTION


#########################################################################
## Function  : Funcion para Escribir y dar formato en Word
##
## Parameters: prog,sheetname, cmd, txt, nl
##
## Returnings: 
##
## Comments  : Funcion Que escribe en Word
#########################################################################

FUNCTION word_exe(prog,sheetname,cmd, txt, nl)
  DEFINE sheetname CHAR(20)
  DEFINE prog CHAR(10)
  DEFINE cmd CHAR(100),
      txt CHAR(512),
      str CHAR(512),
      nl  SMALLINT

  LET str = cmd CLIPPED
  IF txt IS NOT NULL OR nl > 0 THEN
    LET str = str CLIPPED, " \"", txt CLIPPED
    WHILE nl > 0
      LET str = str CLIPPED, ASCII 13
      LET nl = nl - 1
    END WHILE
    LET str = str CLIPPED, "\""
  END IF

  --CALL fgl_strtosend(str) RETURNING str
  IF NOT DDEexecute(prog, sheetname, str) THEN
    MESSAGE "DDEexecute: " , str
  END IF

END FUNCTION