#########################################################################
## Function  : capacidad_empaque_producto
##
## Parameters: vpro_id, vtep_id, vcantidad
##
## Returnings: vdescribe_empaque
##
## Comments  : Devuelve la descripcion del empaque para el producto
#########################################################################
DATABASE db0000

FUNCTION capacidad_empaque_producto(vpro_id,vtep_id,vcantidad)
DEFINE vpro_id INTEGER,
       vtep_id INTEGER,
       vcantidad DECIMAL(18,3),
       vcep_esempobl LIKE mcapemppro.cep_esempobl,
       vcep_canuni   LIKE mcapemppro.cep_canuni,
       vtep_desc     LIKE mtipemppro.tep_desc,
       vcantidad_empaque DECIMAL(18,3),
       vdescribe_empaque VARCHAR(100),
       vcantidad_empaque_txt VARCHAR(30),
       vcantidad_empaque_str VARCHAR(30),
       vcantidad_empaque_str_decimal VARCHAR(30),
       tam_str, i,j SMALLINT

   LET vdescribe_empaque = NULL
   LET vtep_desc = NULL

   -- verifica si el producto utiliza empaque obligatorio
   LET vcep_esempobl = 0
   LET vcep_canuni = 0

   SELECT mcapemppro.cep_esempobl, mcapemppro.cep_canuni
     INTO vcep_esempobl, vcep_canuni
     FROM mcapemppro
    WHERE mcapemppro.tep_id = vtep_id
      AND mcapemppro.pro_id = vpro_id

   IF vcep_esempobl IS NULL THEN
      LET vcep_esempobl = 0
   END IF

   -- Es empaque obligatorio: vcep_esempobl = 1

   IF vcep_esempobl = 1 THEN

      IF vcep_canuni > 0 THEN

         -- Selecciona la descripcion del tipo de empaque

         SELECT mtipemppro.tep_desc
           INTO vtep_desc
           FROM mtipemppro
          WHERE mtipemppro.tep_id = vtep_id

         -- Calcula la capacidad segun el tipo de empaque

         LET vcantidad_empaque = vcantidad / vcep_canuni

         -- Define la descripcion para adicionarla a la descripcion del producto

         LET vcantidad_empaque_str = vcantidad_empaque

			LET tam_str = LENGTH(vcantidad_empaque)

         FOR i = 1 TO tam_str
            IF vcantidad_empaque_str[i] = "." THEN
               LET j = i
            END IF
         END FOR

         LET vcantidad_empaque_str_decimal = vcantidad_empaque_str[j+1,tam_str]

         CASE
            WHEN vcantidad_empaque_str_decimal[3] > 0
               LET vdescribe_empaque = "(",vcantidad_empaque USING "<<<<<<&.<<<"," ",vtep_desc CLIPPED,")"
            WHEN vcantidad_empaque_str_decimal[2] > 0
               LET vdescribe_empaque = "(",vcantidad_empaque USING "<<<<<<&.<<"," ",vtep_desc CLIPPED,")"
            WHEN vcantidad_empaque_str_decimal[1] > 0
               LET vdescribe_empaque = "(",vcantidad_empaque USING "<<<<<<&.<"," ",vtep_desc CLIPPED,")"
            OTHERWISE
               LET vdescribe_empaque = "(",vcantidad_empaque USING "<<<<<<&"," ",vtep_desc CLIPPED,")"
         END CASE

      END IF

   END IF

   RETURN vdescribe_empaque

END FUNCTION

FUNCTION capacidad_empaque_producto_2(vpro_id,vtep_id,vcantidad)
DEFINE vpro_id INTEGER,
       vtep_id INTEGER,
       vcantidad DECIMAL(18,3),
       vcep_esempobl LIKE mcapemppro.cep_esempobl,
       vcep_canuni   LIKE mcapemppro.cep_canuni,
       vtep_desc     LIKE mtipemppro.tep_desc,
       vcantidad_empaque DECIMAL(18,3),
       vdescribe_empaque VARCHAR(100),
       vcantidad_empaque_txt VARCHAR(30),
       vcantidad_empaque_str VARCHAR(30),
       vcantidad_empaque_str_decimal VARCHAR(30),
       tam_str, i,j SMALLINT,
       vdescripcion_completa VARCHAR(100)

   LET vdescribe_empaque = NULL
   LET vdescripcion_completa = NULL
   LET vtep_desc = NULL

   -- verifica si el producto utiliza empaque obligatorio
   LET vcep_esempobl = 0
   LET vcep_canuni = 0

   SELECT mcapemppro.cep_esempobl, mcapemppro.cep_canuni
     INTO vcep_esempobl, vcep_canuni
     FROM mcapemppro
    WHERE mcapemppro.tep_id = vtep_id
      AND mcapemppro.pro_id = vpro_id

   IF vcep_esempobl IS NULL THEN
      LET vcep_esempobl = 0
   END IF

   -- Es empaque obligatorio: vcep_esempobl = 1

   IF vcep_esempobl = 1 THEN

      IF vcep_canuni > 0 THEN

         -- Selecciona la descripcion del tipo de empaque

         SELECT mtipemppro.tep_desc
           INTO vtep_desc
           FROM mtipemppro
          WHERE mtipemppro.tep_id = vtep_id

         -- Calcula la capacidad segun el tipo de empaque

         LET vcantidad_empaque = vcantidad / vcep_canuni

         -- Define la descripcion para adicionarla a la descripcion del producto

         LET vcantidad_empaque_str = vcantidad_empaque

			LET tam_str = LENGTH(vcantidad_empaque)

         FOR i = 1 TO tam_str
            IF vcantidad_empaque_str[i] = "." THEN
               LET j = i
            END IF
         END FOR

         LET vcantidad_empaque_str_decimal = vcantidad_empaque_str[j+1,tam_str]

         CASE
            WHEN vcantidad_empaque_str_decimal[3] > 0
               LET vdescribe_empaque = vcantidad_empaque USING "<<<<<<&.<<<"," ",vtep_desc CLIPPED
            WHEN vcantidad_empaque_str_decimal[2] > 0
               LET vdescribe_empaque = vcantidad_empaque USING "<<<<<<&.<<"," ",vtep_desc CLIPPED
            WHEN vcantidad_empaque_str_decimal[1] > 0
               LET vdescribe_empaque = vcantidad_empaque USING "<<<<<<&.<"," ",vtep_desc CLIPPED
            OTHERWISE
               LET vdescribe_empaque = vcantidad_empaque USING "<<<<<<&"," ",vtep_desc CLIPPED
         END CASE

         LET vdescripcion_completa = "EN: ",vdescribe_empaque CLIPPED," DE ",vcep_canuni USING "<<<<<<&.<<<"," C/U"

      END IF

   END IF

   RETURN vdescripcion_completa

END FUNCTION
