#########################################################################
## Function  : capacidad_empaque_producto
##
## Parameters: vpro_id, vtep_id, vcantidad
##
## Returnings: vdescribe_empaque
##
## Comments  : Devuelve la descripcion del empaque para el producto
#########################################################################
DATABASE db0000

FUNCTION especificaciones_x_producto( vpro_id, vcol_id, vclp_id)
DEFINE 
      --vlinkfac LIKE mfac.linkfac,
       vpro_id  LIKE dpro.pro_id,
       vcol_id  LIKE dpro.col_id,
       vclp_id  SMALLINT,
       vpro_plu INTEGER,
       vped_esp1 LIKE dped.ped_esp1,
       vped_esp2 LIKE dped.ped_esp2,
       vped_num LIKE mped.ped_num

   LET vped_num = NULL

   SELECT mfac.ped_num
     INTO vped_num
     FROM mfac
    WHERE mfac.linkfac = vlinkfac

   SELECT dpro.pro_plu 
     INTO vpro_plu
     FROM dpro
    WHERE dpro.pro_id = vpro_id
      AND dpro.col_id = vcol_id
 
   LET vped_esp1 = NULL
   LET vped_esp2 = NULL

   SELECT dped.ped_esp1, dped.ped_esp2
     INTO vped_esp1, vped_esp2
     FROM dped
    WHERE dped.ped_num = vped_num
      AND dped.pro_plu = vpro_plu
      AND dped.clp_id = vclp_id

   RETURN vped_esp1, vped_esp2

END FUNCTION

