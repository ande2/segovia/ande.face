DATABASE face

	-- mes, Numero de correlativo del mes, del cual se requiere el nombre
	-- tip_desc, Tipos de descripcion que se requiere, ("L"arga, "M"ediana, "C"orta)
	-- Retona el nombre del mes segun el formato solicitado, NULL si ocurrio un error
FUNCTION nombre_mes(num_mes, tip_des)
	DEFINE 
		num_mes 		SMALLINT,
		tip_des		CHAR(1),
		nom_mes		CHAR(10)

	CASE tip_des 
		WHEN "L"
			SELECT des_desc_lg INTO nom_mes 
			FROM sd_des
			WHERE sd_des.des_tipo = 4
			AND sd_des.des_cod = num_mes
		WHEN "M"
			SELECT des_desc_md INTO nom_mes 
			FROM sd_des
			WHERE sd_des.des_tipo = 4
			AND sd_des.des_cod = num_mes
		WHEN "C"
			SELECT des_desc_ct INTO nom_mes 
			FROM sd_des
			WHERE sd_des.des_tipo = 4
			AND sd_des.des_cod = num_mes
	END CASE

	IF SQLCA.SQLCODE = 0 THEN
		RETURN nom_mes
	ELSE
		RETURN NULL
	END IF
END FUNCTION
		

		
	-- dia, Numero de correlativo del dia en la semana, del cual se requiere el nombre 
	-- El dia numero 1 es el dia Lunes. 
	-- tip_desc, Tipos de descripcion que se requiere, ("L"arga, "M"ediana, "C"orta)
	-- Retona el nombre del dia segun el formato solicitado, NULL si ocurrio un error
FUNCTION nombre_dia(num_dia, tip_des)
	DEFINE 
		num_dia 		SMALLINT,
		tip_des		CHAR(1),
		nom_dia		CHAR(10)

	CASE tip_des 
		WHEN "L"
			SELECT des_desc_lg INTO nom_dia 
			FROM sd_des
			WHERE sd_des.des_tipo = 2
			AND sd_des.des_cod = num_dia
		WHEN "M"
			SELECT des_desc_md INTO nom_dia 
			FROM sd_des
			WHERE sd_des.des_tipo = 2
			AND sd_des.des_cod = num_dia
		WHEN "C"
			SELECT des_desc_ct INTO nom_dia 
			FROM sd_des
			WHERE sd_des.des_tipo = 2
			AND sd_des.des_cod = num_dia
	END CASE

DISPLAY SQLCA.SQLCODE

	IF SQLCA.SQLCODE = 0 THEN
		RETURN nom_dia
	ELSE
		RETURN NULL
	END IF
END FUNCTION
		

