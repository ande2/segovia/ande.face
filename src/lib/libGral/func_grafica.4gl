function grafica(g_key,tipo)
define tipo              char(1)
define g_key             integer
define f1,f2,f3,f4,f5,f6 integer
define l1,l2,l3,l4,l5,l6 char(20)

   call drawinit()
   call drawselect("canv")
   let f1 = 87
   let f2 = 27
   let f3 = 79
   let f4 = 11
   let f5 = 34
   let f6 = 56
   let l1 = "Ventas"
   let l2 = "Contab"
   let l3 = "Admin"
   let l4 = "Direccion"
   let l5 = "Dise�o"
   let l6 = "Soporte"
   call cls()
   let g_key = g_key * 200
   call grafica_barra(g_key,25,25,950,950,f1,l1,f2,l2,f3,l3,f4,l4,f5,l5,f6,l6)

end function

function cls()
   define c, s om.DomNode
   define win ui.Window
define ret                  integer

   #--Obtiene la posicion padre de la grafica en la forma

   let win = ui.Window.getCurrent()

   let c = win.findnode("Canvas","canv")

   if c is not null then
      let s=c.getFirstChild()
      while s is not null
         call c.removeChild(s)
         let s=c.getFirstChild()
      end while
   end if

   call drawfillcolor("black")
   call drawrectangle (1,1,1000,1000) RETURNING ret

end function
function grafica_barra(g_key,y,x,w,h,f1,l1,f2,l2,f3,l3,f4,l4,f5,l5,f6,l6)
define y,x,w,h                     integer
define f1,f2,f3,f4,f5,f6           float
define lb,l1,l2,l3,l4,l5,l6        char(20)
define cnt,offset,gap,barras,g_key,keyoff,keyh integer
define af,af1,af2,af3,af4,af5,af6  float
define dcoor                       char(100)
define ret                         integer
define mx                          float


   let offset = 15
   let gap = 20
   let barras = 6
   --let g_key = 200
   let keyh = 900
   let keyoff = (1000-offset) - g_key

   call drawfillcolor("white")

   call drawrectangle(y,x,h,w) returning ret

   --call drawfillcolor("blue")
   --call drawlinewidth(2)
   --call drawline(y,x,0,w) returning ret
   --call drawline(y,x,h,0) returning ret
   --call drawline(y+h,x,0,w) returning ret
   --call drawline(y,x+w,h,0) returning ret


   call drawlinewidth(1)


   let y = offset + 10
   let x = offset + 10
   let w = ((1000-((offset*2)+g_key+gap)) / barras ) - gap
   let h = 1000 - ( offset * 6 )

   let mx = f1
   if f2 > mx then
      let mx = f2
   end if
 if f3 > mx then
      let mx = f3
   end if
   if f4 > mx then
      let mx = f4
   end if
   if f5 > mx then
      let mx = f5
   end if
   if f6 > mx then
      let mx = f6
   end if

   display "y:",y using "##&", " x:",x using "##&", " ky:",g_key using "##&"
   display "h:",h using "##&", " w:",w using "##&", " mx:",mx using "###&"

   let af1 = ( h / mx ) * f1
   let af2 = ( h / mx ) * f2
   let af3 = ( h / mx ) * f3
   let af4 = ( h / mx ) * f4
   let af5 = ( h / mx ) * f5
   let af6 = ( h / mx ) * f6

   call drawanchor("left")
for cnt = 1 to barras
      case cnt
         when 1
            let af = af1
            let lb = l1
            call drawfillcolor("red")
         when 2
            let af = af2
            let lb = l2
            call drawfillcolor("blue")
         when 3
            let af = af3
            let lb = l3
            call drawfillcolor("green")
         when 4
            let af = af4
            let lb = l4
            call drawfillcolor("cyan")
         when 5
            let af = af5
            let lb = l5
            call drawfillcolor("magenta")

         when 6
            let af = af6
            let lb = l6
            call drawfillcolor("yellow")
      end case

      if af > 0 then
         call drawrectangle(y,x,y+af,w) returning ret
         if g_key > 0 then
            call drawtext(keyh,keyoff,lb) returning ret
            call drawrectangle(keyh-25,keyoff,25,g_key-gap) returning ret
            let keyh = keyh - 100
         end if
         let x = x + w + gap
      end if
   end for

end function

