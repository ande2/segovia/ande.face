################################################################################
# Funcion     : Tooltip 
# Descripcion : Especial para manejo de Tooltip de campos de forma
# Funciones   : 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : RPalencia
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################
--Llena temporal con los textos de los tooltips de los campos que estan en la forma actual
FUNCTION fill_tmptooltip(fieldtooltip)
	DEFINE
   valor_tok  base.StringTokenizer,
	val1 INTEGER,
	val2 VARCHAR(40),
	fieldtooltip STRING

	create TEMP TABLE tmp_tooltip
   (link INTEGER,
    campo VARCHAR(40)) WITH NO LOG

	--LLENADO DE TABLE tmp para buscar los tooltip de campos, deben uncluir la tabla o FORMONLY para que los encuentre
   let valor_tok = base.StringTokenizer.create(fieldtooltip, "|")
   while valor_tok.hasMoreTokens()
      let val1 = valor_tok.nextToken()
      let val2 = valor_tok.nextToken()

      INSERT INTO  tmp_tooltip VALUES(val1, val2)
   end while
END FUNCTION


--revisa todos los campos para asignarles el tooltip
FUNCTION checkCommentRecursive(node, stat)
	DEFINE node,child om.DomNode,
		stat SMALLINT

	LET child= node.getFirstChild()
	WHILE child IS NOT NULL
		CALL checkCommentRecursive(child,stat)
		CALL setComment(child,stat)
		LET child=child.getNext()
	END WHILE
END FUNCTION

--chequea el nodo para el tooltip
FUNCTION setComment(node,stat)
	DEFINE 
		stat SMALLINT,
		node,parent om.DomNode,
		cambia SMALLINT,
		padre, hijo, vcampo,tipo VARCHAR(30),
		lr_tooltip RECORD 
			link INTEGER,
			campo VARCHAR(40)
		END RECORD,
		tooltip VARCHAR(200)

	LET parent=node.getParent()
	LET padre = parent.getTagName()
	LET hijo  = node.getTagName()
	LET vcampo = parent.getAttribute('name')

	-- solo setea el "comment" para estas clases de campo en esta FUNCTION
	IF (padre <> "FormField" AND padre <> "TableColumn") AND hijo <> "Button" then 
		RETURN
	END IF

	--y solo estos tipos de campo
	IF hijo = "Label" OR
	   hijo = "ComboBox" OR
	   hijo = "TextEdit" OR
	   hijo = "RadioGroup" OR
	   hijo = "CheckBox" OR
	   hijo = "DateEdit" OR
	   hijo = "Button" OR
	   hijo = "Edit" THEN

		IF hijo = 'Button' THEN
			LET vcampo = node.getAttribute('name')   --los botones NO tienen padre!
			LET padre = hijo
		END IF

		IF stat THEN
			IF node.getAttribute("comment") IS NULL  THEN
				SELECT * INTO lr_tooltip.*
				FROM tmp_tooltip
				WHERE campo = vcampo

				IF STATUS <> NOTFOUND THEN
					--LET tooltip = lib_gethelp(2,lr_tooltip.link)
					let tooltip = '' 
					CALL node.setAttribute("comment",tooltip)
				END IF
			END IF
		ELSE
			CALL node.setAttribute("comment","")
		END IF
	end if
END FUNCTION

