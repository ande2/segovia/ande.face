################################################################################
# Funcion     : %M%
# Descripcion : Funciones para modulo WMS 
# Funciones   : get_cantar(tipart, vtep_id, id_art) --retorna la cantidad por tarima de un articulo MP/PT 
#               get_semaforo(vdias)--valida los dias para determinar el  icono para mostrar el (semaforo) 
#                                    segun la vigencia del lote
#					 chk_dias(vdias) --valida si pasa o NO la salida, segun los dias vencidos del lote MP o PT
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z%
# Autor       : 4js 
# Fecha       : %H%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

DATABASE db0000

--Retorna la cantidad que cabe en la tarima de un articulo MP o PT
FUNCTION get_cantar(tipart, vtep_id, id_art)
	DEFINE 
		tipart SMALLINT,
		vtep_id SMALLINT,
		id_art INTEGER,
		retorna DECIMAL(18,3),
		r_mtipemppro RECORD LIKE mtipemppro.*,
		r_mcapempart RECORD LIKE mcapempart.*,
		r_mcapemppro RECORD LIKE mcapemppro.*

	LET retorna = 0 

	SELECT * INTO r_mtipemppro.*
	FROM mtipemppro
	WHERE tep_id = vtep_id

   IF SQLCA.SQLCODE <> NOTFOUND THEN
		CASE tipart
			WHEN 1  --MP
				SELECT * INTO r_mcapempart.*
				FROM mcapempart
				WHERE tep_id = r_mtipemppro.tep_id
				AND   cat_plu = id_art

   			IF SQLCA.SQLCODE <> NOTFOUND THEN
					LET retorna = r_mcapempart.cea_canuni
				ELSE
					LET retorna = -2
				END IF

			WHEN 2  --PT
				SELECT * INTO r_mcapemppro.*
				FROM mcapemppro
				WHERE tep_id = r_mtipemppro.tep_id
				AND   pro_id  = id_art

   			IF SQLCA.SQLCODE <> NOTFOUND THEN
					LET retorna = r_mcapemppro.cep_canuni
				ELSE
					LET retorna = -3
				END IF
		END CASE
	ELSE
		LET retorna = -1
	END IF

	RETURN retorna
END FUNCTION

--valida los dias para determinar el  icono para mostrar el (semaforo) segun la vigencia del lote
FUNCTION get_semaforo(vdias)
	DEFINE 
	vdias INTEGER,
	vicono VARCHAR(100),
   f_nomlog STRING

   LET f_nomlog =FGL_GETENV("PICSDIR")

	 IF vdias <= 0 THEN
        LET vicono = f_nomlog CLIPPED, "vencido2.bmp"
	 ELSE IF  vdias < 15 THEN
        		LET vicono = f_nomlog CLIPPED, "porvencer2.bmp"
	 		ELSE IF vdias < 30 THEN
        				LET vicono = f_nomlog CLIPPED, "disponible2.bmp"
				  ELSE
				  		IF vdias IS NULL THEN
        					LET vicono = f_nomlog CLIPPED, "porvencer2.bmp"
						ELSE
        					LET vicono = f_nomlog CLIPPED, "disponible3.bmp"
				  		END IF
		  		  END IF
		  END if
	 END IF

	 RETURN vicono

END FUNCTION

--valida si pasa o NO la salida, segun los dias vencidos del lote MP o PT
FUNCTION chk_dias(vpasa, vdias, vcli_id, vaid)
	DEFINE 
		vpasa SMALLINT,  --si importa o NO los dias vencidos
		vdias INTEGER,   --son los dias que tiene a vencer el producto
		vcli_id INTEGER, --codigo de cliente que se este operando
		vaid INTEGER,    --pro_plu del producto
		okpasa SMALLINT,
		lr_dprovencli RECORD LIKE dprovencli.*,
		lr_dpro RECORD LIKE dpro.*,
		lr_dias SMALLINT

	LET okpasa = 0   --DEFAULT

	IF vpasa = 1 THEN  --NO puede operar productos ya vencidos
	 	IF vcli_id IS NOT NULL THEN 
			IF vcli_id = -999 THEN --indica que es MP la que se esta verificando
				SELECT  nvl(art_diasperven,0) INTO lr_dias
				FROM mart
				WHERE mart.cat_plu = vaid

				IF lr_dias >= vdias THEN
					LET okpasa = 1
				ELSE
 					IF vdias > 0 THEN
						MESSAGE "NO TIENE SETEADO LOS DIAS PERMITIDOS, SE BASARA EN LOS DEL LOTE MP!"
     					LET okpasa = 1
 					END IF
				END IF

			ELSE --es PT
				SELECT * INTO lr_dprovencli.*  --dias por cliente y producto
				FROM dprovencli
				WHERE pro_plu = vaid
				AND cli_id = vcli_id

   			IF SQLCA.SQLCODE <> NOTFOUND THEN
					MESSAGE "Dias permitidos cliente: ", lr_dprovencli.pro_diasperven
					IF lr_dprovencli.pro_diasperven >= vdias THEN
        				LET okpasa = 1
					END IF
				ELSE --NO tiene registro por cliente, revisara los dias por producto
					SELECT * INTO lr_dpro.*  --dias por producto
					FROM dpro
					WHERE pro_plu = vaid
					MESSAGE "D�as permitidos producto: ", lr_dpro.pro_diasperven
					IF lr_dpro.pro_diasperven IS NOT NULL THEN  --si tiene seteado dias permitidos
						IF lr_dpro.pro_diasperven >= vdias THEN
        					LET okpasa = 1
						END IF
					ELSE --como NO tiene seteado dias, con que NO este vencido pasa
	 					IF vdias > 0 THEN
	 						--verificar si hay mas validaciones para el PT 
							MESSAGE "NO TIENE SETEADO LOS DIAS PERMITIDOS, SE BASARA EN LOS DEL LOTE PT!"
        					LET okpasa = 1
	 					END IF
					END IF
				END IF
			END IF

		ELSE --como NO tiene registro por cliente, se verificara los dias por producto
				SELECT * INTO lr_dpro.*  --dias por producto
				FROM dpro
				WHERE pro_plu = vaid
				MESSAGE "D�as permitidos producto -->: ", lr_dpro.pro_diasperven
				IF lr_dpro.pro_diasperven IS NOT NULL THEN  --si tiene seteado dias permitidos
					IF lr_dpro.pro_diasperven >= vdias THEN
        				LET okpasa = 1
					END IF
				ELSE --como NO tiene seteado dias, con que NO este vencido pasa
	 				IF vdias > 0 THEN
	 					--verificar si hay mas validaciones para el PT o MP
        				LET okpasa = 1
	 				END IF
				END IF
		END IF

 	ELSE --dejara pasar aunque ya este vencido (deberian ser documentos especiales)
 		LET okpasa = 1
 	END IF

 RETURN okpasa

END FUNCTION

--Retorna el % siendo utilizado en la ubicacion
FUNCTION check_apar(vdttlink,vtep_id)
	DEFINE 
		vdttlink, vtep_id INTEGER,
		totpor DECIMAL(6,3),
		vproid INTEGER,
		err_msg VARCHAR(150),
		capubi DECIMAL(20,5),
		lr_ubica RECORD 
			tipo SMALLINT,
			vid INTEGER,
			ubicado DECIMAL(18,3)
		END RECORD,
		factor decimal(12,6)

		--tomara todos los niveles de la fila, y buscara la MP o PT que este ubicado en la misma
		--ERROR "Inicio chequeo % Ubicado..."

		--esto es lo que esta en la fila, NO se sabe si es MP o PT
		WHENEVER ERROR CONTINUE
			SELECT 1 tipo, a.cat_plu, (uba_canent - uba_cansal) ubicado --MP, capacidad seteada por cat_plu
			FROM  mubiartest a
			WHERE  a.dttlink = vdttlink
			UNION ALL
			SELECT 2 tipo, b.pro_id, (ubp_canent - ubp_cansal) ubicado --PT, la capacidad esta seteada por pro_id
			FROM mubiproest a, dpro b
			WHERE  a.dttlink = vdttlink
			AND b.pro_plu = a.pro_plu
			INTO TEMP tmp_chkubica WITH NO LOG

		WHENEVER ERROR STOP
  	  	IF SQLCA.SQLCODE < 0 THEN  --si es -244 es porque esta bloqueada una tabla!!
			LET err_msg = err_get(SQLCA.SQLCODE) 
			ERROR err_msg CLIPPED
			CALL errorlog(err_msg CLIPPED)
  	  		CALL box_error("Una tabla est� bloqueada, favor repetir el proceso...!")
  	  		RETURN -1
		END IF

			DECLARE cur_chk_apar CURSOR FOR
			SELECT * FROM tmp_chkubica

			LET totpor = 0
			FOREACH cur_chk_apar INTO lr_ubica.*
				LET capubi = get_cantar(lr_ubica.tipo, vtep_id, lr_ubica.vid) 

			 	if lr_ubica.tipo = 1 then --MP	
 
            	SELECT --Una ubicacion, no deberia estar mapeada varias veces para un codigo, con diferente factor!!
                max (x.uec_factor)
					into  factor
            	FROM mubiartest a, mcat b, destant e, mestant f,  dubiestcat x, mubiestcat y
            	WHERE b.cat_plu = a.cat_plu
            	AND e.dttlink = a.dttlink
            	AND f.ettlink = e.ettlink
            	AND a.dttlink = vdttlink
					and a.cat_plu = lr_ubica.vid
                  and y.suc_id = f.suc_id
                  and y.bd_id = f.bd_id
                  and y.cat_plu = a.cat_plu
                  and x.linkuec = y.linkuec
                  and e.dtt_nivel between x.uec_nivini and x.uec_nivfin
                  and e.dtt_fil between x.uec_filini and x.uec_filfin

				else --PT

            	SELECT --OJO, en teoria, deberia estar mapeado solo una vez la ubicacion
                max (x.uep_factor)
						into factor
            	FROM mubiproest a, mpro b,  destant e, mestant f, dpro h, dubiestpro x, mubiestpro y
            	WHERE h.pro_plu = a.pro_plu
            	AND b.pro_id = h.pro_id
            	AND e.dttlink = a.dttlink
            	AND f.ettlink = e.ettlink
            	AND a.dttlink = vdttlink
					and a.pro_id = lr_ubica.vid
                  and y.suc_id = f.suc_id
                  and y.bd_id = f.bd_id
                  and y.pro_id = h.pro_id --OJO, es por pro_id y no por pro_plu
                  and x.linkuep = y.linkuep
                  and e.dtt_nivel between x.uep_nivini and x.uep_nivfin
                  and e.dtt_fil between x.uep_filini and x.uep_filfin
 
				end if

				if factor is null then let factor = 1 end if --por si las moscas, chequer si para por aqui!!

				let capubi = capubi * factor --factor de ubicacion para un codigo

				case capubi 
					WHEN 0 
						CALL alerta(1,"Favor revisar especificaci�n de tarima a ID :"||lr_ubica.vid)
						DISPLAY vdttlink, totpor, "ver"
						LET totpor = 100
						CONTINUE FOREACH
					WHEN -1 
						CALL alerta(1,"No esta especificado el c�digo de tarima para la empresa!")
						DISPLAY vdttlink, totpor, "ver"
						LET totpor = 100
						EXIT FOREACH
					WHEN -2 
						CALL alerta(1,"No esta especificada tarima para ID de MP:"||lr_ubica.vid)
						DISPLAY vdttlink, totpor, "ver"
						LET totpor = 100
						CONTINUE FOREACH
					WHEN -3 
						CALL alerta(1,"No esta especificada tarima para ID de PT:"||lr_ubica.vid)
						DISPLAY vdttlink, totpor, "ver"
						LET totpor = 100
						CONTINUE FOREACH
					OTHERWISE
						CALL alerta(0,"")
				END CASE
				LET totpor = totpor + (lr_ubica.ubicado / capubi * 100)

				IF lr_ubica.vid = 999999999 THEN  --para chequeo de algun problema!!!! (poner pro_id o cat_plu)
					DISPLAY vdttlink, totpor
				END IF
			END forEACH

			--ERROR "Finaliza chequeo % Ubicado..."
			IF totpor > 100 THEN --por si hay mas de lo debido, aunque, NO deberia pasar, ver cuando se cumple esto!!!
				LET totpor = 100
			END IF

			DROP TABLE tmp_chkubica

			RETURN totpor 
END FUNCTION

--despliega mensaje en Titulo en Pie de ventana
FUNCTION alerta(opci, mensaje)
	DEFINE
		opci SMALLINT,
		mensaje VARCHAR(200)

	CASE opci
		WHEN 0
			DISPLAY mensaje TO gtit_pie 
		WHEN 1
			DISPLAY mensaje TO gtit_pie ATTRIBUTE(RED, BOLD)
	END CASE
END FUNCTION

FUNCTION conv_accion(vaccion)
	DEFINE 
		vaccion VARCHAR(30),
		buf base.StringBuffer,
		retorna VARCHAR(30)

	LET buf = base.StringBuffer.create()
	CALL buf.APPEND(vaccion)
	CALL buf.replace("�","a",0)
	CALL buf.replace("�","e",0)
	CALL buf.replace("�","i",0)
	CALL buf.replace("�","o",0)
	CALL buf.replace("�","u",0)
	CALL buf.replace("�","n",0)

	LET retorna = buf.tostring()

	RETURN retorna
END FUNCTION

-- Obtencion del id de la aplicaci�n seg�n el nombre del programa --
FUNCTION ids_ayuda(p_progname, p_descayuda, p_tipayuda)
   DEFINE
      p_progname  VARCHAR(50,0),
      p_descayuda VARCHAR(50,0),
      p_tipayuda  SMALLINT,
      f_ayudaid   INTEGER,
      f_apliid    INTEGER

   INITIALIZE f_ayudaid TO NULL

   SELECT
      m.link,
      m.apl_id
   INTO
      f_ayudaid,
      f_apliid
   FROM face:mayuda m
   WHERE m.ayu_nom = p_progname
   AND m.ayu_tip = 3

   IF p_tipayuda = 1 THEN
      SELECT m.link
      INTO f_ayudaid
      FROM face:mayuda m
      WHERE m.ayu_nom = p_descayuda
      AND m.ayu_tip = p_tipayuda
      AND m.apl_id  = f_apliid

      IF STATUS = NOTFOUND THEN
         LET f_ayudaid = -1
      END IF
   END IF

   IF f_ayudaid IS NULL THEN
      LET f_ayudaid = -1
   END IF

   RETURN f_ayudaid
END FUNCTION

##########################################################################
## funcion     : corre_wms                                              ##
## descripcion : corre el programa de descargas de ubicaciones          ##
## parametros  : vprogra lleva que programa corre del wms               ##
##               1 = descarga de ubicaciones                            ##
##               2 = asignacion de ubicaciones                          ##
## devuelve    :                                                        ##
## author      : isergio                                                ##
## fecha       : Wed Mar 19 08:34:14 CST 2008                           ##
##########################################################################
FUNCTION corre_wms(vprogra,vtip_doc,vser_doc,vnum_doc,vtip_suc,vsuc_id,dbname,name_file)

DEFINE name_file     VARCHAR(50,0)
DEFINE dir_programa  VARCHAR(200)
DEFINE cmd           VARCHAR(200)
DEFINE cpath         CHAR(1)
DEFINE vprogra       SMALLINT
DEFINE vtip_doc      SMALLINT
DEFINE vser_doc      CHAR(2)
DEFINE vnum_doc      INTEGER
DEFINE vtip_suc      SMALLINT
DEFINE vsuc_id       INTEGER
DEFINE gwms          SMALLINT
DEFINE dbname        CHAR(8)
define gcod_emp      INTEGER
DEFINE fcoderr       INTEGER --- codigo de ERROR al momento de ejecutar wms

  SELECT  dg_codemp
  INTO  gcod_emp
  FROM mdatgen

 --El parametro configurado es 1 = WMS NO INTEGRADO, 2 = SI INTEGRADO
  LET gwms = get_parametro(gcod_emp, 2) --codigo de empresa y numero de parametro que se necesita verificar
  IF gwms = -1 THEN
     LET gwms = 0 --("No tiene parametrizado el manejo del m\363dulo WMS!")
  END IF

LET fcoderr = 0
IF gwms = 2 THEN
   CASE vprogra
      WHEN  1
         SELECT face:mprog.prog_dirpro
         INTO dir_programa
         FROM face:mprog
         WHERE face:mprog.prog_nom = "wmsp0861"
         -- parametros
         -- 1. base de datos
         -- 2. tipo de llamada
         -- 3. tipo de suc
         -- 4. sucursal
         -- 5. tipo de de documento
         -- 6. serie
         -- 7. No de documento

         IF vtip_suc IS NOT NULL THEN
            LET cmd =   ". /eec/prod/utils/rungen.sh;cd ",dir_programa CLIPPED,
                        "; fglrun *.42r ",dbname CLIPPED,
                        " 1 ", vtip_suc, vsuc_id, vtip_doc," ",
                        vser_doc, vnum_doc," ",name_file CLIPPED
            DISPLAY "cmd(1):",cmd CLIPPED
            RUN cmd RETURNING fcoderr
         END IF
      WHEN  2
         SELECT face:mprog.prog_dirpro
         INTO dir_programa
         FROM face:mprog
         WHERE face:mprog.prog_nom = "wmsp0858"
         -- parametros
         -- 1. base de datos
         -- 2. tipo de documento
         -- 3. No. de documento
         -- 4. sucursal

         IF vtip_suc IS NOT NULL THEN
         -- tipo 6 (Entrada X traslado)

         LET cmd =" . /eec/prod/utils/rungen.sh;cd ",dir_programa CLIPPED,"; fglrun *.42r ", dbname CLIPPED, vtip_doc, vnum_doc,vsuc_id, " ",
name_file CLIPPED
            DISPLAY "cmd(2):",cmd CLIPPED
            RUN cmd RETURNING fcoderr
         END IF
   END CASE
   IF fcoderr <> 0 THEN
      CALL box_valdato("Ocurri\363 un ERROR al ejecutar esta instruccion: \n"||
                        dir_programa CLIPPED||"/\nfglrun *.42r")
 		LET int_flag = FALSE
   END IF
ELSE
  CALL box_valdato("No configurado el M\363dulo WMS en la empresa!")
END IF
RETURN fcoderr
END FUNCTION