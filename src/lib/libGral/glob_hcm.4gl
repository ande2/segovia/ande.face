# Nombre fisico         : globdbs.4gl
# Programa principal    : de uso global 
# Funciones adicionales : ningunas
# Descripcion           : variables de uso global

GLOBALS
DEFINE
  fec_ini, fec_fin  	DATE,
	bfacera_error				SMALLINT,
	numero_error				INTEGER,
  bfacera							smallint,
	mensaje							char(78)

END GLOBALS