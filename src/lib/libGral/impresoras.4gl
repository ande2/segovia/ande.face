################################################################################
# Funcion     : %M%
# Descripcion : Funciones para impresi�n en l�nea
# Funciones   : 
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z%
# Autor       : 4js 
# Fecha       : %H%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

-- obtiene la impresora al que se necesita imprimir
FUNCTION get_printer(opci, ncampos, text1, text2, text3)
	--opci 1 = solo se quiere la impresora DEFAULT, 2 = seleccionar impresora, 3 = dialogo completo
	DEFINE opci, ncampos SMALLINT,
		text1, text2, text3 VARCHAR(50),
		lr_reg RECORD
			field1 VARCHAR(20),
			field2 VARCHAR(20),
			field3 VARCHAR(20),
			docs VARCHAR(20),
			doce VARCHAR(20),
			alldoc VARCHAR(1),
			impresora VARCHAR(100),
			tip_imp SMALLINT
		END RECORD,
		fo ui.FORM,
		wi ui.WINDOW,
		swap SMALLINT,
		vimpresora VARCHAR(30),
		vusu_id VARCHAR(20),
		vcampo VARCHAR(50)

   INITIALIZE lr_reg.* TO NULL
	LET vimpresora = NULL

   OPEN WINDOW f_selprinter AT 04,03 WITH FORM "print_doc" ATTRIBUTE(BORDER)
   CLEAR FORM

   LET wi = ui.Window.getCurrent()
	LET fo = wi.getForm()

	LET vusu_id = FGL_GETENV("LOGNAME")

   CALL fgl_settitle("Selecci�n de impresora para usuario : "||vusu_id)

   CALL combo_din2("impresora","SELECT TRIM(usu_pcid)||TRIM(diu_impid) FROM face:mimpusu WHERE usu_id = '"||vusu_id||"'")

	--CALL checkCommentRecursive(fo.getNode(),1)  --para cuando ya este la tabla de ayuda en produccion

	CASE ncampos
		WHEN 0
		   CALL fo.setElementHidden("formonly.label1",1)
		   CALL fo.setElementHidden("formonly.field1",1)
		   CALL fo.setElementHidden("formonly.label2",1)
		   CALL fo.setElementHidden("formonly.field2",1)
		   CALL fo.setElementHidden("formonly.label3",1)
		   CALL fo.setElementHidden("formonly.field3",1)
		WHEN 1
		   CALL fo.setElementHidden("formonly.label2",1)
		   CALL fo.setElementHidden("formonly.field2",1)
		   CALL fo.setElementHidden("formonly.label3",1)
		   CALL fo.setElementHidden("formonly.field3",1)
		WHEN 2
		   CALL fo.setElementHidden("formonly.label3",1)
		   CALL fo.setElementHidden("formonly.field3",1)
	END CASE
	DISPLAY text1, text2, text3 TO label1, label2, label3

	DECLARE impdefa CURSOR FOR
	SELECT TRIM(usu_pcid)||TRIM(diu_impid) FROM face:mimpusu
	WHERE face:mimpusu.usu_id = vusu_id
	AND face:mimpusu.usu_default = "1" --es la impresora DEFAULT
	FOREACH impdefa INTO vimpresora
		EXIT FOREACH
	END FOREACH
	FREE impdefa

	LET lr_reg.tip_imp = 1 --debe ser 1, si cambia el DEFAULT, puede que ya NO funcione la impresion desde el programa
								  --print_master.4gs o cuando se desea seleccionar la impresora DEFAULT para enviar a imprimir
								  --de una vez el archivo que deseamos
	LET lr_reg.alldoc  = "0"
	LET lr_reg.impresora = vimpresora
	IF opci <> 1 THEN --entrara al dialogo
   	CALL fo.setElementImage("printers",get_image('fileprint.png'))

		IF opci = 2 THEN
		   CALL fo.setElementHidden("doctos",1)
		   CALL fo.setElementHidden("salida",1)
		END IF

		LET INT_FLAG = FALSE  LET swap = 0
		input by name lr_reg.* WITHOUT defaults
			BEFORE INPUT
				CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
				CALL fgl_dialog_setkeylabel("CONTROL-w", "Ayuda")
         ON KEY ( CONTROL-W )
{
            call FGL_DIALOG_GETFIELDNAME() RETURNING vcampo
            CASE
               WHEN INFIELD(impresora )    CALL lib_showhelp(311,"")
               WHEN INFIELD(alldoc )       CALL lib_showhelp(313,"")
               OTHERWISE  CALL lib_showhelp(312,"")
            END CASE 
}

			ON idle 2
				IF swap THEN
					LET swap = 0
		      	CALL fo.setElementImage("printers",get_image('fileprint.png'))
				ELSE
					LET swap = 1
		      	CALL fo.setElementImage("printers",get_image('imnod.bmp'))
				END IF
			
			AFTER INPUT
				IF INT_FLAG THEN
					LET INT_FLAG = FALSE
					LET lr_reg.tip_imp = 99
				Else
					IF opci = 3 THEN
						IF lr_reg.docs IS NULL THEN
							CALL box_valdato("Debe ingresar el No. de documento Inicial")
							NEXT FIELD docs
						END IF
						IF lr_reg.doce IS NULL THEN
							CALL box_valdato("Debe ingresar el No. de documento Final")
							NEXT FIELD doce
						END IF
					END IF

					IF lr_reg.impresora IS NULL AND lr_reg.tip_imp = 1 then
						CALL alerta(1, "Seleccione la impresora destino!")
						NEXT FIELD impresora
					END IF
				END IF
		END INPUT

	END if

	CLOSE WINDOW f_selprinter

	RETURN lr_reg.field1, lr_reg.field2, lr_reg.field3, lr_reg.impresora, lr_reg.docs, lr_reg.doce, lr_reg.alldoc,
			 lr_reg.tip_imp

END FUNCTION

-- Impresi�n con formato desde genero
FUNCTION imprimir(f_filelenth, f_firstfile, f_secondfile, f_destinationfile, f_printer)
   DEFINE
      f_filelenth       SMALLINT,
      f_firstfile,
      f_secondfile      VARCHAR(50),
      f_destinationfile VARCHAR(50,0),
      f_comando         STRING,
      f_printer         VARCHAR(50),
      f_resultado       SMALLINT

   -- quita una linea al TOTAL de lineas del archivo generado
   LET f_comando  = "head -"||f_filelenth||" "||f_firstfile||" > "||f_secondfile CLIPPED
   RUN f_comando
   LET f_comando  = "rm "||f_firstfile CLIPPED -- borra el primer archivo
   RUN f_comando

	--Cambia caracteres especiales (�-�)
   CALL convert_caracteres_esp(f_secondfile)

   --Traslada el archivo a la PC del usuario
   LET f_comando  = "C:\\\\Documents AND Settings\\All Users\\"||f_destinationfile CLIPPED
   CALL FGL_PUTFILE(f_secondfile CLIPPED, f_comando)

   --Arma el comando de impresi�n
   -- Esta es la parte donde habr�a que ir a traer el listado de Impresora y Nombres de PC's en una tabla
   LET f_comando  = 'cmd.exe /C \PRINT /d:'||f_printer||' "C:\\Documents AND Settings\\All Users\\'||f_destinationfile||'\"'
	DISPLAY "comando: "||f_comando

   --Manda a imprimir el reporte a la impresora compartida de
   --cualquier usuario de la RED

   CALL ui.Interface.frontCall("standard","shellexec",[f_comando],[f_resultado])
   IF NOT f_resultado THEN
      CALL box_valdato("Error al Imprimir el documento")
   END IF
END FUNCTION


FUNCTION convert_caracteres_esp(f_file)
DEFINE f_file VARCHAR(50),
       f_comando STRING

   -- �
   LET f_comando = "perl -pi -e's/�/\\245/g' ",f_file CLIPPED
   RUN f_comando

   -- �
   LET f_comando = "perl -pi -e's/�/\\244/g' ",f_file CLIPPED
   RUN f_comando

   RETURN

END FUNCTION