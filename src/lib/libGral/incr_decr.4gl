################################################################################
# Funcion     : %M%
# Descripcion : Funciones para incremento decremento
# Funciones   : 
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z%
# Autor       : 4js 
# Fecha       : %H%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

-- obtiene datos porcentuales para incrementos-decrementos
FUNCTION porc_incrdecr()
DEFINE
   p_reg RECORD
      dg_porminuni DECIMAL(10,3),
      dg_pormaxuni DECIMAL(10,3),
      dg_porminpre DECIMAL(10,3),
      dg_pormaxpre DECIMAL(10,3)
   END RECORD

   INITIALIZE p_reg.* TO NULL

   SELECT
      NVL(dg_porminuni, 0)/100,
      NVL(dg_pormaxuni, 0)/100,
      NVL(dg_porminpre, 0)/100,
      NVL(dg_pormaxpre, 0)/100
   INTO p_reg.*
   FROM mdatgen

   IF p_reg.dg_porminuni IS NULL THEN
      LET p_reg.dg_porminuni = 0
   END IF
   IF p_reg.dg_pormaxuni IS NULL THEN
      LET p_reg.dg_pormaxuni = 0
   END IF
   IF p_reg.dg_porminpre IS NULL THEN
      LET p_reg.dg_porminpre = 0
   END IF
   IF p_reg.dg_pormaxpre IS NULL THEN
      LET p_reg.dg_pormaxpre = 0
   END IF

   RETURN p_reg.*
END FUNCTION
