FUNCTION set_insdelkey(detalle,cnt)
	DEFINE
		detalle, 
		cnt,
		parcial,
		est_lin,
		i					SMALLINT

	LET parcial = 0
	FOR i = 1 to cnt+1
		CALL estado_linea(detalle,i) RETURNING est_lin
		IF est_lin = 0 THEN
			IF i != cnt+1 THEN
				LET parcial = 1
				EXIT FOR
			END IF
		ELSE
			IF est_lin = 1 THEN
				LET parcial = 1
				EXIT FOR	
			END IF
		END IF
	END FOR

	IF parcial = 0 AND arr_curr() != cnt+1 THEN
		CALL fgl_dialog_setkeylabel("INSERT", "Inserta linea")
	ELSE
		CALL fgl_dialog_setkeylabel("INSERT", "")
	END IF

	LET i = arr_curr()

	IF estado_linea(detalle,i) = 0 AND estado_linea(detalle,i+1) = 0 THEN
		CALL fgl_dialog_setkeylabel("DELETE", "")
	ELSE
		CALL fgl_dialog_setkeylabel("DELETE", "Elimina linea")
	END IF
		
END FUNCTION
