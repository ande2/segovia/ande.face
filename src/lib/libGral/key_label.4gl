##############################################################################  
#       Modulo      :%M%                                                        
#       Descripcion :Cambio de nombre para etiquetas de teclas
#       Funciones   :key_label  Funcion para cambiar etiquetas principales
#
#       SCCS Id No. :%Z% %W%                                                    
#       Autor       :Luis de Leon
#       Fecha       :%H% %T%                                                    
#       Path        :%P%                                                        
############################################################################### 
# Funcion para cambiar nombres de Teclas 

FUNCTION key_label()
  CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
  --CALL fgl_dialog_setkeylabel("HELP","Ayuda")
  CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
END FUNCTION

FUNCTION key_label2()
  CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
  --CALL fgl_dialog_setkeylabel("HELP","Ayuda")
  CALL fgl_dialog_setkeylabel("ACCEPT","Grabar")
END FUNCTION

FUNCTION key_label414(opcion)
DEFINE opcion SMALLINT

  CALL fgl_dialog_setkeylabel("INTERRUPT","")
  CALL fgl_dialog_setkeylabel("DELETE","")
  CALL fgl_dialog_setkeylabel("INSERT","")

  CALL fgl_dialog_setkeylabel("CONTROL-E","Cancelar")
 
  IF opcion = 1 THEN
     CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
  ELSE
     CALL fgl_dialog_setkeylabel("ACCEPT","Grabar")
  END IF
  
END FUNCTION
