################################################################################
# Programa    : lib_glob
# Descripcion : funciones de uso global en los programas
# Funciones   : set_log()
#               connectDB_ok()
#               set_ToolbarTopmenuRep() -- ErickValdez
#               get_menopcRep(dbname,ProgNom,Usuario)
#               set_titulos(dbname, Usuario, progNom) -- ErickValdez
#               set_QueryOk(TxtSQL) -- ErickValdez
#               set_ToolbarTopmenuPrg(ProgNom)
#               get_menopcPrg(dbname,ProgNom,Usuario)
#               QueryRecParam(TipoVal,TxtSQL)
#               set_PrgOpcTbTm(ProgNom,Usuario)
#               set_SerieNoRecibo() -- ErickValdez
################################################################################
# SCCS ID No. : %Z% %W%
# Autor       : MarioPerez, CarlosSantizo, ErickValdez
# Fecha       : 25/03/2009
# Path        : $BASEDIR/lib_gen211
################################################################################
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################
# Descripcion del Programa:
# ------------------------
#
################################################################################

################################################################################
# Nombre   : set_log
# Recibe   : 
# Devuelve : progName
# Descripcion : Se define el log del programa
################################################################################
FUNCTION set_log()
DEFINE i 			SMALLINT
DEFINE progName	CHAR(20)
DEFINE LogPath 	STRING

	LET progName = arg_val(0)
	FOR i=1 TO LENGTH(progName)
		IF progName[i] = "." THEN
			LET i = i-1
			EXIT FOR
		END IF
	END FOR

	LET progName = progName[1,i]
	LET LogPath = FGL_GETENV("BASEDIR")||"/log/"||progName CLIPPED||".log"
	CALL STARTLOG(LogPath)

	RETURN progName

END FUNCTION --set_log


################################################################################
# Nombre   : connectDB_ok
# Recibe   : 
# Devuelve : TRUE, FALSE
# Descripcion : Validacion para permisos de conexion en base de datos
################################################################################
FUNCTION connectDB_ok()
DEFINE n_param	SMALLINT
DEFINE dbname	CHAR(20)

	LET n_param = num_args()
	IF n_param = 0 THEN
		RETURN FALSE, NULL
	ELSE
		LET dbname = arg_val(1)
		WHENEVER ERROR CONTINUE
			CONNECT TO dbname
		WHENEVER ERROR STOP
		IF SQLCA.SQLCODE < 0 THEN
			RETURN FALSE, NULL
		ELSE
			RETURN TRUE, dbname
		END IF
	END IF
END FUNCTION --connectDB_ok


################################################################################
# Nombre   : set_ToolbarTopmenuRep
# Recibe   : 
# Devuelve : 
# Descripcion : Crea Toolbar y Topmenu correspondiente para un reporte
################################################################################
FUNCTION set_ToolbarTopmenuRep()
DEFINE f		ui.Form
DEFINE w		ui.Window
DEFINE aui,tbi,tbs,tms,tmo		om.DomNode
DEFINE tb,tm,tmga,tmge,tmgh	om.DomNode
DEFINE PicsDir STRING

	-- Capturamos la instancia de forma actual
	LET w = ui.Window.getCurrent()
	LET f = w.getForm()

  -- Capturamos el webserver de imagenes
	LET PicsDir = FGL_GETENV("PICSDIR") 

	-- Construimos el Toolbar
	LET aui = ui.Interface.getRootNode()
	LET tb  = aui.createChild("ToolBar")
	LET tbi = createToolBarItem(tb,"accept","Generar","Show help",PicsDir||"ok.bmp")
	LET tbi = createToolBarItem(tb,"control-b","Buscar","Buscar",PicsDir||"find.bmp")
	LET tbs = createToolBarSeparator(tb)
	LET tbi = createToolBarItem(tb,"control-w","Ayuda","Ayuda",PicsDir||"help")
	LET tbs = createToolBarSeparator(tb)
	LET tbi = createToolBarItem(tb,"cancel","Salir","Salir del Programa",PicsDir||"cancel")
	
	-- Construimos el TopMenu
	LET aui = ui.Interface.getRootNode()
	LET tm  = aui.createChild("TopMenu")
		LET tmga = tm.createChild("TopMenuGroup")
		CALL tmga.setAttribute("text","Archivo")
			LET tmo = creaopcion(tmga,"accept","Generar","Generar Reporte","ok")
			LET tms = tmga.CreateChild("TopMenuSeparator")
			LET tmo = creaopcion(tmga,"cancel","Salir","Salir del Programa","cancel")
		LET tmge = tm.createChild("TopMenuGroup")
		CALL tmge.setAttribute("text","Edicion")
			LET tmo = creaopcion(tmge,"editcut","Cortar","Cortar","cut")
			LET tmo = creaopcion(tmge,"editcopy","Copiar","Copiar","copy")
			LET tmo = creaopcion(tmge,"editpaste","Pegar","Pegar","paste")
		LET tmgh = tm.createChild("TopMenuGroup")
		CALL tmgh.setAttribute("text","Herramientas")
			LET tmo = creaopcion(tmgh,"control-b","Buscar","Buscar","find")
			LET tms = tmgh.CreateChild("TopMenuSeparator")
			LET tmo = creaopcion(tmgh,"control-w","Ayuda","Ayuda","help")

END FUNCTION

################################################################################
# Nombre   : set_toolbar_topmenuPrg
# Recibe   : 
# Devuelve : 
# Descripcion : Crea Toolbar y Topmenu correspondiente para un Programa
################################################################################
FUNCTION set_ToolbarTopmenuPrg(ProgNom)
DEFINE ProgNom VARCHAR(30,1)
DEFINE f		ui.Form
DEFINE w		ui.Window
DEFINE aui,tbi,tbs,tms,tmo		om.DomNode
DEFINE tb,tm,tmga,tmge,tmgh	om.DomNode
DEFINE gr_menu RECORD
   prog_ord SMALLINT,
	opcion 	VARCHAR(20,1),
	desc_opc VARCHAR(60,1),
	des_cod 	SMALLINT,
	imagen 	VARCHAR(100,1)
END RECORD
DEFINE reg_arr DYNAMIC ARRAY OF RECORD 
	opcion 	STRING,
	desc_opc STRING,
	des_cod 	SMALLINT,
	imagen 	STRING,
	linkprop	INTEGER
END RECORD
DEFINE vopc STRING
DEFINE cnt	INTEGER

	-- Capturamos la instancia de forma actual
	LET w = ui.Window.getCurrent()
	LET f = w.getForm()
	
	-- Construimos el Toolbar
	LET aui = ui.Interface.getRootNode()
	LET tb  = aui.createChild("ToolBar")

	DECLARE cur_menopc CURSOR FOR
		SELECT b.prog_ord, c.des_desc_md, b.prop_desc, 
				c.des_cod, c.des_desc_ct, b.linkprop
		FROM face:mprog a,face:dprogopc b,sd_des c
		WHERE a.prog_nom = ProgNom
		AND b.prog_id = a.prog_id
      AND c.des_tipo = 22
      AND c.des_cod = b.des_cod
		ORDER BY b.prog_ord
	FOREACH cur_menopc INTO gr_menu.*
		CALL reg_arr.appendElement()
		LET reg_arr[reg_arr.getLength()].opcion = gr_menu.opcion CLIPPED
		LET reg_arr[reg_arr.getLength()].desc_opc = gr_menu.desc_opc
		LET reg_arr[reg_arr.getLength()].des_cod = gr_menu.des_cod
		LET reg_arr[reg_arr.getLength()].imagen = (get_image(gr_menu.imagen))
      LET vopc = 'opc',reg_arr.getLength() USING "<<<<<<<<<"
 		LET tbi = createToolBarItem(tb,vopc,reg_arr[reg_arr.getLength()].opcion,reg_arr[reg_arr.getLength()].desc_opc,reg_arr[reg_arr.getLength()].imagen) 
   END FOREACH

	-- Construimos el TopMenu
	LET aui = ui.Interface.getRootNode()
	LET tm  = aui.createChild("TopMenu")
		LET tmga = tm.createChild("TopMenuGroup")
		CALL tmga.setAttribute("text","Archivo")
			FOR cnt = 1 TO reg_arr.getLength()
 				LET tmo = creaopcion(tmga,reg_arr[cnt].opcion,reg_arr[cnt].opcion,reg_arr[cnt].desc_opc,reg_arr[cnt].imagen) 
			END FOR

		LET tmge = tm.createChild("TopMenuGroup")
		CALL tmge.setAttribute("text","Edicion")
			LET tmo = creaopcion(tmge,"editcut","Cortar","Cortar","cut")
			LET tmo = creaopcion(tmge,"editcopy","Copiar","Copiar","copy")
			LET tmo = creaopcion(tmge,"editpaste","Pegar","Pegar","paste")
		LET tmgh = tm.createChild("TopMenuGroup")
		CALL tmgh.setAttribute("text","Herramientas")
			LET tmo = creaopcion(tmgh,"control-b","Buscar","Buscar","find")
			LET tms = tmgh.CreateChild("TopMenuSeparator")
			LET tmo = creaopcion(tmgh,"control-w","Ayuda","Ayuda","help")

END FUNCTION


################################################################################
# Nombre   : get_menopcPrg
# Recibe   : dbname, ProgNom, Usuario
# Devuelve : cont_ok, Numopc
# Descripcion : Devuelve las opciones de menu a las cuales el usuario tiene acceso
################################################################################
FUNCTION get_menopcPrg(dbname,ProgNom,Usuario)
DEFINE dbname	STRING
DEFINE ProgNom	CHAR(20)
DEFINE Usuario	CHAR(20)
DEFINE cont_ok	SMALLINT
DEFINE linkpro	INTEGER
DEFINE Numopc	CHAR(20)

	LET cont_ok = FALSE
	LET Numopc= NULL
	DECLARE cur_menopcprg CURSOR FOR
		SELECT d.linkprop
		FROM face:mprog a,face:dprogopc b, adm_usu c, OUTER(maccopcusu d)
		WHERE c.usu_nomunix = Usuario
		AND a.prog_nom = ProgNom
		AND b.prog_id = a.prog_id
		AND b.linkprop = d.linkprop
		AND d.usu_id = c.usu_id
		ORDER BY b.prog_ord
	FOREACH cur_menopcprg INTO linkpro
		IF LENGTH(linkpro) = 0 THEN
			LET Numopc = Numopc CLIPPED,"0"
			LET cont_ok = FALSE
		ELSE
			LET Numopc = Numopc CLIPPED,"1"
			LET cont_ok = TRUE
		END IF
	END FOREACH
	FREE cur_menopcprg
 
	RETURN cont_ok, NumOpc
END FUNCTION

################################################################################
# Nombre   : get_menopcRep
# Recibe   : dbname, ProgNom, Usuario
# Devuelve : cont_ok, Numopc
# Descripcion : Devuelve las opciones de menu a las cuales el usuario tiene acceso
################################################################################
FUNCTION get_menopcRep(dbname,ProgNom,Usuario)
DEFINE dbname	STRING
DEFINE ProgNom	CHAR(20)
DEFINE Usuario	CHAR(20)
DEFINE cont_ok	SMALLINT
DEFINE linkpro	INTEGER
DEFINE Numopc	CHAR(20)

	LET cont_ok = FALSE
	LET Numopc= NULL
	DECLARE cur_menopc1 CURSOR FOR
		SELECT d.linkprop
		FROM face:mprog a,face:dprogopc b, adm_usu c, OUTER(maccopcusu d)
		WHERE c.usu_nomunix = Usuario
		AND a.prog_nom = ProgNom
		AND b.prog_id = a.prog_id
		AND b.linkprop = d.linkprop
		AND d.usu_id = c.usu_id
		ORDER BY b.prog_ord
	FOREACH cur_menopc1 INTO linkpro
		IF LENGTH(linkpro) = 0 THEN
			LET Numopc = Numopc CLIPPED,"0"
			LET cont_ok = FALSE
		ELSE
			LET Numopc = Numopc CLIPPED,"1"
			LET cont_ok = TRUE
		END IF
	END FOREACH
	FREE cur_menopc
 
	RETURN cont_ok, NumOpc
END FUNCTION


################################################################################
# Nombre   : set_titulos
# Recibe   : dbname, Usuario, progNom
# Devuelve : 
# Descripcion : Despliega los titulos del programa
################################################################################
FUNCTION set_titulos(dbname, Usuario, progNom)
DEFINE dbname     CHAR(10)
DEFINE Titulo     STRING
DEFINE Usuario    STRING
DEFINE ProgNom    VARCHAR(50)
DEFINE ProgPadre	VARCHAR(255) 
DEFINE ProgTitulo	VARCHAR(255)
DEFINE NomEmpresa VARCHAR(255)

	-- Devolvemos el nombre de la Empresa
	WHENEVER ERROR CONTINUE
		SELECT a.empr_nom
		INTO NomEmpresa
		FROM face:mempr a
		WHERE a.empr_db = dbname
	WHENEVER ERROR STOP
	IF SQLCA.SQLCODE <> 0 THEN
		LET NomEmpresa = "Empresa No Identificada"
	END IF

	-- Devolvemos el nombre del Programa
	WHENEVER ERROR CONTINUE
		SELECT a.prog_des,
				(SELECT b.prog_des FROM face:mprog b 
					WHERE b.prog_id = a.prog_padre)
		INTO progTitulo, progPadre
		FROM face:mprog a
		WHERE a.prog_nom = progNom
	WHENEVER ERROR STOP
	IF SQLCA.SQLCODE <> 0 THEN
		LET progTitulo = "Programa No Identificado"
		LET progPadre = "Modulo No Identificado"
	END IF

	-- Despliega nombre el la ventana
	LET Titulo = NomEmpresa CLIPPED," [",progPadre CLIPPED,"] "
	CALL fgl_settitle(Titulo.ToUpperCase())

	-- Desplegamos el encabezado y pie de pagina
	LET Titulo = progTitulo
	DISPLAY Titulo.ToUpperCase() TO gtit_enc
	DISPLAY "Presione CTRL-W para Ayuda" TO gtit_pie

	-- Desplegamos la fecha actual del sistema
	MESSAGE "Usuario : ",Usuario CLIPPED,"          Fecha : ",DATE(CURRENT) CLIPPED
END FUNCTION


################################################################################
# Nombre   : set_QueryOk
# Recibe   : TxtSQL
# Devuelve : Query_Ok
# Descripcion : Realiza query's que no necesiten retornar valores y devuelve el 
#					estado del mismo
#					Ej. 0 = Todo Bien
#                100 = No se encontraron datos 
#                < 0 = Error en el query
################################################################################
FUNCTION set_QueryOk(TxtSQL)
DEFINE TxtSQL		STRING	-- Query a Validar
DEFINE NumSqlCode	STRING	-- Numero de Error de SQL 
DEFINE MsgSqlCode	STRING	-- Mensaje de Error de SQL 
DEFINE Query_Ok	SMALLINT	-- TRUE=Correcto, FALSE=Error

	LET Query_Ok = TRUE
	WHENEVER ERROR CONTINUE
		PREPARE QueryOk FROM TxtSQL
	WHENEVER ERROR STOP
	IF SQLCA.SQLCODE <> 0 THEN
		LET Query_Ok = FALSE
		LET NumSqlCode = SQLCA.SQLCODE
		LET MsgSqlCode = 'Error al preparar una Instrucción'||'\n'||SQLERRMESSAGE
		CALL box_error(MsgSqlCode)
		LET MsgSqlCode = NumSqlCode||' '||SQLERRMESSAGE||'\n'||TxtSQL 
		CALL errorlog(MsgSqlCode)
	ELSE
		WHENEVER ERROR CONTINUE
			EXECUTE QueryOk
		WHENEVER ERROR STOP
		IF SQLCA.SQLCODE < 0 THEN
			LET Query_Ok = FALSE
			LET NumSqlCode = SQLCA.SQLCODE
			LET MsgSqlCode = 'Error al ejecutar una Instrucción'||'\n'||SQLERRMESSAGE
			CALL box_error(MsgSqlCode)
			LET MsgSqlCode = NumSqlCode||' '||SQLERRMESSAGE||'\n'||TxtSQL 
			CALL errorlog(MsgSqlCode)
		ELSE
			IF SQLCA.SQLCODE = 100 THEN
				LET Query_Ok = FALSE
				LET NumSqlCode = SQLCA.SQLCODE
				LET MsgSqlCode = 'Error al ejecutar una Instrucción'||'\n'||SQLERRMESSAGE
				CALL box_error(MsgSqlCode)
			ELSE
				LET NumSqlCode = SQLCA.SQLCODE
				LET MsgSqlCode = NULL
			END IF
		END IF
	END IF

	RETURN Query_Ok
END FUNCTION



################################################################################
# Nombre   : QueryRecParam
# Recibe   : TipoVal,TxtSQL
# Devuelve : Valor
# Descripcion : Realiza un query que busca un solo valor y regresa el valor
#					numerico que el sql asigna segun la operacion
#					Ej. 0 = Todo Bien
#                100 = No se encontraron datos 
#                < 0 = Error en el query
################################################################################
FUNCTION QueryRecParam(TipoVal,TxtSQL)
DEFINE Valor		STRING 			-- Valor que retorna la funcion
DEFINE TxtSQL		STRING			-- Query a Validar
DEFINE ValStr		VARCHAR(255)	-- Valor encontrado tipo cadena
DEFINE ValNum		INTEGER			-- Valor encontrado tipo numerico
DEFINE TipoVal		STRING			-- Define el tipo de valor que se busca (String,Numeric)
DEFINE NumSqlCode	STRING			-- Numero de Error de SQL 
DEFINE MsgSqlCode	STRING			-- Mensaje de Error de SQL 

	WHENEVER ERROR CONTINUE
		PREPARE QueryRecParam FROM TxtSQL
	WHENEVER ERROR STOP
	IF SQLCA.SQLCODE <> 0 THEN
		LET NumSqlCode = SQLCA.SQLCODE
		LET MsgSqlCode = 'Error al preparar una Instrucción'||'\n'||SQLERRMESSAGE
		CALL box_error(MsgSqlCode)
		LET MsgSqlCode = NumSqlCode||' '||SQLERRMESSAGE||'\n'||TxtSQL 
		CALL errorlog(MsgSqlCode)
	ELSE
		WHENEVER ERROR CONTINUE
		CASE 
			WHEN TipoVal.ToUpperCase() = "S"
				EXECUTE QueryRecParam INTO ValStr
			WHEN TipoVal.ToUpperCase() = "N"
				EXECUTE QueryRecParam INTO ValNum
			OTHERWISE
				CALL box_error("Tipo de Valor no Encontrado para ejecutar el Query")
		END CASE
		WHENEVER ERROR STOP
		IF SQLCA.SQLCODE < 0 THEN
			LET NumSqlCode = SQLCA.SQLCODE
			LET MsgSqlCode = 'Error al ejecutar una Instrucción'||'\n'||SQLERRMESSAGE
			CALL box_error(MsgSqlCode)
			LET MsgSqlCode = NumSqlCode||' '||SQLERRMESSAGE||'\n'||TxtSQL 
			CALL errorlog(MsgSqlCode)
		ELSE
			IF SQLCA.SQLCODE = 100 THEN
				LET NumSqlCode = SQLCA.SQLCODE
				LET MsgSqlCode = 'Error al ejecutar una Instrucción'||'\n'||SQLERRMESSAGE
				CALL box_error(MsgSqlCode)
			ELSE
				LET NumSqlCode = SQLCA.SQLCODE
				LET MsgSqlCode = NULL
				CASE
					WHEN TipoVal.ToUpperCase() = "S"
						LET Valor = ValStr
					WHEN TipoVal.ToUpperCase() = "N"
						LET Valor = ValNum
					OTHERWISE
						LET Valor = NULL
				END CASE
			END IF
		END IF
	END IF

	RETURN Valor
END FUNCTION


################################################################################
# Nombre   : set_PrgOpcTbTm
# Recibe   : 
# Devuelve : 
# Descripcion :
################################################################################
FUNCTION set_PrgOpcTbTm(ProgNom,Usuario)
DEFINE sql		STRING
DEFINE Numopc	CHAR(20)
DEFINE ProgNom	STRING
DEFINE Usuario	STRING
DEFINE cont_ok	SMALLINT
DEFINE linkpro	INTEGER
DEFINE f		ui.Form
DEFINE w		ui.Window
DEFINE aui,tbi,tbs,tms,tmo		om.DomNode
DEFINE tb,tm,tmga,tmge,tmgh	om.DomNode
DEFINE gr_menu RECORD
   prog_ord SMALLINT,
	opcion 	VARCHAR(20,1),
	desc_opc VARCHAR(60,1),
	des_cod 	SMALLINT,
	imagen 	VARCHAR(100,1),
	linkprop	INTEGER
END RECORD
DEFINE reg_arr DYNAMIC ARRAY OF RECORD 
	opcion 	STRING,
	desc_opc STRING,
	des_cod 	SMALLINT,
	imagen 	STRING,
	linkprop	INTEGER
END RECORD
DEFINE cnt	INTEGER

	-- Capturamos la instancia de forma actual
	LET w = ui.Window.getCurrent()
	LET f = w.getForm()
	
	-- Construimos el Toolbar
	LET aui = ui.Interface.getRootNode()
	LET tb  = aui.createChild("ToolBar")

	LET cont_ok = 0
	LET Numopc= NULL
	
	LET sql = "SELECT b.prog_ord, c.des_desc_md, b.prop_desc, ",
						" c.des_cod, c.des_desc_ct, e.linkprop ",
				" FROM face:.gmprog a, face:dprogopc b, sd_des c, adm_usu d, OUTER(maccopcusu e) ",
				" WHERE a.prog_nom = '",ProgNom CLIPPED,"'",
				" AND d.usu_nomunix = '",Usuario CLIPPED,"'",
				" AND b.prog_id = a.prog_id ",
      		" AND c.des_tipo = 22 ",
      		" AND c.des_cod = b.des_cod ",
				" AND e.usu_id = d.usu_id ",
				" AND b.linkprop = e.linkprop ",
				" ORDER BY b.prog_ord "

	PREPARE sql_opctbtm FROM sql
	DECLARE cur_opctbtm CURSOR FOR sql_opctbtm
	FOREACH cur_opctbtm INTO gr_menu.*
		CALL reg_arr.appendElement()
		LET reg_arr[reg_arr.getLength()].opcion = gr_menu.opcion CLIPPED
		LET reg_arr[reg_arr.getLength()].desc_opc = gr_menu.desc_opc
		LET reg_arr[reg_arr.getLength()].des_cod = gr_menu.des_cod
		LET reg_arr[reg_arr.getLength()].imagen = (get_image(gr_menu.imagen))
		LET reg_arr[reg_arr.getLength()].linkprop = gr_menu.linkprop
 		LET tbi = createToolBarItem(tb,DOWNSHIFT(reg_arr[reg_arr.getLength()].opcion),reg_arr[reg_arr.getLength()].opcion,
																reg_arr[reg_arr.getLength()].desc_opc,reg_arr[reg_arr.getLength()].imagen) 
		IF LENGTH(gr_menu.linkprop) = 0 THEN
			LET Numopc = Numopc CLIPPED,"0"
			LET cont_ok = cont_ok + 0
		ELSE
			LET Numopc = Numopc CLIPPED,"1"
			LET cont_ok = cont_ok + 1
		END IF
   END FOREACH
		LET tbs = tb.createChild("ToolBarSeparator")
 		LET tbi = createToolBarItem(tb,'control-w','Ayuda','Ayuda','help')

	-- Construimos el TopMenu
	LET aui = ui.Interface.getRootNode()
	LET tm  = aui.createChild("TopMenu")
		LET tmga = tm.createChild("TopMenuGroup")
		CALL tmga.setAttribute("text","Archivo")
			FOR cnt = 1 TO reg_arr.getLength()
 				LET tmo = creaopcion(tmga,DOWNSHIFT(reg_arr[cnt].opcion),reg_arr[cnt].opcion,reg_arr[cnt].desc_opc,reg_arr[cnt].imagen) 
			END FOR

		LET tmge = tm.createChild("TopMenuGroup")
		CALL tmge.setAttribute("text","Edicion")
			LET tmo = creaopcion(tmge,"editcut","Cortar","Cortar","cut")
			LET tmo = creaopcion(tmge,"editcopy","Copiar","Copiar","copy")
			LET tmo = creaopcion(tmge,"editpaste","Pegar","Pegar","paste")
		LET tmgh = tm.createChild("TopMenuGroup")
		CALL tmgh.setAttribute("text","Herramientas")
			LET tmo = creaopcion(tmgh,"control-b","Buscar","Buscar","find")
			LET tms = tmgh.CreateChild("TopMenuSeparator")
			LET tmo = creaopcion(tmgh,"control-w","Ayuda","Ayuda","help")

	RETURN cont_ok, NumOpc, sql
END FUNCTION


################################################################################
# Nombre   : set_SerieNoRecibo
# Recibe   : 
# Devuelve : LinkTal, CtaSer, IniNoRec, UltNoRec
# Descripcion : Busca la serie y el numero de recibo que debe utilizarse en la
#               generacion de un nuevo recibo.
################################################################################
FUNCTION set_SerieNoRecibo()
DEFINE Usuario		VARCHAR(255)
DEFINE CtaSer		CHAR(2)
DEFINE LinkTal		INTEGER
DEFINE UltNoRec	SMALLINT
DEFINE IniNoRec	SMALLINT
DEFINE NoTalVig	SMALLINT
DEFINE Mensaje		STRING
DEFINE ser_rec RECORD
	linktal	INTEGER,
	ser_rec	CHAR(2),
	rec_ini	INTEGER,
	rec_ult	INTEGER
END RECORD

	-- Valores default
	LET Usuario = fgl_getenv("LOGNAME")
	LET CtaSer = NULL
	LET LinkTal = 0
	LET UltNoRec = 0
	LET IniNoRec = 0
	LET NoTalVig = 0 

	-- Verificacion de Talonario Vigente por Usuario
	WHENEVER ERROR CONTINUE
		SELECT NVL(COUNT(c.usu_id),0)
		INTO NoTalVig
		FROM dtalrecser a, dtalrecusu b, adm_usu c
		WHERE a.est_id = 1
		AND b.est_id = 1
		AND a.linktal = b.linktal
		AND c.usu_id = b.usu_id
		AND c.usu_nomunix = Usuario
	WHENEVER ERROR STOP
	IF SQLCA.SQLCODE <> 0 THEN
		LET NoTalVig = 0
	END IF

	IF NoTalVig > 1 THEN
		CALL box_valdato("Favor de elegir una serie para recibos")
		CALL set_NSeries(Usuario) RETURNING ser_rec.*, int_flag
		IF int_flag THEN
			LET int_flag = FALSE
			LET Mensaje = "Se cancelo la seleccion de serie para recibos."
			CALL box_error(Mensaje)
		ELSE
			LET LinkTal = ser_rec.linktal
			LET CtaSer = ser_rec.ser_rec
			LET IniNoRec = ser_rec.rec_ini
			LET UltNoRec = ser_rec.rec_ult
		END IF
		RETURN LinkTal, CtaSer, IniNoRec, UltNoRec
	ELSE
		IF NoTalVig = 1 THEN
			WHENEVER ERROR CONTINUE
         	SELECT a.linktal, a.trs_serrec, a.trs_numrecini, a.trs_numrecfin
				INTO LinkTal, CtaSer, IniNoRec, UltNoRec
         	FROM dtalrecser a, dtalrecusu b, adm_usu c
         	WHERE a.est_id = 1
         	AND b.est_id = 1
         	AND c.usu_id = b.usu_id
         	AND a.linktal = b.linktal
         	AND c.usu_nomunix = Usuario
			WHENEVER ERROR STOP
			IF SQLCA.SQLCODE <> 0 THEN
				LET Mensaje = "Error al asignar talonario, serie y numero de recibo."
				CALL box_error(Mensaje)
			END IF

			RETURN LinkTal, CtaSer, IniNoRec, UltNoRec
		ELSE
			LET Mensaje = "El usuario ",Usuario CLIPPED," no tiene talonario asignado."
			CALL box_error(Mensaje)
			RETURN LinkTal, CtaSer, IniNoRec, UltNoRec
		END IF
	END IF
END FUNCTION


################################################################################
# Nombre   : set_NSeries
# Recibe   : Usuario
# Devuelve : ser_rec[pos].* y int_flag
# Descripcion : Permite escoger de una lista una serie de recibo
################################################################################
FUNCTION set_NSeries(Usuario)
DEFINE pos		SMALLINT
DEFINE lin_act	SMALLINT
DEFINE Mensaje	STRING
DEFINE Usuario	VARCHAR(255)
DEFINE sqlstmnt STRING
DEFINE ser_rec DYNAMIC ARRAY OF RECORD
	linktal	INTEGER,
	ser_rec	CHAR(2),
	rec_ini	INTEGER,
	rec_ult	INTEGER
END RECORD

	-- Valores por Default
	LET pos = 1
	LET int_flag = FALSE
	CALL ser_rec.clear()

	-- Buscamos las series para seleccionar
   LET sqlstmnt = " SELECT a.linktal, a.trs_serrec, a.trs_numrecini, a.trs_numrecfin ",
            		" FROM dtalrecser a, dtalrecusu b, adm_usu c ",
            		" WHERE a.est_id = 1 ",
            		" AND b.est_id = 1 ",
            		" AND c.usu_id = b.usu_id ",
            		" AND a.linktal = b.linktal ",
            		" AND c.usu_nomunix = '",Usuario CLIPPED,"'"

	WHENEVER ERROR CONTINUE
		PREPARE prep_sql FROM sqlstmnt
	WHENEVER ERROR STOP
	IF SQLCA.SQLCODE <> 0 THEN
		LET pos = 0
	ELSE
		-- Recorremos el cursor para asignar valores 
		DECLARE cur_nseries CURSOR FOR prep_sql
		FOREACH cur_nseries INTO ser_rec[pos].linktal,
										ser_rec[pos].ser_rec,
										ser_rec[pos].rec_ini,
										ser_rec[pos].rec_ult
			LET pos = pos + 1
		END FOREACH
		LET pos = pos - 1
	END IF

	IF pos > 0 THEN
		-- Abre forma temporal
		OPEN WINDOW Nseries WITH FORM "forma_nseries" ATTRIBUTES(FORM LINE 1,BORDER)

		-- seleccionar la serie a utilizar
		CALL SET_COUNT(pos)
		DISPLAY ARRAY ser_rec TO sa_serrec.*
			BEFORE DISPLAY
				CALL fgl_dialog_setkeylabel("ACCEPT","Seleccionar")
				CALL fgl_dialog_setkeylabel("INTERRUPT","Salir")

			BEFORE ROW
         	LET pos = arr_curr()
				IF LENGTH(ser_rec[pos].ser_rec) = 0 THEN
					CALL fgl_dialog_setcurrline(pos-1,pos-1)
				END IF

			AFTER DISPLAY
				LET pos = arr_curr()
				IF int_flag THEN
					LET Mensaje = "Se cancelo la selección de series."
					CALL box_error(Mensaje)
					EXIT DISPLAY
				ELSE
					EXIT DISPLAY
				END IF
		END DISPLAY

		-- Cierra forma temporal
		CLOSE WINDOW Nseries
	ELSE
		-- Sino se encontraron series que seleccionar
		LET pos = 1
		LET int_flag = TRUE
		LET Mensaje = "No se encontraron series para seleccionar."
		CALL box_error(Mensaje)
	END IF

	RETURN ser_rec[pos].*, int_flag
END FUNCTION


################################################################################
# Nombre   : setret_ToolbarTopmenuRep
# Recibe   :
# Devuelve :
# Descripcion : Crea Toolbar y Topmenu correspondiente para un reporte
################################################################################
FUNCTION setret_ToolbarTopmenuRep()
DEFINE f    ui.Form
DEFINE w    ui.Window
DEFINE aui,tbi,tbs,tms,tmo    om.DomNode
DEFINE tb,tm,tmga,tmge,tmgh   om.DomNode
DEFINE PicsDir STRING

   -- Capturamos la instancia de forma actual
   LET w = ui.Window.getCurrent()
   LET f = w.getForm()

  -- Capturamos el webserver de imagenes
   LET PicsDir = FGL_GETENV("PICSDIR")

   -- Construimos el Toolbar
   LET aui = ui.Interface.getRootNode()
   LET tb  = aui.createChild("ToolBar")
   LET tbi = createToolBarItem(tb,"accept","Generar","Show help",PicsDir||"ok.bmp")
   LET tbi = createToolBarItem(tb,"control-b","Buscar","Buscar",PicsDir||"find.bmp")
   LET tbs = createToolBarSeparator(tb)
   LET tbi = createToolBarItem(tb,"control-w","Ayuda","Ayuda",PicsDir||"help")
   LET tbs = createToolBarSeparator(tb)
   LET tbi = createToolBarItem(tb,"cancel","Salir","Salir del Programa",PicsDir||"cancel")

   -- Construimos el TopMenu
   LET aui = ui.Interface.getRootNode()
   LET tm  = aui.createChild("TopMenu")
      LET tmga = tm.createChild("TopMenuGroup")
      CALL tmga.setAttribute("text","Archivo")
         LET tmo = creaopcion(tmga,"accept","Generar","Generar Reporte","ok")
         LET tms = tmga.CreateChild("TopMenuSeparator")
         LET tmo = creaopcion(tmga,"cancel","Salir","Salir del Programa","cancel")
      LET tmge = tm.createChild("TopMenuGroup")
      CALL tmge.setAttribute("text","Edicion")
         LET tmo = creaopcion(tmge,"editcut","Cortar","Cortar","cut")
         LET tmo = creaopcion(tmge,"editcopy","Copiar","Copiar","copy")
         LET tmo = creaopcion(tmge,"editpaste","Pegar","Pegar","paste")
      LET tmgh = tm.createChild("TopMenuGroup")
      CALL tmgh.setAttribute("text","Herramientas")
         LET tmo = creaopcion(tmgh,"control-b","Buscar","Buscar","find")
         LET tms = tmgh.CreateChild("TopMenuSeparator")
         LET tmo = creaopcion(tmgh,"control-w","Ayuda","Ayuda","help")

   RETURN tb, tm
END FUNCTION