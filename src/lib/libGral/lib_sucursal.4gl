################################################################################
# Funcion     : %M%
# Descripcion : Funciones Especiales Sucursales
# Funciones   : desplazac(p_tipsuc)      -- determina hacia donde se est� moviendo el CURSOR
#               veri_tipodato(f_valor)   -- Verifica si f_valor es num�rico o caracter
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z%
# Autor       : 4js 
# Fecha       : %H%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

-- Verifica hacia donde se est� moviendo el CURSOR
FUNCTION get_desplazac()
   DEFINE
      direccion CHAR(3)

   IF FGL_LASTKEY() = FGL_KEYVAL("LEFT") OR FGL_LASTKEY() = FGL_KEYVAL("UP") THEN
     	LET direccion = "izq"
   ELSE
     	IF FGL_LASTKEY() = FGL_KEYVAL("RIGHT") OR FGL_LASTKEY() = FGL_KEYVAL("DOWN") OR
   		FGL_LASTKEY() = FGL_KEYVAL("ACCEPT") OR FGL_LASTKEY() = FGL_KEYVAL("TAB") OR
			FGL_LASTKEY() = 13 THEN
        	LET direccion = "der"
     	END IF
   END IF
   RETURN direccion
END FUNCTION

-- Verifica si f_valor es num�rico o caracter
FUNCTION veri_tipodato(f_valor)
	DEFINE 
		f_valor 	VARCHAR(100,0),
		contador,
		contador2 SMALLINT,
		caracter1,
		tipo_dato CHAR(1)

	LET tipo_dato 	= "N"  -- NUMERICO

	FOR contador = 1 TO LENGTH(f_valor)
		FOR contador2 = 0 TO 9 -- Ciclo para comparar, si el dato en esta posici�n es num�rico o NO
			LET caracter1 = contador2
			IF f_valor[contador,contador] = caracter1 THEN
				EXIT FOR
			ELSE	
				IF contador2 = 9 THEN
					LET tipo_dato = "C"	-- CARACTER
					EXIT FOR
				END IF
			END IF
			IF tipo_dato = "C" THEN
				EXIT FOR
			END IF
		END FOR
	END FOR
	RETURN tipo_dato
END FUNCTION
