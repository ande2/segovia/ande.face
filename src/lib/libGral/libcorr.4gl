DATABASE face

FUNCTION GetCorre( lTipo, lAnio  )
DEFINE
   lTipo       SMALLINT,
   lAnio       SMALLINT,
   lCorre      INTEGER

   IF lAnio = 0 THEN
      LET lAnio = YEAR ( TODAY )
   END IF

   SELECT   corr_num
      INTO  lCorre
      FROM  mcorr
      WHERE mcorr.corr_id     = lTipo
      AND   mcorr.corr_anio   = lAnio
      
   IF STATUS = NOTFOUND THEN
      LET lCorre = 1
      INSERT 
         INTO  mcorr    ( corr_anio, corr_id, corr_num )
         VALUES         ( lAnio, lTipo, lCorre )
   END IF

   UPDATE   mcorr
      SET   corr_num = corr_num + 1
      WHERE corr_id     = ltipo
      AND   corr_anio   = lAnio
   RETURN lCorre
END FUNCTION
