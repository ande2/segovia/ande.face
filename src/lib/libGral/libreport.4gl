FUNCTION configureOutput()
    IF NOT fgl_report_loadCurrentSettings(NULL) THEN
        RETURN NULL
    END IF
    --CALL fgl_report_selectLogicalPageMapping("multipage")
    --CALL fgl_report_configureMultipageOutput(2, 4, TRUE)
    --CALL fgl_report_setPageMargins("4.5cm","0.5cm","2.5cm","0.5cm")
    --CALL fgl_report_selectDevice("XLS")
    CALL fgl_report_selectDevice(getPreviewDevice())
    CALL fgl_report_selectPreview(TRUE)
    RETURN fgl_report_commitCurrentSettings()
END FUNCTION

FUNCTION getPreviewDevice()
    DEFINE fename String
    CALL ui.interface.frontcall("standard", "feinfo", ["fename"],[fename])
    IF fename == "Genero Desktop Client" THEN
        RETURN "SVG"
    ELSE
        RETURN "PDF"
    END IF        
END FUNCTION
