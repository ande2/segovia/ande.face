FUNCTION revisa_caracter ( Dato, lFind, lReplace )
DEFINE i SMALLINT
DEFINE Dato CHAR(100)
DEFINE retorna VARCHAR(100)
DEFINE lFind CHAR(1)
DEFINE lReplace CHAR(1)

   --IF lFind IS NULL THEN LET lFind = "'" END IF
   --IF lReplace IS NULL THEN LET lReplace = "`" END IF
   LET Retorna = ""
   FOR i = 1 TO LENGTH(Dato)
      IF Dato[i] = lFind THEN
         IF i = 1 THEN
            LET Retorna = lReplace CLIPPED
         ELSE
            LET Retorna = Retorna, lReplace CLIPPED
         END IF
      ELSE
         IF i = 1 then
            LET Retorna = Dato[i] CLIPPED
         ELSE
            LET Retorna = Retorna, Dato[i] CLIPPED
         END if
      END IF
   END FOR
   RETURN Retorna
END FUNCTION