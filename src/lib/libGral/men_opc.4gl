##############################################################################
#  Funcion: %M%
#  Descripcion: funcion para determinar opciones de mantenimiento por usuario 
#  Funciones: 
#
#
#
#  Parametros
#  recibidos:
#  Parametros
#  devueltos:
#
#  SCCS Id No.: %Z% %W%
#  Autor: Giovanni Yanes
#  Fecha: %H% %T%
#  Path: %P%
#
#  Control de cambios
#
#  Programador       Fecha                Descripcion de la modificacion
#
############################################################################## 

FUNCTION men_opc(usuario,programa,dbperm)
DEFINE 
   usuario,
   programa             VARCHAR(30,1), -- captura usuario y programa para la seleccion
   ord_num,
   cod_opc              SMALLINT, 
   vopciones            CHAR(255),
   permisos             SMALLINT,
   dbperm               VARCHAR(40),
   QueryBuffer          VARCHAR(1000)

   LET vopciones = NULL
   LET ord_num = NULL
   LET cod_opc = NULL
   LET permisos = NULL

   --Armar query de busqueda
   LET QueryBuffer = "SELECT  b.prog_ord, b.des_cod, d.linkprop",
                     " FROM   face:mprog a, ",
                     "        face:dprogopc b, ",
                     "        face:adm_usu c, ",
                     "        OUTER(", dbperm CLIPPED, ":maccopcusu d) ",
                     " WHERE  c.usu_nomunix  =  '", usuario CLIPPED, "'", 
                     " AND    a.prog_nom     =  '", programa CLIPPED, "'",
                     " AND    b.prog_id      = a.prog_id ",
                     " AND    b.linkprop     = d.linkprop ",
                     " AND    d.usu_id       = c.usu_id ",
                     " ORDER  BY b.prog_ord "

                        
   -- verificar permisos del usuario sobre las opciones del menu
   PREPARE QPerm FROM QueryBuffer
   DECLARE opcion_usuario CURSOR FOR QPerm
   FOREACH opcion_usuario INTO ord_num,cod_opc,permisos
      IF permisos IS NULL THEN
         LET vopciones = vopciones CLIPPED,"0"  -- si no tiene permisos pone cero
      ELSE
         LET vopciones = vopciones CLIPPED,"1"  -- si tiene permisos pone uno
      END IF
      LET permisos = NULL
   END FOREACH
   FREE opcion_usuario
   RETURN vopciones
END FUNCTION