##############################################################################
#  Funcion: %M%
#  Descripcion: funcion para determinar opciones de mantenimiento por usuario 
#  Funciones: 
#
#
#
#  Parametros
#  recibidos:
#  Parametros
#  devueltos:
#
#  SCCS Id No.: %Z% %W%
#  Autor: Giovanni Yanes
#  Fecha: %H% %T%
#  Path: %P%
#
#  Control de cambios
#
#  Programador       Fecha                Descripcion de la modificacion
#
############################################################################## 

FUNCTION men_opc1(usuario,programa)
DEFINE 
	usuario,programa CHAR(15), -- captura usuario y programa para la seleccion
	ord_num,cod_opc SMALLINT, 
	vopciones CHAR(255),
	permisos SMALLINT

LET vopciones = NULL
LET ord_num = NULL
LET cod_opc = NULL
LET permisos = NULL


-- verificar permisos del usuario sobre las opciones del menu
DECLARE opcion_usuario CURSOR FOR
SELECT dprogopc.prog_ord,dprogopc.des_cod,maccopcusu.linkprop
FROM mprog,dprogopc,mae_usu, OUTER(maccopcusu)
WHERE mae_usu.usu_nomunix = usuario
AND mprog.prog_nom = programa
AND dprogopc.prog_id = mprog.prog_id
AND dprogopc.linkprop = maccopcusu.linkprop
AND maccopcusu.usu_id = mae_usu.usu_id
order by dprogopc.prog_ord

FOREACH opcion_usuario INTO ord_num,cod_opc,permisos

	IF permisos IS NULL THEN
		LET vopciones = vopciones CLIPPED,"0"  -- si no tiene permisos pone cero
	ELSE
		LET vopciones = vopciones CLIPPED,"1"  -- si tiene permisos pone uno
	END IF

	LET permisos = NULL

END FOREACH

FREE opcion_usuario

RETURN vopciones

END FUNCTION

