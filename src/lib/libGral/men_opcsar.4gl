################################################################################
# Programa    : men_opcsar.4gl
# Funcion     :
# Descripcion : Regresa los permisos del usuario sobre las opciones del programa
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Paramscros
# Devueltos   :
#
# SCCS id No. :
# Autor       :
# Fecha       :
# Path        :
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################
FUNCTION men_opcsar(usuario,programa,dbname)
DEFINE
	usuario,programa	CHAR(15),
	ord_num,cod_opc SMALLINT,
	vopciones CHAR(255),
	permisos	SMALLINT, dbname CHAR(20), 
	lsql CHAR(1000)

-- inicializacion de variables
LET vopciones = NULL
LET ord_num = NULL
LET cod_opc = NULL
LET permisos = NULL

-- verificacion de permisos del usuario sobre las opciones del programa

LET lsql ="SELECT varios:dprogopc.prog_ord,varios:dprogopc.des_cod,maccopcusu.linkprop",
    " FROM varios:mprog,varios:dprogopc,",dbname CLIPPED, ":adm_usu adm_usu, OUTER(maccopcusu)",
	 " WHERE adm_usu.usu_nomunix = '",usuario CLIPPED, "'",
	 " AND varios:mprog.prog_nom = '",programa CLIPPED,"'",
	 " AND varios:dprogopc.prog_id = varios:mprog.prog_id",
	 " AND varios:dprogopc.linkprop = maccopcusu.linkprop",
	 " AND maccopcusu.usu_id = adm_usu.usu_id",
	 " order by varios:dprogopc.prog_ord"

PREPARE opcioness FROM lsql 
	 
DECLARE opcion_usuario CURSOR FOR opcioness

FOREACH opcion_usuario INTO ord_num,cod_opc --,permisos
	IF cod_opc IS NULL THEN
		LET vopciones = vopciones CLIPPED,"0" -- si no tiene permisos pone cero
	ELSE
		LET vopciones = vopciones CLIPPED,"1" -- si tiene permisos pone uno
	END IF

	LET cod_opc = NULL
END FOREACH

FREE opcion_usuario

RETURN vopciones

END FUNCTION

################################################################################
#	programa    :	busca_acceso al sar
#	Descripcion	:	funciones permite  acceso por programa  a usuarios dbname 
#	Funciones	:  aaccesosar()
#
#	Parametros	
#		Recibe	:
#		Devuelve	: g_emp_cod, g_usr_cod,vempr_nomlog,nombre_departamento,g_usr_des,existe
#
#	SCCS id No  : %Z% %W%
#	Autor			: eAlvarez
#	Fecha			: 02/03/2007
#	Path			: %P%
#
#	Control de cambios
#
#	Programador			Fecha			Descripcion de la modificacion
#
################################################################################

FUNCTION accesosar(vbase1,vnomlogo)
DEFINE vemp_id  integer, lsql CHAR(1000),vnomlogo CHAR(30),
       emp_cod, usr_cod INTEGER, vbase CHAR(20),
       lsq char(1000), usuario CHAR(15),vbase1 CHAR(10),
		 vdepnom CHAR(60), vnom_usu CHAR(40), existe SMALLINT 

   SELECT empr_id,empr_nomlog,empr_db INTO emp_cod,vnomlogo,vbase
         FROM  varios:mempr WHERE empr_db=vbase1 
   IF STATUS = NOTFOUND THEN
          LET existe = FALSE  
   ELSE
          LET existe = TRUE 
   END IF
   IF existe = FALSE THEN  
     LET existe = FALSE 
     RETURN 0,0,0,0,0, existe 
   END IF 
   LET usuario = FGL_GETENV("LOGNAME")
   LET lsq = "select a.usu_id,a.usu_nom, b.dpe_desc from ",vbase clipped , ":adm_usu a, ",
            vbase CLIPPED,":mdepemp b  where a.dpe_id=b.dpe_id AND  a.usu_nomunix= '",usuario clipped,"'"
   PREPARE reg_ac FROM lsq
   DECLARE acc_eso cursor for reg_ac
   open acc_eso
   FETCH acc_eso into usr_cod,vnom_usu,vdepnom  -- asigno el codigo de la empresa ala que pertenece el usuario
   CLOSE acc_eso
   IF emp_cod IS NOT NULL AND usr_cod IS NOT null THEN 
         LET existe = TRUE
         RETURN emp_cod, usr_cod, vnomlogo, vdepnom, vnom_usu, existe
   ELSE 
			LET existe = FALSE
         RETURN 0,0,0,0,0, existe 
   END IF

END FUNCTION 

FUNCTION acceso1(vbase1,vnomlogo)
DEFINE vemp_id  integer, lsql CHAR(1000),vnomlogo CHAR(30),
       emp_cod, usr_cod INTEGER, vbase CHAR(20),existe,vusu_tip SMALLINT,
       lsq char(1000), usuario CHAR(15),vbase1 CHAR(10),
       vdepnom CHAR(60), vdep_id integer,vnomemp CHAR(40), 
		 vusu_nom CHAR(30)

  SELECT empr_id,empr_nomlog,empr_db,empr_nom INTO emp_cod,vnomlogo,vbase, vnomemp
        FROM  varios:mempr WHERE empr_db=vbase1
		  IF STATUS = NOTFOUND THEN
	          LET existe = FALSE
		  ELSE
             LET existe = TRUE
		  END IF
		  IF existe = FALSE THEN
		     LET existe = FALSE
		     RETURN 0,0,0,0,0,0,0,0, existe
		  END IF

		  LET usuario = FGL_GETENV("LOGNAME")
		  LET lsq = "select a.usu_id, a.usu_nom,b.dpe_desc, a.dpe_id ,a.usu_tip from ",vbase clipped , ":adm_usu a, ",
                  vbase CLIPPED,":mdepemp b  where a.dpe_id=b.dpe_id AND  a.usu_nomunix= '",usuario clipped,"'"
		  PREPARE reg_ac2 FROM lsq
		  DECLARE acc_eso2 cursor for reg_ac2
		  open acc_eso2
		  fetch acc_eso2 INTO usr_cod,vusu_nom,vdepnom,vdep_id,vusu_tip  -- asigno el codigo de la empresa ala que pertenece el usuario
		  close acc_eso2
		  IF vusu_tip IS NULL THEN 
		      LET vusu_tip=0
		  END IF 
	     IF emp_cod IS NOT NULL AND usr_cod IS NOT NULL THEN
         	LET existe = TRUE
	         RETURN emp_cod, usr_cod, vnomlogo, vdepnom, vdep_id,vnomemp,vusu_nom,vbase,vusu_tip,existe
   	  ELSE
            LET existe = FALSE
            RETURN 0,0,0,0,0,0,0,0,0,existe
        END IF
		  
END FUNCTION

FUNCTION acceso2(vbase1,vnomlogo)
DEFINE vemp_id  integer, lsql CHAR(1000),vnomlogo CHAR(30),
       emp_cod, usr_cod INTEGER, vbase CHAR(20),existe SMALLINT,
       lsq char(1000), usuario CHAR(15),vbase1 CHAR(10),
       vdepnom CHAR(60), vdep_id integer,vnomemp CHAR(40)

  SELECT empr_id,empr_nomlog,empr_db,empr_nom INTO emp_cod,vnomlogo,vbase, vnomemp
        FROM  varios:mempr WHERE empr_db=vbase1
        IF STATUS = NOTFOUND THEN
             LET existe = FALSE
        ELSE
             LET existe = TRUE
        END IF
        IF existe = FALSE THEN
           LET existe = FALSE
           RETURN 0,0,0,0,0,0,0,existe
        END IF

        LET usuario = FGL_GETENV("LOGNAME")
        LET lsq = "select a.usu_id, b.dpe_desc, a.dpe_id from ",vbase clipped , ":adm_usu a, ",
                  vbase CLIPPED,":mdepemp b  where a.dpe_id=b.dpe_id AND  a.usu_nomunix= '",usuario clipped,"'"
        PREPARE reg_ac5 FROM lsq
        DECLARE acc_eso5 cursor for reg_ac5
        open acc_eso5
        fetch acc_eso5 into usr_cod,vdepnom,vdep_id  -- asigno el codigo de la empresa ala que pertenece el usuario
        close acc_eso5
        IF emp_cod IS NOT NULL AND usr_cod IS NOT null THEN
            LET existe = TRUE
            RETURN emp_cod, usr_cod, vnomlogo, vdepnom, vdep_id,vnomemp, vbase,existe
        ELSE
            LET existe = FALSE
            RETURN 0,0,0,0,0,0,0, existe
        END IF
END FUNCTION


################################################################################
#  programa    :  lib_random 
#  Descripcion :  funcion permite generar un correlativo por usuario
#  Funciones   :  lib_randomr()
#
#  Parametros
#     Recibe   :
#     Devuelve : p_seed
#
#
##################################################################

FUNCTION lib_Random(p_seed)

    DEFINE
        p_seed,s_random   INTEGER

    DISPLAY "recibe ",p_seed
    #### Is this a seed ####
    IF p_seed > 0  THEN
        LET s_random = p_seed
	
    END IF

    #### Next sequence ####
    LET s_random = (s_random * 25173 + 13849) MOD 65536

    RETURN s_random

END FUNCTION
################################################################################
#  programa    :  borra_tmp()
#  Descripcion :  funcion que elimina archivo temporal en el directorio temp de SAR
#  Funciones   :  borra_tmp()
#
#  Parametros
#     Recibe   :lnfile ( nombre y directorio del archivo )
#     Devuelve : none
#
#
##################################################################
function borra_tmp(rmFile) 
define rmFile string 
      if rmFile.getLength()>0 then
          LET rmFile="rm "||rmFile
          RUN rmFile
      END IF

END FUNCTION

