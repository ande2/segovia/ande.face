### RUTINA PARA DESPLEGAR ERROR ###
globals "glob_tran.4gl"

FUNCTION mensaje01(no_err)
	DEFINE
		enter		CHAR(1),
		no_err	INTEGER
	
	OPTIONS
		PROMPT LINE LAST

	OPEN WINDOW venta10 AT 10,10 WITH 7 ROWS, 50 COLUMNS ATTRIBUTE(BORDER)
		DISPLAY "*** ERROR ***" at 01,18 attribute(bold, blink)

		DISPLAY "El Error No. ", no_err using "&&&&&&&, Ha ocurrido, por favor"
						AT 03,01
		DISPLAY "anote este mensaje y reportelo al deptartamento"
						AT 04,01
		DISPLAY	"de Informatica  G R A C I A S ..."
						AT 05,01
		PROMPT "ENTER ... Para Continuar " FOR enter	
		EXIT PROGRAM
	CLOSE WINDOW venta10

	OPTIONS
		PROMPT LINE FIRST
END FUNCTION

### FUNCION que Verifica si es fin de semana ###
{
function fin_semana(fecha_semana)
	define
		fecha_semana date
	
	if weekday(fecha_semana) = 0 or weekday(fecha_semana) = 6 then
		call emite_l2(15,"No es permitido ingresar documentos los fines de semana")
		return true
	end if
	return false
end function
}
