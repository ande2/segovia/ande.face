FUNCTION mesp(fechar)          
	DEFINE fechar date,
		nos smallint,
		nom_mes char(11)
	LET nos = month(fechar)  
	CASE(nos)
		WHEN 1 LET nom_mes = "ENERO"
		WHEN 2 LET nom_mes = "FEBRERO"
		WHEN 3 LET nom_mes = "MARZO"
		WHEN 4 LET nom_mes = "ABRIL"
		WHEN 5 LET nom_mes = "MAYO"
		WHEN 6 LET nom_mes = "JUNIO"
		WHEN 7 LET nom_mes = "JULIO"
		WHEN 8 LET nom_mes = "AGOSTO"
		WHEN 9 LET nom_mes = "SEPTIEMBRE"
		WHEN 10 LET nom_mes = "OCTUBRE"
		WHEN 11 LET nom_mes = "NOVIEMBRE"
		WHEN 12 LET nom_mes = "DICIEMBRE"
	END CASE
	RETURN nom_mes
END FUNCTION

FUNCTION mesn(nos)          
	DEFINE nos smallint,
		nom_mes char(11)
	CASE(nos)
		WHEN 1 LET nom_mes = "ENERO"
		WHEN 2 LET nom_mes = "FEBRERO"
		WHEN 3 LET nom_mes = "MARZO"
		WHEN 4 LET nom_mes = "ABRIL"
		WHEN 5 LET nom_mes = "MAYO"
		WHEN 6 LET nom_mes = "JUNIO"
		WHEN 7 LET nom_mes = "JULIO"
		WHEN 8 LET nom_mes = "AGOSTO"
		WHEN 9 LET nom_mes = "SEPTIEMBRE"
		WHEN 10 LET nom_mes = "OCTUBRE"
		WHEN 11 LET nom_mes = "NOVIEMBRE"
		WHEN 12 LET nom_mes = "DICIEMBRE"
	END CASE
	RETURN nom_mes
END FUNCTION
