###############################################################################t
# Funcion     : %M%
# Descripcion : conversion de numeros a letras sin la palabra quetzales
# Funciones	  :
#	
#	
#	
# Parametros	
# Recibidos   :
# Paramentros
# Devueltos	  :
#
# SCCS id No. : %Z% %W%
# Autor		  : Adaptado por Giovanni Yanes
# Fecha		  : %H% %T%
# Path		  : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################


FUNCTION num_letra(valor)
DEFINE
   f_tentxt ARRAY[8] OF RECORD
      tentxt    CHAR(10)
   END RECORD,

   f_unittxt ARRAY[20] OF RECORD
      unittxt   CHAR(11)
   END RECORD,

   f_gruptxt ARRAY[3] OF RECORD
      gruptxt   CHAR(7)
   END RECORD,

   f_letras     CHAR(100),
   f_cox        CHAR(20),
   f_deci       DECIMAL(2,0),
   valor        DECIMAL(10,2),
   f_numero, f_stp, f_divisor, f_grup2,f_grup, f_unit, f_ten, f_kk  INTEGER,
   f_deci2      CHAR(1)

LET f_tentxt[1].tentxt = "VEINTI"
LET f_tentxt[2].tentxt = "TREINTA "
LET f_tentxt[3].tentxt = "CUARENTA "
LET f_tentxt[4].tentxt = "CINCUENTA "
LET f_tentxt[5].tentxt = "SESENTA "
LET f_tentxt[6].tentxt = "SETENTA "
LET f_tentxt[7].tentxt = "OCHENTA "
LET f_tentxt[8].tentxt = "NOVENTA "

LET f_unittxt[1].unittxt = "UN "
LET f_unittxt[2].unittxt = "DOS "
LET f_unittxt[3].unittxt = "TRES "
LET f_unittxt[4].unittxt = "CUATRO "
LET f_unittxt[5].unittxt = "CINCO "
LET f_unittxt[6].unittxt = "SEIS "
LET f_unittxt[7].unittxt = "SIETE "
LET f_unittxt[8].unittxt = "OCHO "
LET f_unittxt[9].unittxt = "NUEVE "
LET f_unittxt[10].unittxt = "DIEZ "
LET f_unittxt[11].unittxt = "ONCE "
LET f_unittxt[12].unittxt = "DOCE "
LET f_unittxt[13].unittxt = "TRECE "
LET f_unittxt[14].unittxt = "CATORCE "
LET f_unittxt[15].unittxt = "QUINCE "
LET f_unittxt[16].unittxt = "DIECISEIS "
LET f_unittxt[17].unittxt = "DIECISIETE "
LET f_unittxt[18].unittxt = "DIECIOCHO "
LET f_unittxt[19].unittxt = "DIECINUEVE "
LET f_unittxt[20].unittxt = "VEINTE "

LET f_gruptxt[1].gruptxt = "MILLON "
LET f_gruptxt[2].gruptxt = "MIL "
LET f_gruptxt[3].gruptxt = " "

LET f_numero = valor
LET f_letras = NULL
LET f_stp = 1
LET f_divisor = 1000000
LET f_grup = 0
LET f_unit = 0
LET f_ten  = 0

WHILE f_stp <= 3
   LET f_grup = (f_numero / f_divisor)
   LET f_grup = f_grup * f_divisor
   LET f_numero = f_numero - f_grup
   LET f_grup = f_grup / f_divisor
   IF f_grup > 0 THEN
      LET f_kk = f_grup
      LET f_ten = 0
      IF f_grup > 99 THEN
         LET f_unit = (f_grup / 100)
         LET f_grup = f_grup - (f_unit * 100)
         LET f_cox = f_unittxt[f_unit].unittxt CLIPPED,"CIENTOS "
         IF f_unit = 1 THEN
            IF f_grup = 0 THEN
               LET f_cox = "CIEN "
            ELSE 
               LET f_cox = "CIENTO "
            END IF
         END IF
         IF f_unit = 5 THEN
            LET f_letras = f_letras CLIPPED," ","QUINIENTOS "
         ELSE 
            IF f_unit = 7 THEN
               LET f_letras = f_letras CLIPPED," ","SETECIENTOS "
            ELSE 
               IF f_unit = 9 THEN
                  LET f_letras = f_letras CLIPPED," ","NOVECIENTOS "
               ELSE 
                  LET f_letras = f_letras CLIPPED," ",f_cox
               END IF
            END IF
         END IF
      END IF
		DISPLAY "FGRUP: ",f_grup
      IF f_grup > 30 THEN
         LET f_ten = (f_grup / 10)
         LET f_grup2 = f_grup 
         LET f_grup = f_grup - (f_ten * 10)
         LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
         IF ((f_grup > 0) AND (f_letras IS NOT NULL)) THEN
            LET f_letras = f_letras CLIPPED," ","Y "
         END IF
      ELSE 
         IF f_grup > 20 THEN
            LET f_ten = (f_grup / 10)
            LET f_grup2 = f_grup
            LET f_grup = f_grup - (f_ten * 10)
            LET f_letras = f_letras CLIPPED," ",f_tentxt[f_ten-1].tentxt
			ELSE
            LET f_grup2 = f_grup
         END IF
      END IF

      IF f_grup > 0 THEN
         IF f_ten = 2 THEN
            LET f_letras = f_letras CLIPPED,f_unittxt[f_grup].unittxt
         ELSE 
            LET f_letras = f_letras CLIPPED," ",f_unittxt[f_grup].unittxt
         END IF
      END IF
      LET f_letras = f_letras CLIPPED," ",f_gruptxt[f_stp].gruptxt
		display " f_stp : ",f_stp
		display " f_grup: ",f_grup
      IF ((f_stp = 1) AND (f_grup2 > 1)) THEN
         LET f_letras = f_letras CLIPPED,"ES "
      END IF
		LET f_grup = f_grup2
   END IF
   LET f_stp = f_stp + 1
   LET f_divisor = f_divisor /1000
END WHILE

LET f_numero = valor
LET f_deci = (valor - f_numero)*100
IF valor >= 2 THEN
   IF f_deci = 0 THEN
      LET f_cox = " EXACTOS"
   ELSE 
      LET f_cox = " CON ",f_deci,"/100"
   END IF
   IF f_deci > 0 AND f_deci < 9 THEN
      LET f_deci2 = f_deci
      LET f_cox = " CON 0",f_deci2,"/100" 
   END IF 
   LET f_letras = "*",f_letras CLIPPED," ",f_cox CLIPPED," *"
ELSE 
   IF valor < 1 THEN
      IF f_deci > 1 THEN
         LET f_letras = "* ",f_deci," CENTAVOS *"
      ELSE 
         LET f_letras = "* ",f_deci," CENTAVO *"
      END IF
   ELSE 
      IF f_deci = 0 THEN
         LET f_cox = " EXACTO"
      ELSE 
         LET f_cox = " CON ",f_deci,"/100"
      END IF
      IF f_deci > 0 AND f_deci < 9 THEN
         LET f_deci2 = f_deci
         LET f_cox = " CON 0",f_deci2,"/100" 
      END IF 
      LET f_letras="* ",f_letras CLIPPED," ",f_cox CLIPPED," *"
   END IF
END IF
RETURN f_letras
SLEEP 3
END FUNCTION
