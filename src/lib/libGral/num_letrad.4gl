## Empresa: Unipharm Guatemala, S.A.
## Archivo: num_letrad.4gl
##   Fecha: Septiembre 3, 2010
##   Razon: rutina de numero a letras en dolares
##
## Modificaciones
##  Quien:
##  Fecha:
##  Razon:
##


FUNCTION num_letraD(numero)
DEFINE
  numero      DECIMAL(12,2),
  num_st      CHAR(190),
  num_st_dec  CHAR(12),
  num_st_ent  CHAR(9),
  num_st_frac CHAR(2),
  sec         DECIMAL(3,0),
  uni_st,
  dec_st,
  cen_st,
  centavos_st CHAR(50)

  LET num_st_dec  = numero USING "#########.##"
  LET num_st_ent  = num_st_dec[1,9]
  LET num_st_frac = num_st_dec[11,12]
  LET sec = num_st_ent[1,3]
  LET cen_st = ""
  LET dec_st = ""
  LET uni_st = ""

  IF sec <> 0 THEN
    IF sec > 1 THEN
      CALL undece_d(sec,uni_st,dec_st,cen_st) RETURNING uni_st,dec_st,cen_st
      LET num_st = cen_st CLIPPED, " ", dec_st CLIPPED, " ",uni_st CLIPPED,
                   " MILLONES"
    ELSE
      CALL undece_d(sec,uni_st,dec_st,cen_st) RETURNING uni_st,dec_st,cen_st
      LET num_st = cen_st CLIPPED, " ", dec_st CLIPPED, " ",uni_st CLIPPED,
                   " MILLON"
    END IF
  END IF

  LET sec = num_st_ent[4,6]
  LET cen_st = ""
  LET dec_st = ""
  LET uni_st = ""

  IF sec <> 0 THEN
    CALL undece_d(sec,uni_st,dec_st,cen_st) RETURNING uni_st,dec_st,cen_st
    LET num_st = num_st CLIPPED, " ", cen_st CLIPPED, " ", dec_st CLIPPED,
                 " ", uni_st CLIPPED, " MIL"
  END IF

  LET sec = num_st_ent[7,9]
  LET cen_st = ""
  LET dec_st = ""
  LET uni_st = ""

  IF sec <> 0 THEN
    CALL undece_d(sec,uni_st,dec_st,cen_st) RETURNING uni_st,dec_st,cen_st
    LET num_st = num_st CLIPPED, " ", cen_st CLIPPED, " ",
                 dec_st CLIPPED, " ", uni_st CLIPPED
  END IF

  LET sec = num_st_frac

  IF numero > 0.99 THEN
    IF sec = 0 THEN
      IF numero < 2 THEN LET centavos_st = "EXACTO"
      ELSE LET centavos_st = "EXACTOS"
       END IF
    ELSE
      IF numero < 2 THEN LET centavos_st = "CON ", num_st_frac CLIPPED, "/100"
      ELSE LET centavos_st = "CON ", num_st_frac CLIPPED, "/100"
      END IF
    END IF
    LET num_st = num_st CLIPPED, " DOLARES ", centavos_st CLIPPED
  ELSE
    LET cen_st = ""
    LET dec_st = ""
    LET uni_st = ""
    LET sec = num_st_frac
    CALL undece_d(sec,uni_st,dec_st,cen_st) RETURNING uni_st,dec_st,cen_st
    LET num_st = cen_st CLIPPED, " ", dec_st CLIPPED, " ",
                 uni_st CLIPPED, " CENTAVOS"
  END IF

  RETURN num_st
END FUNCTION

FUNCTION undece_d(sec,uni_st,dec_st,cen_st)
DEFINE
  uni_st,
  dec_st,
  cen_st,
  centavos_st   CHAR(50),
  sec,
  cen,
  dec,
  uni           DECIMAL(3,0)

  LET cen = sec - (sec mod 100)
  LET dec = ( sec mod 100 ) - ( ( sec mod 100 ) mod 10 )
  LET uni = sec - cen - dec
  IF ( (dec + uni) > 10 and (dec + uni) <= 15 ) THEN
    LET dec = dec + uni
    LET uni = 0
  END IF

  IF ( cen >= 100 and cen <= 900 ) THEN
    LET cen_st = letras_num_d(cen)
    IF ( (dec + uni) <> 0 and cen = 100 ) THEN LET cen_st=cen_st CLIPPED, "TO "
    END IF
  END IF

  IF ( dec >= 10 AND dec <= 90 ) THEN LET dec_st = letras_num_d(dec) END IF

  IF ( uni >= 1 AND uni <= 15 ) THEN
    LET uni_st = letras_num_d(uni)
    IF dec <> 0 THEN LET uni_st = " Y ", uni_st CLIPPED END IF
  END IF

  RETURN uni_st,dec_st,cen_st
END FUNCTION

FUNCTION letras_num_d(x)
DEFINE x DECIMAL (3,0), x2 CHAR(13)

  CASE x
    WHEN 1 LET x2 = "UN"
    WHEN 2 LET x2 = "DOS"
    WHEN 3 LET x2 = "TRES"
    WHEN 4 LET x2 = "CUATRO"
    WHEN 5 LET x2 = "CINCO"
    WHEN 6 LET x2 = "SEIS"
    WHEN 7 LET x2 = "SIETE"
    WHEN 8 LET x2 = "OCHO"
    WHEN 9 LET x2 = "NUEVE"
    WHEN 10 LET x2 = "DIEZ"
    WHEN 11 LET x2 = "ONCE"
    WHEN 12 LET x2 = "DOCE"
    WHEN 13 LET x2 = "TRECE"
    WHEN 14 LET x2 = "CATORCE"
    WHEN 15 LET x2 = "QUINCE"
    WHEN 20 LET x2 = "VEINTE"
    WHEN 30 LET x2 = "TREINTA"
    WHEN 40 LET x2 = "CUARENTA"
    WHEN 50 LET x2 = "CINCUENTA"
    WHEN 60 LET x2 = "SESENTA"
    WHEN 70 LET x2 = "SETENTA"
    WHEN 80 LET x2 = "OCHENTA"
    WHEN 90 LET x2 = "NOVENTA"
    WHEN 100 LET x2 = "CIEN"
    WHEN 200 LET x2 = "DOSCIENTOS"
    WHEN 300 LET x2 = "TRESCIENTOS"
    WHEN 400 LET x2 = "CUATROCIENTOS"
    WHEN 500 LET x2 = "QUINIENTOS"
    WHEN 600 LET x2 = "SEISCIENTOS"
    WHEN 700 LET x2 = "SETECIENTOS"
    WHEN 800 LET x2 = "OCHOCIENTOS"
    WHEN 900 LET x2 = "NOVECIENTOS"
  END CASE

  RETURN x2 CLIPPED
END FUNCTION

