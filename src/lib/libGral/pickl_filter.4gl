################################################################################
# Funcion     : %M%
# Descripcion : Funcion para picklist 
# Funciones   : picklist01(Title1,Title2,Field1,Field2,Tabname,Condicion1
#               ,OrderNum,WROWPos,WCOLPos)
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Jorge Monterroso
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# Hcuj        Fri Feb  9 10:17:31 CST 2007 Se modific� para que en la picklist
#                                          incluyera un filtro
################################################################################

FUNCTION pickl_filter(Title1,Title2,Field1,Field2,Tabname,Condicion1,OrderNum,WROWPos,WCOLPos)
DEFINE Title1 CHAR(10),    -- Titles to display for the Fields
       Title2 CHAR(38),
       Field1,             -- Field names in the table 
       Field2,
       Tabname CHAR(50),   -- Table name to query from
       OrderNum  SMALLINT, -- Order by 1 or 2, a Number in a range of 1-2
       WROWPos,            -- Row and Col position for the window
       WCOLPos SMALLINT,
		 Condicion CHAR(1000),
		 Condicion1 CHAR(1000),
		 message01 char(80),
		 Salida SMALLINT, 
       Accepted,
       StackLimit,
       SetLimit,
       StackPos SMALLINT,
		 Centinela SMALLINT,
       TagPage CHAR(12),
       Tpages,             -- Total Pages in the Display Array
       Cpage SMALLINT,     -- Current Page in the Display Array
		 err_msg CHAR(80),
       sqlstmnt CHAR(1000), -- Char variable to contain the SQL statement
       PickArrRecord ARRAY[10] OF RECORD -- Record to contain the rows
              Field1 CHAR(20),
              Field2 CHAR(60) 
	    END RECORD,
       Indx1,              -- Used for the FOR Loop 
       Indx2   SMALLINT,   -- Index integer to point to arrays elements
       FieldToReturn CHAR(20), -- Informix converts automatically between
       FieldToReturn1 CHAR(60), -- Char and Numerics, so the candidate 
			                      -- datatype to hold the field1 result is CHAR.
       abre_cursor SMALLINT

LET int_flag = 0
LET abre_cursor = 0

WHENEVER ERROR CONTINUE

--Open the window at the position specified by the parameters Wxpos and Wypos
OPEN WINDOW w_ArrPickList AT  WROWPos,WCOLPos WITH FORM "pickl_filter"
ATTRIBUTES(FORM LINE 1)

WHENEVER ERROR STOP

-- For a 4GL statement like OPEN WINDOW, the STATUS global variable will be
-- used to check for errors.

IF (STATUS < 0 ) THEN -- The Window could not be opened, 
                      --the form does not exist
                      -- or the parameters WxPos, WyPos are wrong
   ERROR "Wrong values suppied to OpenWindow(PickList) ", 
		 	"-- Contact Programmer --"
   SLEEP 1
   RETURN -1,1,INT_FLAG
END IF

-- Change the ACCEPT key to RETURN, useful for display array (picklist)
OPTIONS
   ACCEPT KEY RETURN

-- Display the Titles  
DISPLAY BY NAME Title1, Title2

WHILE true 
	DISPLAY "Presione ENTER o ACEPTAR para realizar una    " AT 2,17
	DISPLAY "busqueda general o CANCELAR para salir        " AT 3,17

	INPUT BY NAME condicion
		BEFORE INPUT
      	CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
      	CALL fgl_dialog_setkeylabel("ACCEPT", "Aceptar")
	END INPUT

	-- arma la cadena de condicion, segun sea el caso
	CASE 
		WHEN LENGTH(condicion) = 0 AND LENGTH(condicion1) = 0
			LET condicion = Condicion1 CLIPPED, " ORDER BY ",  OrderNum
		WHEN LENGTH(condicion) = 0 AND LENGTH(condicion1) <> 0
			LET Condicion = 	Condicion1 CLIPPED, " ORDER BY ", OrderNum
		WHEN LENGTH(condicion) <> 0 AND LENGTH(condicion1) = 0
			LET Condicion = " ",	Condicion,  " ORDER BY ", OrderNum
		WHEN LENGTH(condicion) <> 0 AND LENGTH(condicion1) <> 0
			LET Condicion = 	Condicion1 CLIPPED, " AND ", 
									field2 CLIPPED, " MATCHES ", ASCII 34, "*",
									Condicion CLIPPED, "*",  ASCII 34, 
								 	" ORDER BY ", OrderNum
	END CASE

 	IF (INT_FLAG) THEN
	 	LET Centinela = 0
     	EXIT WHILE -- Exit Infinite Loop
  	END IF

	LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, " FROM ",Tabname CLIPPED, " WHERE ", Condicion 

	WHENEVER ERROR CONTINUE
	PREPARE EX_sqlst FROM sqlstmnt
	WHENEVER ERROR STOP

--If the user supplied wrong values
	IF (SQLCA.SQLCODE < 0) THEN
   	LET err_msg = err_get(SQLCA.SQLCODE)
   	ERROR err_msg
   	CALL errorlog(err_msg CLIPPED) 
   	ERROR "Par�metros incorrectos hacia funcion PickList -- Contacte al Programador --"
   	SLEEP 1
   	RETURN -1,1,INT_FLAG
	END IF

	DECLARE GenPickCur SCROLL CURSOR FOR EX_sqlst

-- Open the Cursor, prepare the buffer communication mechanism
	OPEN GenPickCur

   LET abre_cursor = 1
-- Initialize the Stack boundary

	LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED, " FROM ",Tabname CLIPPED, " WHERE ", Condicion 


-- 
	LET StackLimit = CountRows(Tabname CLIPPED, Condicion CLIPPED)
	LET StackPos   = 01

-- Initialize Stack Pointers
	IF StackLimit >= 10 THEN
   	LET SetLimit = 10
	ELSE
   	LET SetLimit = StackPos + 9
	END IF

-- Calculate How many pages
	LET Tpages = (StackLimit/10)
	IF (StackLimit MOD 10) != 0 THEN
   	LET Tpages = Tpages + 1
	END IF
	IF Tpages IS NULL OR Tpages = 0 THEN
		LET Tpages = 01
	END IF
	LET Cpage = 01
   IF Tpages  > 1 THEN
         LET message01 = "Ctrl-U = P\341gina Siguiente Ctrl-P = P\341gina Anteri
or"
         DISPLAY message01 AT 3,14
      ELSE
         LET  message01 = "
  "
         DISPLAY message01 AT 3,14
      END IF


	WHILE (TRUE) -- Infinite Loop
   	LET Indx2 = 01 -- Pointer to elements in the array

-- Initialize the Array
   	FOR indx1 = 1 TO 10
      	INITIALIZE PickArrRecord[indx1].* TO NULL
   	END FOR

-- Fill the array with rows from the table
   	FOR indx1 = StackPos TO SetLimit
      	FETCH ABSOLUTE indx1 GenPickCur INTO  PickArrRecord[indx2].*
      	IF (SQLCA.SQLCODE = NOTFOUND) THEN
          	EXIT FOR
     		END IF
      	LET Indx2 = Indx2 + 1
   	END FOR

		LET Salida = 0

		FOR indx1 = 1 TO 10
			IF PickArrRecord[indx1].Field1 IS NOT NULL THEN
				LET Centinela = 1
				LET Salida = Salida + 1
			END IF
		END FOR 

		IF Salida = 0 AND Tpages = 1 THEN
   		CALL box_valdato("No existen datos con estas condiciones.") 
			LET Centinela = 1
			EXIT WHILE
		END IF

   	LET Indx2 = Indx2 - 1

-- Tell Display Array How many elements were filled
   	CALL SET_COUNT(indx2)

   	LET Accepted = TRUE
		LET Centinela = 0

-- Print the information about pages
   	LET TagPage = "(",Cpage USING "<&&#",",",Tpages USING "<&&#",")"
   	DISPLAY BY NAME TagPage
      DISPLAY "ACEPTAR = Selecciona  CANCELAR = Sale         " AT 2,17
      DISPLAY "" AT 3,1
		DISPLAY "CONTROL-U = Pag.Ant.  CONTROL-P = Pag.Sig.    " AT 3,17 

-- Display the Array , and Turns Control over the User
   	DISPLAY ARRAY PickArrRecord TO pickArrRecord.*
      	BEFORE DISPLAY
				IF Tpages = 1 THEN
            	CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
            	CALL fgl_dialog_setkeylabel("ACCEPT", "Aceptar")
            	CALL fgl_dialog_setkeylabel("CONTROL-U", "")
            	CALL fgl_dialog_setkeylabel("CONTROL-P", "") 
					DISPLAY "                                              " AT 3,17
				ELSE
            	CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
            	CALL fgl_dialog_setkeylabel("ACCEPT", "Aceptar")
            	CALL fgl_dialog_setkeylabel("CONTROL-U", "Pagina anterior")
            	CALL fgl_dialog_setkeylabel("CONTROL-P", "Pagina siguiente") 
				END IF
			ON KEY(CONTROL-B)
				LET Centinela = 1
				EXIT DISPLAY 
      	ON KEY(CONTROL-U) -- Page Up
	  			LET Accepted = FALSE
         	IF (StackPos = 01) THEN
            	ERROR "Se encuentra en el inicio de la lista..."
	  			   LET Accepted = TRUE
         	ELSE
            	LET StackPos = Stackpos - 10
            	LET Cpage = Cpage - 1
            	EXIT DISPLAY
         	END IF
      	ON KEY(CONTROL-P) -- Page Down
         	LET Accepted = FALSE
         	IF ((StackPos+10) > StackLimit) THEN
            	ERROR "Se encuentra en el final de la lista..."
	  			   LET Accepted = TRUE
         	ELSE
            	LET StackPos = StackPos + 10
            	LET Cpage = Cpage + 1
            	EXIT DISPLAY
         	END IF
   	END DISPLAY

   	LET Indx1 = arr_curr()
   	LET FieldToReturn = PickArrRecord[Indx1].Field1  
   	LET FieldToReturn1 = PickArrRecord[Indx1].Field2

   	IF (INT_FLAG) OR (Accepted) THEN
		 	LET Centinela = 0
       	EXIT WHILE -- Exit Infinite Loop
   	END IF

   	LET SetLimit = StackPos + 9
   	IF (SetLimit > StackLimit) THEN
      	LET SetLimit = StackLimit
   	END IF 

	END WHILE

	IF Centinela = 0 THEN
		EXIT WHILE
	END IF
END WHILE

-- Close the Cursor
IF abre_cursor = 1 THEN
   CLOSE GenPickCur
END IF

-- The User Leaves the Display Array, change the ACCEPT key 
OPTIONS
    ACCEPT KEY ESCAPE

--Close The Window
CLOSE WINDOW w_ArrPickList

RETURN FieldToReturn, FieldToReturn1, INT_FLAG

END FUNCTION
