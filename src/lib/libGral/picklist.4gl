{
Program: picklist.4gl
Author : Carlos Santizo
Date   : 17-Dec-1997
Purpose: Provide pop-up window functionality, for unlimited number of
         rows, for any given table.

Function picklist receives the following parameters:
Title1, Title2   -- Titles to be displayed for field1 and field2
Field1, Field2   -- Specify the fields you want to retrieve and display
                    from the table.
                    NOTE: 1) picklist will return only Field1
                    to the calling function.
                          2) You can specify the conrcatenation operator for
                             field2, thus, if you want to fname and lname to
                             be displayed in the field2 position, you can
                             call picklist with the concatenation operation,
                             (ie. "customer_num", "fname || lname" ) , in this
                             case, customer_num will be the field1, and
                             fname + lname will be the field2 and will be
                             displayed in the second position.
                          3) If your table contains more than one field in
                             the primary key, then you can modify the code,
                             or you can specify the rowid field as field1,
                             (ie. "rowid","description" ) , in this case
                             rowid will be the value to be returned, and then
                             you can use that rowid to select the appropriate
                             key columns.
                    yo can achieve it by using "fname || lname" as the field2.
Tabname          -- Specify the table from the database
OrderNum         -- Specify a numeric value 1 or 2, indicates by which
                    field you want the data to be ordered by. if you specify
                    any number, then no order by will be performed.
WROWPos, WCOLPos -- Specify the initial position for the window to be
                    displayed.

}

###############################################################
FUNCTION picklist(Title1,Title2,Field1,Field2,Tabname,OrderNum,
                  WROWPos,WCOLPos)
###############################################################

DEFINE Title1 CHAR(10),  -- Titles to display for the Fields
       Title2 CHAR(38),
       Field1 CHAR(30),           -- Field names in the table 
       Field2 CHAR(100),
       Tabname CHAR(20), -- Table name to query from
       OrderNum  SMALLINT, -- Order by 1 or 2, a Number in a range of 1-2
       WROWPos,            -- Row and Col position for the window
       WCOLPos  SMALLINT,
		 Salida SMALLINT,
       Accepted,
       StackLimit,
       SetLimit,
       StackPos   SMALLINT,
       TagPage    CHAR(12),
       Tpages,              -- Total Pages in the Display Array
       Cpage      SMALLINT, -- Current Page in the Display Array
       sqlstmnt CHAR(300), -- Char variable to contain the SQL statement
       PickArrRecord ARRAY[10] OF RECORD -- Record to contain the rows
              Field1 CHAR(20),
              Field2 CHAR(100) 
	   END RECORD,
       Indx1,            -- Used for the FOR Loop 
       Indx2   SMALLINT, -- Index integer to point to arrays elements
       FieldToReturn CHAR(20), -- Informix converts automatically between
			       -- Char and Numerics, so the candidate 
			       -- datatype to hold the field1 result is CHAR.
       FieldToReturn1 CHAR(60)

	
	LET int_flag = 0
IF (OrderNum = 1 or OrderNum = 2) THEN

    LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
               " FROM ",Tabname CLIPPED, " ORDER BY ",OrderNum
ELSE
    LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",", Field2 CLIPPED,
               " FROM ",Tabname CLIPPED
END IF

WHENEVER ERROR CONTINUE
PREPARE EX_sqlst FROM sqlstmnt
WHENEVER ERROR STOP

--If the user supplied wrong values
IF (SQLCA.SQLCODE < 0) THEN
   ERROR "Parametros incorrectos hacia funcion PickList -- Contacte al Programador --"
   SLEEP 1
   RETURN -1,1,INT_FLAG
END IF

--Revisa si no hay datos
IF (SQLCA.SQLCODE <> 0) THEN
   ERROR "-- No se encotraron datos validos --"
   SLEEP 1
   RETURN -1,1,INT_FLAG
END IF 

DECLARE GenPickCur SCROLL CURSOR FOR EX_sqlst

WHENEVER ERROR CONTINUE
   --Open the window at the position specified by the parameters Wxpos and Wypos
   OPEN WINDOW w_ArrPickList AT  WROWPos,WCOLPos WITH FORM "picklist"
      ATTRIBUTES(BORDER,FORM LINE 1)
	CURRENT WINDOW IS w_ArrPickList
WHENEVER ERROR STOP

   -- For a 4GL statement like OPEN WINDOW, the STATUS global variable will be
   -- used to check for errors.

   IF (STATUS < 0 ) THEN -- The Window could not be opened, 
                         --the form does not exist
                         -- or the parameters WxPos, WyPos are wrong
      ERROR "Wrong values suppied to OpenWindow(PickList) ",
            "-- Contact Programmer --"
      SLEEP 1
     RETURN -1,1,INT_FLAG
   END IF

   -- Change the ACCEPT key to RETURN, useful for display array (picklist)
   OPTIONS
      ACCEPT KEY RETURN

   -- Display the Titles  
   DISPLAY BY NAME Title1, Title2


-- Open the Cursor, prepare the buffer communication mechanism
OPEN GenPickCur

-- Initialize the Stack boundary
LET StackLimit = CountRows(Tabname CLIPPED, "1=1")
LET StackPos   = 01
-- Initialize Stack Pointers
IF StackLimit >= 10 THEN
   LET SetLimit = 10
ELSE
   LET SetLimit = StackPos + 9
END IF

-- Calculate How many pages
LET Tpages = (StackLimit/10)
IF (StackLimit MOD 10) != 0 THEN
   LET Tpages = Tpages + 1
END IF
IF Tpages IS NULL OR Tpages = 0 THEN
   LET Tpages = 01
END IF 
LET Cpage = 01

WHILE (TRUE) -- Infinite Loop

   LET Indx2 = 01 -- Pointer to elements in the array

   -- Initialize the Array
   FOR indx1 = 1 TO 10
      INITIALIZE PickArrRecord[indx1].* TO NULL
   END FOR

   -- Fill the array with rows from the table
   FOR indx1 = StackPos TO SetLimit
      FETCH ABSOLUTE indx1 GenPickCur INTO  PickArrRecord[indx2].*
      IF (SQLCA.SQLCODE = NOTFOUND) THEN
          EXIT FOR
      END IF
      LET Indx2 = Indx2 + 1
   END FOR

   LET Salida = 0

   FOR indx1 = 1 TO 10
      IF PickArrRecord[indx1].Field1 IS NOT NULL THEN
         LET Salida = Salida + 1
      END IF
   END FOR

   IF Salida = 0 AND Tpages = 1 THEN
      CALL box_valdato("No existen datos con estas condiciones.")
      LET INT_FLAG=TRUE
      EXIT WHILE
   END IF  

   LET Indx2 = Indx2 - 1

   -- Tell Display Array How many elements were filled
   CALL SET_COUNT(indx2)

   LET Accepted = TRUE

   -- Print the information about pages
   LET TagPage = "(",Cpage USING "<&&#",",",Tpages USING "<&&#",")"
   DISPLAY BY NAME TagPage

   -- Display the Array , and Turns Control over the User
   DISPLAY ARRAY PickArrRecord TO pickArrRecord.*
		BEFORE DISPLAY
			IF Tpages = 1 THEN
            CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
            CALL fgl_dialog_setkeylabel("ACCEPT", "Aceptar")
            CALL fgl_dialog_setkeylabel("CONTROL-U", "")
            CALL fgl_dialog_setkeylabel("CONTROL-P", "")  
            DISPLAY "                                              " AT 3,17
			ELSE
            CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
            CALL fgl_dialog_setkeylabel("ACCEPT", "Aceptar")
            CALL fgl_dialog_setkeylabel("CONTROL-U", "Pagina anterior")
            CALL fgl_dialog_setkeylabel("CONTROL-P", "Pagina siguiente")  
			END IF
       ON KEY(CONTROL-U) -- Page Up
	  LET Accepted = FALSE
          IF (StackPos = 01) THEN
              ERROR "Se encuentra en el inicio de la lista..."
	      LET Accepted = TRUE 
          ELSE
              LET StackPos = Stackpos - 10
              LET Cpage = Cpage - 1
              EXIT DISPLAY
          END IF
      
       ON KEY(CONTROL-P) -- Page Down
          LET Accepted = FALSE
          IF ((StackPos+10) > StackLimit) THEN
              ERROR "Se encuentra en el final de la lista..."
	      LET Accepted = TRUE 
          ELSE
              LET StackPos = StackPos + 10
              LET Cpage = Cpage + 1
              EXIT DISPLAY
          END IF

   END DISPLAY

   LET Indx1 = ARR_CURR()
   LET FieldToReturn = PickArrRecord[Indx1].Field1  
   LET FieldToReturn1 = PickArrRecord[Indx1].Field2

   IF (INT_FLAG) OR (Accepted) THEN
       EXIT WHILE -- Exit Infinite Loop
   END IF

   LET SetLimit = StackPos + 9
   IF (SetLimit > StackLimit) THEN
       LET SetLimit = StackLimit
   END IF 

END WHILE

-- Close the Cursor
CLOSE GenPickCur

-- The User Leaves the Display Array, change the ACCEPT key 
OPTIONS
    ACCEPT KEY ESCAPE

--Close The Window
CLOSE WINDOW w_ArrPickList

RETURN FieldToReturn, FieldToReturn1,  INT_FLAG

END FUNCTION

##############################################
FUNCTION CountRows(TabName,WhereCond) 
##############################################
DEFINE TabName CHAR(1000),
       WhereCond CHAR(1000),
       Stmnt     CHAR(1000),
       NumRows   INTEGER 

LET Stmnt = "SELECT COUNT(*) FROM ",TabName CLIPPED,
	    " WHERE ",WhereCond CLIPPED

WHENEVER ERROR CONTINUE
   PREPARE ExStmnt FROM Stmnt
WHENEVER ERROR STOP

IF (SQLCA.SQLCODE = 0) THEN
   WHENEVER ERROR CONTINUE 
   DECLARE CounCur CURSOR FOR ExStmnt
   FOREACH CounCur INTO NumRows
   END FOREACH
ELSE
   LET NumRows = -1
END IF

RETURN NumRows -- Return the number of rows

END FUNCTION

