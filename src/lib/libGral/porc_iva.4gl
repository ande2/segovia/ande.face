################################################################################
#	Programa : porc_iva.4gl
#	Descripcion : Funcion para determinar el porcentaje de iva utilizado
#	Funciones 	: porc_iva
#	Parametros que recibe : fecha del documento y No.de empresa que envia el req.
#	Parametros que Envia  : Porcentaje de iva
#	Hecho Por	: GYanes Tue Jul 31 08:54:12 Guate 2001
################################################################################


FUNCTION porc_iva(v_fecha,v_empresa)
DEFINE
	v_fecha	date,
	v_porcentaje decimal(5,2),
	v_empresa smallint

### Inicializa la variable que va a enviar

	LET v_porcentaje = NULL

### Valida la empresa que recibio, para proceder a cambiar de base de datos

	IF v_empresa = 201 OR
		v_empresa = 202 OR
		v_empresa = 414 OR
		v_empresa = 523 OR
		v_empresa = 524 OR
		v_empresa = 802 THEN
		DATABASE face
	ELSE
		ERROR "!!! Numero de empresa no es valida !!!"
		SLEEP 3
		RETURN v_porcentaje
	END IF


### Trata de verificar y seleccionar el porcentaje de iva vigenta

	SELECT iva.porcentaje
	INTO v_porcentaje
	FROM iva
	WHERE iva.fecha_ini_vigencia <= v_fecha
	AND 	iva.fecha_fin_vigencia is null 
	AND 	iva.estado = "V"

### Si la fecha que fue recibida no es la del iva vigente selecciona del hist.

	IF status = NOTFOUND OR v_porcentaje is null THEN
		SELECT iva.porcentaje
		INTO v_porcentaje
		FROM iva
		WHERE v_fecha BETWEEN iva.fecha_ini_vigencia AND iva.fecha_fin_vigencia
		AND 	iva.fecha_fin_vigencia is not null
		AND	iva.estado != "A"
	END IF

### Regresa la base dedatos, de acuerdo con la empresa que la funcion recibio

	CASE (v_empresa)
		WHEN 201 DATABASE ipsa
		WHEN 202 DATABASE picasa
		WHEN 414 DATABASE inv_414
		WHEN 523 DATABASE inv_523
		WHEN 524 DATABASE inv_524
		WHEN 802 DATABASE inv_802
	END CASE

	RETURN v_porcentaje

END FUNCTION