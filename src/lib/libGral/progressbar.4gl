################################################################################
# Programa    : progressbar.4gl
# Funcion     :
# Descripcion : Programa que contiene una funcion para desplegar en pantalla el
#               funcionamiento del progress bar
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Paramscros
# Devueltos   :
#
# SCCS id No. :
# Autor       : Erick Valdez
# Fecha       : 10/09/07
# Path        : 
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################
FUNCTION desp_progressb(num_vals)
DEFINE num_vals	INTEGER

	CALL ui.Interface.refresh()
	DISPLAY num_vals TO progressb
	DISPLAY num_vals TO porcentaje
	CALL ui.Interface.refresh()

END FUNCTION
