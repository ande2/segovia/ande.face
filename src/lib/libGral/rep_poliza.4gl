################################################################################
# Funcion     : %M%
# Descripcion : Generacion de Poliza  a Excel
# Funciones   : tras_polr(sal, vsigf, vsigc, lr_datos)
# Parametros	
# Recibidos   : sal -> registro con datos de la partida, vsigf y vsigc para formato de Row y Columna, 
#               lr_datos -> datos generales
# Parametros
# Devueltos	  :
#
# SCCS ID No  : %Z% %W%
# Autor       : Ronald Palencia
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador			Fecha			                 Descripcion de la modificacion
################################################################################
DATABASE db0000

REPORT rep_poliza(sal, vsigf, vsigc, lr_datos)
	DEFINE 
	sal RECORD
		tipo			SMALLINT,
		cue_desc 	VARCHAR(100),
		fecha DATE, 
		cue_rub     LIKE mcue.cue_rub,
		cue_may		LIKE mcue.cue_may,	
		cue_men		LIKE mcue.cue_men,	
		cue_det		LIKE mcue.cue_det,	
		cue_ref		LIKE mcue.cue_ref,	
		valor_debe  DECIMAL(18,2),
		valor_haber DECIMAL(18,2)
	END RECORD,
   lr_datos RECORD
      empr_nomlog VARCHAR(50),
      empr_nombre VARCHAR(100),
      despol VARCHAR(250), --descripcion de la poliza
      lindes VARCHAR(150), --linea de descripcion para otros datos
      fec_polini DATE,
      fec_polfin DATE,
      nomprog VARCHAR(50)
   END RECORD,
	vsigf, vsigc CHAR(1),
	temp,contador, lint_flag, i SMALLINT,
	condicion CHAR(500),
   num_fila CHAR(50),
	usuario CHAR(10),
	tot_debe, tot_debe1 DECIMAL(18,2),
	tot_haber, tot_haber1 DECIMAL(18,2),
	tot_mayor, tot_menor, tot_detalle, tot_ref INTEGER,
   ccuenta varchar(50)

OUTPUT
	TOP MARGIN 0
	BOTTOM MARGIN 0
   LEFT MARGIN 0
	PAGE LENGTH 1
	ORDER BY sal.tipo, sal.cue_may, sal.cue_men, sal.cue_det, sal.cue_ref

FORMAT
FIRST PAGE HEADER

	LET i = 8
   LET contador = 0
	LET tot_debe = 0
	LET tot_debe1 = 0
	LET tot_haber = 0
	LET tot_haber1 = 0
	LET tot_mayor = 0
	LET tot_menor = 0
	LET tot_detalle = 0
	LET tot_ref = 0
	
--envio a excel el encabezado y titulos del reporte incluyendo el logo
   LET condicion = lr_datos.empr_nomlog CLIPPED
   LET num_fila = vsigf,"2",vsigc,"2:",vsigf,"2",vsigc,"2"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1"   ,'logo')

    LET condicion = "Departamento de Contabilidad"
    LET num_fila = vsigf,5 USING "<<<<<",vsigc,"3"
    CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

    LET condicion = "POLIZA DE DIARIO"
    LET num_fila = vsigf,6 USING "<<<<<",vsigc,"3"
    CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

   -- Combina celdas
   FOR i = 1 TO 6
      LET condicion =  "C", i USING "<", ":D", i USING "<"
      LET num_fila = vsigf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
      CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
      CALL macroe("Libro1"   ,'combina_celda')
   END FOR

  	LET condicion = "Fecha :", TODAY USING "dd/mm/yyyy"
   LET num_fila =   vsigf,"1",  vsigc, 5 USING "<<<:", vsigf,"1", vsigc, 5 USING "<<<"
	CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag

	LET condicion = "Hora  :", TIME
	LET num_fila =   vsigf,"2",  vsigc, 5 USING "<<<:", vsigf,"2", vsigc, 5 USING "<<<"
	CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag

   -- cambiar tama�o de la mayoria de columna
   LET condicion = "c:G"
   LET num_fila = vsigf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   LET condicion = "20"
   LET num_fila = vsigf,"6",vsigc,"1:",vsigf,"6",vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1"   ,'for_cel')

   --Cambiar de ancho de la primera columna, para que NO se mire.
   LET condicion = "A"
   LET num_fila = vsigf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   LET condicion = "0"
   LET num_fila = vsigf,"6",vsigc,"1:",vsigf,"6",vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1"   ,'for_cel')

   --Cambiar de ancho de de una columna, para que NO se mire.
   LET condicion = "B"
   LET num_fila = vsigf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
   LET condicion = "35"
   LET num_fila = vsigf,"6",vsigc,"1:",vsigf,"6",vsigc,"1"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1",'for_cel')

   --- Formatea la columna a texto antes de enviar la informacion
   LET condicion =  "B:B, C:C"
   LET num_fila = vsIgf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1"   ,'cel_texto')

   -- Formatea columna a decimales
   LET condicion =  "d:e"
   LET num_fila = vsIgf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
   LET condicion = "2"
   LET num_fila = vsigf,"6",vsigc,"1:",vsigf,"6",vsigc,"1"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1",'decimales2')

            -- Background COLOR
            LET condicion =  "b8:e8"
            LET num_fila =   vsigf, "5", vsigc,"1:", vsigf,"5", vsigc,"1"
            CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
				CALL macroe("Libro1"   ,'centra_texto')
            LET condicion =  "15"
            LET num_fila =   vsigf, "6", vsigc,"1:", vsigf,"6", vsigc,"1"
            CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
            CALL macroe("Libro1"   ,'FillColor')

           -- Negrita
           LET condicion = "B1:n8"
           LET num_fila = vsigf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
           CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
           CALL macroe("Libro1"   ,'Negrita')

	LET i = 8
   LET condicion =
                "DESCRIPCION DE LA CUENTA\\tCODIGO CONTABLE\\tDEBE\\tHABER"
   LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:",vsigf,i USING "<<<<<",vsigc,"7"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
	
	 -- Inmoviliza Panel
	 LET condicion = "b9"
	 LET num_fila = vsigf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
	 CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
	 CALL macroe("Libro1", 'inmoviliza_panel') 

BEFORE GROUP OF sal.cue_may
	LET temp = 0

BEFORE GROUP OF sal.cue_men
	LET temp = 0

BEFORE GROUP OF sal.cue_det
	LET temp = 0

AFTER GROUP OF sal.cue_ref
	IF sal.tipo = 1 THEN
  		LET contador = contador + 1
		LET tot_debe = GROUP SUM(sal.valor_debe)
		LET tot_debe1 = tot_debe1 + tot_debe

		PRINT sal.cue_may USING "&&&",",",
				sal.cue_men USING "&&&&",",",
				sal.cue_det USING "&&&",",",
				sal.cue_ref USING "&&&&&",",",
				" C,",GROUP SUM(sal.valor_debe) USING "##########.##",
				" ", sal.cue_desc CLIPPED
	ELSE
      LET contador = contador + 1
		LET tot_haber = GROUP SUM(sal.valor_haber)
		LET tot_haber1 = tot_haber1 + tot_haber

      PRINT sal.cue_may USING "&&&",",",
				sal.cue_men USING "&&&&",",",
				sal.cue_det USING "&&&",",",
				sal.cue_ref USING "&&&&&",",",
				" A,", GROUP SUM(sal.valor_haber) USING "##########.##",
				" ", sal.cue_desc CLIPPED
	END IF
	
	IF sal.tipo <> 0 THEN
		IF sal.cue_may IS NULL THEN
			LET sal.cue_may = 0
		END IF
	
		LET tot_mayor = tot_mayor + sal.cue_may
	
		IF sal.cue_men IS NULL THEN
			LET sal.cue_men = 0
		END IF

		LET tot_menor = tot_menor + sal.cue_men

		IF sal.cue_det IS NULL THEN
			LET sal.cue_det = 0
		END IF

		LET tot_detalle = tot_detalle + sal.cue_det

		IF sal.cue_ref IS NULL THEN
			LET sal.cue_ref = 0
		END IF

		LET tot_ref = tot_ref + sal.cue_ref
	END IF

	IF (sal.cue_ref IS NULL OR sal.cue_ref = 0) then
		let ccuenta = sal.cue_may using "&&&","-", sal.cue_men using "&&&&", "-", sal.cue_det using "&&&"
	ELSE
		let ccuenta = sal.cue_may using "&&&","-", sal.cue_men using "&&&&", "-", sal.cue_det using "&&&", "-",
						  sal.cue_ref USING "&&&&&"
	END IF

	IF  sal.tipo = 1 THEN
   	LET i = i+1
   	LET condicion = sal.cue_desc CLIPPED,
				"\\t", ccuenta clipped, "\\t",GROUP SUM(sal.valor_debe)
		LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:",vsigf,i USING "<<<<<",vsigc,"6"
		CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
	ELSE
   	LET i = i+1
   	LET condicion = sal.cue_desc CLIPPED,
				"\\t", ccuenta clipped, "\\t", "\\t", GROUP SUM(sal.valor_haber)
		LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:",vsigf,i USING "<<<<<",vsigc,"7"
		CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
	END IF 

ON LAST ROW

   -- Formato de letra
   LET condicion =  "b9:b",i USING "<<<<<<<"
   LET num_fila =   vsigf, "5", vsigc,"1:", vsigf,"5", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   LET condicion =  "8"
   LET num_fila =   vsigf, "6", vsigc,"1:", vsigf,"6", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   LET condicion =  "arial"
   LET num_fila =   vsigf, "7", vsigc,"1:", vsigf,"7", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1"   ,'for_letra2')

   --quita negrita
   LET condicion =  "b9:b",i USING "<<<<<<<"
   LET num_fila =   vsigf, "5", vsigc,"1:", vsigf,"5", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1"   ,'no_negrita')

	LET i = i+1 

   -- Negrita
   LET condicion =  "B", i USING "<<<<<<<", ":e", i+5 USING "<<<<<<<"
   LET num_fila =   vsigf, "5", vsigc,"1:", vsigf,"5", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   CALL macroe("Libro1"   ,'Negrita')

	LET condicion = "T O T A L . . . ", "\\t", tot_debe1,"\\t",tot_haber1
	LET num_fila = vsigf,i USING "<<<<<",vsigc,"3:",vsigf,i USING "<<<<<",vsigc,"7"
	CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
	LET i = i + 2
	LET condicion = "Mayor:", tot_mayor,"  Menor:",tot_menor,"  Detalle:",tot_detalle, "   Ref:",tot_ref
	LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:",vsigf,i USING "<<<<<",vsigc,"7"
	CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
	LET i = i + 1
   LET condicion = lr_datos.despol CLIPPED
   LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:", vsigf, i USING "<<<<<", vsigc,"7"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
	LET i = i + 1
   LET condicion = lr_datos.lindes CLIPPED
   LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:", vsigf, i USING "<<<<<", vsigc,"7"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

	IF (tot_debe1-tot_haber1) <> 0 THEN
		LET i = i + 1
		LET condicion = "*** REVISAR DESCUADRE !!!  ", "\\t\\t", (tot_debe1-tot_haber1)
		LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:",vsigf,i USING "<<<<<",vsigc,"7"
		CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

		-- Colorea Rango de celdas segun codigo (ver colorindex de la ayuda de msexcel)
  		LET condicion = 	"b",i USING "<<<<<<<", ":e",i USING "<<<<<<<" --primer  parametro
  		LET num_fila = vsIgf,"5",vsigc,"1:",vsigf,"5",vsigc,"1"
  		CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

   	LET condicion = "3"  --segundo parametro  (ROJO)
   	LET num_fila = vsigf,"6",vsigc,"1:",vsigf,"6",vsigc,"1"
   	CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag
   	CALL macroe("Libro1",'SetColor')  
	END IF

	LET i = i + 3
   LET condicion = "f.____________________","\\t", "f.______________________", "\\t\\t", "f.___________________"
   LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:", vsigf, i USING "<<<<<", vsigc,"7"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

	LET i = i + 1
   LET condicion = "        Elabor�","\\t", "      Revis�", "\\t\\t", "       Vo. Bo."
   LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:", vsigf, i USING "<<<<<", vsigc,"7"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

	LET i = i + 2
   LET condicion = lr_datos.nomprog
   LET num_fila = vsigf,i USING "<<<<<",vsigc,"2:", vsigf, i USING "<<<<<", vsigc,"7"
   CALL send_dde("Libro1",num_fila,condicion) RETURNING lint_flag

   -- Formato de letra
   LET condicion =  "B", i USING "<<<<<<<", ":b", i USING "<<<<<<<"
   LET num_fila =   vsigf, "5", vsigc,"1:", vsigf,"5", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   LET condicion =  "6"
   LET num_fila =   vsigf, "6", vsigc,"1:", vsigf,"6", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag
   LET condicion =  "arial"
   LET num_fila =   vsigf, "7", vsigc,"1:", vsigf,"7", vsigc,"1"
   CALL send_dde("Libro1"   ,num_fila,condicion) RETURNING lint_flag

   CALL macroe("Libro1"   ,'for_letra2')

END REPORT

