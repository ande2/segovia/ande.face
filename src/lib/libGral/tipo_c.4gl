################################################################################
# Funcion     : %M%
# Descripcion : Funcion que verifica el tipo de cambio del dia 
# Funciones   : verifica_tipoc() 
#
# Parametros
# Recibidos   :
# Parametros
# Descripcion : 
#               
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : HREYES
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# HREYES      Mon Aug  7 14:22:06 CST 2006 funcion para seleccion de pais
################################################################################

FUNCTION verifica_tipoc()
DEFINE 
	tipo_cambio SMALLINT,
   vpais_id INTEGER

   LET vpais_id = codigo_pais()

   LET tipo_cambio = 0
SELECT COUNT(*)
  INTO tipo_cambio
  FROM face:tipo_c
 WHERE face:tipo_c.fecha_cambio = TODAY
   AND face:tipo_c.pais_id = vpais_id

 IF tipo_cambio IS NULL THEN
    LET tipo_cambio = 0
 END IF
 IF tipo_cambio > 0 THEN
	 RETURN FALSE
 END IF

RETURN TRUE

END FUNCTION

FUNCTION codigo_pais()
DEFINE 
   vpais_id INTEGER

   LET vpais_id = 0
SELECT mdatgen.pais_id
  INTO vpais_id
  FROM mdatgen

display " pais: ",vpais_id

RETURN vpais_id

END FUNCTION

FUNCTION porcentaje_iva()
DEFINE vpiva  DECIMAL(8,2),
       vpais_id INTEGER

   LET vpais_id = codigo_pais()

   LET vpiva = 0

   SELECT face:iva.porcentaje
     INTO vpiva
     FROM face:iva
    WHERE face:iva.estado = "V"
      AND face:iva.pais_id = vpais_id

   RETURN vpiva

END FUNCTION