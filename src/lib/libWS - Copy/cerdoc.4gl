IMPORT FGL WSHelper
IMPORT com
IMPORT XML

SCHEMA face


GLOBALS "ingface.inc"

FUNCTION certidoc(ldoc_id)
DEFINE 
   ldoc_id     LIKE mdoc.doc_id,
   idx         SMALLINT,
   re          RECORD LIKE mdoc.*,
   rdet        DYNAMIC ARRAY OF RECORD LIKE ddoc.*,
   rd          RECORD LIKE ddoc.*,
   pStatus2,
   pStatus     INTEGER,
   lRequestor  LIKE mesta.esta_requestor,
   lUserFace   LIKE mesta.esta_userface,
   lDispo      LIKE mesta.esta_dispo,
   lRangoI     INTEGER,
   lRangoF     INTEGER,
   lFirma      VARCHAR(100),
   QueryBuffer  STRING,
   pError      STRING

   

   INITIALIZE registrarDte.dte.dte.* TO NULL
   CALL  registrardte.dte.dte.detalleDte.clear()
   LET pStatus = 0

   --Leer Usuario y clave
   SELECT   b.esta_userface, b.esta_requestor, b.esta_dispo, 
            c.res_rangode, c.res_rangoa
      INTO  lUserFace, lRequestor, lDispo, lRangoI, lRangoF
      FROM  mdoc a, mesta b, dres c
      WHERE a.doc_id = ldoc_id
      AND   b.esta_id = a.esta_id
      AND   c.esta_id = b.esta_id
      AND   c.res_id = a.res_id
   
   --Lee el encabezado
   SELECT   *
      INTO  re.*
      FROM  mdoc 
      WHERE doc_id = ldoc_id

   IF sqlCA.sqlcode = 0 THEN

      LET registrarDte.dte.usuario =  lUserFace
      LET registrardte.dte.clave = lRequestor
      LET registrardte.dte.dte.idDispositivo = lDispo CLIPPED
      LET registrardte.dte.dte.rangoInicialAutorizado = lRangoI
      LET registrardte.dte.dte.rangoFinalAutorizado = lRangoF
      
      LET registrarDte.dte.dte.estadoDocumento =  re.estadoDocumento
      LET registrarDte.dte.dte.codigoMoneda = re.codigoMoneda  
      LET registrarDte.dte.dte.tipoDocumento = re.tipoDocumento
      LET registrarDte.dte.dte.numeroResolucion = re.numeroResolucion CLIPPED
      LET registrarDte.dte.dte.fechaResolucion = re.fechaResolucion
      LET registrarDte.dte.dte.serieAutorizada = re.serieAutorizada
      LET registrarDte.dte.dte.fechaDocumento = re.fechaDocumento
      LET registrarDte.dte.dte.tipoCambio = re.tipoCambio
      IF re.fechaAnulacion  IS NOT NULL THEN
         LET registrarDte.dte.dte.fechaAnulacion = re.fechaAnulacion
      ELSE
         LET registrarDte.dte.dte.fechaAnulacion = re.fechaDocumento
      END if
      LET registrarDte.dte.dte.numeroDocumento = re.numeroDocumento
      LET registrarDte.dte.dte.serieDocumento = re.serieDocumento
      LET registrarDte.dte.dte.regimenISR = re.regimenISR
      LET registrarDte.dte.dte.codigoEstablecimiento = re.codigoEstablecimiento USING "<<<"
      LET registrarDte.dte.dte.nitGFACE = re.nitGFACE
      LET registrarDte.dte.dte.nitVendedor = re.nitVendedor
      LET registrarDte.dte.dte.municipioVendedor = re.municipioVendedor
      LET registrarDte.dte.dte.departamentoVendedor = re.departamentoVendedor
      LET registrarDte.dte.dte.direccionComercialVendedor = re.direccionComercialVendedor
      LET registrarDte.dte.dte.nombreComercialRazonSocialVendedor = re.nombreComercialRazonSocialVendedor
      LET registrarDte.dte.dte.nombreCompletoVendedor = re.nombreCompletoVendedor
      LET registrarDte.dte.dte.nitComprador = re.nitComprador  
      LET registrarDte.dte.dte.regimen2989 = "2"
      IF re.telefonoComprador IS NOT NULL THEN
         LET registrarDte.dte.dte.telefonoComprador = re.telefonoComprador
      ELSE
         LET registrarDte.dte.dte.telefonoComprador = "N/A"
      END IF
      LET registrarDte.dte.dte.direccionComercialComprador = re.direccionComercialComprador
      LET registrarDte.dte.dte.municipioComprador = re.municipioComprador
      LET registrarDte.dte.dte.nombreComercialComprador = re.nombreComercialComprador
      LET registrarDte.dte.dte.departamentoComprador = re.departamentoComprador
      LET registrarDte.dte.dte.correoComprador = re.correoComprador
      LET registrarDte.dte.dte.montoTotalOperacion = re.montoTotalOperacion
      LET registrarDte.dte.dte.importeDescuento = re.importeDescuento
      LET registrarDte.dte.dte.importeTotalExento = re.importeTotalExento
      LET registrarDte.dte.dte.importeNetoGravado = re.importeNetoGravado
      LET registrarDte.dte.dte.detalleImpuestosIva = re.detalleImpuestosIva
      LET registrarDte.dte.dte.importeOtrosImpuestos = re.importeOtrosImpuestos
      LET registrarDte.dte.dte.descripcionOtroImpuesto = re.descripcionOtroImpuesto
      LET registrarDte.dte.dte.importeBruto = re.importeBruto

      --Campos no utilizados
      LET registrarDte.dte.dte.cae = "N/A"
      LET registrarDte.dte.dte.personalizado_01 = "N/A"
      LET registrarDte.dte.dte.personalizado_02 = "N/A"
      LET registrarDte.dte.dte.personalizado_03 = "N/A"
      LET registrarDte.dte.dte.personalizado_04 = "N/A"
      LET registrarDte.dte.dte.personalizado_05 = "N/A"
      LET registrarDte.dte.dte.personalizado_06 = "N/A"
      LET registrarDte.dte.dte.personalizado_07 = "N/A"
      LET registrarDte.dte.dte.personalizado_08 = "N/A"
      LET registrarDte.dte.dte.personalizado_09 = "N/A"
      LET registrarDte.dte.dte.personalizado_10 = "N/A"
      LET registrarDte.dte.dte.personalizado_11 = "N/A"
      LET registrarDte.dte.dte.personalizado_12 = "N/A"
      LET registrarDte.dte.dte.personalizado_13 = "N/A"
      LET registrarDte.dte.dte.personalizado_14 = "N/A"
      LET registrarDte.dte.dte.personalizado_15 = "N/A"
      LET registrarDte.dte.dte.personalizado_16 = "N/A"
      LET registrarDte.dte.dte.personalizado_17 = "N/A"
      LET registrarDte.dte.dte.personalizado_18 = "N/A"
      LET registrarDte.dte.dte.personalizado_19 = "N/A"
      LET registrarDte.dte.dte.personalizado_20 = "N/A"
      LET registrardte.dte.dte.observaciones = "N/A"
      LET registrarDte.dte.dte.numeroDte = 0
      


      --Lee el detalle
      DECLARE ListaDeta CURSOR FOR
         SELECT   *
            FROM  ddoc
            WHERE doc_id = ldoc_id

      LET idx = 1
      FOREACH ListaDeta INTO rd.*
         LET registrardte.dte.dte.detalleDte[idx].tipoProducto = rd.tipoProducto
         LET registrardte.dte.dte.detalleDte[idx].codigoProducto = rd.codigoProducto
         LET registrardte.dte.dte.detalleDte[idx].descripcionProducto =rd.descripcionProducto
         LET registrardte.dte.dte.detalleDte[idx].unidadMedida = rd.unidadMedida  CLIPPED
         LET registrardte.dte.dte.detalleDte[idx].cantidad = rd.cantidad
         LET registrardte.dte.dte.detalleDte[idx].precioUnitario = rd.precioUnitario
         LET registrardte.dte.dte.detalleDte[idx].importeTotalOperacion = rd.importeTotalOperacion
         LET registrardte.dte.dte.detalleDte[idx].montoBruto = rd.montoBruto
         LET registrardte.dte.dte.detalleDte[idx].importeExento = rd.importeExento
         LET registrardte.dte.dte.detalleDte[idx].importeNetoGravado = rd.importenetogravado
         LET registrardte.dte.dte.detalleDte[idx].montoDescuento = rd.montoDescuento
         LET registrardte.dte.dte.detalleDte[idx].importeOtrosImpuestos = rd.importeOtrosImpuestos
         LET registrardte.dte.dte.detalleDte[idx].detalleImpuestosIva = rd.detalleImpuestosIva
      END FOREACH

      --Certificar el documento
     CALL registrarDte_g( ) RETURNING pStatus
      LET lFirma = registrardteresponse.return.cae
      IF lFirma IS NULL THEN
        LET perror = registrardteresponse.return.descripcion
        LET QueryBuffer = "INSERT ",
                           " INTO     ldoc ( doc_id, err_num, err_message) ",
                          " VALUES   (",  ldoc_id, ",", SQLCA.sqlcode, ", '", registrardteresponse.return.descripcion, "')"
        PREPARE QERR FROM QueryBuffer 
        EXECUTE QERR
          RETURN ""
      ELSE
         LET lFirma = registrardteresponse.return.cae
          RETURN lfirma
         END IF
      
   END IF
END FUNCTION