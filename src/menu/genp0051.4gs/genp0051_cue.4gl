################################################################################
# Funcion     : %M%
# Descripcion : picklist 4gl  
# Funciones   : list_cue(Title1,Title2,Field1,Field3,Field4,Field5, Field6,Field7,Tabname,OrderNum,WROWPos,WCOLPos)
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : DENER MEDRANO
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

###############################################################
FUNCTION list_cue(Title1,Title2,Field1,Field2,Tabname,condicion,OrderNum,WROWPos,WCOLPos)
###############################################################
DEFINE Title1 CHAR(10),  -- Titles to display for the Fields
       Title2 CHAR(50),
       Field1 CHAR(100),
       Field2 CHAR(100),
       Field3 CHAR(100),
       Field4 CHAR(100),
       Field5 CHAR(100),
       Field6 CHAR(100),
       Field7 CHAR(100),
       Tabname CHAR(50), -- Table name to query from
       OrderNum  SMALLINT, -- Order by 1 or 2, a Number in a range of 1-2
       WROWPos,            -- Row and Col position for the window
       WCOLPos  SMALLINT,
		 salida SMALLINT,
       Accepted,
       StackLimit,
       SetLimit,
       StackPos   SMALLINT,
       condition  CHAR(100),
		 condicion  CHAR(400),
       comia      CHAR(3),
       TagPage    CHAR(12),
       Tpages,              -- Total Pages in the Display Array
       Cpage      SMALLINT, -- Current Page in the Display Array
       sqlstmnt CHAR(1000), -- Char variable to contain the SQL statement
       PickArrRecord ARRAY[10] OF RECORD -- Record to contain the rows
              Field1 CHAR(100), 
              Field2 CHAR(100)
	   END RECORD,
       Indx1,            -- Used for the FOR Loop 
       Indx2   SMALLINT, -- Index integer to point to arrays elements
       FieldToReturn CHAR(20), -- Informix converts automatically between
			       -- Char and Numerics, so the candidate 
			       -- datatype to hold the field1 result is CHAR.
       FieldToReturn1 CHAR(60),
		 vval_temp SMALLINT,
		 vvar3 CHAR(100),
       varcount   CHAR(300),
       interrumpe CHAR(1),
       regresa    SMALLINT 

   LET int_flag = 0
   LET interrumpe = "N"

	OPTIONS INPUT NO WRAP

   WHENEVER ERROR CONTINUE
   --Open the window at the position specified by the parameters Wxpos and Wypos
   OPEN WINDOW w_ArrPickList AT  WROWPos,WCOLPos WITH FORM "list_cue"
      ATTRIBUTES(BORDER,FORM LINE 1)
   CURRENT WINDOW IS w_ArrPickList
   WHENEVER ERROR STOP

   -- For a 4GL statement like OPEN WINDOW, the STATUS global variable will be
   -- used to check for errors.

   IF (STATUS < 0 ) THEN -- The Window could not be opened,
                         --the form does not exist
                         -- or the parameters WxPos, WyPos are wrong
      ERROR "Wrong values suppied to OpenWindow(PickList) ",
            "-- Contact Programmer --"
      SLEEP 1
      RETURN -1,1,INT_FLAG
   END IF

WHILE TRUE
   MESSAGE("Ingrese el criterio de busqueda.") ATTRIBUTE(RED)
   INPUT BY NAME condition
      BEFORE INPUT
         CALL fgl_dialog_setkeylabel("ACCEPT","Seleccionar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")

   END INPUT

   DISPLAY "" AT 3,14
   IF int_flag THEN
      LET regresa = 0
      LET interrumpe = "S"
      EXIT WHILE
   END IF

   LET comia = ASCII 34
   LET condition = "*", condition CLIPPED, "*"

   IF (OrderNum = 1 or OrderNum = 2) THEN
      LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",UPPER(", Field2 CLIPPED, ")", 
                     " FROM ",Tabname CLIPPED,
               		" WHERE UPPER(",Field2 CLIPPED, ") MATCHES ",comia CLIPPED,condition CLIPPED,comia,
							" AND ", condicion CLIPPED,
               		" ORDER BY 1 "
   ELSE
      LET sqlstmnt = "SELECT ",Field1 CLIPPED, ",UPPER(", Field2 CLIPPED, ")",
                     " FROM ",Tabname CLIPPED,
               		" WHERE UPPER(", Field2 CLIPPED ,") MATCHES ",comia CLIPPED,condition CLIPPED,comia,
							" AND ", condicion CLIPPED,
               		" ORDER BY 1 "
   END IF


WHENEVER ERROR CONTINUE
   PREPARE EX_sqlst FROM sqlstmnt
WHENEVER ERROR STOP

--If the user supplied wrong values
IF (SQLCA.SQLCODE < 0) THEN
   ERROR "Parametros incorrectos hacia funcion PickList -- Contacte al Programador --"
   SLEEP 1
   RETURN -1,1,INT_FLAG
END IF

--Revisa si no hay datos
IF (SQLCA.SQLCODE <> 0) THEN
   ERROR "-- No se encotraron datos validos --"
   SLEEP 1
   RETURN -1,1,INT_FLAG
END IF 

DECLARE GenPickCur SCROLL CURSOR FOR EX_sqlst

   -- Change the ACCEPT key to RETURN, useful for display array (picklist)
   OPTIONS
      ACCEPT KEY RETURN

   -- Display the Titles  
   DISPLAY BY NAME  Title2


-- Open the Cursor, prepare the buffer communication mechanism
OPEN GenPickCur

LET varcount = " UPPER (",Field2 CLIPPED ,") MATCHES ",comia CLIPPED,condition CLIPPED,comia,
							" AND ", condicion CLIPPED,
               		" ORDER BY 1 "

-- Initialize the Stack boundary
LET StackLimit = CountRows(Tabname CLIPPED, varcount) 
LET StackPos   = 01
-- Initialize Stack Pointers
IF StackLimit >= 10 THEN
   LET SetLimit = 10
ELSE
   LET SetLimit = StackPos + 9
END IF

-- Calculate How many pages
LET Tpages = (StackLimit/10)
IF (StackLimit MOD 10) != 0 THEN
   LET Tpages = Tpages + 1
END IF
LET Cpage = 01 

WHILE (TRUE) -- Infinite Loop

   LET Indx2 = 01 -- Pointer to elements in the array

   -- Initialize the Array
   FOR indx1 = 1 TO 10
      INITIALIZE PickArrRecord[indx1].* TO NULL
   END FOR  

   FOR indx1 = StackPos TO SetLimit
      FETCH ABSOLUTE indx1 GenPickCur INTO PickArrRecord[indx2].*
      IF (SQLCA.SQLCODE = NOTFOUND) THEN
          EXIT FOR
		ELSE  
			LET vval_temp = pickArrRecord[indx2].Field1 
			IF vval_temp < 1000 THEN
				LET vvar3 = pickArrRecord[indx2].Field1
				LET pickArrRecord[indx2].Field1 = NULL
				LET pickArrRecord[indx2].Field1 = vvar3
			END IF
      END IF
      LET Indx2 = Indx2 + 1
   END FOR

   LET salida = 0

   FOR indx1 = 1 TO 10
      IF PickArrRecord[indx1].Field1 IS NOT NULL THEN
         LET salida = salida + 1
         LET regresa = 1
      END IF
   END FOR

   IF salida = 0 THEN
      CALL box_valdato("No existen datos con estas condiciones.")
      LET regresa = 0
      EXIT WHILE
   END IF  

   LET Indx2 = Indx2 - 1

   -- Tell Display Array How many elements were filled
   CALL SET_COUNT(indx2)

   LET Accepted = TRUE
   LET regresa = 0

   -- Print the information about pages
   LET TagPage = "(",Cpage USING "<&&#",",",Tpages USING "<&&#",")"
   DISPLAY BY NAME TagPage

   -- Display the Array , and Turns Control over the User
   DISPLAY ARRAY PickArrRecord TO pickArrRecord.*
		BEFORE DISPLAY
            CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
            CALL fgl_dialog_setkeylabel("ACCEPT", "Aceptar")
            CALL fgl_dialog_setkeylabel("CONTROL-O", "Buscar Otro")
            CALL fgl_dialog_setkeylabel("CONTROL-U", "Pagina anterior")
            CALL fgl_dialog_setkeylabel("CONTROL-P", "Pagina siguiente")  

       ON KEY(CONTROL-U) -- Page Up
	       LET Accepted = FALSE
          IF (StackPos = 01) THEN
              ERROR "Top has been reached ..."
          ELSE
              LET StackPos = Stackpos - 10
              LET Cpage = Cpage - 1
              EXIT DISPLAY
          END IF
      
       ON KEY(CONTROL-P) -- Page Down
          LET Accepted = FALSE
          IF ((StackPos+10) > StackLimit) THEN
              ERROR "Se encuentra en el final de la list..."
          ELSE
              LET StackPos = StackPos + 10
              LET Cpage = Cpage + 1
              EXIT DISPLAY
          END IF

       ON KEY(CONTROL-O)
          LET regresa = 1
          EXIT DISPLAY

   END DISPLAY

   IF regresa = 1 THEN
      EXIT WHILE
   END IF

   LET Indx1 = ARR_CURR()
   LET FieldToReturn = PickArrRecord[Indx1].Field1
   LET FieldToReturn1 = PickArrRecord[Indx1].Field2

   IF (INT_FLAG) OR (Accepted) THEN
       LET regresa = 0
       EXIT WHILE -- Exit Infinite Loop
   END IF

   LET SetLimit = StackPos + 9
   IF (SetLimit > StackLimit) THEN
       LET SetLimit = StackLimit
   END IF 

END WHILE
   IF regresa = 0 THEN
      EXIT WHILE
   END IF

END WHILE

-- Close the Cursor
IF interrumpe <> "S" THEN
   CLOSE GenPickCur
END IF

-- The User Leaves the Display Array, change the ACCEPT key 
OPTIONS
    ACCEPT KEY ESCAPE

--Close The Window
CLOSE WINDOW w_ArrPickList


RETURN FieldToReturn, FieldToReturn1,  INT_FLAG

END FUNCTION
