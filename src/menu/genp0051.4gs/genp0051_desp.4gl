################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de paises 
# Funciones   : prog_qry_init() -> Prepara la seleccion
#               quey_prog() -> Ingreso de criterios de busqueda
#               fetch_prog() -> Navega sobre seleccion de datos
#               clean_up() -> Limpia el cursor de trabajo
#               display_prog -> Despliega el registro en pantalla 
#
# Parametros	
# Recibidos   :
# Parametros
# Devueltos	  :
#
# SCCS ID No  : %Z% %W%
# Autor       : JMonterroso 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# idener		  04/07/2006						 Se agrego los picklist de Busqueda
# 														 y se arreglo cuando cerraba el programa porque no encontraba un programa
################################################################################

GLOBALS "genp0051_glob.4gl"

FUNCTION prgo_qry_init()
DEFINE 
	select_stmt3 CHAR(1000)

LET select_stmt3 = "SELECT UNIQUE ventas:mprog.prog_id,ventas:mprog.prog_des,adm_usu.usu_nomunix,adm_usu.usu_nom FROM ventas:mprog,adm_usu,ventas:dprogopc,maccopcusu WHERE ventas:mprog.prog_id = ? AND ventas:dprogopc.prog_id = ventas:mprog.prog_id AND maccopcusu.linkprop = ventas:dprogopc.linkprop AND adm_usu.usu_id = maccopcusu.usu_id AND adm_usu.usu_nomunix = ?"

PREPARE ex_stmt3 FROM select_stmt3

END FUNCTION

FUNCTION query_prgo()
DEFINE 
	prog_query CHAR(1000),
	where_clause CHAR(250),
	prog_cnt	SMALLINT,
	prog_count CHAR(600),
	vprog_id LIKE ventas:mprog.prog_id,
	vprog_des LIKE ventas:mprog.prog_des,
	vusu_nomunix  LIKE adm_usu.usu_nomunix,
	vusu_nom  LIKE adm_usu.usu_nom,
	mensaje CHAR(100),
	x_condicion VARCHAR(100),
	lint_flag SMALLINT

DISPLAY "BUSQUEDA DE OPCIONES DE PROGRAMA POR USUARIO" AT 05,25 ATTRIBUTE(magenta)

MESSAGE "Use CONTROL-W para AYUDA a nivel de campo."      

LET int_flag = FALSE
LET prog_cnt = 0

CONSTRUCT BY NAME where_clause on mprog.prog_id,
											 adm_usu.usu_nomunix

	BEFORE CONSTRUCT                                      
  		CALL key_label()
		CALL fgl_dialog_setkeylabel("CONTROL-B","")
   ON KEY(CONTROL-W)                   
      CASE                                
         WHEN INFIELD(prog_id)            
            CALL SHOWHELP(102)            
         WHEN INFIELD(prog_des)           
            CALL SHOWHELP(103)            
         WHEN INFIELD(usu_nomunix)           
            CALL SHOWHELP(104)            
         WHEN INFIELD(usu_nom)           
            CALL SHOWHELP(105)            
		END CASE                            

	ON KEY (CONTROL-B)
      CASE
         WHEN INFIELD(prog_id)
            LET lint_flag=FALSE
            LET x_condicion = " 1 = 1 "
            CALL picklist_2('Busqueda de Programas','Codigo','Descripcion',
                            'prog_id','prog_des','mprog',x_condicion,1,1)
            RETURNING vprog_id,vprog_des, lINT_FLAG
            IF lint_flag THEN
               LET lint_flag = FALSE
            ELSE
               LET gr_prgo.prog_id = vprog_id
               LET gr_prgo.prog_des = vprog_des
               DISPLAY BY NAME gr_prgo.prog_id,
                               gr_prgo.prog_des

               NEXT FIELD usu_nomunix
            END IF

         WHEN INFIELD(usu_nomunix)
            LET lint_flag=FALSE
            LET x_condicion = " 1 = 1 "
            CALL picklist_2('Busqueda de Usuario','Codigo','Descripcion',
                            'usu_nomunix','usu_nom','adm_usu',x_condicion,1,1)
            RETURNING vusu_nomunix,vusu_nom, lINT_FLAG
            IF lint_flag THEN
               LET lint_flag = FALSE
            ELSE
               LET gr_prgo.usu_nomunix = vusu_nomunix
               LET gr_prgo.usu_nom = vusu_nom
               DISPLAY BY NAME gr_prgo.usu_nomunix,
                               gr_prgo.usu_nom
            END IF
      END CASE

								  

	AFTER CONSTRUCT
		IF int_flag THEN 
			CALL box_inter() RETURNING int_flag 
			IF int_flag = TRUE THEN
				LET int_flag = FALSE
				CLEAR FORM
   			DISPLAY "OPCIONES NO ASIGNADAS" TO btdet1
   			DISPLAY "OPCIONES ASIGNADAS" TO btdet2
				LET mensaje = "B�squeda cancelada por el usuario"
				CALL box_valdato(mensaje) 
				RETURN FALSE, prog_cnt
			ELSE
        		CALL get_fldbuf(prog_id, prog_des, usu_nomunix, usu_nom) RETURNING gr_prgo.prog_id, gr_prgo.prog_des, gr_prgo.usu_nomunix, gr_prgo.usu_nom
        		CONTINUE CONSTRUCT
     		END IF
		END IF
END CONSTRUCT

LET prog_query = "SELECT UNIQUE mprog.prog_id,adm_usu.usu_nomunix ",
                 "FROM ventas:mprog mprog,adm_usu,maccopcusu,ventas:dprogopc dprogopc WHERE dprogopc.prog_id = mprog.prog_id AND ",
                 " maccopcusu.linkprop = dprogopc.linkprop AND ",
                 " adm_usu.usu_id = maccopcusu.usu_id AND ",
                 where_clause CLIPPED," INTO TEMP tmp_count WITH NO LOG "

PREPARE ex_tmp FROM prog_query

EXECUTE ex_tmp

LET prog_query = "SELECT UNIQUE mprog.prog_id,adm_usu.usu_nomunix ",
                 "FROM ventas:mprog mprog,adm_usu,maccopcusu,ventas:dprogopc dprogopc WHERE dprogopc.prog_id = mprog.prog_id AND ",
                 " maccopcusu.linkprop = dprogopc.linkprop AND ",
                 " adm_usu.usu_id = maccopcusu.usu_id AND ",
                 where_clause CLIPPED

LET prog_count = "SELECT COUNT(*) ",
                 "FROM tmp_count"

PREPARE ex_stmt FROM prog_query

PREPARE ex_stmt2 FROM prog_count

DECLARE prog_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt
 
OPEN prog_ptr

FETCH FIRST prog_ptr INTO gr_prgo.prog_id,gr_prgo.usu_nomunix

IF SQLCA.SQLCODE = 100 THEN
   LET mensaje = "Ning�n registro cumple con la condici�n"
   CALL box_valdato(mensaje)
   CLOSE prog_ptr
	DROP TABLE tmp_count
   RETURN FALSE, prog_cnt
ELSE
   DECLARE prog_all CURSOR FOR ex_stmt3
     
   OPEN prog_all USING gr_prgo.prog_id,gr_prgo.usu_nomunix

   FETCH prog_all INTO gr_prgo.*
   
   IF SQLCA.SQLCODE = NOTFOUND THEN
      LET mensaje = "Ning�n registro cumple con la condici�n"
      CALL box_valdato(mensaje)
		DROP TABLE tmp_count
      RETURN FALSE, prog_cnt
   ELSE
      DECLARE counter_ptr CURSOR FOR ex_stmt2

      OPEN counter_ptr

      FETCH counter_ptr INTO prog_cnt

      CLOSE counter_ptr

      CALL display_prgo()

      ERROR "Hay ", prog_cnt USING "<<<<",  " registros encontrados."
      SLEEP 1
      ERROR " "
		DROP TABLE tmp_count
      RETURN TRUE, prog_cnt 
	END IF
END IF  
  
END FUNCTION

FUNCTION fetch_prgo(fetch_flag)
DEFINE 
	fetch_flag SMALLINT, 
   mensaje CHAR(100)

FETCH RELATIVE fetch_flag prog_ptr INTO gr_prgo.prog_id,gr_prgo.usu_nomunix

IF SQLCA.SQLCODE = NOTFOUND THEN
   IF fetch_flag = 1 THEN
      LET mensaje = "Usted est� al final de la lista."
      CALL box_valdato(mensaje)
   ELSE
      LET mensaje = "Usted est� al inicio de la lista."
      CALL box_valdato(mensaje)
   END IF
ELSE
   OPEN prog_all USING gr_prgo.prog_id,gr_prgo.usu_nomunix

   FETCH prog_all INTO gr_prgo.*

   IF SQLCA.SQLCODE = NOTFOUND THEN
      CLEAR FORM
   	DISPLAY "OPCIONES NO ASIGNADAS" TO btdet1
   	DISPLAY "OPCIONES ASIGNADAS" TO btdet2
      DISPLAY BY NAME gr_prgo.prog_id 
      LET mensaje = "El registro fue anulado."
      CALL box_valdato(mensaje)
   ELSE 
      CALL display_prgo()
   END IF
   CLOSE prog_all
END IF
  
END FUNCTION

FUNCTION clean_up()
WHENEVER ERROR CONTINUE
CLOSE prog_ptr
WHENEVER ERROR stop 
END FUNCTION

FUNCTION display_prgo()
DEFINE
	cnt,cnt2 SMALLINT,
	vbandera SMALLINT

DISPLAY BY NAME gr_prgo.*  

CALL clean_det1(1)
CALL clean_det2(1)

-- opciones asignadas al usuario
DECLARE opc_asig CURSOR FOR
SELECT ventas:dprogopc.prog_ord,sd_des.des_desc_md
FROM ventas:dprogopc,adm_usu,maccopcusu,sd_des
WHERE ventas:dprogopc.prog_id = gr_prgo.prog_id
AND   maccopcusu.linkprop = ventas:dprogopc.linkprop
AND   adm_usu.usu_id = maccopcusu.usu_id
AND   adm_usu.usu_nomunix = gr_prgo.usu_nomunix
AND   sd_des.des_tipo = 22
AND   sd_des.des_cod = ventas:dprogopc.des_cod
ORDER BY 1

LET cnt = 1

FOREACH opc_asig INTO ga_prgo2[cnt].*

	IF cnt <= 13 THEN
		DISPLAY ga_prgo2[cnt].* TO s_opc2[cnt].*
	END IF

	LET cnt = cnt + 1

END FOREACH

FREE opc_asig

-- opciones NO asignadas al usuario
DECLARE opc_noasig CURSOR FOR
SELECT ventas:dprogopc.prog_ord,sd_des.des_desc_md
FROM ventas:dprogopc,sd_des
WHERE ventas:dprogopc.prog_id = gr_prgo.prog_id
AND   sd_des.des_tipo = 22
AND   sd_des.des_cod = ventas:dprogopc.des_cod
ORDER BY 1

LET cnt = 1

FOREACH opc_noasig INTO ga_prgo1[cnt].*

	-- verifica si la opcion ya esta asignada al usuario
	LET vbandera = 0
	FOR cnt2 = 1 TO tam_arr
		IF ga_prgo2[cnt2].opc_id2 IS NOT NULL THEN
			IF ga_prgo1[cnt].opc_id1 = ga_prgo2[cnt2].opc_id2 THEN
				LET vbandera = 1
				EXIT FOR
			END IF
		END IF
	END FOR

	IF vbandera = 1 THEN
		LET ga_prgo1[cnt].* = na_prgo1[cnt].*
		CONTINUE FOREACH
	END IF

	IF cnt <= 13 THEN
		DISPLAY ga_prgo1[cnt].* TO s_opc1[cnt].*
	END IF

	LET cnt = cnt + 1

END FOREACH

FREE opc_noasig


END FUNCTION
