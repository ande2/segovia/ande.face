################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo programa 
# Funciones   : prgo_det1() funcion para opciones no asignadas
#               asigna_alldet1(prm1,prtm)
# Parametros	
# Recibidos   :
# Parametros
# Devueltos	  :
#
# Funciones   : prgo_det1() funcion para opciones no asignadas
# Parametros	
# Recibidos   : numero de linea en vector, numero de linea en la pantalla
# Parametros
# Devueltos	  :
#
# SCCS ID No  : %Z% %W%
# Autor       : GYanes
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "genp0051_glob.4gl"

FUNCTION prgo_det1(tip_opc)
DEFINE 
	err_msg CHAR(80),
   mensaje CHAR(100),
   respuesta CHAR(6), 
   repite SMALLINT,
   cnt,cnt1,cnt2,cnt3 SMALLINT,
	tip_opc SMALLINT, --(1=Ingreso, 2=Modificacion)
	accion CHAR(2)

-- activa botones > y >>
--DISPLAY "!" TO bt1
--DISPLAY "!" TO bt3

LET cnt2 = 0

FOR cnt = 1 TO tam_arr
	IF ga_prgo1[cnt].opc_id1 IS NOT NULL THEN
		LET cnt2 = cnt
	END IF
END FOR

CALL set_count(cnt2)

LET ua_prgo1.* = ga_prgo1.*
LET int_flag = FALSE

LET cnt2 = 0
FOR cnt3 = 1 TO tam_arr
	IF ga_prgo2[cnt3].opc_id2 IS NOT NULL THEN
		LET cnt2 = cnt3
	END IF
END FOR

IF cnt2 = 0 THEN
--	DISPLAY "*" TO btdet2
--	DISPLAY "*" TO bt4
ELSE
--	DISPLAY "!" TO btdet2
END IF

--DISPLAY "*" TO bt2

LET int_flag = FALSE

DISPLAY ARRAY ga_prgo1 TO s_opc1.* 
	BEFORE DISPLAY 
		--CALL key_label()
		CALL fgl_dialog_setkeylabel("CONTROL-E","Grabar")

		CALL fgl_dialog_setcurrline(lineap1,linea1)
		LET cnt = linea1
		LET cnt1 = lineap1

		IF ga_prgo1[1].opc_id1 IS NULL THEN
			--DISPLAY "*" to bt1
			--DISPLAY "*" to bt3
		END IF

		LET cnt2 = 0
		FOR cnt3 = 1 TO tam_arr
			IF ga_prgo2[cnt3].opc_id2 IS NOT NULL THEN
				LET cnt2 = cnt3
			END IF
		END FOR

		IF cnt2 > 0 THEN
			--DISPLAY "!" to bt4
		END IF



	--ON KEY (F6)
	ON ACTION btdet2
		LET cnt2 = 0
		FOR cnt3 = 1 TO tam_arr
			IF ga_prgo2[cnt3].opc_id2 IS NOT NULL THEN
				LET cnt2 = cnt3
			END IF
		END FOR

		IF cnt2 > 0 THEN
			LET accion = "D2"
			LET linea1 = 1
			LET lineap1 = 1
			EXIT DISPLAY	
		END IF

	-- asigna una opcion
	--ON KEY (F1)
	ON ACTION bt1
		LET cnt = arr_curr()
		LET cnt1 = scr_line()

		IF ga_prgo1[cnt].opc_id1 IS NOT NULL THEN
			CALL asigna_det1(cnt,cnt1)

			LET accion = "D1"

			IF ga_prgo1[cnt].opc_id1 IS NOT NULL THEN
				LET linea1 = cnt
				LET lineap1 = cnt1
			ELSE
				LET linea1 = cnt-1
				LET lineap1 = cnt1
			END IF

			EXIT DISPLAY
		END IF


	-- asigna todas las opciones
	--ON KEY (F3)
	ON ACTION bt3
		CALL asigna_alldet1()

		LET accion = "D1"
		LET linea1 = 1
		LET lineap1 = 1
		EXIT DISPLAY

	-- desasigna todas las opciones
	--ON KEY (F4)
	ON ACTION bt4

		CALL asigna_alldet2()

		LET accion = "D1"
		LET linea1 = 1
		LET lineap1 = 1
		EXIT DISPLAY

	AFTER DISPLAY
		LET accion = "E"
		LET linea1 = 1
		LET lineap1 = 1

	ON KEY (CONTROL-E)
		IF valida_cambio() THEN
			CALL box_error("No se han hecho cambios a las opciones del usuario")
		ELSE
			LET accion = "G"
			LET linea1 = 1
			LET lineap1 = 1
			EXIT DISPLAY
		END IF
	


   ON KEY(CONTROL-W)
      CALL SHOWHELP(201)


END DISPLAY

IF int_flag = TRUE THEN
   LET int_flag = FALSE
	LET accion = "C"
END IF

-- asigna variables de trabajo a registro de modificacion
LET ua_prgo1.* = ga_prgo1.*

RETURN accion,"D1"

END FUNCTION


FUNCTION asigna_det1(cnt,cnt1)
DEFINE
	cnt,cnt1,cnt2 SMALLINT,
	vopc_id1 INTEGER, 
   vopc_des1 CHAR(20)
	   
	CREATE TEMP TABLE tmp_det1
   (opc_id1 INTEGER,
    opc_des1 CHAR(20)
	) WITH NO LOG

	CREATE TEMP TABLE tmp_det2
   (opc_id2 INTEGER,
    opc_des2 CHAR(20)
	) WITH NO LOG


		-- baja opcion de detalle uno a variables temporales
		LET vopc_id1 = ga_prgo1[cnt].opc_id1
		LET vopc_des1 = ga_prgo1[cnt].opc_des1

		-- elimina opcion de detalle 1
		LET ga_prgo1[cnt].* = na_prgo1[cnt].*

		-- inserta registros del detalle 1 en tabla temporal
		FOR cnt2 = 1 TO tam_arr
			IF ga_prgo1[cnt2].opc_id1 IS NOT NULL THEN
				INSERT INTO tmp_det1 
				VALUES(ga_prgo1[cnt2].*)
			END IF
		END FOR

		-- inserta registros del detalle 2 en tabla temporal
		FOR cnt2 = 1 TO tam_arr
			IF ga_prgo2[cnt2].opc_id2 IS NOT NULL THEN
				INSERT INTO tmp_det2
				VALUES(ga_prgo2[cnt2].*)
			END IF
		END FOR

		-- asigna registro opcion de detalle 1 a detalle 2
		INSERT INTO tmp_det2
		VALUES(vopc_id1, vopc_des1)

		-- limpia detalle 1 y detalle 2
		CALL clean_det1(1)
		CALL clean_det2(1)

		-- asigna datos de tabla temporal 1 a detalle 1
		DECLARE mi_tmp1 CURSOR FOR
		SELECT tmp_det1.*
		FROM tmp_det1
		ORDER BY 1

		LET cnt2 = 1

		FOREACH mi_tmp1 INTO ga_prgo1[cnt2].*
			LET cnt2 = cnt2 + 1
		END FOREACH

		FREE mi_tmp1

		-- asigna datos de tabla temporal 2 a detalle 2
		DECLARE mi_tmp2 CURSOR FOR
		SELECT tmp_det2.*
		FROM tmp_det2
		ORDER BY 1

		LET cnt2 = 1

		FOREACH mi_tmp2 INTO ga_prgo2[cnt2].*
			IF cnt2 <= 13 THEN
				DISPLAY ga_prgo2[cnt2].* TO s_opc2[cnt2].*
			END IF

			LET cnt2 = cnt2 + 1
		
		END FOREACH

		FREE mi_tmp2

	-- elimina tabla tamporal 1y2
	DROP TABLE tmp_det1
	DROP TABLE tmp_det2


END FUNCTION


FUNCTION asigna_alldet1()
DEFINE 
	cnt2 SMALLINT

	-- crea tablas temporales
	CREATE TEMP TABLE tmp_det1
   (opc_id1 INTEGER,
    opc_des1 CHAR(20)
	) WITH NO LOG


	FOR cnt2 = 1 TO tam_arr
		IF ga_prgo1[cnt2].opc_id1 IS NOT NULL THEN
			INSERT INTO tmp_det1 
			VALUES(ga_prgo1[cnt2].*)
		END IF
	END FOR

	FOR cnt2 = 1 TO tam_arr
		IF ga_prgo2[cnt2].opc_id2 IS NOT NULL THEN
			INSERT INTO tmp_det1 
			VALUES(ga_prgo2[cnt2].*)
		END IF
	END FOR

	-- limpia detalle 1 y detalle 2
	CALL clean_det1(1)
	CALL clean_det2(1)

	-- asigna datos de tabla temporal 1 al detalle 2
	DECLARE mi_alltmp1 CURSOR FOR
	SELECT tmp_det1.*
	FROM tmp_det1
	ORDER BY 1

	LET cnt2 = 1

	FOREACH mi_alltmp1 INTO ga_prgo2[cnt2].*
		IF cnt2 <= 13 THEN
			DISPLAY ga_prgo2[cnt2].* TO s_opc2[cnt2].*
		END IF
		LET cnt2 = cnt2 + 1
	END FOREACH

	FREE mi_alltmp1

	-- elimina tabla tamporal 1y2
	DROP TABLE tmp_det1

END FUNCTION
