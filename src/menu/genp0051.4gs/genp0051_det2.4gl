################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo programa 
# Funciones   : prgo_det2() funcion para opciones asignadas
#               asigna_alldet2(prm1,prtm)
# Parametros	
# Recibidos   :
# Parametros
# Devueltos	  :
#
# Funciones   : asigna_det2() funcion para opciones asignadas
# Parametros	
# Recibidos   : numero de linea en vector, numero de linea en la pantalla
# Parametros
# Devueltos	  :
#
# SCCS ID No  : %Z% %W%
# Autor       : GYanes
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "genp0051_glob.4gl"

FUNCTION prgo_det2(tip_opc)
DEFINE 
	err_msg CHAR(80),
   mensaje CHAR(100),
   respuesta CHAR(6), 
   repite SMALLINT,
   cnt,cnt1,cnt2,cnt3 SMALLINT,
	tip_opc SMALLINT, --(1=Ingreso, 2=Modificacion)
	accion CHAR(2)

-- activa botones < y <<
--DISPLAY "!" TO bt2
--DISPLAY "!" TO bt4

LET cnt2 = 0

FOR cnt = 1 TO tam_arr
	IF ga_prgo2[cnt].opc_id2 IS NOT NULL THEN
		LET cnt2 = cnt
	END IF
END FOR

CALL set_count(cnt2)

LET ua_prgo2.* = ga_prgo2.*
LET int_flag = FALSE

LET cnt2 = 0
FOR cnt3 = 1 TO tam_arr
	IF ga_prgo1[cnt3].opc_id1 IS NOT NULL THEN
		LET cnt2 = cnt3
	END IF
END FOR

IF cnt2 = 0 THEN
--	DISPLAY "*" TO btdet1
--	DISPLAY "*" TO bt3
ELSE
--	DISPLAY "!" TO btdet1
END IF

--ISPLAY "*" TO bt1

LET int_flag = FALSE

DISPLAY ARRAY ga_prgo2 TO s_opc2.* 
	BEFORE DISPLAY 
		--CALL key_label()
   	CALL fgl_dialog_setkeylabel("CONTROL-E","Grabar")

     	CALL fgl_dialog_setcurrline(lineap2,linea2)
     	LET cnt = linea2
     	LET cnt1 = lineap2

		IF ga_prgo1[1].opc_id1 IS NULL THEN
			--DISPLAY "*" to bt1
			--DISPLAY "*" to bt3
		END IF

		LET cnt2 = 0
		FOR cnt3 = 1 TO tam_arr
			IF ga_prgo1[cnt3].opc_id1 IS NOT NULL THEN
				LET cnt2 = cnt3
			END IF
		END FOR

		IF cnt2 > 0 THEN
			--DISPLAY "!" to bt3
		END IF



	--ON KEY (F5)
	ON ACTION btdet1

		LET cnt2 = 0
		FOR cnt3 = 1 TO tam_arr
			IF ga_prgo1[cnt3].opc_id1 IS NOT NULL THEN
				LET cnt2 = cnt3
			END IF
		END FOR

		IF cnt2 > 0 THEN
			LET accion = "D1"
			LET linea2 = 1
			LET lineap2 = 1
			EXIT DISPLAY	
		END IF

	-- desasigna asigna una opcion
	--ON KEY (F2)
	ON ACTION bt2
		LET cnt = arr_curr()
		LET cnt1 = scr_line()

		IF ga_prgo2[cnt].opc_id2 IS NOT NULL THEN
			CALL asigna_det2(cnt,cnt1)
			LET accion = "D2"

         IF ga_prgo2[cnt].opc_id2 IS NOT NULL THEN
            LET linea2 = cnt
            LET lineap2 = cnt1
         ELSE
            LET linea2 = cnt-1
            LET lineap2 = cnt1
         END IF

			EXIT DISPLAY
		END IF


	-- desasigna todas las opciones
	--ON KEY (F4)
	ON ACTION bt4

		CALL asigna_alldet2()

		LET accion = "D2"
		LET linea2 = 1
		LET lineap2 = 1
		EXIT DISPLAY

	-- asigna todas las opciones
	--ON KEY (F3)
	ON ACTION bt3	
		CALL asigna_alldet1()
		LET accion = "D2"
		LET linea2 = 1
		LET lineap2 = 1
		EXIT DISPLAY

	AFTER DISPLAY
		LET accion = "E"
		LET linea2 = 1
		LET lineap2 = 1

   ON KEY (CONTROL-E)
      IF valida_cambio() THEN
         CALL box_error("No se han hecho cambios a las opciones del usuario")
      ELSE
      	LET accion = "G"
      	LET linea1 = 1
      	LET lineap1 = 1
      	EXIT DISPLAY
		END IF

   ON KEY(CONTROL-W)
      CALL SHOWHELP(202)


END DISPLAY

IF int_flag = TRUE THEN
   LET int_flag = FALSE
	LET accion = "C"
END IF

-- asigna variables de trabajo a registro de modificacion
LET ua_prgo2.* = ga_prgo2.*

RETURN accion,"D2"

END FUNCTION


FUNCTION asigna_det2(cnt,cnt1)
DEFINE
	cnt,cnt1,cnt2 SMALLINT,
	vopc_id2 INTEGER, 
   vopc_des2 CHAR(20)
	   
	CREATE TEMP TABLE tmp_det2
   (opc_id2 INTEGER,
    opc_des2 CHAR(20)
	) WITH NO LOG

	CREATE TEMP TABLE tmp_det1
   (opc_id1 INTEGER,
    opc_des1 CHAR(20)
	) WITH NO LOG


		-- baja opcion de detalle uno a variables temporales
		LET vopc_id2 = ga_prgo2[cnt].opc_id2
		LET vopc_des2 = ga_prgo2[cnt].opc_des2

		-- elimina opcion de detalle 2
		LET ga_prgo2[cnt].* = na_prgo2[cnt].*

		-- inserta registros del detalle 2 en tabla temporal
		FOR cnt2 = 1 TO tam_arr
			IF ga_prgo2[cnt2].opc_id2 IS NOT NULL THEN
				INSERT INTO tmp_det2 
				VALUES(ga_prgo2[cnt2].*)
			END IF
		END FOR

		-- inserta registros del detalle 1 en tabla temporal
		FOR cnt2 = 1 TO tam_arr
			IF ga_prgo1[cnt2].opc_id1 IS NOT NULL THEN
				INSERT INTO tmp_det1
				VALUES(ga_prgo1[cnt2].*)
			END IF
		END FOR

		-- asigna registro opcion de detalle 2 a detalle 1
		INSERT INTO tmp_det1
		VALUES(vopc_id2, vopc_des2)

		-- limpia detalle 1 y detalle 2
		CALL clean_det1(1)
		CALL clean_det2(1)

		-- asigna datos de tabla temporal 2 a detalle 2
		DECLARE mi_tmp2a CURSOR FOR
		SELECT tmp_det2.*
		FROM tmp_det2
		ORDER BY 1

		LET cnt2 = 1

		FOREACH mi_tmp2a INTO ga_prgo2[cnt2].*
			LET cnt2 = cnt2 + 1
		END FOREACH

		FREE mi_tmp2a

		-- asigna datos de tabla temporal 1 a detalle 1
		DECLARE mi_tmp1a CURSOR FOR
		SELECT tmp_det1.*
		FROM tmp_det1
		ORDER BY 1

		LET cnt2 = 1

		FOREACH mi_tmp1a INTO ga_prgo1[cnt2].*
			IF cnt2 <= 13 THEN
				DISPLAY ga_prgo1[cnt2].* TO s_opc1[cnt2].*
			END IF

			LET cnt2 = cnt2 + 1
		
		END FOREACH

		FREE mi_tmp1a

	-- elimina tabla tamporal 1y2
	DROP TABLE tmp_det1
	DROP TABLE tmp_det2


END FUNCTION


FUNCTION asigna_alldet2()
DEFINE 
	cnt2 SMALLINT

	-- crea tablas temporales
	CREATE TEMP TABLE tmp_det2
   (opc_id2 INTEGER,
    opc_des2 CHAR(20)
	) WITH NO LOG


	FOR cnt2 = 1 TO tam_arr
		IF ga_prgo2[cnt2].opc_id2 IS NOT NULL THEN
			INSERT INTO tmp_det2 
			VALUES(ga_prgo2[cnt2].*)
		END IF
	END FOR

	FOR cnt2 = 1 TO tam_arr
		IF ga_prgo1[cnt2].opc_id1 IS NOT NULL THEN
			INSERT INTO tmp_det2 
			VALUES(ga_prgo1[cnt2].*)
		END IF
	END FOR

	-- limpia detalle 1 y detalle 2
	CALL clean_det1(1)
	CALL clean_det2(1)

	-- asigna datos de tabla temporal 1 al detalle 2
	DECLARE mi_alltmp2 CURSOR FOR
	SELECT tmp_det2.*
	FROM tmp_det2
	ORDER BY 1

	LET cnt2 = 1

	FOREACH mi_alltmp2 INTO ga_prgo1[cnt2].*
		IF cnt2 <= 13 THEN
			DISPLAY ga_prgo1[cnt2].* TO s_opc1[cnt2].*
		END IF
		LET cnt2 = cnt2 + 1
	END FOREACH

	FREE mi_alltmp2

	-- elimina tabla tamporal 1y2
	DROP TABLE tmp_det2

END FUNCTION
