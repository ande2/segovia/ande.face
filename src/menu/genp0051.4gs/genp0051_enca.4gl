################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo programa 
# Funciones   : prgo_enca() funcion para insertar datos
#               pk_exists() funcion para verificar datos existentes	
#	
#	
# Parametros	
# Recibidos   :
# Parametros
# Devueltos	  :
#
# SCCS ID No  : %Z% %W%
# Autor       : GYanes
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# idener		  04/07/2006						 Se agrego los picklist de ayuda en los campos
#														 de Programa y Usuario
################################################################################

GLOBALS "genp0051_glob.4gl"

FUNCTION prgo_enca(tip_opc)
DEFINE 
	err_msg CHAR(80),
   mensaje CHAR(100),
   respuesta CHAR(6), 
   repite SMALLINT,
	tip_opc SMALLINT, --(1=Ingreso, 2=Modificacion)
	vprog_id LIKE   ventas:mprog.prog_id,
   vprog_des LIKE ventas:mprog.prog_des,
	vusu_nomunix LIKE adm_usu.usu_nomunix,
	vusu_nom LIKE  adm_usu.usu_nom,
	accion CHAR(2),
	estado_e,lint_flag SMALLINT,
	x_condicion VARCHAR(100)

DISPLAY "ACCESO A OPCIONES DE PROGRAMA POR USUARIO" AT 05,25 ATTRIBUTE(magenta)
-- desactiva botones >,< y activa >>, <<
--DISPLAY "*" TO bt1
--DISPLAY "*" TO bt2
--DISPLAY "!" TO bt3
--DISPLAY "!" TO bt4

LET ur_prgo.* = gr_prgo.*
LET int_flag = FALSE

INPUT BY NAME gr_prgo.prog_id, gr_prgo.usu_nomunix WITHOUT DEFAULTS
   BEFORE INPUT
      CALL key_label()
 		CALL fgl_dialog_setkeylabel("CONTROL-E","Grabar")
 		CALL fgl_dialog_setkeylabel("CONTROL-B","")
      MESSAGE "Use CONTROL-W para AYUDA a nivel de campo."
		IF gr_prgo.prog_id IS NOT NULL AND gr_prgo.usu_nomunix IS NOT NULL THEN
		ELSE
	   END IF

   ON KEY (CONTROL-E)
      IF valida_cambio() THEN
         CALL box_error("No se han hecho cambios a las opciones del usuario")
      ELSE
      	LET accion = "G"
      	EXIT INPUT
		END IF

   AFTER FIELD prog_id
		IF gr_prgo.prog_id IS NOT NULL THEN
			SELECT ventas:mprog.prog_des
			INTO vprog_des
			FROM ventas:mprog
			WHERE ventas:mprog.prog_id = gr_prgo.prog_id 
			AND ventas:mprog.est_id = 13

			IF SQLCA.SQLCODE = NOTFOUND THEN
				CALL box_error("Programa no est� registrado en la base de datos")
				LET gr_prgo.prog_id = ur_prgo.prog_id
				DISPLAY BY NAME gr_prgo.prog_id
				NEXT FIELD prog_id
			ELSE
				LET gr_prgo.prog_des = vprog_des
				DISPLAY BY NAME gr_prgo.prog_des
				IF tip_opc = 1 THEN
					CALL carga_det1()
					LET ba_prgo1.* = ga_prgo1.*
				END IF

			END IF

			IF gr_prgo.prog_id IS NOT NULL AND gr_prgo.usu_nomunix IS NOT NULL THEN
				--DISPLAY "!" TO bt3
				--DISPLAY "!" TO bt4
			ELSE
				--DISPLAY "*" TO bt3
				--DISPLAY "*" TO bt4
	   	END IF
		END IF

   AFTER FIELD usu_nomunix
		IF LENGTH(gr_prgo.usu_nomunix CLIPPED) > 0 THEN
			SELECT adm_usu.usu_nom
			INTO vusu_nom
			FROM adm_usu
			WHERE usu_nomunix = gr_prgo.usu_nomunix
			IF SQLCA.SQLCODE = NOTFOUND THEN
				CALL box_error("Nombre de usuario no est� registrado en la base de datos")
				LET gr_prgo.usu_nomunix = ur_prgo.usu_nomunix
				DISPLAY BY NAME gr_prgo.usu_nomunix
				NEXT FIELD usu_nomunix
			ELSE
				LET gr_prgo.usu_nom = vusu_nom
				DISPLAY BY NAME gr_prgo.usu_nom
			END IF

			IF gr_prgo.prog_id IS NOT NULL AND gr_prgo.usu_nomunix IS NOT NULL THEN
				--DISPLAY "!" TO bt3
				--DISPLAY "!" TO bt4
			ELSE
				--DISPLAY "*" TO bt3
				--DISPLAY "*" TO bt4
	   	END IF
		END IF

	ON KEY (CONTROL-B)
		CASE
			WHEN INFIELD(prog_id)
				LET lint_flag=FALSE
				LET x_condicion = " 1 = 1 "
				CALL picklist_2('Busqueda de Programas','Codigo','Descripcion',
                            'prog_id','prog_des','mprog',x_condicion,1,1)
            RETURNING vprog_id,vprog_des, lINT_FLAG
				IF lint_flag THEN
					LET lint_flag = FALSE
				ELSE
					LET gr_prgo.prog_id = vprog_id
					LET gr_prgo.prog_des = vprog_des
					DISPLAY BY NAME gr_prgo.prog_id,
										 gr_prgo.prog_des
					IF tip_opc = 1 THEN
					CALL carga_det1()
					LET ba_prgo1.* = ga_prgo1.*
					END IF
					NEXT FIELD usu_nomunix
				END IF

			WHEN INFIELD(usu_nomunix)
				LET lint_flag=FALSE
				LET x_condicion = " 1 = 1 "
				CALL picklist_2('Busqueda de Usuario','Codigo','Descripcion',
                            'usu_nomunix','usu_nom','adm_usu',x_condicion,1,1)
            RETURNING vusu_nomunix,vusu_nom, lINT_FLAG
				IF lint_flag THEN
					LET lint_flag = FALSE
				ELSE
					LET gr_prgo.usu_nomunix = vusu_nomunix
					LET gr_prgo.usu_nom = vusu_nom
					DISPLAY BY NAME gr_prgo.usu_nomunix,
										 gr_prgo.usu_nom
				END IF
		END CASE
								  
	AFTER INPUT 
      IF NOT int_flag THEN
         IF gr_prgo.prog_id IS NULL OR gr_prgo.prog_id <= 0 THEN
				LET mensaje = "Debe de ingresar una c�digo valido"
            CALL box_error(mensaje)
            NEXT FIELD prog_id
         END IF
         IF LENGTH(gr_prgo.usu_nomunix CLIPPED) = 0 THEN
            LET mensaje = "Debe de ingresar un usuario valido"
            CALL box_error(mensaje)
            NEXT FIELD usu_nomunix
         END IF

			SELECT *
			FROM adm_usu
			WHERE usu_nomunix = gr_prgo.usu_nomunix
			IF SQLCA.SQLCODE = NOTFOUND THEN
				LET mensaje="Nombre de usuario no est� registrado en la base de datos"
            CALL box_error(mensaje)
            NEXT FIELD usu_nomunix
			END IF

			SELECT *
			FROM ventas:mprog
			WHERE ventas:mprog.prog_id = gr_prgo.prog_id 
			AND ventas:mprog.est_id = 13

			IF SQLCA.SQLCODE = NOTFOUND THEN
				LET mensaje="Programa no est� registrado en la base de datos"
            CALL box_error(mensaje)
            NEXT FIELD prog_id
			END IF

			IF tip_opc = 1 THEN
      		IF pk_exists() THEN
         		LET mensaje = "Ya existen accesos para el usuario"
         		CALL box_valdato(mensaje)
					LET gr_prgo.usu_nomunix = ur_prgo.usu_nomunix
					LET gr_prgo.usu_nom = ur_prgo.usu_nom
					DISPLAY BY NAME gr_prgo.usu_nomunix, gr_prgo.usu_nom
         		NEXT FIELD prog_id
      		END IF
			END IF

			LET accion = "D1"
			LET estado_e = 1

		ELSE
         CALL box_inter() RETURNING int_flag
         IF int_flag = TRUE THEN 
            EXIT INPUT
         ELSE  
            CALL get_fldbuf(prog_id, usu_nomunix) RETURNING gr_prgo.prog_id, gr_prgo.usu_nomunix
            CONTINUE INPUT
         END IF
      END IF

	ON KEY(CONTROL-W)
      CASE
         WHEN INFIELD(prog_id)
            CALL SHOWHELP(100)
            CALL key_label()
         WHEN INFIELD(usu_nomunix)
            CALL SHOWHELP(101)
      END CASE

   -- asigna todas las opciones
   ON KEY (F3)
      CALL asigna_alldet1()

      LET accion = "E"
      LET linea1 = 1
      LET lineap1 = 1
      EXIT INPUT

   -- desasigna todas las opciones
   ON KEY (F4)
      CALL asigna_alldet2()

      LET accion = "E"
      LET linea1 = 1
      LET lineap1 = 1
      EXIT INPUT


END INPUT

IF int_flag = TRUE THEN
   LET int_flag = FALSE
	LET accion = "C"
END IF

-- asigna variables de trabajo a registro de modificacion
LET ur_prgo.* = gr_prgo.*

RETURN accion,estado_e

END FUNCTION

FUNCTION pk_exists()
DEFINE 
   p_count SMALLINT
      
SELECT COUNT(*) 
INTO p_count
FROM maccopcusu,ventas:dprogopc,adm_usu
WHERE ventas:dprogopc.prog_id = gr_prgo.prog_id
AND   maccopcusu.linkprop = ventas:dprogopc.linkprop
AND   adm_usu.usu_id = maccopcusu.usu_id
AND   adm_usu.usu_nomunix = gr_prgo.usu_nomunix

IF p_count > 0 THEN
   RETURN TRUE
ELSE
   RETURN FALSE
END IF

END FUNCTION

FUNCTION carga_det1()
DEFINE
	cnt SMALLINT

-- opciones NO asignadas al usuario
DECLARE opc_prog CURSOR FOR
SELECT ventas:dprogopc.prog_ord,sd_des.des_desc_md
FROM ventas:dprogopc,sd_des
WHERE ventas:dprogopc.prog_id = gr_prgo.prog_id
AND   sd_des.des_tipo = 22
AND   sd_des.des_cod = ventas:dprogopc.des_cod
ORDER BY 1

LET cnt = 1

FOREACH opc_prog INTO ga_prgo1[cnt].*
	IF cnt <= 13 THEN
		DISPLAY ga_prgo1[cnt].* TO s_opc1[cnt].*
	END IF

	LET cnt = cnt + 1
END FOREACH

FREE opc_prog

END FUNCTION
