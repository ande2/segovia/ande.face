################################################################################
# Funcion     : %M%
# Descripcion : Funciones generales
# Funciones   :
#	
#	
#	
# Parametros	
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : GYanes
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "genp0051_glob.4gl"

FUNCTION valida_cambio()
DEFINE
	cnt SMALLINT

--valida cambios al primer detalle
FOR cnt = 1 TO tam_arr
	IF LENGTH(ba_prgo1[cnt].opc_id1 CLIPPED) > 0 AND
	   LENGTH(ga_prgo1[cnt].opc_id1 CLIPPED) > 0 THEN
		IF ga_prgo1[cnt].opc_id1 <> ba_prgo1[cnt].opc_id1 THEN
			RETURN FALSE
		END IF
	ELSE
		IF LENGTH(ba_prgo1[cnt].opc_id1 CLIPPED) <>
	   	LENGTH(ga_prgo1[cnt].opc_id1 CLIPPED) THEN
			RETURN FALSE
		END IF
	END IF

	IF LENGTH(ba_prgo1[cnt].opc_des1 CLIPPED) > 0 AND
	   LENGTH(ga_prgo1[cnt].opc_des1 CLIPPED) > 0 THEN
		IF ga_prgo1[cnt].opc_des1 <> ba_prgo1[cnt].opc_des1 THEN
			RETURN FALSE
		END IF
	ELSE
		IF LENGTH(ba_prgo1[cnt].opc_des1 CLIPPED) <>
	   	LENGTH(ga_prgo1[cnt].opc_des1 CLIPPED) THEN
			RETURN FALSE
		END IF
	END IF
END FOR

--valida cambios al segundo detalle
FOR cnt = 1 TO tam_arr
	IF LENGTH(ba_prgo2[cnt].opc_id2 CLIPPED) > 0 AND
	   LENGTH(ga_prgo2[cnt].opc_id2 CLIPPED) > 0 THEN
		IF ga_prgo2[cnt].opc_id2 <> ba_prgo2[cnt].opc_id2 THEN
			RETURN FALSE
		END IF
	ELSE
		IF LENGTH(ba_prgo2[cnt].opc_id2 CLIPPED) <>
	   	LENGTH(ga_prgo2[cnt].opc_id2 CLIPPED) THEN
			RETURN FALSE
		END IF
	END IF

	IF LENGTH(ba_prgo2[cnt].opc_des2 CLIPPED) > 0 AND
	   LENGTH(ga_prgo2[cnt].opc_des2 CLIPPED) > 0 THEN
		IF ga_prgo2[cnt].opc_des2 <> ba_prgo2[cnt].opc_des2 THEN
			RETURN FALSE
		END IF
	ELSE
		IF LENGTH(ba_prgo2[cnt].opc_des2 CLIPPED) <>
	   	LENGTH(ga_prgo2[cnt].opc_des2 CLIPPED) THEN
			RETURN FALSE
		END IF
	END IF
END FOR

RETURN TRUE

END FUNCTION

