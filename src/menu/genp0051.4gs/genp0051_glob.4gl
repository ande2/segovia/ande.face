################################################################################
# Funcion     : %M%
# Descripcion : Variables globales 
# Funciones   :
#	
#	
#	
# Parametros	
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Erick Alvarelz
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

DATABASE ventas 

GLOBALS
DEFINE
	g_sccs_var CHAR(70),  -- Variable para SCCS
	vempr_log  CHAR(80),

   gr_prgo RECORD  	-- Registro de trabajo
		prog_id     INTEGER,
		prog_des	   LIKE ventas:mprog.prog_des,
		usu_nomunix LIKE adm_usu.usu_nomunix,
		usu_nom		LIKE adm_usu.usu_nom
   END RECORD,

   nr_prgo RECORD  	-- Registro nulo
		prog_id     INTEGER,
		prog_des	   LIKE ventas:mprog.prog_des,
		usu_nomunix LIKE adm_usu.usu_nomunix,
		usu_nom		LIKE adm_usu.usu_nom
   END RECORD,

   ur_prgo RECORD  	-- Registro para actualizacion
		prog_id     INTEGER,
		prog_des	   LIKE ventas:mprog.prog_des,
		usu_nomunix LIKE adm_usu.usu_nomunix,
		usu_nom		LIKE adm_usu.usu_nom
   END RECORD,

   br_prgo RECORD  	-- Registro para backup
		prog_id     INTEGER,
		prog_des	   LIKE ventas:mprog.prog_des,
		usu_nomunix LIKE adm_usu.usu_nomunix,
		usu_nom		LIKE adm_usu.usu_nom
   END RECORD,

	ga_prgo1 ARRAY[25] OF RECORD    -- Arreglo de trabajo
		opc_id1 INTEGER,
		opc_des1 CHAR(20)
	END RECORD,

	na_prgo1 ARRAY[25] OF RECORD    -- Arreglo nulo
		opc_id1 INTEGER,
		opc_des1 CHAR(20)
	END RECORD,

	ua_prgo1 ARRAY[25] OF RECORD    -- Arreglo para actualizacion
		opc_id1 INTEGER,
		opc_des1 CHAR(20)
	END RECORD,

	ba_prgo1 ARRAY[25] OF RECORD    -- Arreglo para backup
		opc_id1 INTEGER,
		opc_des1 CHAR(20)
	END RECORD,

	ga_prgo2 ARRAY[25] OF RECORD    -- Arreglo de trabajo
		opc_id2 INTEGER,
		opc_des2 CHAR(20)
	END RECORD,

	na_prgo2 ARRAY[25] OF RECORD    -- Arreglo nulo
		opc_id2 INTEGER,
		opc_des2 CHAR(20)
	END RECORD,

	ua_prgo2 ARRAY[25] OF RECORD    -- Arreglo para actualizacion
		opc_id2 INTEGER,
		opc_des2 CHAR(20)
	END RECORD,

	ba_prgo2 ARRAY[25] OF RECORD    -- Arreglo para backup
		opc_id2 INTEGER,
		opc_des2 CHAR(20)
	END RECORD,

	linea1,lineap1,linea2,lineap2 SMALLINT,
	tam_arr SMALLINT,  -- para control de tamano de arreglo
   vusu_id,vlinkprop INTEGER,
	gtit_enc, gtit_pie VARCHAR(100)

END GLOBALS
