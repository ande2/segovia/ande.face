################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo programa 
# Funciones   : input_prgo() funcion para insertar datos
#	
#	
# Parametros	
# Recibidos   :
# Parametros
# Devueltos	  :
#
# SCCS ID No  : %Z% %W%
# Autor       : Gyanes
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "genp0051_glob.4gl"

-- funcion para preparar cursores for UPDATE
FUNCTION cur_init()
DEFINE
   prepvar CHAR(250)

DECLARE lockmprogopc CURSOR FOR
   SELECT *
   FROM maccopcusu
   WHERE maccopcusu.usu_id = vusu_id
   AND   maccopcusu.linkprop = vlinkprop
FOR UPDATE

LET prepvar = "DELETE FROM maccopcusu ",
              "WHERE CURRENT OF lockmprogopc"

PREPARE del_mprogopc FROM prepvar

LET prepvar = "INSERT INTO maccopcusu (usu_id,linkprop) VALUES(?,?)"
PREPARE ins_mprogopc FROM prepvar

END FUNCTION


FUNCTION input_prgo(tipo_operacion)
DEFINE 
	err_msg CHAR(80),
   mensaje CHAR(100),
   respuesta CHAR(6), 
   repite SMALLINT,
	accion CHAR(2),
	tipo_operacion SMALLINT,
	estado CHAR(2),
	estado_e,estado_d1,estado_d2 SMALLINT,
   tmpusu_id,tmplinkprop INTEGER,
	cnt SMALLINT,
	tip_det CHAR(2) --tipo de detalle 

-- inicializa variablaes
LET int_flag = FALSE
LET linea1 = 1
LET lineap1 = 1
LET linea2 = 1
LET lineap2 = 1
LET accion = "E"

IF tipo_operacion = 1 THEN
	CALL clean_enca(1)
	CALL clean_det1(1)
	CALL clean_det2(1)

	LET estado_e = 0
	LET estado_d1 = 0
	LET estado_d2 = 0
	CLEAR FORM
   --DISPLAY "OPCIONES NO ASIGNADAS" TO btdet1
   --DISPLAY "OPCIONES ASIGNADAS" TO btdet2
ELSE
	LET estado_e = 1
	LET estado_d1 = 1
	LET estado_d2 = 1
	CALL bck_prgo()
END IF

-- While que contiene el input del encabezado y el while del los input
-- a los detalles
WHILE TRUE
   MESSAGE ""

	IF accion = "E" THEN
		CALL prgo_enca(tipo_operacion) RETURNING accion,estado_e
		CASE accion
			WHEN "G"

				CALL box_gradato("Desea grabar la información?") RETURNING respuesta
				IF respuesta = "Si" THEN
					EXIT WHILE
				END IF

				IF respuesta = "No" THEN
					LET accion = "C" 
					LET int_flag = TRUE
					EXIT WHILE
				END IF

				IF respuesta = "Cancelar" THEN
					LET accion = "E"
				END IF

			WHEN "C" --cancelar
				EXIT WHILE
			WHEN "D1" --Detalle
				
		END CASE

	END IF

	IF accion <> "E" THEN
		WHILE TRUE
			CASE accion

				WHEN "D1" --detalle uno
					CALL prgo_det1(tipo_operacion) RETURNING accion,tip_det

				WHEN "G" --Grabar
					CALL box_gradato("Desea grabar la información?") RETURNING respuesta
					IF respuesta = "Si" THEN
						EXIT WHILE
					END IF

					IF respuesta = "No" THEN
						LET accion = "C" 
						LET int_flag = TRUE
						EXIT WHILE
					END IF

					IF respuesta = "Cancelar" THEN
						IF tip_det = "D1" THEN
							LET accion = "D1"
						ELSE
							LET accion = "D2"
						END IF
					END IF

				 WHEN "C"  -- cancelar
						EXIT WHILE

				 WHEN "E" --Encabezado
						EXIT WHILE

				 WHEN "D2" --Encabezado
					CALL prgo_det2(tipo_operacion) RETURNING accion,tip_det

				END CASE

		END WHILE

	END IF

	CASE accion
		WHEN "G"
			EXIT WHILE
		WHEN "C"
			EXIT WHILE
	END CASE

END WHILE

IF accion = "C" THEN
	LET int_flag = FALSE
	IF tipo_operacion = 1 THEN
		CLEAR FORM
   	--DISPLAY "OPCIONES NO ASIGNADAS" TO btdet1
   	--DISPLAY "OPCIONES ASIGNADAS" TO btdet2
		CALL box_error("El ingreso fue interrumpido")
	ELSE
		CALL box_error("La modificación fue cancelada")
		-- limpia detalle 1 y 2
		CALL clean_det1(1)
		CALL clean_det2(1)
		-- recupera informacion de backup
		CALL rbck_prgo()
	END IF
	RETURN FALSE
END IF


BEGIN WORK
IF tipo_operacion = 2 THEN
	IF SQLCA.SQLCODE < 0 THEN
		LET err_msg = err_get(SQLCA.SQLCODE)
		ROLLBACK WORK
		LET mensaje = "No se pudo completar la transacción"
		ERROR err_msg
		CALL box_error(mensaje)
		CALL errorlog(err_msg CLIPPED)
		RETURN FALSE
	END IF	

	DECLARE mi_del CURSOR FOR
	SELECT maccopcusu.usu_id,maccopcusu.linkprop
	FROM ventas:dprogopc,adm_usu,maccopcusu
	WHERE ventas:dprogopc.prog_id = gr_prgo.prog_id
	AND   maccopcusu.linkprop = ventas:dprogopc.linkprop
	AND 	adm_usu.usu_id = maccopcusu.usu_id
	AND 	adm_usu.usu_nomunix = gr_prgo.usu_nomunix

	FOREACH mi_del INTO vusu_id,vlinkprop
		WHENEVER ERROR CONTINUE
			OPEN lockmprogopc
			FETCH lockmprogopc INTO tmpusu_id,tmplinkprop
		WHENEVER ERROR STOP

		IF SQLCA.SQLCODE < 0 THEN
			LET err_msg = err_get(SQLCA.SQLCODE)
			ROLLBACK WORK
			LET mensaje = "No se pudo completar la transacción1"
			ERROR err_msg
			CALL box_error(mensaje)
			CALL errorlog(err_msg CLIPPED)
			RETURN FALSE
		END IF	

		WHENEVER ERROR CONTINUE
			EXECUTE del_mprogopc
		WHENEVER ERROR STOP

		IF SQLCA.SQLCODE < 0 THEN
			LET err_msg = err_get(SQLCA.SQLCODE)
			ROLLBACK WORK
			LET mensaje = "No se pudo completar la transacción2"
			ERROR err_msg
			CALL box_error(mensaje)
			CALL errorlog(err_msg CLIPPED)
			RETURN FALSE
		END IF	

		CLOSE lockmprogopc

	END FOREACH

	FREE mi_del

END IF
  
LET tmpusu_id = NULL

-- selecciona el codigo del usuario
SELECT adm_usu.usu_id
INTO tmpusu_id
FROM adm_usu
WHERE adm_usu.usu_nomunix = gr_prgo.usu_nomunix

FOR cnt = 1 TO tam_arr
	LET tmplinkprop = NULL

	IF ga_prgo2[cnt].opc_id2 IS NOT NULL THEN

	   -- selecciona la llave de la opcion
		SELECT ventas:dprogopc.linkprop
		INTO tmplinkprop
		FROM ventas:dprogopc
		WHERE ventas:dprogopc.prog_id = gr_prgo.prog_id
		AND 	ventas:dprogopc.prog_ord = ga_prgo2[cnt].opc_id2

		DISPLAY "CNT ",cnt," OPC ",ga_prgo2[cnt].opc_id2," USU_ID ",tmpusu_id," LINK ",tmplinkprop

		WHENEVER ERROR CONTINUE
			EXECUTE ins_mprogopc USING tmpusu_id,tmplinkprop
		WHENEVER ERROR STOP

		IF SQLCA.SQLCODE < 0 THEN
			LET err_msg = err_get(SQLCA.SQLCODE)
			ROLLBACK WORK
			LET mensaje = "No se pudo completar la transacción3"
			ERROR err_msg
			CALL box_error(mensaje)
			CALL errorlog(err_msg CLIPPED)
			RETURN FALSE
		END IF	

	END IF
END FOR

COMMIT WORK

IF tipo_operacion = 1 THEN
   LET mensaje = "Informacion grabada con Exito"
   CALL box_valdato(mensaje)
   RETURN TRUE
ELSE
   LET mensaje = "Informacion modificada con Exito"
   CALL box_valdato(mensaje)
   RETURN TRUE
END IF

END FUNCTION
