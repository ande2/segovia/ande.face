################################################################################
# Funcion     : %M%
# Descripcion : Modulo principal para opciones de programas 
# Funciones   : 
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : GYanes
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# idener      04/07/2006					    Se quito la extraccion del logo y pasarlo FTP
#														 por desplegar el logo que se encuentra en la maquina cliente
#
################################################################################

GLOBALS "genp0051_glob.4gl"

MAIN
DEFINE
	dbname CHAR(20),
	n_param SMALLINT     

DEFER INTERRUPT

OPTIONS
   INPUT WRAP,
   HELP FILE "prgo_help.ex",
   HELP KEY CONTROL-W,
   COMMENT LINE OFF,
   PROMPT LINE LAST - 2,
   MESSAGE LINE LAST - 1,
   ERROR LINE LAST

LET n_param = num_args()
IF n_param = 0 THEN
   RETURN
ELSE
   LET dbname = arg_val(1)
   DATABASE dbname
END IF      

MESSAGE "Seleccione la opcion con las flechas, Presione CONTROL-W para ayuda"

CALL startlog("prgo_err.log")
CALL main_init(dbname)
CALL inicializa()
CALL cur_init()
CALL prgo_qry_init()
CALL fgl_settitle("OPCIONES DE PROGRAMAS")
CALL main_menu()

LET g_sccs_var = "%W% created on %E%"

END MAIN

FUNCTION main_init(dbname)
DEFINE 
	nombre_departamento, nombre_empresa CHAR(50),
	cmd CHAR(40),
	ip_usu, dbname CHAR(20),
	usuario CHAR(8),
   fecha_actual DATE,
   dir_destino CHAR(100)

LET usuario = fgl_getenv("LOGNAME")                                     
LET fecha_actual = today                                                

SELECT ventas:mempr.empr_nom, ventas:mempr.empr_nomlog
INTO nombre_empresa, vempr_log
FROM ventas:mempr
WHERE ventas:mempr.empr_db = dbname 

SELECT mdepemp.dpe_desc
INTO nombre_departamento 
FROM mdepemp, adm_usu
WHERE mdepemp.dpe_id = adm_usu.dpe_id
AND adm_usu.usu_nomunix = usuario 

LET nombre_departamento = "DEPARTAMENTO ", nombre_departamento CLIPPED

CALL usu_destino() RETURNING ip_usu

OPEN FORM f_prog FROM "genp0051_form"

DISPLAY FORM f_prog
  
CLEAR FORM

--DISPLAY "OPCIONES NO ASIGNADAS" TO btdet1
--DISPLAY "OPCIONES ASIGNADAS" TO btdet2

DISPLAY nombre_empresa AT 03,29 ATTRIBUTE (BLUE,BOLD)
DISPLAY nombre_departamento AT 04,29 ATTRIBUTE (BLUE,BOLD)
DISPLAY "Usuario : ", usuario AT 03,60 ATTRIBUTE (BLUE,BOLD)
DISPLAY "  Fecha : ", fecha_actual AT 04,60 ATTRIBUTE (BLUE,BOLD)
CALL ui.Interface.loadStyles("styles")

END FUNCTION

FUNCTION main_menu()                                                            
DEFINE  
	respuesta CHAR(5),
   cuantos SMALLINT,
   usuario CHAR(8),
   vopciones CHAR(255), --cadena de ceros y unos para control de permisos
   ord_opc SMALLINT,    --orden de las opciones en el menu
   cnt SMALLINT,        --contador
   reg_arr ARRAY[15] OF RECORD -- control de opciones del menu
     opcion   CHAR(15),
     desc_opc CHAR(60),
     des_cod  SMALLINT,
     imagen   CHAR(20)
   END RECORD,
   tlb ARRAY[15] OF RECORD -- control de opciones del toolbar
     acc CHAR(15), --ACTION
     opc CHAR(15), --opcion
     tltip CHAR(60),
     img CHAR(20) --imagen
   END RECORD,
   aui om.DOMnode,
   tb om.DomNode,
   tbi om.DomNode,
   tbs om.DomNode,
   f, tm, tms, tmi, tmg om.DomNode,
   fo ui.FORM,
   wi ui.WINDOW,
	sql_query VARCHAR(30,1)

LET cnt = 1
LET ord_opc = NULL

-- captura nombre de usuario
LET usuario = fgl_getenv("LOGNAME")

-- captura permisos del usuario sobre las opciones del menu
--CALL men_opc(usuario,"prgo_main") RETURNING vopciones
	LET vopciones ="1111111"

display "vopciones ",vopciones

-- carga opciones de menu
DECLARE opciones_menu CURSOR FOR
	SELECT ventas:dprogopc.prog_ord,sd_des.des_desc_md,ventas:dprogopc.prop_desc,
			 sd_des.des_cod,sd_des.des_desc_ct
   FROM ventas:mprog, ventas:dprogopc,sd_des
   WHERE ventas:mprog.prog_nom = "genp0051"
   AND ventas:dprogopc.prog_id = ventas:mprog.prog_id
   AND sd_des.des_tipo = 22
   AND sd_des.des_cod = ventas:dprogopc.des_cod
   ORDER BY ventas:dprogopc.prog_ord

FOREACH opciones_menu INTO ord_opc,reg_arr[cnt].*
   display " --- > ",reg_arr[cnt].opcion
   LET cnt = cnt + 1
END FOREACH
FREE opciones_menu
   LET aui = ui.Interface.getRootNode()
   LET tb  = aui.createChild("ToolBar")

   FOR cnt = 1 TO LENGTH(vopciones)
     LET tlb[cnt].acc  = DOWNSHIFT(reg_arr[cnt].opcion)
     LET tlb[cnt].opc  = reg_arr[cnt].opcion
     LET tlb[cnt].tltip = reg_arr[cnt].desc_opc
     LET tlb[cnt].img  = DOWNSHIFT(reg_arr[cnt].imagen)
   END FOR

   LET tbi = createToolBarItem(tb, tlb[01].acc , tlb[01].opc, tlb[01].tltip, tlb[01].img ) --ingreso
   LET tbi = createToolBarItem(tb, tlb[02].acc , tlb[02].opc, tlb[02].tltip, tlb[02].img ) --busqueda
   LET tbi = createToolBarItem(tb, tlb[03].acc , tlb[03].opc, tlb[03].tltip, tlb[03].img ) --Modifica
   LET tbi = createToolBarItem(tb, tlb[04].acc , tlb[04].opc, tlb[04].tltip, tlb[04].img ) --Modifica
   LET tbs = tb.createChild("ToolBarSeparator")
   LET tbi = createToolBarItem(tb, tlb[05].acc , tlb[05].opc, tlb[05].tltip, tlb[05].img ) --anterior
   LET tbi = createToolBarItem(tb, tlb[06].acc , tlb[06].opc, tlb[06].tltip, tlb[06].img ) --siguiente
   LET tbs = tb.createChild("ToolBarSeparator")
   LET tbi = createToolBarItem(tb, tlb[07].acc , tlb[07].opc, tlb[07].tltip, tlb[07].img ) --salir
-- menu top
   LET f  = ui.Interface.getRootNode()
   LET tm = f.createChild("TopMenu")
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Archivo")

   LET tbi =creaopcion(tmg,tlb[01].acc,tlb[01].opc, tlb[01].tltip, tlb[01].img) --ingreso
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tlb[07].acc,tlb[07].opc, tlb[07].tltip, tlb[07].img) --salir
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Edicion")
   LET tmi =creaopcion(tmg,tlb[02].acc,tlb[02].opc, tlb[02].tltip, tlb[02].img) --buscar
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tlb[03].acc,tlb[03].opc, tlb[03].tltip, tlb[03].img) -- modificar
   LET tmi =creaopcion(tmg,tlb[04].acc,tlb[04].opc, tlb[04].tltip, tlb[04].img) -- Detalle
   LET tmi =creaopcion(tmg,tlb[05].acc,tlb[05].opc, tlb[05].tltip, tlb[05].img) -- anterior
   LET tmi =creaopcion(tmg,tlb[06].acc,tlb[06].opc, tlb[06].tltip, tlb[06].img) -- proximo
   LET tmi =creaopcion(tmg,tlb[07].acc,tlb[07].opc, tlb[07].tltip, tlb[07].img) -- Detalle


MENU ""
   BEFORE MENU
      --CALL key_label()
      LET cuantos = 0
		CALL fgl_dialog_setkeylabel("CONTROL-W", "Ayuda")
      CALL fgl_dialog_setkeylabel("CONTROL-S", "")


-- despliega opciones a las que tiene acceso el usuario
      HIDE OPTION ALL
      FOR cnt = 1 TO LENGTH(vopciones)
         IF vopciones[cnt] = 1 AND (reg_arr[cnt].des_cod = 1 OR 
				reg_arr[cnt].des_cod = 6 OR reg_arr[cnt].des_cod = 9) THEN
            SHOW OPTION reg_arr[cnt].opcion
            LET cuantos = cuantos + 1
         END IF
      END FOR

      display " CUANTOS ",cuantos

      {IF cuantos = 0 THEN
         CALL box_error("Lo siento, no tiene acceso a este programa")
         EXIT MENU
      END IF
}

-- opcion busqueda
	COMMAND reg_arr[2].opcion reg_arr[2].desc_opc
      HELP 1
      CALL query_prgo() RETURNING respuesta, cuantos
      IF respuesta = TRUE THEN
			IF cuantos > 1 THEN

-- despliega opciones a las que tiene acceso el usuario
				HIDE OPTION ALL
      		FOR cnt = 1 TO LENGTH(vopciones)
         		IF vopciones[cnt] = 1 THEN
						display reg_arr[cnt].opcion
						display " linea ",cnt
            		SHOW OPTION reg_arr[cnt].opcion
         		END IF
      		END FOR

			ELSE

-- despliega opciones a las que tiene acceso el usuario
      		HIDE OPTION ALL
      		FOR cnt = 1 TO LENGTH(vopciones)
         		IF vopciones[cnt] = 1 AND (reg_arr[cnt].des_cod <> 2 AND 
						reg_arr[cnt].des_cod <> 3) THEN
            		SHOW OPTION reg_arr[cnt].opcion
         		END IF
      		END FOR

			END IF 
		ELSE

-- despliega opciones a las que tiene acceso el usuario
      	HIDE OPTION ALL
      	FOR cnt = 1 TO LENGTH(vopciones)
         	IF vopciones[cnt] = 1 AND (reg_arr[cnt].des_cod = 1 OR 
					reg_arr[cnt].des_cod = 6 OR reg_arr[cnt].des_cod = 9) THEN
            	SHOW OPTION reg_arr[cnt].opcion
         	END IF
      	END FOR

      END IF
      DISPLAY "                                                        " AT 5,25

-- opcion proximo
	COMMAND reg_arr[6].opcion reg_arr[6].desc_opc
      HELP 3
      CALL fetch_prgo(1)

-- opcion anterior
   COMMAND reg_arr[5].opcion reg_arr[5].desc_opc
      HELP 4
      CALL fetch_prgo(-1)

-- opcion modificar
   COMMAND KEY("O") reg_arr[3].opcion reg_arr[3].desc_opc
      HELP 5
      IF input_prgo(2) = TRUE THEN

-- despliega opciones a las que tiene acceso el usuario
			HIDE OPTION ALL
      	FOR cnt = 1 TO LENGTH(vopciones)
         	IF vopciones[cnt] = 1 AND (reg_arr[cnt].des_cod = 1 OR 
					reg_arr[cnt].des_cod = 6 OR reg_arr[cnt].des_cod = 9) THEN
            	SHOW OPTION reg_arr[cnt].opcion
         	END IF
      	END FOR

		END IF
      DISPLAY "                                                        " AT 5,25
    
-- opcion ingreso
	COMMAND reg_arr[1].opcion reg_arr[1].desc_opc
      HELP 2
      IF input_prgo(1) = TRUE THEN
-- despliega opciones a las que tiene acceso el usuario
      	HIDE OPTION ALL
      	FOR cnt = 1 TO LENGTH(vopciones)

         	IF vopciones[cnt] = 1 AND (reg_arr[cnt].des_cod = 1 OR 
					reg_arr[cnt].des_cod = 6 OR reg_arr[cnt].des_cod = 9) THEN
            	SHOW OPTION reg_arr[cnt].opcion
         	END IF
      	END FOR
      END IF

      DISPLAY "                                                          " AT 5,25
-- opcion anular
   COMMAND reg_arr[4].opcion reg_arr[4].desc_opc


-- opcion salir
	COMMAND reg_arr[7].opcion reg_arr[7].desc_opc
      HELP 7
      EXIT MENU
    
END MENU
END FUNCTION

FUNCTION usu_destino()
DEFINE 
	ip_usu CHAR(20),
   i SMALLINT

LET ip_usu = fgl_getenv("FGLSERVER")

FOR i = 1 TO 20
	IF ip_usu[i] = ":" THEN 
		EXIT FOR
	END IF
END FOR

LET ip_usu = ip_usu[1,i-1] CLIPPED

RETURN ip_usu

END FUNCTION


-- inicializacion de variables
FUNCTION inicializa()
DEFINE
	cnt SMALLINT

	LET tam_arr = 25

	INITIALIZE vusu_id,vlinkprop,nr_prgo.*,na_prgo1[1].*, na_prgo2[1].* TO NULL

	FOR cnt = 2 TO tam_arr
		LET na_prgo1[cnt].* = na_prgo1[1].*
		LET na_prgo2[cnt].* = na_prgo2[1].*
	END FOR

	LET gr_prgo.* = nr_prgo.*
	LET ur_prgo.* = nr_prgo.*
	LET br_prgo.* = nr_prgo.*

	LET ga_prgo1.* = na_prgo1.*
	LET ua_prgo1.* = na_prgo1.*
	LET ba_prgo1.* = na_prgo1.*

	LET ga_prgo2.* = na_prgo2.*
	LET ua_prgo2.* = na_prgo2.*
	LET ba_prgo2.* = na_prgo2.*

	--DISPLAY "@goforw.bmp" TO bt1
	--DISPLAY "@gorev.bmp" TO bt2
	--DISPLAY "@goend.bmp" TO bt3
	--DISPLAY "@gobegin.bmp" TO bt4

	--DISPLAY "OPCIONES NO ASIGNADAS" TO btdet1
	--DISPLAY "OPCIONES ASIGNADAS" TO btdet2

	--DISPLAY "*" TO bt1
	--DISPLAY "*" TO bt2
	--DISPLAY "*" TO bt3
	--DISPLAY "*" TO bt4

END FUNCTION

FUNCTION clean_enca(vtip_opc)
DEFINE
	vtip_opc SMALLINT
	LET gr_prgo.* = nr_prgo.*
	LET ur_prgo.* = nr_prgo.*

	IF vtip_opc = 1 THEN
		DISPLAY BY NAME gr_prgo.*
	END IF

END FUNCTION

FUNCTION clean_det1(vtip_opc)
DEFINE 
	cnt,vtip_opc SMALLINT

	LET ga_prgo1.* = na_prgo1.*
	LET ua_prgo1.* = na_prgo1.*

	IF vtip_opc = 1 THEN
		FOR cnt = 1 TO 13     
			DISPLAY ga_prgo1[cnt].* TO s_opc1[cnt].*
		END FOR
	END IF
END FUNCTION

FUNCTION clean_det2(vtip_opc)
DEFINE 
	cnt,vtip_opc SMALLINT

	LET ga_prgo2.* = na_prgo2.*
	LET ua_prgo2.* = na_prgo2.*

	IF vtip_opc = 1 THEN
		FOR cnt = 1 TO 13     
			DISPLAY ga_prgo2[cnt].* TO s_opc2[cnt].*
		END FOR
	END IF
END FUNCTION

FUNCTION bck_prgo()
	LET br_prgo.* = gr_prgo.*
	LET ba_prgo1.* = ga_prgo1.*
	LET ba_prgo2.* = ga_prgo2.*
END FUNCTION

FUNCTION rbck_prgo()
DEFINE
	cnt SMALLINT

	-- recupera encabezado
	LET gr_prgo.* = br_prgo.*
	DISPLAY BY NAME br_prgo.*

	-- recupera detalle 1 y 2
	LET ga_prgo1.* = ba_prgo1.*
	LET ga_prgo2.* = ba_prgo2.*

	FOR cnt = 1 TO tam_arr
		IF cnt <= 13 THEN
			IF ga_prgo1[cnt].opc_id1 IS NOT NULL THEN
				DISPLAY ga_prgo1[cnt].* TO s_opc1[cnt].*
			END IF
			IF ga_prgo2[cnt].opc_id2 IS NOT NULL THEN
				DISPLAY ga_prgo2[cnt].* TO s_opc2[cnt].*
			END IF
		END IF
	END FOR

END FUNCTION
