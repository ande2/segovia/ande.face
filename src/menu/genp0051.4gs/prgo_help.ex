    .   t   � f  � {     �   � dS  � e�  
 fX  � gB  � h{  : i �  � ��  � ��  fEsta opci�n le permite realizar una b�squeda general de los datos de
las opciones de cada programa asignadas a uno o varios usuarios,
podr� ingresar una serie de criterios o patrones de b�squeda que 
le permitiran la localizaci�n y despliegue de la informaci�n de
su inter�s. 

Luego de realizar la b�squeda podr� MODIFICAR (Agregar o quitar
opciones de programas a los usuarios).

Para seleccionar esta opci�n presione Enter sobre la casilla de
B�squeda o d� un click con el mouse sobre �sta.


            [Screen] = Siguiente P�gina     [Resume] = Salir

 Esta opci�n le permite asignar accesos a opciones de programas a un 
usuario en especifico.

Para seleccionar esta opci�n presione Enter sobre la casilla de 
Ingreso o d� un click con el mouse sobre �sta.



            [Screen] = Siguiente P�gina     [Resume] = Salir

 Esta opci�n le permite ir al siguiente registro encontrado en la 
b�squeda realizada. Si usted ya llego al final de la b�squeda, el sistema  
le indicar� que ya no hay m�s registros.

Para seleccionar esta opci�n presione Enter sobre la casilla 
Pr�ximo o d� un click con el mouse sobre �sta.



            [Screen] = Siguiente P�gina     [Resume] = Salir

 Esta opci�n le permite retornar al registro anterior encontrado en la 
b�squeda realizada.  Si usted ya llego al inicio de la b�squeda, el sistema 
le indicar� que ya no hay m�s registros que consultar.

Para seleccionar esta opci�n presione Enter sobre la casilla 
Anterior o d� un click con el mouse sobre �sta.



            [Screen] = Siguiente P�gina     [Resume] = Salir

 Esta opci�n le permite modificar las opciones a las que el usuario tiene 
accesos en los programas.

Para seleccionar esta opci�n presione Enter sobre la casilla 
Modificar o d� un click con el mouse sobre �sta.



            [Screen] = Siguiente P�gina     [Resume] = Salir

 Esta opci�n le permite salir del Men� para abandonar
el mantenimiento de opciones por usuario.

Para seleccionar esta opci�n presione Enter sobre la casilla 
Salir o d� un click con el mouse sobre �sta.



             [Screen] = Siguiente P�gina     [Resume] = Salir



 En este campo ingrese el Codigo que el programa tiene asignado en el sistema. 

Si no conoce el c�digo del programa puede hacer una b�squeda presionando la
combinaci�n de teclas CONTROL-B o presionando el bot�n "B�scar" que se 
encuentra en el lado derecho de su pantalla.

    
            [Screen] = Siguiente P�gina   [Resume] = Salir

 En este campo ingrese el Nombre del usuario de unix, al que le desea dar acceso,
el usuario debe estar registrando en el sistema.

Si no conoce el nombre del usuario de unix, puede hacer una b�squeda 
presionando la combinaci�n de teclas CONTROL-B o presionando el bot�n "B�scar" 
que se encuentra en el lado derecho de su pantalla.



            [Screen] = Siguiente P�gina   [Resume] = Salir



 En este campo ingrese el C�digo del programa que desea consultar
b�scando en el sistema.

Si no conoce el c�digo del programa puede hacer una b�squeda presionando la
combinaci�n de teclas CONTROL-B o presionando el bot�n "B�scar" que se 
encuentra en el lado derecho de su pantalla.

            [Screen] = Siguiente P�gina   [Resume] = Salir

 En este campo ingrese la descripci�n del programa que desea consultar, puede
ingresar una parte de la descripci�n.
Ejemplo: *art�culos*.
RESULTADO: +----------------------------------------------------+
           |1 Ingreso de art�culos de almac�n             |
           |4 Ingreso de familias de art�culos de almac�n |
           |5 Ingreso de tipos de art�culos de almac�n    |
           |                                                    | 
           +----------------------------------------------------+


            [Screen] = Siguiente Pagina   [Resume] = Salir

 En este campo ingrese el nombre del usuario de unix del cual desea consultar
las opciones a las que tiene acceso.

Si no conoce el nombre del usuario de unix, puede hacer una b�squeda 
presionando la combinaci�n de teclas CONTROL-B o presionando el bot�n "B�scar" 
que se encuentra en el lado derecho de su pantalla.


            [Screen] = Siguiente P�gina   [Resume] = Salir

 En este campo ingrese el nombre del usuario del cual desea consultar los
accesos a los programas.  Puede ingresar una parte de la descripcion por 
ejemplo *JUAN PEREZ*.


            [Screen] = Siguiente P�gina   [Resume] = Salir

 En �sta opci�n puede asignar opciones al usuario presionando los botones
que acontinuaci�n se especifican.

Bot�n ">" Asigna la opci�n en la cual el cursor est� posicionado.

Bot�n ">>" Asigna todas las opciones que el usuario no tiene asignadas del 
programa que se est� trabajando.

Bot�n "<<" Desasigna todas las opciones que el usuario tiene asignadas del
programa que se est� trabajando.


            [Screen] = Siguiente P�gina   [Resume] = Salir

 En �sta opci�n puede asignar opciones al usuario de presionando los botones
que acontinuaci�n se especifican.

Bot�n "<" Desasigna la opci�n en la cual el cursor est� posicionado.

Bot�n ">>" Asigna todas las opciones que el usuario no tiene asignadas del 
programa que se est� trabajando.

Bot�n "<<" Desasigna todas las opciones que el usuario tiene asignadas del
programa que se est� trabajando.


            [Screen] = Siguiente P�gina   [Resume] = Salir

 