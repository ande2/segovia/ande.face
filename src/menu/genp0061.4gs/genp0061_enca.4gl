################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de datos encabezado (mimpusu)
# Funciones   : silo_enca(tipo_operacion)
#					 veri_datos_enca()
#
# Parametros
# Recibidos   :
# Parametros
# Descripcion :  tipo_operacion = 1 es Ingreso
#                tipo_operacion = 2 es Actualizacion
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Erick Alvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
################################################################################
GLOBALS "genp0061_glob.4gl"
# Ingreso del encabezado 
FUNCTION reg_enca(tipo_operacion)
DEFINE 
 	tipo_operacion,
	con_inp,
	lint_flag,
	vdeta,
	estado_e, 
	accion 				CHAR(2),
	respuesta 			CHAR(15),
	mensaje CHAR(100),
	vpais_id          SMALLINT,
   vcampo VARCHAR(50),
   condicion CHAR(150),
	lr_mimpusu RECORD LIKE ventas:mimpusu.*

	LET ur_mimpusu.* = gr_mimpusu.*

	IF tipo_operacion = 1 THEN
		LET gr_mimpusu.usu_tipptr = 1
	END IF 

	INPUT BY NAME gr_mimpusu.* WITHOUT DEFAULTS
		BEFORE INPUT
			CALL fgl_dialog_setkeylabel("ACCEPT", "")
			CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
			CALL fgl_dialog_setkeylabel("CONTROL-G", "Grabar")
			--CALL fgl_dialog_setkeylabel("CONTROL-w","Ayuda")

		if tipo_operacion = 1 then
			CALL DIALOG.setFieldActive("usu_id",1)
		else
			CALL DIALOG.setFieldActive("usu_id",0)
		end if

		ON KEY (CONTROL-G)									-- Grabar
        	CALL get_fldbuf(usu_id,usu_pcid,diu_impid,usu_tipptr)
        		RETURNING gr_mimpusu.usu_id, gr_mimpusu.usu_pcid, gr_mimpusu.diu_impid, gr_mimpusu.usu_tipptr

      	LET lint_flag = FALSE
      	CALL veri_datos_enca() RETURNING lint_flag
      	IF lint_flag = TRUE THEN
         	CALL box_error("Existe Informaci�n incompleta.")
         	NEXT FIELD usu_id
      	END IF 

			LET estado_e = 1
			LET accion = "G"
			LET vdeta = 1
			EXIT INPUT
{
		ON KEY ( CONTROL-W )
			call FGL_DIALOG_GETFIELDNAME() RETURNING vcampo
			CASE
				WHEN INFIELD(usu_id) CALL lib_showhelp(333,"")
            OTHERWISE  CALL lib_helpfield(478,1,vcampo)
			END CASE
}
		AFTER INPUT
        	CALL get_fldbuf(usu_id,usu_pcid,diu_impid, usu_tipptr)
        		RETURNING  gr_mimpusu.usu_id, gr_mimpusu.usu_pcid, gr_mimpusu.diu_impid, gr_mimpusu.usu_tipptr
			IF int_flag THEN
				CALL box_inter() RETURNING int_flag
				IF int_flag = TRUE THEN
					EXIT INPUT
				ELSE
					CONTINUE INPUT
				END IF
			END IF

			IF tipo_operacion = 1 THEN
				SELECT * INTO lr_mimpusu.*
				FROM ventas:mimpusu mimpusu
				WHERE mimpusu.usu_id = gr_mimpusu.usu_id
	  			AND   mimpusu.usu_pcid = gr_mimpusu.usu_pcid
	  			AND   mimpusu.diu_impid = gr_mimpusu.diu_impid

				IF STATUS <> NOTFOUND THEN  
					CALL box_valdato("Ya existe un registro con estos datos!!")
					NEXT FIELD usu_id
				END IF
			ELSE
				IF bmv_mimpusu.* <> gr_mimpusu.* THEN --tuvo cambios el registro 
					SELECT * INTO lr_mimpusu.*
					FROM ventas:mimpusu mimpusu
					WHERE mimpusu.usu_id = gr_mimpusu.usu_id
	  				AND   mimpusu.usu_pcid = gr_mimpusu.usu_pcid
	  				AND   mimpusu.diu_impid = gr_mimpusu.diu_impid
	  				AND   mimpusu.usu_tipptr= gr_mimpusu.usu_tipptr

					IF STATUS <> NOTFOUND THEN  
						CALL box_valdato("Ya existe un registro con estos datos!!")
						NEXT FIELD usu_id
					END IF
				ELSE
					CALL box_valdato("No hizo ning�n cambio!!")
					LET INT_FLAG = TRUE
					EXIT INPUT
				END IF
			END IF

      	LET lint_flag = FALSE
      	CALL veri_datos_enca() RETURNING lint_flag
      	IF lint_flag = TRUE THEN
         	CALL box_error("Existe Informaci�n incompleta.")
         	NEXT FIELD usu_id
      	END IF 

			LET estado_e = 1
			LET accion = "G" --D para detalle
			LET vdeta = 1
	END INPUT

	IF int_flag THEN
		LET accion = "C"
	ELSE
 		LET  ur_mimpusu.* = gr_mimpusu.*
	END IF
	RETURN accion, estado_e, vdeta
END FUNCTION

FUNCTION veri_datos_enca()
	DEFINE  i  SMALLINT

	IF gr_mimpusu.usu_id IS NULL  OR gr_mimpusu.usu_pcid IS NULL OR gr_mimpusu.diu_impid IS NULL THEN
		RETURN TRUE
	ELSE
		RETURN FALSE
	END IF
END FUNCTION	


