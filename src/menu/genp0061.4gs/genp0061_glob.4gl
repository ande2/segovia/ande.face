################################################################################
# Funcion     : %M%
# Descripcion : Variables de uso global para el manto. de tabla impresoras
# Funciones   :
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Erick Alvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################
DATABASE  ventas

GLOBALS
DEFINE

--variables para el encabezado de la forma

	gnom_emp, gnom_dep CHAR(50),
	gcod_emp SMALLINT,
	gusu_sii CHAR(8),
   glog_emp CHAR(15),
	gtit_enc, gtit_pie varCHAR(100),
	gfec_act DATE,
	gnom_win CHAR(30),

--variables de uso general

	dbname CHAR(20),
	n_param SMALLINT,

	bmv_mimpusu RECORD LIKE ventas:mimpusu.*, --BEFORE modification VALUES
	gr_mimpusu RECORD LIKE ventas:mimpusu.*,
	nr_mimpusu RECORD LIKE ventas:mimpusu.*,
	ur_mimpusu RECORD LIKE ventas:mimpusu.*,

	ga_det1 dynamic ARRAY OF RECORD 
		pc_id VARCHAR(100),
		imp_id VARCHAR(100),
		usu_default CHAR(1),
		tipptr SMALLINT
	END RECORD,

	ga_det2 dynamic ARRAY OF RECORD 
		usu_id VARCHAR(20),
		usu_nom VARCHAR(100),
		pc_id VARCHAR(100),
		imp_id VARCHAR(100),
		usu_default CHAR(1),
		tipptr SMALLINT
	END RECORD,

	gaudita SMALLINT,

	c_prog_name VARCHAR(20,1)

END GLOBALS

