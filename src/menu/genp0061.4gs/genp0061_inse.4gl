################################################################################
# Funcion     : %M%
# Descripcion : Inserta registros tabla maestro impresoras
# Funciones   : ins_init()
#  				 reg_inse(tipo_operacion, cnt1, cnt2)
#					 validar(estado_e,estado_d)
#
# Parametros
# Recibidos   :
# Parametros
# Descripcion :  tipo_operacion = 1 es Ingreso
#                tipo_operacion = 2 es Actualizacion
#
#
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Erick Alvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################

GLOBALS "genp0061_glob.4gl"

FUNCTION ins_init()
	DEFINE prepvar CHAR(250)

	--bloqueo especial  para cuando se actualiza/modifica
   DECLARE lockrec1 CURSOR FOR
	  SELECT *
	  FROM ventas:mimpusu mimpusu
	  WHERE mimpusu.usu_id = bmv_mimpusu.usu_id
	  AND   mimpusu.usu_pcid = bmv_mimpusu.usu_pcid
	  AND   mimpusu.diu_impid = bmv_mimpusu.diu_impid
	  FOR UPDATE

	LET prepvar = "INSERT INTO ventas:mimpusu VALUES(?,?,?,?,?)"
	PREPARE ins_mimpusu FROM prepvar

	LET prepvar = "UPDATE  ventas:mimpusu SET ",
       			  " usu_pcid = ?, ",
       			  " diu_impid = ?, ",
       			  " usu_tipptr = ? ",
					  " WHERE CURRENT OF lockrec1 "
	PREPARE upd_mimpusu FROM prepvar

	LET prepvar = "UPDATE  ventas:mimpusu SET ",
       			  " usu_default = ? ",
					  " WHERE ventas:mimpusu.usu_id = ? ",
       			  " AND ventas:mimpusu.usu_pcid = ? ",
       			  " AND ventas:mimpusu.diu_impid = ? "
	PREPARE upd_mimpusuest1 FROM prepvar

	LET prepvar = "UPDATE  ventas:mimpusu SET ",
       			  " usu_default = '0' ",
					  " WHERE ventas:mimpusu.usu_id = ? "
	PREPARE upd_mimpusuest0 FROM prepvar
END FUNCTION


FUNCTION reg_inse(tipo_operacion, cnt1, cnt2)
DEFINE 
	tipo_operacion,		-- 1 = Ingreso, 2 = Modificacion	
	i, linea, lineap, cnt1, cnt2  SMALLINT,
	accion,
	estado_e,
	estado_d 		CHAR(1),
	estado 			CHAR(2),
	err_msg 			CHAR(80),
	respuesta 		CHAR(6),
	mensaje			CHAR(100),
	contador,
   vdeta,
	bandera, lint_flag 		SMALLINT,
	r_mimpusu RECORD LIKE ventas:mimpusu.*

	LET int_flag = FALSE
	LET estado_e = 0
	LET estado_d = 1 --NO se necesita detalle
	LET linea = 0
	LET lineap = 0

	IF tipo_operacion = 2 THEN
		LET bmv_mimpusu.* = gr_mimpusu.*
	end if

   # While que contiene el input del HEADER y while del input al detalle
	WHILE TRUE
		MESSAGE "" 
		CALL reg_enca(tipo_operacion) RETURNING accion, estado_e,vdeta

		CASE accion
			WHEN "G" -- Grabar
				CALL validar(estado_e, estado_d) RETURNING estado
				-- Valida estado_e, estado_d  
				CASE estado
					WHEN "C"
							   LET mensaje = "Desea grabar el Registro "
							   CALL box_gradato(mensaje) RETURNING respuesta
							   IF respuesta = "Si" THEN
					   			EXIT WHILE 
							   END IF
							   IF respuesta = "No" THEN
							   	LET accion = "C"
							   	LET int_flag = TRUE
							   	EXIT WHILE
							   END IF
							   IF respuesta = "Cancel" THEN
							   	LET accion = "E" 
						   	END IF
					WHEN "E"
						LET err_msg = "Error en el encabezado"
					WHEN "D"
						LET err_msg = "(1)Existe información incompleta en detalle"
				END CASE
				IF estado <> "C" THEN
					# Llamar a la funcion que despliegue un mensaje diciendo que
					# input esta incorrecto 
					CALL box_error(err_msg)
				END IF
			WHEN "C" -- Cancelar
				EXIT WHILE
			WHEN "D" -- Detalle
		END CASE
		IF vdeta IS NULL THEN
			LET vdeta = 1
		END IF

		CASE accion
			WHEN "G"
				EXIT WHILE
			WHEN "C"
				EXIT WHILE
		END CASE
	END WHILE

	IF accion = "C" THEN
		LET int_flag = TRUE
		CALL clear_form()
		IF tipo_operacion = 1 THEN
			CALL box_valdato("El ingreso fue cancelado.")
		ELSE
			CALL fetch_rec(0) returning cnt1, cnt2
			CALL box_valdato("La modificación fue cancelada.")
		END IF
		LET gr_mimpusu.* = ur_mimpusu.*
		RETURN int_flag
	END IF

 ###############################  INICIO TRANSACCION ########################
 	SET LOCK MODE TO WAIT
	BEGIN WORK

	CASE (tipo_operacion)
    WHEN 1 -- Grabacion 
			LET r_mimpusu.usu_id = gr_mimpusu.usu_id
			LET r_mimpusu.usu_pcid = gr_mimpusu.usu_pcid
			LET r_mimpusu.diu_impid = gr_mimpusu.diu_impid
			LET r_mimpusu.usu_default = "0" --NO se setea por DEFAULT, debe usar el MENU
			LET r_mimpusu.usu_tipptr  = 1 --gr_mimpusu.usu_tipptr

			WHENEVER ERROR CONTINUE
				EXECUTE ins_mimpusu  USING r_mimpusu.*
			WHENEVER ERROR STOP
  	   	IF SQLCA.SQLCODE < 0 THEN
				LET err_msg = err_get(SQLCA.SQLCODE) 
				ERROR err_msg
				CALL errorlog(err_msg CLIPPED)
				ROLLBACK WORK
  	   		CALL box_error("Error en inserción del encabezado.")
  	   		LET int_flag = TRUE
  	   		RETURN int_flag
			END IF

    WHEN 2 -- Modificacion 
         OPEN lockrec1
         WHENEVER ERROR CONTINUE
            FETCH lockrec1 INTO ur_mimpusu.*
         WHENEVER ERROR STOP

         IF SQLCA.SQLCODE < 0 OR SQLCA.SQLCODE = NOTFOUND THEN
            LET err_msg = err_get(SQLCA.SQLCODE)
            ERROR err_msg
            CALL errorlog(err_msg CLIPPED)
            LET mensaje = "Registro ha sido eliminado por otro usuario! "
            CALL box_valdato(mensaje)
            ROLLBACK WORK
            RETURN TRUE
         end if

         IF ur_mimpusu.* != BMV_mimpusu.* THEN 
            LET mensaje = "Datos ya han sido modificados por otro usuario!! " 
					--||ur_mactprog.userid|| " El "||ur_mactprog.fectran||"! registro("||gr_enca1.linkaprg||")"
            CALL box_valdato(mensaje)
            ROLLBACK WORK
            RETURN TRUE
         END IF


			WHENEVER ERROR CONTINUE
				EXECUTE upd_mimpusu  USING gr_mimpusu.usu_pcid, gr_mimpusu.diu_impid, gr_mimpusu.usu_tipptr
			WHENEVER ERROR STOP
  	   	IF SQLCA.SQLCODE < 0 THEN
				LET err_msg = err_get(SQLCA.SQLCODE) 
				ERROR err_msg
				CALL errorlog(err_msg CLIPPED)
				ROLLBACK WORK
  	   		CALL box_error("Error en modificación del registro.")
  	   		LET int_flag = TRUE
  	   		RETURN int_flag
			END IF
			CALL alerta(1, "Registro ("||gr_mimpusu.usu_id|| ") Actualizado!" )

 END CASE

    COMMIT WORK

	CALL box_valdato("Transaccion exitosa!")

  ######################   FINALIZACION TRANSACCION ####################3
	RETURN int_flag

END FUNCTION

FUNCTION validar(estado_e,estado_d)
	DEFINE
		estado		CHAR(02),
		estado_e,
		estado_d		SMALLINT

	LET estado = "C"
	# Valida que el input del primer detalle este "Completo"
	IF estado_d = 0 THEN
		LET estado = "D"
	END IF
	# Valida que el input del encabezado este "Completo"
	IF estado_e = 0 THEN
		LET estado = "E"
	END IF
	RETURN estado
END FUNCTION

FUNCTION upd_est()
	DEFINE 
	err_msg CHAR(80)

 	SET LOCK MODE TO WAIT
	BEGIN WORK

	WHENEVER ERROR CONTINUE
		EXECUTE upd_mimpusuest0  USING  gr_mimpusu.usu_id 
	WHENEVER ERROR STOP
  	IF SQLCA.SQLCODE < 0 THEN
		LET err_msg = err_get(SQLCA.SQLCODE) 
		ERROR err_msg
		CALL errorlog(err_msg CLIPPED)
		ROLLBACK WORK
  		CALL box_error("Error al quitar la impresora DEFAULT!")
  		RETURN 1
	END IF

	WHENEVER ERROR CONTINUE
		EXECUTE upd_mimpusuest1  USING gr_mimpusu.usu_default,  gr_mimpusu.usu_id, gr_mimpusu.usu_pcid, gr_mimpusu.diu_impid
	WHENEVER ERROR STOP
  	IF SQLCA.SQLCODE < 0 THEN
		LET err_msg = err_get(SQLCA.SQLCODE) 
		ERROR err_msg
		CALL errorlog(err_msg CLIPPED)
		ROLLBACK WORK
  		CALL box_error("Error al asignar la impresora DEFAULT!")
  		RETURN 1
	END IF

   COMMIT WORK
	CALL box_valdato("Transaccion exitosa!")
	RETURN 0
END FUNCTION

