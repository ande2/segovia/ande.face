################################################################################
# Funcion     : %M%
# Descripcion : Funciones principales para el mantenimiento de la tabla
# Funciones   : desp_forma1()
#					 informacion_general()
# 					 menu_MAIN()
# 					 inicializa()
# 					 clean_up(vtipo)
# 					 clear_form()  -- limpia campos del encabezado 
# 					 query_init()
# 					 query_dat()
# 					 fetch_rec(fetch_flag)
# 					 desp_rec()
# 					 sel_deta(linkrec01)
# 					 desp_deta(row_cnt)
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Erick Alvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
################################################################################
GLOBALS "genp0061_glob.4gl"
MAIN
	DEFER INTERRUPT

	OPTIONS
   	INPUT WRAP,
   	HELP KEY CONTROL-W,
   	COMMENT LINE OFF,
   	PROMPT LINE LAST - 2,
   	MESSAGE LINE LAST - 1,
   	ERROR LINE LAST

	LET n_param = num_args()
	LET c_prog_name = "genp0061"

	IF n_param = 0 THEN
   	RETURN
	ELSE
   	LET dbname = arg_val(1)
   	DATABASE dbname

	END IF

   CALL ui.Interface.loadStyles("styles")
	CALL ui.interface.setImage(get_image("logo.bmp"))
	LET gaudita = 0

	CALL inicializa()
	CALL desp_forma1()
	CALL informacion_general()

	CALL startlog(c_prog_name||".log")
	CALL ins_init()
	CALL query_init()
	CALL menu_MAIN()

END MAIN

--funcion que despliega la forma en la pantalla
FUNCTION desp_forma1()
	DEFINE fieldtooltip STRING

	CLOSE WINDOW SCREEN
	OPEN WINDOW forma1 AT 01,01 WITH FORM "genp0061_form" ATTRIBUTE(MESSAGE LINE LAST)

	--LLENADO DE TABLE tmp para buscar los tooltip de campos, deben uncluir la tabla o FORMONLY para que los encuentre
	--segun como fueron creados en la forma. (el # es el link del tooltip)
   #-- Obtiene la lista de valores que no son de base de datos
	LET fieldtooltip = 
			--el primero NO lleva el simbolo |, el # es el link de la tabla de Ayuda
			--Un tooltip puede ser asignado a un campo en especifico (ver ejemplo de pais y pais_id
			--lo que se asigne aqui en la temporal, sera utilizado en todas las pantallas de este programa
			"333|formonly.usu_id",
         "|334|formonly.usu_pcid",
         "|335|formonly.diu_impid",
         "|336|formonly.usu_default",
         "|337|formonly.usu_tipptr"

	CALL fill_tmptooltip(fieldtooltip)
	CURRENT WINDOW IS forma1
	CLEAR FORM
END FUNCTION

FUNCTION informacion_general()
	DEFINE nomwin varchar(200)

	LET gusu_sii = fgl_getenv("LOGNAME") --usuario del SII
	LET gfec_act = TODAY --fecha actual

	MESSAGE get_usuname(gusu_sii), "     ",gfec_act  ATTRIBUTE ( BLUE )


	SELECT ventas:mempr.empr_nom, ventas:mempr.empr_id, ventas:mempr.empr_nomlog
	INTO gnom_emp, gcod_emp, glog_emp
	FROM ventas:mempr
	WHERE ventas:mempr.empr_db = dbname

	SELECT adm_usu.usu_nomwin
	INTO gnom_win
	FROM adm_usu
	WHERE adm_usu.usu_nomunix = gusu_sii

   CALL get_nprog(c_prog_name) returning  gtit_enc

   CALL busca_depto(gusu_sii) RETURNING gnom_dep
   LET nomwin = gnom_emp clipped || " - ["||gtit_enc CLIPPED||"]"
   CALL fgl_settitle(nomwin)

   display by name gtit_enc
END FUNCTION

# Rutina menu 
FUNCTION menu_MAIN()
	DEFINE
	lint_flag, cuantos, tooltip_stat SMALLINT,
   usuario VARCHAR(8),
   vopciones VARCHAR(255), --cadena de ceros y unos para control de permisos
   ord_opc SMALLINT,    --orden de las opciones en el menu
   cnt     SMALLINT,    --contdor
	det_cnt,det_cnt2 INTEGER,     -- cantidad de lineas en el detalle
   reg_arr ARRAY[12] OF RECORD -- control de opciones del menu
     opcion   VARCHAR(15),
     desc_opc VARCHAR(60),
     des_cod  SMALLINT,
     imagen    VARCHAR(100)
   end record,
	respuesta CHAR(20),
   tlb ARRAY[12] OF RECORD -- control de opciones del toolbar
     acc VARCHAR(15), --ACTION
     opc VARCHAR(15), --opcion
     tltip VARCHAR(60),
     img VARCHAR(100) --imagen
   END RECORD,
   aui om.DOMnode,
   tb om.DomNode,
   tbi om.DomNode,
   tbs om.DomNode,
   f, tm, tms, tmi, tmg om.DomNode,
	fo ui.FORM,
	wi ui.WINDOW,
	vprinter, fileprint, printfile VARCHAR(100)

   LET ord_opc = NULL

   LET usuario = gusu_sii

-- captura permisos del usuario sobre las opciones del menu
   CALL men_opc(usuario, c_prog_name) RETURNING vopciones
	LET vopciones='11111111111'

-- carga opciones de menu
   DECLARE opciones_menu CURSOR FOR
   SELECT ventas:dprogopc.prog_ord,sd_des.des_desc_md,ventas:dprogopc.prop_desc,
   sd_des.des_cod,sd_des.des_desc_ct
   FROM ventas:mprog,ventas:dprogopc,sd_des
   WHERE ventas:mprog.prog_nom = c_prog_name
   AND ventas:dprogopc.prog_id = ventas:mprog.prog_id
   AND sd_des.des_tipo = 22
   AND sd_des.des_cod = ventas:dprogopc.des_cod
   order by ventas:dprogopc.prog_ord

   LET cnt = 1
   FOREACH opciones_menu INTO ord_opc,reg_arr[cnt].*
      LET cnt = cnt + 1
   END FOREACH

   FREE opciones_menu

 	LET aui = ui.Interface.getRootNode()
   LET tb  = aui.createChild("ToolBar")

   FOR cnt = 1 TO LENGTH(vopciones)
     LET tlb[cnt].acc  = DOWNSHIFT(reg_arr[cnt].opcion)
	  LET tlb[cnt].opc  = reg_arr[cnt].opcion 
	  LET tlb[cnt].tltip = reg_arr[cnt].desc_opc 
	  LET tlb[cnt].img  = DOWNSHIFT(get_image(reg_arr[cnt].imagen))
   END FOR

   LET tbi = createToolBarItem(tb, tlb[05].acc , tlb[05].opc, tlb[05].tltip, tlb[05].img ) --ingreso
   LET tbi = createToolBarItem(tb, tlb[01].acc , tlb[01].opc, tlb[01].tltip, tlb[01].img ) --busqueda
   LET tbi = createToolBarItem(tb, tlb[04].acc , tlb[04].opc, tlb[04].tltip, tlb[04].img ) --Modifica
	LET tbs = tb.createChild("ToolBarSeparator")
   LET tbi = createToolBarItem(tb, tlb[03].acc , tlb[03].opc, tlb[03].tltip, tlb[03].img ) --anterior
   LET tbi = createToolBarItem(tb, tlb[02].acc , tlb[02].opc, tlb[02].tltip, tlb[02].img ) --siguiente
   LET tbi = createToolBarItem(tb, tlb[07].acc , tlb[07].opc, tlb[07].tltip, tlb[07].img ) --detalle
   LET tbi = createToolBarItem(tb, tlb[06].acc , tlb[06].opc, tlb[06].tltip, tlb[06].img ) --impresion
	LET tbs = tb.createChild("ToolBarSeparator")
   LET tbi = createToolBarItem(tb, tlb[09].acc , tlb[09].opc, tlb[09].tltip, tlb[09].img ) --deshabilita
   LET tbi = createToolBarItem(tb, tlb[10].acc , tlb[10].opc, tlb[10].tltip, tlb[10].img ) --habilita
   LET tbi = createToolBarItem(tb, tlb[11].acc , tlb[11].opc, tlb[11].tltip, tlb[11].img ) --Historial
	LET tbs = tb.createChild("ToolBarSeparator")
   LET tbi = createToolBarItem(tb, tlb[08].acc , tlb[08].opc, tlb[08].tltip, tlb[08].img ) --salir

	CALL tb.setAttribute("buttonTextHidden",1)

-- menu top
   LET f  = ui.Interface.getRootNode()
   LET tm = f.createChild("TopMenu")
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Archivo")

   LET tbi =creaopcion(tmg,tlb[05].acc,tlb[05].opc, tlb[05].tltip, tlb[05].img) --ingreso
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tlb[08].acc,tlb[08].opc, tlb[08].tltip, tlb[08].img) --salir
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Edicion")
   LET tmi =creaopcion(tmg,tlb[01].acc,tlb[01].opc, tlb[01].tltip, tlb[01].img) --buscar
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tlb[03].acc,tlb[03].opc, tlb[03].tltip, tlb[03].img) -- anterior
   LET tmi =creaopcion(tmg,tlb[02].acc,tlb[02].opc, tlb[02].tltip, tlb[02].img) -- proximo
   LET tmi =creaopcion(tmg,tlb[04].acc,tlb[04].opc, tlb[04].tltip, tlb[04].img) -- modifica
   LET tmi =creaopcion(tmg,tlb[06].acc,tlb[06].opc, tlb[06].tltip, tlb[06].img) -- Impresion
   LET tmi =creaopcion(tmg,tlb[07].acc,tlb[07].opc, tlb[07].tltip, tlb[07].img) -- Detalle
   LET tmg =tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Herramientas")
   LET tmi =creaopcion(tmg,tlb[10].acc,tlb[10].opc, tlb[10].tltip, tlb[10].img)  --habilitar
   LET tmi =creaopcion(tmg,tlb[09].acc,tlb[09].opc, tlb[09].tltip, tlb[09].img)  --deshabilitar
   LET tmi =creaopcion(tmg,tlb[11].acc,tlb[11].opc, tlb[11].tltip, tlb[11].img)  --historial
   LET tmi =creaopcion(tmg,'tooltip','Activar/Desactivar Tooltip', 'Mostrar� el tooltip del campo', get_image('help.png'))


	MENU ""
		BEFORE MENU
			LET tooltip_stat = 1
			LET wi = ui.Window.getCurrent()
			LET fo = wi.getForm()
			--CALL checkCommentRecursive(fo.getNode(),tooltip_stat)

		   CALL fo.setElementImage("pg1",get_image("contacto.png"))
		   CALL fo.setElementImage("pg2",get_image("cliente.png"))

			--CALL fgl_dialog_setkeylabel("CONTROL-w", "Ayuda")
      	LET cuantos = 0

-- despliega opciones a las que tiene acceso el usuario
      	HIDE OPTION ALL
      	FOR cnt = 1 TO LENGTH(vopciones)
         	IF vopciones[cnt] = 1 AND
            	(reg_arr[cnt].des_cod = 1 OR
            	reg_arr[cnt].des_cod = 6 OR
            	reg_arr[cnt].des_cod = 8 OR
            	reg_arr[cnt].des_cod = 9) THEN
            	SHOW OPTION reg_arr[cnt].opcion
            	LET cuantos = cuantos + 1
         	END IF
      	END FOR
			SHOW OPTION "Tooltip"

      	IF cuantos = 0 THEN
         	CALL box_error("Lo siento, no tiene acceso a este programa")
         	EXIT MENU
      	END IF

		--habilita/deshabilita tooltip
		COMMAND "Tooltip"
{ hasta que este habilitada la tabla mayuda en produccion
			IF tooltip_stat THEN
				LET tooltip_stat = 0
				CALL checkCommentRecursive(fo.getNode(),tooltip_stat)
				CALL box_valdato("Tooltip Deshabilitado!")
			ELSE
				LET tooltip_stat = 1
				CALL checkCommentRecursive(fo.getNode(),tooltip_stat)
				CALL box_valdato("Tooltip Habilitado!")
			END IF

		--ayuda en linea
		COMMAND KEY (CONTROL-w)
				CALL lib_showhelp(343,' ')
}

-- opcion busqueda
      COMMAND KEY("B") reg_arr[1].opcion reg_arr[1].desc_opc HELP 2
			CALL clean_up(1, reg_arr[1].desc_opc)
			CALL query_dat() RETURNING int_flag, cuantos , det_cnt, det_cnt2
			IF int_flag THEN
				CALL clean_up(0, "MAESTRO DE IMPRESORAS")
				IF cuantos > 1 THEN
      			-- despliega opciones a las que tiene acceso el usuario
      			HIDE OPTION ALL
      			FOR cnt = 1 TO LENGTH(vopciones)
         			IF vopciones[cnt] = 1 THEN
							--revisa opciones 18 y 19 (deshablitar, habilitar)
							IF cnt = 9 OR cnt = 10 THEN
								IF gr_mimpusu.usu_default <> "1" THEN 
            					SHOW OPTION reg_arr[10].opcion --habilitar
								ELSE
            					HIDE OPTION reg_arr[10].opcion --habilitar
								END IF
							ELSE
            				SHOW OPTION reg_arr[cnt].opcion
							END IF
         			END IF
      			END FOR
					SHOW OPTION "Tooltip"
				ELSE
-- despliega opciones a las que tiene acceso el usuario
      			HIDE OPTION ALL
      			FOR cnt = 1 TO LENGTH(vopciones)
         			IF vopciones[cnt] = 1 AND
							(reg_arr[cnt].des_cod <> 2 AND
							 reg_arr[cnt].des_cod <> 3) THEN
							--revisa opciones 18 y 19 (deshablitar, habilitar)
							IF cnt = 9 OR cnt = 10 THEN
								IF gr_mimpusu.usu_default <> "1" THEN 
            					SHOW OPTION reg_arr[10].opcion --habilitar
								ELSE
            					HIDE OPTION reg_arr[10].opcion --habilitar
								END IF
							ELSE
            				SHOW OPTION reg_arr[cnt].opcion
							END IF
         			END IF
      			END FOR
					SHOW OPTION "Tooltip"

				END IF
			ELSE
				CALL clean_up(1, "MAESTRO DE IMPRESORAS")
-- despliega opciones a las que tiene acceso el usuario
      		HIDE OPTION ALL
      		FOR cnt = 1 TO LENGTH(vopciones)
         		IF vopciones[cnt] = 1 AND
            		(reg_arr[cnt].des_cod = 1 OR
            		reg_arr[cnt].des_cod = 6 OR
            		reg_arr[cnt].des_cod = 8 OR
            		reg_arr[cnt].des_cod = 9) THEN
            		SHOW OPTION reg_arr[cnt].opcion
         		END IF
      		END FOR
				SHOW OPTION "Tooltip"

			END IF

-- opcion proximo
      COMMAND KEY("P") reg_arr[2].opcion reg_arr[2].desc_opc HELP 5
			CALL fetch_rec(1) returning det_cnt, det_cnt2
			IF gr_mimpusu.usu_default <> "1" THEN 
     			SHOW OPTION reg_arr[10].opcion --habilitar
			ELSE
     			HIDE OPTION reg_arr[10].opcion --habilitar
			END IF

-- opcion anterior
      COMMAND KEY("A") reg_arr[3].opcion reg_arr[3].desc_opc HELP 6
			CALL fetch_rec(-1) returning det_cnt, det_cnt2
			IF gr_mimpusu.usu_default <> "1" THEN 
     			SHOW OPTION reg_arr[10].opcion --habilitar
			ELSE
     			HIDE OPTION reg_arr[10].opcion --habilitar
			END IF

-- opcion modificar
      COMMAND KEY("M") reg_arr[4].opcion reg_arr[4].desc_opc HELP 3
					CALL clean_up(0, reg_arr[4].desc_opc)
               LET int_flag = FALSE
			      CALL reg_inse(2,det_cnt, det_cnt2) RETURNING int_flag

					IF NOT(INT_FLAG) THEN
            		LET int_flag = FALSE
						CALL clean_up(1, "MAESTRO DE IMPRESORAS")
            		-- despliega opciones a las que tiene acceso el usuario
     	      		HIDE OPTION ALL
     	      		FOR cnt = 1 TO LENGTH(vopciones)
      	      		IF vopciones[cnt] = 1 AND
        		      		(reg_arr[cnt].des_cod = 1 OR
        		      		reg_arr[cnt].des_cod = 6 OR
        		      		reg_arr[cnt].des_cod = 8 OR
        		      		reg_arr[cnt].des_cod = 9) THEN
        		      		SHOW OPTION reg_arr[cnt].opcion
        	      		END IF
     	      		END FOR
						SHOW OPTION "Tooltip"
					ELSE
						CALL clean_up(0, "MAESTRO DE IMPRESORAS")
					END IF

-- opcion ingreso
      COMMAND KEY("I") reg_arr[5].opcion reg_arr[5].desc_opc HELP 1
               LET int_flag = FALSE
					CALL clean_up(1, reg_arr[5].desc_opc)
			      CALL set_count(0)
               -- despliega opciones a las que tiene acceso el usuario
     		      HIDE OPTION ALL
     		      FOR cnt = 1 TO LENGTH(vopciones)
        		      IF vopciones[cnt] = 1 AND
           		      (reg_arr[cnt].des_cod = 1 OR
           		      reg_arr[cnt].des_cod = 6 OR
           		      reg_arr[cnt].des_cod = 8 OR
          		      reg_arr[cnt].des_cod = 9) THEN
           		      SHOW OPTION reg_arr[cnt].opcion
        		      END IF
     		      END FOR
					SHOW OPTION "Tooltip"
			      CALL reg_inse(1,det_cnt, det_cnt2) RETURNING int_flag
					CALL clean_up(1, "MAESTRO DE IMPRESORAS")

-- opcion impresion
      COMMAND KEY ("R") reg_arr[6].opcion reg_arr[6].desc_opc HELP 4
			IF gr_mimpusu.usu_id IS NULL THEN
				CALL box_valdato("Debe seleccionar un registro para poder imprimir la pagina de prueba!")
			ELSE

			   CALL box_gradato("Desea imprimir p�gina de prueba") RETURNING respuesta
			   IF respuesta = "Si" THEN
					RUN "echo Test ARGOSS  Test ARGOSS >  $HOME/test.txt"
					RUN "cal "||YEAR(TODAY)||' >> $HOME/test.txt'

            	LET fileprint = fgl_getenv("HOME")||"/test.txt"
            	LET printfile = fgl_getenv("HOME")||"/test_p.txt"
					LET vprinter  = gr_mimpusu.usu_pcid CLIPPED||gr_mimpusu.diu_impid CLIPPED
            	CALL imprimir(35, fileprint, printfile, "test_p.txt", vprinter)
					CALL box_valdato("Test de impresion enviada a : "||vprinter)
			   END IF
			END IF

	-- opcion ver detalle
		COMMAND KEY ("D") reg_arr[7].opcion reg_arr[7].desc_opc HELP 8
	      CALL disp_det(det_cnt,det_cnt2)

		--deshabilitar
      COMMAND KEY("E") reg_arr[9].opcion reg_arr[9].desc_opc

		--habilitar
      COMMAND KEY("H") reg_arr[10].opcion reg_arr[10].desc_opc
			CALL cambia_est("1")

		   CALL box_gradato("Desea setear la impresora DEFAULT ?") RETURNING respuesta
		   IF respuesta = "Si" THEN
				CALL upd_est() RETURNING INT_FLAG
            LET int_flag = FALSE
		      CALL clean_up(1, "MAESTRO DE SILOS")
            -- despliega opciones a las que tiene acceso el usuario
     	      HIDE OPTION ALL
     	      FOR cnt = 1 TO LENGTH(vopciones)
      	      IF vopciones[cnt] = 1 AND
        		      (reg_arr[cnt].des_cod = 1 OR
        		      reg_arr[cnt].des_cod = 6 OR
        		      reg_arr[cnt].des_cod = 8 OR
        		      reg_arr[cnt].des_cod = 9) THEN
        		      SHOW OPTION reg_arr[cnt].opcion
        	      END IF
     	      END FOR
				SHOW OPTION "Tooltip"
			ELSE
				CALL cambia_est("0")
			END IF

		--Muestra historia de bitacora
      COMMAND KEY("T") reg_arr[11].opcion reg_arr[11].desc_opc
			--CALL lib_showhist(60, gr_mimpusu.usu_id)

      -- opcion salir
      COMMAND KEY("S") reg_arr[8].opcion reg_arr[8].desc_opc
			HELP 7 
			EXIT MENU
	END MENU
END FUNCTION

# Rutina para inicializar variables
FUNCTION inicializa()
	DEFINE 
		i SMALLINT

	INITIALIZE gr_mimpusu.*, nr_mimpusu.*, ur_mimpusu.* TO NULL

	CALL ga_det1.clear()
	CALL ga_det2.clear()
END FUNCTION

# Despliega los datos fijos
FUNCTION clean_up(vtipo, descrip)
	DEFINE
		i  INTEGER,
		vtipo SMALLINT,
		descrip VARCHAR(200)

	IF vtipo = 1 THEN
		LET gr_mimpusu.* = nr_mimpusu.*
		LET ur_mimpusu.* = nr_mimpusu.*

		CALL ga_det1.clear()
		CALL ga_det2.clear()

		CALL clear_form()
	END IF
	DISPLAY descrip TO gtit_enc 
END FUNCTION

FUNCTION clear_form()  
	CLEAR FORM

	LET gr_mimpusu.* = nr_mimpusu.* 
END FUNCTION  

FUNCTION query_init()
DEFINE
	select_stmt3			CHAR(350)

	LET select_stmt3 = 
	  "SELECT * FROM ventas:mimpusu ",
	  "WHERE ventas:mimpusu.usu_id = ? ",
	  " AND ventas:mimpusu.usu_pcid = ? ",
	  " AND ventas:mimpusu.diu_impid = ? ",
	  " ORDER BY 1, 2, 3"

	PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION

FUNCTION query_dat()
	DEFINE
		sub_query			CHAR(300),
		where_clause		CHAR(250),
		sub_cnt				SMALLINT,
		det_cnt,det_cnt2	SMALLINT,
		sub_count			CHAR(300),
		vcampo VARCHAR(50)

	CALL alerta(0, "Ingrese criterio de busqueda, luego presione ACEPTAR o CANCELAR" )
	LET int_flag = FALSE

	CONSTRUCT where_clause ON 
		mimpusu.usu_id, mimpusu.usu_pcid, mimpusu.diu_impid
        FROM usu_id, usu_pcid, diu_impid

		BEFORE CONSTRUCT
			CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
			CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
			--CALL fgl_dialog_setkeylabel("CONTROL-w","Ayuda")

		ON KEY (INTERRUPT)
			CALL box_inter() RETURNING int_flag
			IF int_flag THEN
				EXIT CONSTRUCT
			END IF
{ hasta que este habilitada tabla mayuda en produccion
		ON KEY ( CONTROL-W )
			call FGL_DIALOG_GETFIELDNAME() RETURNING vcampo
			CASE
				WHEN INFIELD(usu_id) CALL lib_showhelp(333,"")
            OTHERWISE  CALL lib_helpfield(478,1,vcampo)
			END CASE
}
	END CONSTRUCT
	CALL alerta(0, "")

	IF int_flag = TRUE THEN
		LET int_flag = FALSE
		CALL box_error("Busqueda cancelada...")
		RETURN FALSE, 0, 0, 0
	END IF

	LET sub_query = "SELECT * FROM ventas:mimpusu mimpusu WHERE ", where_clause CLIPPED,
	  					 " ORDER BY 1, 2, 3"

   LET sub_COUNT = "SELECT COUNT(*) FROM ventas:mimpusu mimpusu WHERE ", where_clause CLIPPED

	WHENever ERROR CONTINUE
	PREPARE ex_stmt FROM sub_query
	PREPARE ex_stmt2 FROM sub_count

	DECLARE sub_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt
	OPEN sub_ptr 
	FETCH FIRST sub_ptr INTO gr_mimpusu.*

	WHENEVER ERROR STOP
	IF SQLCA.SQLCODE = 100 THEN
		CALL box_error("No se encontraron registros...")
		RETURN FALSE, 0,0,0
	ELSE

		DECLARE sub_all CURSOR FOR ex_stmt3
		OPEN sub_all USING gr_mimpusu.usu_id, gr_mimpusu.usu_pcid, gr_mimpusu.diu_impid
		FETCH sub_all INTO gr_mimpusu.*
		IF SQLCA.SQLCODE = NOTFOUND THEN
			CALL box_error("No se encontraron registros...")
			RETURN FALSE, 0,0,0
		ELSE
			DECLARE counter_ptr CURSOR FOR ex_stmt2
			OPEN counter_ptr
			FETCH counter_ptr INTO sub_cnt
			CLOSE counter_ptr
			CALL desp_rec()RETURNING det_cnt,det_cnt2

			RETURN TRUE, sub_cnt , det_cnt,det_cnt2
		END IF
	END IF

END FUNCTIOn

FUNCTION fetch_rec(fetch_flag)
	DEFINE
		fetch_flag		SMALLINT,
		det_cnt,det_cnt2 INTEGER

	FETCH RELATIVE fetch_flag sub_ptr INTO gr_mimpusu.*
	IF SQLCA.SQLCODE = NOTFOUND THEN
		IF fetch_flag = 1 THEN
			CALL box_valdato("Usted esta posicionado en el final de la lista...")
			CALL desp_rec() RETURNING det_cnt,det_cnt2 
		ELSE
			CALL box_valdato("Usted esta en el principio de la lista...")
			CALL desp_rec() RETURNING det_cnt,det_cnt2
		END IF
	ELSE

		OPEN sub_all USING gr_mimpusu.usu_id, gr_mimpusu.usu_pcid, gr_mimpusu.diu_impid
		FETCH sub_all INTO gr_mimpusu.*

		IF SQLCA.SQLCODE = NOTFOUND THEN
			DISPLAY BY NAME gr_mimpusu.*
			CALL box_error("Registro ha sido eliminado desde la �ltima busqueda.")
		ELSE
			CALL desp_rec() RETURNING det_cnt,det_cnt2
		END IF
		RETURN det_cnt,det_cnt2
		CLOSE sub_all
	END IF

	RETURN det_cnt, det_cnt2
END FUNCTION
	
FUNCTION desp_rec()
	DEFINE
		cnt,
		det_cnt,det_cnt2 SMALLINT

	DISPLAY BY NAME gr_mimpusu.*
	
	CALL sel_deta(gr_mimpusu.usu_id)  RETURNING det_cnt, det_cnt2

	CALL desp_deta(det_cnt,det_cnt2)

	return det_cnt, det_cnt2
END FUNCTION


FUNCTION sel_deta(vusuid)
	DEFINE
	vusuid VARCHAR(20),
   i, j, cnt0              SMALLINT,
	row_cnt,row_cnt2, row_cnt3	INTEGER,
	lr_reg RECORD 
		usu_id VARCHAR(20),
		usu_nom VARCHAR(100),
		pc_id VARCHAR(100),
		imp_id VARCHAR(100),
		usu_default CHAR(1),
		tipptr SMALLINT
	END RECORD

		CALL ga_det1.clear()
		CALL ga_det2.clear()

	--Busca las impresoras definidas
	WHENEVER ERROR CONTINUE
			DECLARE sel_deta CURSOR FOR 
				SELECT usu_id, '' usunom, usu_pcid, diu_impid, usu_default, usu_tipptr 
				FROM ventas:mimpusu
				ORDER BY 1, 2, 3
	WHENEVER ERROR STOP

	LET i = 0 LET j = 0

	IF SQLCA.SQLCODE = 0 THEN
		FOREACH sel_deta INTO lr_reg.*
			LET i = i + 1
			LET ga_det2[i].usu_id = lr_reg.usu_id
			LET ga_det2[i].usu_nom = lr_reg.usu_nom
			LET ga_det2[i].pc_id = lr_reg.pc_id
			LET ga_det2[i].imp_id = lr_reg.imp_id
			LET ga_det2[i].usu_default = lr_reg.usu_default
			LET ga_det2[i].tipptr = lr_reg.tipptr

			IF lr_reg.usu_id = vusuid THEN
				LET j = j + 1
				LET ga_det1[j].pc_id = lr_reg.pc_id
				LET ga_det1[j].imp_id = lr_reg.imp_id
				LET ga_det1[j].usu_default = lr_reg.usu_default
				LET ga_det1[j].tipptr = lr_reg.tipptr
			END IF
		END FOREACH
	END IF

	DISPLAY ARRAY ga_det2 TO sa_det2.*
		BEFORE DISPLAY
		EXIT DISPLAY
	END DISPLAY

	LET row_cnt = j
	LET row_cnt2 = i

	RETURN row_cnt, row_cnt2
END FUNCTION

FUNCTION desp_deta(row_cnt, row_cnt2)
	DEFINE 
		row_cnt, row_cnt2	INTEGER

	DISPLAY ARRAY ga_det1 TO sa_det1.*
		BEFORE DISPLAY
		EXIT DISPLAY
	END DISPLAY
END FUNCTION


FUNCTION disp_det(det_cnt, det_cnt2)
	DEFINE
    det_cnt, det_cnt2 INTEGER

 LET int_flag = FALSE
 WHILE TRUE

	DISPLAY ARRAY ga_det1 TO sa_det1.*
  	 BEFORE DISPLAY
      CALL fgl_dialog_setkeylabel("ACCEPT", "Todos los\nUsuarios")
      CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
		ON ACTION pg1
			error "estoy en usuario"
	END DISPLAY

	IF int_flag = TRUE THEN
   	LET int_flag = FALSE
		exit while
	END IF

	DISPLAY ARRAY ga_det2 TO sa_det2.*
  	 BEFORE DISPLAY
      CALL fgl_dialog_setkeylabel("ACCEPT", "Usuario")
      CALL fgl_dialog_setkeylabel("INTERRUPT", "Cancelar")
		ON ACTION pg2
			ERROR "estoy en todos los usuarios"
	END DISPLAY

	IF int_flag = TRUE THEN
   	LET int_flag = FALSE
		exit while
	END IF
 END WHILE

  RETURN
END FUNCTION


FUNCTION cambia_est(vdefault)
	DEFINE 
		vdefault CHAR(1)

	LET gr_mimpusu.usu_default = vdefault
	DISPLAY BY NAME  gr_mimpusu.usu_default
END FUNCTION

