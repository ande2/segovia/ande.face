################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : cat_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               cat_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "menm0045_glob.4gl"

FUNCTION cat_busca_init()
DEFINE
	select_stmt3			VARCHAR(1000)
   LET select_stmt3 = "SELECT a.gru_id, a.gru_nom ",
                      " FROM adm_gru a WHERE a.gru_id = ? "
PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION

FUNCTION busca_cat()
DEFINE
	cat_query		VARCHAR(1000),
	where_clause	VARCHAR(1500),
	cat_cnt        SMALLINT,
   condicion      CHAR(300),
	cat_count		CHAR(300),
   mensaje        CHAR(100),
   respuesta   CHAR(6),
   lint_flag   smallint,
	x_condicion VARCHAR(100)

   CLEAR FORM

   --CALL encabezado()

   LET gtit_enc="BUSQUEDA REGISTRO"
   INITIALIZE gr_gru.*  TO NULL
   CALL tree_arr.clear()


   DISPLAY BY NAME gtit_enc, gr_gru.*                                                                              
   LET int_flag = FALSE

   CONSTRUCT   where_clause 
      ON      adm_gru.gru_id, adm_gru.gru_nom
      FROM    gru_id, gru_nom

      BEFORE CONSTRUCT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

      ON KEY(CONTROL-B)
         IF INFIELD(gru_id) THEN 
            LET lint_flag = FALSE
            CALL picklist_2("Busqueda de Grupos","C�digo","Nombre","gru_id", "gru_nom",
                            "adm_gru","1= 1",1,1)
               RETURNING gr_gru.gru_id, gr_gru.gru_nom, lint_flag
            IF lint_flag = TRUE THEN
               LET lint_flag = FALSE
               LET gr_gru.gru_id= NULL
               LET gr_gru.gru_nom= NULL
               DISPLAY BY NAME gr_gru.gru_id,gr_gru.gru_nom
               NEXT FIELD gru_id
            ELSE
               DISPLAY BY NAME gr_gru.gru_id, gr_gru.gru_nom
            END IF
         END IF

      AFTER FIELD gru_id
         IF gr_gru.gru_id IS NOT NULL THEN
            LET gr_gru.gru_nom =NULL
            CALL busca_grupo (gr_gru.gru_id ) RETURNING gr_gru.gru_nom
            IF gr_gru.gru_nom  IS NULL THEN
               LET gr_gru.gru_id    = NULL
               CALL box_valdato("Codigo No existe")
               NEXT FIELD gru_id
            END IF
         END IF

      AFTER CONSTRUCT
         IF int_flag THEN
            CALL box_inter() RETURNING int_flag 
            IF int_flag = TRUE THEN
               EXIT CONSTRUCT
            ELSE
               CONTINUE CONSTRUCT
            END IF
         END IF
   END CONSTRUCT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      --CALL encabezado()
      ERROR "B�squeda Abortada."
      RETURN FALSE, 0
   END IF

   LET where_report = where_clause

   LET cat_query ="SELECT adm_gru.gru_id, adm_gru.gru_nom",
                  " FROM adm_gru WHERE ", where_clause CLIPPED,
                  " ORDER BY adm_gru.gru_id"
   LET cat_count = "SELECT count(*) FROM adm_gru WHERE ", where_clause CLIPPED

   PREPARE ex_stmt FROM cat_query

   PREPARE ex_stmt2 FROM cat_count

   DECLARE cat_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

   OPEN cat_ptr

   FETCH FIRST cat_ptr INTO gr_gru.*

   IF SQLCA.SQLCODE = 100 THEN
      CALL box_valdato("No existen datos con estas condiciones.")
      CLOSE cat_ptr
      RETURN FALSE,0
   ELSE
      DECLARE cat_all CURSOR FOR ex_stmt3	
      OPEN cat_all USING gr_gru.gru_id

      FETCH cat_all INTO gr_gru.*

      IF SQLCA.SQLCODE = NOTFOUND THEN
         CALL box_valdato("No existen datos con estas Condiciones.")
         CLOSE cat_ptr
         RETURN FALSE,0
      ELSE
         DECLARE contador_ptr CURSOR FOR ex_stmt2
         OPEN contador_ptr
         FETCH contador_ptr INTO cat_cnt
         CLOSE contador_ptr
         CALL cat_desp15()
         RETURN TRUE, cat_cnt
      END IF				
   END IF				
END FUNCTION

FUNCTION fetch_cat(fetch_flag)
DEFINE
	fetch_flag		SMALLINT

   FETCH RELATIVE fetch_flag cat_ptr INTO gr_gru.*

   IF SQLCA.SQLCODE = NOTFOUND THEN
      IF fetch_flag = 1 THEN
         CALL box_valdato("Est� posicionado en el final de la lista.")
      ELSE
         CALL box_valdato("Est� posicionado en el principio de la lista.")
      END IF
   ELSE
      OPEN cat_all USING gr_gru.gru_id

      FETCH cat_all INTO gr_gru.*
      IF SQLCA.SQLCODE = NOTFOUND THEN
         CLEAR FORM
         CALL cat_desp15()
         CALL box_valdato("El registro fue eliminado desde la �ltima Consulta.")
      ELSE
         CALL cat_desp15()
      END IF
      CLOSE cat_all
   END IF
END FUNCTION

FUNCTION cat_desp15()
DEFINE idx INT
   CALL limpiarArre ( )
  CALL  CargarOpciones ( -1 )
	DISPLAY BY NAME gr_gru.*
   DISPLAY ARRAY tree_arr TO sr_tree.* 
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
END FUNCTION


FUNCTION limpiar() 
WHENEVER ERROR CONTINUE
CLOSE cat_ptr
WHENEVER ERROR STOP
CALL LimpiarArre ( )
END FUNCTION

FUNCTION limpiarArre ()
CALL tree_arr.clear()
CLEAR FORM
   DISPLAY ARRAY tree_arr TO sr_tree.* 
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
END FUNCTION