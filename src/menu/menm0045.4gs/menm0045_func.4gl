GLOBALS "menm0045_glob.4gl"

FUNCTION CargarOpciones ( p )
DEFINE 
	p	INTEGER,
	a  DYNAMIC ARRAY OF tree_t,
	x,i,j 		INTEGER,
	Query STRING,
	q	STRING
   
	DEFINE t tree_t
   DEFINE
      sm                om.DomNode

   --LET aui = ui.Interface@docelec_tcp.getRootNode()
   --LET sm = aui.createChild("StartMenu")
   

	LET Query = "SELECT 	prog_id, prog_padre, prog_des, NVL(prog_idx,999)", 
					" FROM 	mprog ",
					" WHERE 	prog_padre = ", p,
               " AND    prog_id != -1",
               " AND    est_id = 13", 
					" ORDER BY prog_padre, NVL(prog_idx,999)"

	PREPARE stmt1 FROM Query

 	DECLARE lista CURSOR FOR stmt1
	LET x = 0
	FOREACH lista INTO t.id, t.parentid, t.NAME
		LET x = x + 1
		LET a[x].* = t.*
	END FOREACH

     FOR i = 1 TO x
         LET j = tree_arr.getLength() + 1
         LET tree_arr[j].name = a[i].name
         LET tree_arr[j].id = a[i].id
         LET tree_arr[j].parentid = a[i].parentid
         LET tree_arr[j].expanded = TRUE
         IF EstadoOpcion ( a[i].id ) THEN
            LET tree_arr[j].estado = TRUE
         ELSE
            LET tree_arr[j].estado = FALSE
         END IF
         CALL CargarOpciones(a[i].id)
     END FOR
END FUNCTION

FUNCTION EstadoOpcion ( idx  )
DEFINE
   idx      SMALLINT,
   estado   SMALLINT,
   QB       VARCHAR(1000)


   LET QB =
   " SELECT   COUNT(*)",
   "   FROM ", dbperm, ":dmenprog ",
   "   WHERE gru_id = ", gr_gru.gru_id ,
   "   AND   prog_id = ", idx
   PREPARE QSum FROM QB
   DECLARE CSum CURSOR FOR QSum
   FOREACH CSum INTO estado
   END FOREACH
   IF STATUS = NOTFOUND OR estado IS NULL THEN
      LET estado = FALSE
   END IF
   RETURN estado
END FUNCTION

FUNCTION Busca_Grupo ( lgru_id )
DEFINE
   lgru_id     LIKE adm_gru.gru_id,
   lgru_nom    LIKE adm_gru.gru_nom

   SELECT   a.gru_nom
      INTO  lgru_nom
      FROM  adm_gru a
      WHERE a.gru_id = lgru_id

   IF lgru_nom IS NULL THEN
      LET lgru_nom = ''
   END IF
   RETURN lgru_nom
END FUNCTION

FUNCTION ModificarHijos ( fid, lestado )
DEFINE
   idx,
   fid   INTEGER,
   lestado   SMALLINT,
    n om.DomNode

   FOR idx = 1 TO tree_arr.getLength ()
      IF tree_arr[idx].parentid = fid THEN
         LET tree_arr[idx].estado = lestado
      END IF
   END FOR
END FUNCTION


FUNCTION CargarUsuario ( lid )
DEFINE
   idx,
   lid         INTEGER,
   r RECORD
      linkprop    LIKE dprogopc.linkprop,
      prog_ord    LIKE dprogopc.prog_ord,
      prop_desc   LIKE dprogopc.prop_desc
   END RECORD
   CALL gr_det.clear()
   DECLARE ListaDet CURSOR FOR      
      SELECT   a.linkprop,  a.prog_ord, a.prop_desc
         FROM  dprogopc a, sd_des b
         WHERE a.prog_id = lid
         AND   b.des_tipo = 22
         AND   b.des_cod = a.des_cod
         ORDER BY a.prog_ord
   FOREACH ListaDet INTO r.*
      CALL gr_det.appendElement()
      LET idx = gr_det.getLength()
      LET gr_det[idx].linkprop = r.linkprop
      LET gr_det[idx].prog_ord = r.prog_ord
      LET gr_det[idx].prop_desc = r.prop_desc
      LET gr_det[idx].habilitada = OpcionHabilitada ( r.linkprop )
   END FOREACH
   DISPLAY ARRAY gr_det TO sr_det.*
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
END FUNCTION


FUNCTION OpcionHabilitada ( lid )
DEFINE
   lid      INTEGER,
   cuantos  INTEGER
   SELECT   COUNT(*)
      INTO  cuantos
      FROM  maccopcusu
      WHERE linkprop = lid
      AND   usu_id = lusu_id
   IF STATUS = NOTFOUND OR cuantos IS NULL THEN 
      LET cuantos = 0
   END IF
   IF cuantos = 0 THEN
      RETURN FALSE
   ELSE
      RETURN TRUE
   END IF
END FUNCTION


FUNCTION ClearDet ( )
   CALL gr_det.clear()
END FUNCTION


FUNCTION SaveDetalle ( opcid )
DEFINE
   opcid    LIKE mprog.prog_id,
   cuantos,
   idx      SMALLINT
   
   FOR idx = 1 TO gr_det.getLength()
      IF gr_det[idx].linkprop IS NOT NULL THEN
         SELECT   count(*)
            INTO  cuantos
            FROM  maccopcusu
            WHERE usu_id = lusu_id
            AND   linkprop = gr_det[idx].linkprop
         IF STATUS = NOTFOUND OR cuantos IS NULL THEN
            LET cuantos = 0
         END IF
         IF cuantos > 0 THEN
            DELETE   
               FROM  maccopcusu
               WHERE usu_id = lusu_id
               AND   linkprop = gr_det[idx].linkprop
         END IF
         IF gr_det[idx].habilitada THEN
            INSERT
               INTO     maccopcusu
               VALUES ( lusu_id, gr_det[idx].linkprop )
         END IF
      END IF
   END FOR
END FUNCTION