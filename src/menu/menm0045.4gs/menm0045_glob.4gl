################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Ricardo Ram�rez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# 
################################################################################
DATABASE face

GLOBALS
TYPE tree_t RECORD
   id             INTEGER,
   parentid       INTEGER,
   NAME           VARCHAR(50)
END RECORD
DEFINE 
   tree DYNAMIC ARRAY OF RECORD
      NAME           STRING,
      pid            STRING,
      hasChildren    BOOLEAN
   END RECORD,
   tree_arr DYNAMIC ARRAY OF RECORD
      NAME           STRING,         -- text to be displayed for the node
      estado         SMALLINT,
      parentid       STRING,          -- id of the parent node
      id             STRING,           -- id of the current node
      expanded       BOOLEAN,    -- node expansion flag (TRUE/FALSE) (optional)
      isnode         BOOLEAN       -- children indicator flag (TRUE/FALSE) (optional)
   END RECORD,
   gr_gru, nr_gru, ur_gru RECORD
      gru_id         LIKE adm_gru.gru_id,
      gru_nom        LIKE adm_gru.gru_nom
   END RECORD,
   lusu_id            LIKE adm_usu.usu_id,
	gr_det,nr_det,ur_det DYNAMIC ARRAY OF RECORD
      linkprop          LIKE maccopcusu.linkprop,
      prog_ord          LIKE dprogopc.prog_ord,
      prop_desc         LIKE dprogopc.prop_desc,
      habilitada        SMALLINT
   END RECORD,
   g_empr               RECORD LIKE mempr.*,
   id_empresa           INTEGER,
   nombre_empresa       VARCHAR(50),
   dbperm,
   dbname            CHAR(40),
	vempr_nomlog CHAR(15),
	usuario char(10),
	hr   date, 
	gtit_enc, gtit_pie,nombre_depproamento VARCHAR(80),
    where_report varchar (1000),
    aui om.DOMnode
END GLOBALS