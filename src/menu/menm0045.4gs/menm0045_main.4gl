################################################################################
# Funcion     : %M%
# Descripcion : Modulo principal para cliente
# Funciones   : main
#					 cat_init()
#               cat_menu()
#               encabezado()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : eAlvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################

GLOBALS "menm0045_glob.4gl"

MAIN
DEFINE
   n_param SMALLINT

DEFER INTERRUPT

OPTIONS
   INPUT WRAP,
   HELP FILE "cat_help.ex",
   HELP KEY CONTROL-W,
   COMMENT 	LINE OFF,
   PROMPT 	LINE LAST - 2,
   MESSAGE 	LINE LAST - 1,
   ERROR 	LINE LAST
                                                                                
	LET n_param = num_args()
	IF n_param = 0 THEN
		RETURN
	ELSE
		LET dbname = arg_val(1)
	END IF
   LET dbperm = dbname
   LET dbname = "face"
   DATABASE dbname
   SELECT   a.empr_id, a.empr_nom      
      INTO  id_empresa, nombre_empresa
      FROM  mempr a
      WHERE a.empr_db = dbname
   CALL busca_empresa(dbname) RETURNING g_empr.*
   CALL cat_init(dbname)
   CALL ins_init()
   CALL cat_busca_init()
   CALL cat_menu()
END MAIN

FUNCTION cat_init(dbname)
DEFINE
   nombre_departamento, 
   nombre_empresa             CHAR(50),
   cmd                        CHAR(40),
   ip_usu, dbname             CHAR(20),
   fecha_actual               DATE,
   dir_destino                CHAR(100)
   INITIALIZE nr_gru.* TO NULL
   LET gr_gru.* = nr_gru.*
   LET usuario = fgl_getenv("LOGNAME")
   LET fecha_actual = today
   let hr =fecha_actual

   OPEN FORM cat_form FROM "menm0045_form"

   DISPLAY FORM cat_form
   CLEAR FORM
   CALL fgl_settitle("PERMISOS POR GRUPO")
END FUNCTION

FUNCTION cat_menu()
DEFINE 
   cuantos		SMALLINT,
   usuario CHAR(8),
   vopciones CHAR(255), --cadena de ceros y unos para control de permisos
   ord_opc SMALLINT,    --orden de las opciones en el menu
   cnt     SMALLINT,    --contdor
   tol ARRAY[13] OF RECORD
      opcion   CHAR(15),
      acc      CHAR(15),
      desc_opc CHAR(60),
      des_cod  SMALLINT,
      imagen    CHAR(20),
      linkprop INTEGER
   END RECORD,
   cpo ARRAY[13] OF RECORD
      opcion   CHAR(15),
      desc_opc CHAR(60),
      des_cod  SMALLINT,
      imagen    CHAR(20),
      linkprop INTEGER
   END RECORD,

   tb om.DomNode,
   tbi om.DomNode,
   tbs om.DomNode,
   tm  om.DomNode,
   tmg om.DomNode,
   tmi om.DomNode,
   tms om.DomNode,
   f  om.DomNode

   LET cnt = 1
   LET ord_opc = NULL

   -- captura nombre de usuario
   LET usuario = fgl_getenv("LOGNAME")

   -- captura permisos del usuario sobre las opciones del menu
   CALL men_opc(usuario,"menm0045", dbperm) RETURNING vopciones
   LET VOPCIONES = "11111"

   -- carga opciones de menu
   DECLARE opciones_menu CURSOR FOR
      SELECT   b.prog_ord,c.des_desc_md,b.prop_desc,
               c.des_cod,c.des_desc_ct
         FROM  mprog a,dprogopc b, sd_des c
         WHERE a.prog_nom = "menm0045"
         AND   b.prog_id = a.prog_id
         AND   c.des_tipo = 22
         AND   c.des_cod = b.des_cod
         ORDER BY b.prog_ord
         
	LET cnt = 1
   
   FOREACH opciones_menu INTO ord_opc,cpo[cnt].*
          LET cnt = cnt + 1
   END FOREACH
   FREE opciones_menu

   FOR cnt = 1 TO LENGTH(vopciones)
          LET tol[cnt].acc     =DOWNSHIFT(cpo[cnt].opcion CLIPPED)
          LET tol[cnt].opcion  =cpo[cnt].opcion CLIPPED
          LET tol[cnt].desc_opc = cpo[cnt].desc_opc CLIPPED
          LET tol[cnt].des_cod  = cpo[cnt].des_cod
          LET tol[cnt].imagen   = DOWNSHIFT(cpo[cnt].imagen CLIPPED)
          LET tol[cnt].linkprop = cpo[cnt].linkprop
   END FOR

   LET aui=ui.Interface.getRootNode()
   LET tb =aui.createChild("ToolBar")
   LET tbi=createToolBarItem(tb,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --Buscar
   LET tbi=createToolBarItem(tb,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) --Anterior
   LET tbi=createToolBarItem(tb,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) --proximo
   LET tbs=createToolBarSeparator(tb)
   LET tbi=createToolBarItem(tb,tol[4].acc,tol[4].opcion,tol[4].desc_opc,tol[4].imagen) --Ingreso
   --LET tbs = createToolBarSeparator(tb) -- separador
   --LET tbi= createToolBarItem(tb,tol[5].acc,tol[5].opcion,tol[5].desc_opc,tol[5].imagen) -- imprimir
   LET tbs=createToolBarSeparator(tb)
   LET tbi= createToolBarItem(tb,tol[5].acc,tol[5].opcion,tol[5].desc_opc,tol[5].imagen) -- salir

-- menu top
{
   LET f  = ui.Interface@docelec_tcp.getRootNode()
   LET tm = f.createChild("TopMenu")
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Archivo")

   LET tbi =creaopcion(tmg,tol[4].acc,tol[4].opcion,tol[4].desc_opc,tol[4].imagen) --ingreso
   LET tbi =creaopcion(tmg,tol[5].acc,tol[5].opcion,tol[5].desc_opc,tol[5].imagen) --modificar
   LET tbi =creaopcion(tmg,tol[6].acc,tol[6].opcion,tol[6].desc_opc,tol[6].imagen) --anular
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tol[7].acc,tol[7].opcion,tol[7].desc_opc,tol[7].imagen) --salir
   LET tmg = tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Edici�n")
   LET tmi =creaopcion(tmg,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --buscar
   LET tms =tmg.CreateChild("TopMenuSeparator")
   LET tmi =creaopcion(tmg,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) -- anterior
   LET tmi =creaopcion(tmg,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) -- proximo
   LET tmg =tm.createChild("TopMenuGroup")
   CALL tmg.setAttribute("text","Herramientas")
}
   MENU ""
      BEFORE MENU

         LET cuantos = 0
      -- despliega opciones a las que tiene acceso el usuario
         HIDE OPTION ALL
         FOR cnt = 1 TO LENGTH(vopciones)
            IF vopciones[cnt] = 1 AND
               (tol[cnt].des_cod = 1 OR
               tol[cnt].des_cod = 6 OR
               tol[cnt].des_cod = 9) THEN
               DISPLAY tol[cnt].opcion
               SHOW OPTION tol[cnt].opcion
               LET cuantos = cuantos + 1
            END IF
         END FOR

         IF cuantos = 0 THEN
            CALL box_error("Lo siento, no tiene acceso a este programa")
            EXIT MENU
         END IF

-- opcion busqueda
	COMMAND KEY("B") tol[1].opcion tol[1].desc_opc
		HELP 2
		CALL busca_cat() RETURNING int_flag, cuantos
		IF int_flag = TRUE THEN
			LET int_flag = FALSE
			IF cuantos > 1 THEN

-- despliega opciones a las que tiene acceso el usuario
        		HIDE OPTION ALL
        		FOR cnt = 1 TO LENGTH(vopciones)
	        		IF vopciones[cnt] = 1 THEN
                 				SHOW OPTION tol[cnt].opcion
                 	IF tol[cnt].des_cod = 2 THEN
                 		NEXT OPTION tol[cnt].opcion
                  	END IF
              	END IF
           	END FOR

			ELSE
-- despliega opciones a las que tiene acceso el usuario
			HIDE OPTION ALL
				FOR cnt = 1 TO LENGTH(vopciones)
					IF vopciones[cnt] = 1 AND
						(tol[cnt].des_cod <> 2 AND
						tol[cnt].des_cod <> 3) THEN
                  DISPLAY tol[cnt].opcion
						SHOW OPTION tol[cnt].opcion
						IF tol[cnt].des_cod = 2 THEN
							NEXT OPTION tol[cnt].opcion
						END IF
					END IF
				END FOR
			END IF
		ELSE
-- despliega opciones a las que tiene acceso el usuario
			HIDE OPTION ALL
        		FOR cnt = 1 TO LENGTH(vopciones)
               IF vopciones[cnt] = 1 AND
                  (tol[cnt].des_cod = 1 OR
                  tol[cnt].des_cod = 6 OR
                  tol[cnt].des_cod = 9) THEN
                  SHOW OPTION tol[cnt].opcion
               END IF
            END FOR

		END IF
      
-- opcion anterior
   COMMAND KEY("A") tol[2].opcion tol[2].desc_opc
		CALL fetch_cat(-1)


-- opcion Siguiente
	COMMAND KEY("S") tol[3].opcion tol[3].desc_opc
		CALL fetch_cat(1)

-- opcion modificar
   COMMAND KEY("O") tol[4].opcion tol[4].desc_opc
		IF gr_gru.gru_id IS NOT NULL  THEN 
			CALL cat_inse( 2 ) RETURNING int_flag
			IF int_flag = TRUE THEN
				LET int_flag = FALSE
				CALL limpiar()
-- despliega opciones a las que tiene acceso el usuario
         	HIDE OPTION ALL
         	FOR cnt = 1 TO LENGTH(vopciones)
         		IF vopciones[cnt] = 1 AND
            		(tol[cnt].des_cod = 1 OR
            		tol[cnt].des_cod = 6 OR
            		tol[cnt].des_cod = 21 OR
            		tol[cnt].des_cod = 9) THEN
            		SHOW OPTION tol[cnt].opcion
         		END IF
         	END FOR
			END IF
		ELSE
			CALL box_valdato("Registro se encuentra Anulado y/o Registro Nulo")
		END IF
      
 {-- opcion imprimir
   COMMAND KEY ("R") tol[5].opcion tol[5].desc_opc
		HELP 8
      --CALL PrintReport ()}

      
-- opcion salir
   COMMAND key("Q") tol[5].opcion tol[5].desc_opc
		HELP 8
		EXIT MENU
END MENU
END FUNCTION


FUNCTION usu_destino()
DEFINE
   ip_usu CHAR(20),
   i SMALLINT
   LET ip_usu = fgl_getenv("FGLSERVER")
   FOR i = 1 TO 20
      IF ip_usu[i] = ":" THEN
         EXIT FOR
      END IF
   END FOR
   LET ip_usu = ip_usu[1,i-1] CLIPPED
   RETURN ip_usu
END FUNCTION