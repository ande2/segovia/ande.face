################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : cat_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               cat_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "menm0046_glob.4gl"

FUNCTION cat_busca_init()
DEFINE
	select_stmt3			VARCHAR(1000)
   LET select_stmt3 = "SELECT usu_id, usu_nom ",
                      " FROM adm_usu WHERE usu_id = ? "
PREPARE ex_stmt3 FROM select_stmt3
END FUNCTION

FUNCTION busca_cat()
DEFINE
	cat_query		VARCHAR(1000),
	where_clause	VARCHAR(1500),
	cat_cnt        SMALLINT,
   condicion      CHAR(300),
	cat_count		CHAR(300),
   mensaje        CHAR(100),
   respuesta   CHAR(6),
   lint_flag   smallint,
	x_condicion VARCHAR(100)

   CLEAR FORM

   --CALL encabezado()

   LET gtit_enc="BUSQUEDA REGISTRO"
   INITIALIZE gr_usu.*  TO NULL
   CALL tree_arr.clear()


   DISPLAY BY NAME gtit_enc, gr_usu.*                                                                              
   LET int_flag = FALSE

   CONSTRUCT   where_clause 
      ON      adm_usu.usu_id, adm_usu.usu_nom
      FROM    usu_id, usu_nom

      BEFORE CONSTRUCT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

      ON KEY(CONTROL-B)
         IF INFIELD(usu_id) THEN 
            LET lint_flag = FALSE
            CALL picklist_2("Busqueda de Usuarios","C�digo","Nombre","usu_id", "usu_nom",
                            "adm_usu","1= 1",1,1)
               RETURNING gr_usu.usu_id, gr_usu.usu_nom, lint_flag
            IF lint_flag = TRUE THEN
               LET lint_flag = FALSE
               LET gr_usu.usu_id= NULL
               LET gr_usu.usu_nom= NULL
               DISPLAY BY NAME gr_usu.usu_id,gr_usu.usu_nom
               NEXT FIELD usu_id
            ELSE
               DISPLAY BY NAME gr_usu.usu_id, gr_usu.usu_nom
            END IF
         END IF

      AFTER FIELD usu_id
         IF gr_usu.usu_id IS NOT NULL THEN
            LET gr_usu.usu_nom =NULL
            CALL busca_usuario (gr_usu.usu_id ) RETURNING gr_usu.usu_nom
            IF gr_usu.usu_nom  IS NULL THEN
               LET gr_usu.usu_id    = NULL
               CALL box_valdato("Codigo No existe")
               NEXT FIELD usu_id
            END IF
         END IF

      AFTER CONSTRUCT
         IF int_flag THEN
            CALL box_inter() RETURNING int_flag 
            IF int_flag = TRUE THEN
               EXIT CONSTRUCT
            ELSE
               CONTINUE CONSTRUCT
            END IF
         END IF
   END CONSTRUCT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      --CALL encabezado()
      ERROR "B�squeda Abortada."
      RETURN FALSE, 0
   END IF

   LET where_report = where_clause

   LET cat_query ="SELECT adm_usu.usu_id, adm_usu.usu_nom",
                  " FROM adm_usu WHERE ", where_clause CLIPPED,
                  " ORDER BY adm_usu.usu_id"
   LET cat_count = "SELECT count(*) FROM adm_usu WHERE ", where_clause CLIPPED

   PREPARE ex_stmt FROM cat_query

   PREPARE ex_stmt2 FROM cat_count

   DECLARE cat_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

   OPEN cat_ptr

   FETCH FIRST cat_ptr INTO gr_usu.*

   IF SQLCA.SQLCODE = 100 THEN
      CALL box_valdato("No existen datos con estas condiciones.")
      CLOSE cat_ptr
      RETURN FALSE,0
   ELSE
      DECLARE cat_all CURSOR FOR ex_stmt3	
      OPEN cat_all USING gr_usu.usu_id

      FETCH cat_all INTO gr_usu.*

      IF SQLCA.SQLCODE = NOTFOUND THEN
         CALL box_valdato("No existen datos con estas Condiciones.")
         CLOSE cat_ptr
         RETURN FALSE,0
      ELSE
         DECLARE contador_ptr CURSOR FOR ex_stmt2
         OPEN contador_ptr
         FETCH contador_ptr INTO cat_cnt
         CLOSE contador_ptr
         CALL cat_desp15()
         RETURN TRUE, cat_cnt
      END IF				
   END IF				
END FUNCTION

FUNCTION fetch_cat(fetch_flag)
DEFINE
	fetch_flag		SMALLINT

   FETCH RELATIVE fetch_flag cat_ptr INTO gr_usu.*

   IF SQLCA.SQLCODE = NOTFOUND THEN
      IF fetch_flag = 1 THEN
         CALL box_valdato("Est� posicionado en el final de la lista.")
      ELSE
         CALL box_valdato("Est� posicionado en el principio de la lista.")
      END IF
   ELSE
      OPEN cat_all USING gr_usu.usu_id

      FETCH cat_all INTO gr_usu.*
      IF SQLCA.SQLCODE = NOTFOUND THEN
         CLEAR FORM
         CALL cat_desp15()
         CALL box_valdato("El registro fue eliminado desde la �ltima Consulta.")
      ELSE
         CALL cat_desp15()
      END IF
      CLOSE cat_all
   END IF
END FUNCTION

FUNCTION cat_desp15()
DEFINE idx INT
   CALL tree_arr.clear()
   
  CALL  CargarOpciones ( -1 )
	DISPLAY BY NAME gr_usu.*
   DISPLAY ARRAY tree_arr TO sr_tree.* 
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
   CALL CargarUsuario ( gr_usu.usu_id)
END FUNCTION



FUNCTION limpiar() 
WHENEVER ERROR CONTINUE
CLOSE cat_ptr
WHENEVER ERROR STOP
CALL tree_arr.clear()
CALL gr_det.clear()
CLEAR FORM
   DISPLAY ARRAY tree_arr TO sr_tree.* 
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
   DISPLAY ARRAY gr_det TO sr_det.*
      BEFORE DISPLAY
         EXIT DISPLAY
   END DISPLAY
      
END FUNCTION