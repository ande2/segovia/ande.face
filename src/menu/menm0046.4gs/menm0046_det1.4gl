GLOBALS "menm0046_glob.4gl"

FUNCTION Inse_Opciones ( opcid )
DEFINE 
   opcid       INTEGER,
   scr,
   idx         SMALLINT
   
      INPUT ARRAY gr_det FROM sr_det.* ATTRIBUTES(WITHOUT DEFAULTS, AUTO APPEND=FALSE )
         BEFORE INPUT
      		CALL DIALOG.setActionHidden( "append", 1 )
				CALL DIALOG.setActionHidden( "insert", 1 )
				CALL DIALOG.setActionHidden( "delete", 1 )
            CALL fgl_dialog_setkeylabel( "accept","Grabar" )
            CALL fgl_dialog_setkeylabel( "cancel","Cancelar" )

            
         ON ACTION accept
            CALL SaveDetalle ( idx )

         ON ACTION CANCEL
            CALL ClearDet ( )

      END INPUT
      
END FUNCTION