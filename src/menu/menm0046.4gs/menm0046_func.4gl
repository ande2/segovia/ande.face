GLOBALS "menm0046_glob.4gl"

FUNCTION CargarOpciones ( p )
DEFINE 
	p	INTEGER,
	a  DYNAMIC ARRAY OF tree_t,
	x,i,j 		INTEGER,
	Query STRING,
	q	STRING
   
	DEFINE t tree_t
   DEFINE
      sm                om.DomNode

	LET Query = "SELECT 	a.prog_id, a.prog_padre, a.prog_des, NVL(a.prog_idx,999)", 
					" FROM 	mprog a, ", dbperm, ":dmenprog b, adm_usu c ",
					" WHERE 	a.prog_padre = ", p,
               " AND    a.prog_id != -1",
               " AND    a.est_id = 13", 
               " AND    c.usu_id = ", gr_usu.usu_id,
               " AND    b.prog_id = a.prog_id",
               " AND    b.gru_id = c.gru_id",
					" ORDER BY a.prog_padre, NVL(a.prog_idx,999)"

	PREPARE stmt1 FROM Query

 	DECLARE lista CURSOR FOR stmt1
	LET x = 0
	FOREACH lista INTO t.id, t.parentid, t.NAME
		LET x = x + 1
		LET a[x].* = t.*
	END FOREACH

     FOR i = 1 TO x
         LET j = tree_arr.getLength() + 1
         LET tree_arr[j].name = a[i].name
         LET tree_arr[j].id = a[i].id
         LET tree_arr[j].parentid = a[i].parentid
         LET tree_arr[j].expfaced = TRUE
         CALL CargarOpciones(a[i].id)
     END FOR
END FUNCTION

FUNCTION Busca_Usuario ( lusu_id )
DEFINE
   lusu_id     LIKE adm_usu.usu_id,
   lusu_nom    LIKE adm_usu.usu_nom

   SELECT   usu_nom
      INTO  lusu_nom
      FROM  adm_usu
      WHERE usu_id = lusu_id

   IF lusu_nom IS NULL THEN
      LET lusu_nom = ''
   END IF
   RETURN lusu_nom
END FUNCTION


FUNCTION CargarUsuario ( lid )
DEFINE
   idx,
   lid         INTEGER,
   r RECORD
      prog_id     LIKE dprogopc.prog_id,
      prog_ord    LIKE dprogopc.prog_ord,
      linkprop    LIKE dprogopc.linkprop,
      habilitada  INTEGER
   END RECORD
   CALL gr_tdet.clear()

   LET QueryBuffer = 
      "SELECT   a.prog_id, a.prog_ord, a.linkprop, NVL(b.linkprop, 0)",
      "   FROM  dprogopc a, OUTER(", dbperm, ":maccopcusu b)",
      "   WHERE b.linkprop = a.linkprop",
      "   AND   b.usu_id = ", lid,
      "   ORDER BY a.prog_id, a.prog_ord"

   PREPARE QDet FROM QueryBuffer
   DECLARE ListaDet CURSOR FOR   QDet    

   FOREACH ListaDet INTO r.*
      CALL gr_tdet.appendElement()
      LET idx = gr_tdet.getLength()
      LET gr_tdet[idx].prog_id   = r.prog_id
      LET gr_tdet[idx].linkprop = r.linkprop
      LET gr_tdet[idx].prog_ord = r.prog_ord
      IF r.habilitada > 0 THEN
         LET gr_tdet[idx].habilitada = TRUE
      ELSE
         LET gr_tdet[idx].habilitada = FALSE
      END IF
   END FOREACH
END FUNCTION

FUNCTION CargarDetalle ( pid )
DEFINE 
   pid INTEGER,
   idx INTEGER,
   idx2 INTEGER
   CALL gr_det.clear()
   FOR idx = 1 TO gr_tdet.getLength()
      IF gr_tdet[idx].prog_id = pid THEN
         CALL gr_det.appendElement()
         LET idx2 = gr_det.getLength()
         LET gr_det[idx2].linkprop = gr_tdet[idx].linkprop
         LET gr_det[idx2].prog_ord = gr_tdet[idx].prog_ord
         LET gr_det[idx2].prop_desc = BuscarOpcion ( gr_tdet[idx].linkprop )
         LET gr_det[idx2].habilitada = gr_tdet[idx].habilitada
      END IF
   END FOR
   LET idx2 = gr_det.getLength()

   DISPLAY ARRAY gr_det TO sr_det.*
      BEFORE DISPLAY 
         EXIT DISPLAY
   END DISPLAY
END FUNCTION

FUNCTION MoverDetalle ( pid, oid, estado )
DEFINE 
   oid INTEGER,
   pid INTEGER,
   idx INTEGER,
   idx2 INTEGER,
   estado BOOLEAN

      FOR idx2 = 1 TO gr_tdet.getLength()
         IF gr_tdet[idx2].prog_id = pid AND 
            gr_tdet[idx2].linkprop = oid THEN
            LET gr_tdet[idx2].habilitada = estado
            EXIT FOR
         END IF
      END FOR

END FUNCTION

FUNCTION BuscarOpcion ( link )
DEFINE 
   link INTEGER,
   ldes LIKE dprogopc.prop_desc
   SELECT   prop_desc
      INTO  ldes
      FROM  dprogopc
      WHERE linkprop = link
   RETURN ldes
END FUNCTION
   
FUNCTION ClearDet ( )
   CALL gr_det.clear()
END FUNCTION


--{FUNCTION SaveDetalle ( opcid )
--DEFINE
   --opcid    LIKE mprog.prog_id,
   --cuantos,
   --idx      SMALLINT
   --
   --FOR idx = 1 TO gr_det.getLength()
      --IF gr_det[idx].linkprop IS NOT NULL THEN
         --LET gr_det[idx]
         --LET QueryBuffer = 
         --"SELECT   count(*)",
         --"   FROM  maccopcusu ",
         --"   WHERE usu_id = ?" ,
         --"   AND   linkprop = ?" 
         --PREPARE Q01 FROM QueryBUFFER
         --EXECUTE Q01 USING gr_usu.usu_id,gr_det[idx].linkprop INTO cuantos 
         --IF STATUS = NOTFOUND OR cuantos IS NULL THEN
            --LET cuantos = 0
         --END IF
         --IF cuantos > 0 THEN
            --DELETE   
               --FROM  maccopcusu
               --WHERE usu_id = lusu_id
               --AND   linkprop = gr_det[idx].linkprop
         --END IF
         --
         --IF gr_det[idx].habilitada THEN
            --INSERT
               --INTO     maccopcusu
               --VALUES ( lusu_id, gr_det[idx].linkprop )
         --END IF
         --
      --END IF
   --END FOR
--END FUNCTION}