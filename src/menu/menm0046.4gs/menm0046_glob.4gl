################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Ricardo Ram�rez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# 
################################################################################

SCHEMA "face"

GLOBALS
TYPE tree_t RECORD
   id             INTEGER,
   parentid       INTEGER,
   NAME           VARCHAR(50)
END RECORD
DEFINE 
   tree DYNAMIC ARRAY OF RECORD
      NAME           STRING,
      pid            STRING,
      hasChildren    BOOLEAN
   END RECORD,
   tree_arr DYNAMIC ARRAY OF RECORD
      NAME           STRING,         -- text to be displayed for the node
      parentid       STRING,          -- id of the parent node
      id             STRING,           -- id of the current node
      expfaced       BOOLEAN,    -- node expansion flag (TRUE/FALSE) (optional)
      isnode         BOOLEAN       -- children indicator flag (TRUE/FALSE) (optional)
   END RECORD,
   gr_usu, nr_usu, ur_usu RECORD
      usu_id         LIKE adm_usu.usu_id,
      usu_nom        LIKE adm_usu.usu_nom
   END RECORD,
	gr_det,nr_det,ur_det DYNAMIC ARRAY OF RECORD
      linkprop    LIKE maccopcusu.linkprop,
      prog_ord    LIKE dprogopc.prog_ord,
      prop_desc   LIKE dprogopc.prop_desc,
      habilitada  BOOLEAN
   END RECORD,
	gr_tdet DYNAMIC ARRAY OF RECORD
      prog_id     LIKE dprogopc.prog_id,
      prog_ord    LIKE dprogopc.prog_ord,
      prop_desc   LIKE dprogopc.prop_desc,
      linkprop    LIKE maccopcusu.linkprop,
      habilitada  BOOLEAN
   END RECORD,
   g_empr         RECORD LIKE mempr.*,
   dbperm,
   dbname         CHAR(40),
	vempr_nomlog   CHAR(15),
	usuario        CHAR(10),
	hr             DATE, 
   id_empresa     INTEGER,
   nombre_empresa VARCHAR(60),
	gtit_enc, gtit_pie,nombre_depproamento VARCHAR(80),
    where_report varchar (1000),
    aui om.DOMnode,
    QueryBuffer      VARCHAR(1000)
END GLOBALS