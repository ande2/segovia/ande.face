################################################################################
# Funcion     : %M%
# ccliente : Modulo para ingreso de catalogo 
# Funciones   : cat_inse()
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : eAlvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        ccliente de la modificacion
################################################################################

GLOBALS "menm0046_glob.4gl"


FUNCTION inse_init()
DEFINE
   stmt     VARCHAR(1000)

   LET stmt = "INSERT ",
              " INTO     ", dbperm, ":maccopcusu ( usu_id, linkprop )",
              "    VALUES	(?,?) "
   PREPARE permIns FROM stmt

   LET stmt = "DELETE FROM ", dbperm, ":maccopcusu WHERE usu_id = ? "
   PREPARE permDelete FROM stmt
   
END FUNCTION

FUNCTION cat_inse( operacion )
DEFINE
   operacion 	SMALLINT,
   err_msg 		CHAR(80),
   w_cuenta		INTEGER, -- Cuenta los registros que ya existen en mcat
   condicion	CHAR(300),
   respuesta   CHAR(06),
   lint_flag   SMALLINT,
   flagerr     SMALLINT,
   lexiste     SMALLINT,
   x_condicion VARCHAR(100),
   j,
   scr,
   idx			SMALLINT,
   scrd,
   idxd			SMALLINT,
   opc         LIKE mprog.prog_id

   LET int_flag = FALSE

	CLEAR FORM

   LET gtit_enc="MODIFICAR PERMISOS"
   DISPLAY BY NAME gr_usu.*

   DIALOG
   DISPLAY ARRAY tree_arr TO sr_tree.* 
      BEFORE DISPLAY
				CALL DIALOG.setActionHidden( "close", 1 )

        ON ACTION Todos
            FOR j = 1 TO gr_det.getLength()
               LET gr_det[j].habilitada = TRUE
               CALL MoverDetalle ( tree_arr[idx].id, gr_det[j].linkprop, gr_det[j].habilitada )
               DISPLAY gr_det[j].* TO sr_det[j].*
            END FOR
            
        ON ACTION Ninguno
            FOR j = 1 TO gr_det.getLength()
               LET gr_det[j].habilitada = FALSE
               CALL MoverDetalle ( tree_arr[idx].id, gr_det[j].linkprop, gr_det[j].habilitada )
               DISPLAY gr_det[j].* TO sr_det[j].*
            END FOR
       BEFORE ROW 
         LET idx = ARR_CURR()
         LET scr = SCR_LINE()
         CALL CargarDetalle ( tree_arr[idx].id )

      END DISPLAY

      
      INPUT ARRAY gr_det FROM sr_det.* ATTRIBUTES(WITHOUT DEFAULTS, AUTO APPEND=FALSE )


        
         BEFORE INPUT
      		CALL DIALOG.setActionHidden( "append", 1 )
				CALL DIALOG.setActionHidden( "insert", 1 )
				CALL DIALOG.setActionHidden( "delete", 1 )
				CALL DIALOG.setActionHidden( "close", 1 )

            ON CHANGE habilitada
            LET idxd = ARR_CURR()
            LET scrd = SCR_LINE()
            CALL MoverDetalle ( tree_arr[idx].id, gr_det[idxd].linkprop, gr_det[idxd].habilitada )
            --LET gr_det[idxd].habilitada --= GET_FLDBUF( formonly.habilitada)
            --DISPLAY "pase ", idxd, "-",gr_det[idxd].habilitada 
            NEXT FIELD NEXT 

         AFTER ROW
            LET idxd = ARR_CURR()
            LET scrd = SCR_LINE()
            CALL MoverDetalle ( tree_arr[idx].id, gr_det[idxd].linkprop, gr_det[idxd].habilitada )

      END INPUT
      
      ON ACTION accept
         CALL box_gradato("Desea guardar la información.")
         RETURNING respuesta
         CASE
            WHEN respuesta = "Si"
               LET int_flag = FALSE
            WHEN respuesta = "No"
               LET int_flag = TRUE
         END CASE
         EXIT DIALOG

      ON ACTION cancel
         LET INT_FLAG = TRUE
         EXIT DIALOG

   END DIALOG
      
	IF int_flag = TRUE THEN
		LET int_flag = FALSE
		LET gr_usu.* = nr_usu.*
      CALL Limpiar()
		RETURN FALSE
	END IF


	BEGIN WORK

   WHENEVER ERROR CONTINUE

   EXECUTE permDelete USING gr_usu.usu_id

   IF SQLCA.SQLCODE = 0 THEN
      IF gr_tdet.getLength() IS NOT NULL THEN
         FOR idx = 1 TO gr_tdet.getLength ()
            WHENEVER ERROR CONTINUE
            IF	gr_tdet[idx].prog_id IS NOT NULL AND
               gr_tdet[idx].linkprop AND 
               gr_tdet[idx].habilitada THEN
               EXECUTE permIns USING  gr_usu.usu_id, gr_tdet[idx].linkprop 
            END IF
            WHENEVER ERROR STOP
            IF SQLCA.SQLCODE < 0 THEN
               LET err_msg = err_get(SQLCA.SQLCODE)
               ROLLBACK WORK
               ERROR err_msg
               CALL errorlog(err_msg CLIPPED)
               CALL box_valdato("Error al grabar el detalle.")
            END IF
         END FOR
      END IF
   END IF

   IF SQLCA.SQLCODE < 0 THEN
      ROLLBACK WORK
      RETURN FALSE
   ELSE
      COMMIT WORK
      CALL cat_desp15()
      CALL box_valdato("Los permisos han sido modificados.")
      RETURN TRUE
   END IF
END FUNCTION
