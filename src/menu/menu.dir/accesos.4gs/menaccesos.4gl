DATABASE ande
TYPE tree_t RECORD
   id             INTEGER,
   parentid       INTEGER,
   NAME           VARCHAR(50)
END RECORD
DEFINE 
   tree DYNAMIC ARRAY OF RECORD
      NAME           STRING,
      pid            STRING,
      hasChildren    BOOLEAN
   END RECORD,
   aui               om.DomNode,
   sm                om.DomNode,
   tree_arr DYNAMIC ARRAY OF RECORD
      NAME           STRING,         -- text to be displayed for the node
      IMAGE          STRING,        -- name of the image file for the node (can be null)
      parentid       STRING,          -- id of the parent node
      id             STRING,           -- id of the current node
      expanded       BOOLEAN,    -- node expansion flag (TRUE/FALSE) (optional)
      isnode         BOOLEAN,       -- children indicator flag (TRUE/FALSE) (optional)
      estado         BOOLEAN
   END RECORD,
   gr_usu RECORD
      usu_id         LIKE adm_usu.usu_id,
      usu_nom        LIKE adm_usu.usu_nom
   END RECORD
   
  
MAIN
DEFINE
   idx,scr,lint_flag SMALLINT
      OPEN WINDOW f WITH FORM "menaccesos"
		CALL init ()
		CALL fill (-1)	

      DIALOG 

      INPUT BY NAME gr_usu.usu_id

         ON KEY (CONTROL-B)
            LET lint_flag = FALSE
            CALL picklist_2("Busqueda de Usuarios","C�digo","Descripci�n","usu_id", "usu_nom",
                            "adm_usu","1= 1",1,1)
            RETURNING gr_usu.usu_id, gr_usu.usu_nom, lint_flag
            IF lint_flag = TRUE THEN
               LET lint_flag = FALSE
               LET gr_usu.usu_id= NULL
               DISPLAY BY NAME gr_usu.usu_id,gr_usu.usu_nom
               NEXT FIELD usu_id
            ELSE
               DISPLAY BY NAME gr_usu.usu_id, gr_usu.usu_nom
            END IF
      END INPUT
      
      DISPLAY ARRAY tree_arr TO sr_tree.* 
         ON ACTION Modificar
            LET idx = ARR_CURR()
            LET scr = SCR_LINE()
            LET tree_arr[idx].estado = NOT tree_arr[idx].estado
            DISPLAY tree_arr[idx].estado TO sr_tree[scr].estado
            

         ON ACTION EditUser
            CALL box_valdato ( "Pendiente de Implementacion" )

            
     END DISPLAY

      ON ACTION CANCEL
         EXIT DIALOG
         
     END DIALOG

      CLOSE WINDOW f
  END MAIN

FUNCTION init ()
	LET aui = ui.Interface.getRootNode()
	LET sm = aui.createChild("StartMenu")

END FUNCTION


FUNCTION fill( p )
DEFINE 
	p	INTEGER,
	a  DYNAMIC ARRAY OF tree_t,
	x,i,j 		INTEGER,
	Query STRING,
	q	STRING
	DEFINE t tree_t


	LET Query = "SELECT 	prog_id, prog_padre, prog_des", 
					" FROM 	mprog ",
					" WHERE 	prog_padre = ", p,
               " AND    prog_id != -1",
					" ORDER BY prog_id, prog_padre"

	PREPARE stmt1 FROM Query

 	DECLARE lista CURSOR FOR stmt1

	LET x = 0

	FOREACH lista INTO t.*
		LET x = x + 1
		LET a[x].* = t.*
	END FOREACH



     FOR i = 1 TO x
         LET j = tree_arr.getLength() + 1
         LET tree_arr[j].name = a[i].name
         LET tree_arr[j].id = a[i].id
         LET tree_arr[j].parentid = a[i].parentid
         LET tree_arr[j].estado = FALSE
			--DISPLAY a[i].id
			--PROMPT 'Presione '  FOR q
			--WHENEVER ERROR CONTINUE
         CALL fill(a[i].id)
			--WHENEVER ERROR STOP
     END FOR
END FUNCTION
