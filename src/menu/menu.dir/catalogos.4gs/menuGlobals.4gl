#########################################################################
## Function  : GLOBALS
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Declaracion de funciones globales
#########################################################################
SCHEMA face

GLOBALS
CONSTANT cErr = "Error. La operación no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"
CONSTANT cRaiz = -1
DEFINE usuario STRING
DEFINE dbname  CHAR(20)
DEFINE vempr_nom CHAR(60)
DEFINE vgru_id INTEGER
DEFINE vusuario CHAR(15)
END GLOBALS
