SCHEMA face
GLOBALS "menuGlobals.4gl"
########################################################################
##  : MAIN
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Funcion principal del programa
#########################################################################
MAIN
DEFINE n_param SMALLINT

   DEFER INTERRUPT
   OPTIONS
      INPUT WRAP,
      ON CLOSE APPLICATION STOP
   WHENEVER ERROR CONTINUE

	  LET n_param = num_args()
     display 'num_args ', n_param

    IF n_param = 0 THEN
      RETURN
   ELSE
      LET dbname = arg_val(1)
      display 'dbname ', dbname
      DATABASE dbname

		SELECT empr_nom
		INTO vempr_nom
		FROM face:mempr
		WHERE mempr.empr_db = dbname
   END IF
   CALL catalogos()
END MAIN


#########################################################################
## Function  : catalogos()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciones del Menu Catalogos 
#########################################################################
FUNCTION catalogos()
   DEFINE w ui.Window
	DEFINE tit STRING
   OPEN WINDOW menCatalogos AT 1,1 WITH FORM "menCatalogos" 
		LET tit = vempr_nom CLIPPED, " - [Catalogos]"
		CALL fgl_settitle(tit)
      CALL lib_cleanTinyScreen()
      CALL ui.Interface.loadActionDefaults("actiondefaults")
--      CALL ui.Interface.loadActionDefaults("default.4ad")

      CALL ui.Interface.loadStyles("styles")
      CALL ui.Interface.loadToolbar("toolbar")
      MENU ""
         ON ACTION grupos
            CALL catGrupo()
         ON ACTION usuarios
            CALL catUsuario()
         ON ACTION menus
            CALL catMenu()
         ON ACTION permisos
            CALL catPermiso()
            --CALL accesos ()
         ON ACTION cancel
            EXIT MENU
      END MENU
   CLOSE WINDOW menCatalogos
END FUNCTION