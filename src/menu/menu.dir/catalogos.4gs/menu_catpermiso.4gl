SCHEMA face
GLOBALS "menuGlobals.4gl"
#########################################################################
## Function  : catPermiso()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciondes de Programa Permisos de Catalogos
##
## Control de Cambios:
## Programador  Fecha
## MPerez      Thu May 17 10:56:07 CST 2007 se modifico la seleccion de 
##                                          perfiles para que no condicione
##                                          la empresa
#########################################################################
FUNCTION catPermiso()
DEFINE qry, cmb   STRING
DEFINE id         LIKE adm_gru.gru_id
DEFINE sigue      SMALLINT
DEFINE vempr_id 	SMALLINT
DEFINE tit 			STRING
DEFINE w ui.Window
DEFINE f ui.Form
LET sigue = TRUE

SELECT empr_id
INTO vempr_id
FROM mempr
WHERE empr_db = dbname

OPEN WINDOW menCatPermiso AT 1,1 WITH FORM "menCatPermiso" 
LET tit = vempr_nom CLIPPED, " - [C�talogos de Permisos]"
CALL fgl_settitle(tit)

WHILE sigue

LET cmb = "SELECT gru_id, gru_nom FROM adm_gru WHERE est_id = 13",
" AND empr_id = ", vempr_id

CALL combo_din2("id", cmb)
LET int_flag = FALSE
INPUT BY NAME id
ON ACTION interrupt
LET int_flag = FALSE
LET sigue = FALSE
EXIT INPUT	

AFTER INPUT
IF (NOT int_flag) THEN
CALL crea_temp(id)
LET w = ui.Window.getCurrent()
LET f = w.getForm()
CALL f.setElementHidden("group2",1)
LET vgru_id = id
CALL tablaPermisos(id)
CALL elimina_temp()
ELSE
LET int_flag = FALSE
LET sigue = FALSE
END IF
END INPUT
END WHILE
CLOSE WINDOW menCatPermiso
END FUNCTION

#########################################################################
## Function  : tablaPermisos(id)
##
## Parameters: none
##
## Returnings: none
		  ##
## Comments  : Opciondes de Programa Permisos de Catalogos
#########################################################################

FUNCTION tablaPermisos(id)
DEFINE id         LIKE adm_gru.gru_id
DEFINE cnt1			SMALLINT
DEFINE arr        ARRAY [100] OF
RECORD
permiso     SMALLINT,
menNombre    LIKE mprog.prog_des,
menId     LIKE mprog.prog_id
END RECORD
DEFINE qry        STRING
DEFINE arr2        DYNAMIC ARRAY OF
RECORD
gru_id     	SMALLINT,
prog_id    LIKE mprog.prog_id
END RECORD
DEFINE i,cnt,pos  SMALLINT
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE vusuario   INTEGER
DEFINE usuario    CHAR(15)
DEFINE vaccion 	SMALLINT
DEFINE err, x     SMALLINT

LET qry = "SELECT 0, prog_nom, prog_id FROM face:mprog WHERE prog_tip = 'M' AND est_id = 13 AND prog_id <> -1 ORDER BY 2 "
PREPARE prpTP1 FROM qry
DECLARE curTP1 CURSOR FOR prpTP1
LET i = 1

FOREACH curTP1 INTO arr[i].*
LET qry = "SELECT COUNT(*) FROM dmenprog WHERE dmenprog.gru_id = ? AND dmenprog.prog_id = ?"
PREPARE prpTP2 FROM qry
EXECUTE prpTP2 USING vgru_id, arr[i].menId INTO arr[i].permiso
LET i = i + 1
END FOREACH

LET i = i - 1
CALL SET_COUNT(i)

INPUT ARRAY arr WITHOUT DEFAULTS FROM scr.* ATTRIBUTE(BLUE)

--Despliega las opciones del otro menu
ON ACTION despopc
LET cnt1 = arr_curr()
CASE
WHEN INFIELD(permiso)
IF field_touched(permiso) THEN
CALL get_fldbuf(permiso) RETURNING arr[cnt1].permiso
END IF
END CASE

IF arr[cnt1].permiso = 1 THEN
LET w = ui.Window.getCurrent()
LET f = w.getForm()
CALL f.setElementHidden("group2",0)
CALL tablaPermisos2(arr[cnt1].menId)
CALL f.setElementHidden("group2",1)
LET int_flag = FALSE
ELSE
IF arr[cnt1].permiso = 0 THEN
DELETE FROM  tmp_dmenprog 
WHERE prog_id IN
( SELECT  mprog.prog_id
FROM face:mprog
WHERE mprog.prog_padre = arr[cnt].menId)
AND gru_id = vgru_id
END IF
CALL msg("No Tiene seleccionado la Opci�n del Menu") 
END IF

--Verifica la Poscion de la Linea
BEFORE FIELD permiso
LET cnt = arr_curr()
LET cnt1 = scr_line()
IF cnt > i THEN
CALL fgl_dialog_setcurrline(cnt1-1,cnt-1)
END IF

--Campo Permiso
AFTER FIELD permiso
LET cnt = arr_curr()
IF arr[cnt].permiso = 0 THEN
DELETE FROM  tmp_dmenprog 
WHERE prog_id IN
( SELECT  prog_id
FROM face:mprog
WHERE prog_padre = arr[cnt].menId)
AND gru_id = vgru_id
END IF
AFTER INPUT
IF (NOT int_flag) THEN
LET err = FALSE

BEGIN WORK
LET usuario = fgl_getenv("LOGNAME")

--Borra las opciones de los programas
WHENEVER ERROR CONTINUE
DELETE FROM dmenprog
WHERE gru_id = vgru_id
WHENEVER ERROR STOP
IF STATUS <> 0 THEN
LET err=TRUE
CALL msg(cErr)
EXIT INPUT
END IF

FOR pos = 1 TO i
LET qry = "SELECT COUNT(*) FROM dmenprog  WHERE dmenprog.gru_id = ? AND dmenprog.prog_id = ?"
PREPARE prpTP3 FROM qry
EXECUTE prpTP3 USING vgru_id, arr[pos].menId INTO cnt

IF STATUS <> 0 THEN
LET err=TRUE
EXIT FOR 
END IF

DELETE FROM tmp_dmenprog
WHERE prog_id = arr[pos].menID
AND gru_id = vgru_id

IF STATUS <> 0 THEN
LET err=TRUE
EXIT FOR 
END IF

SELECT adm_usu.usu_id
INTO vusuario
FROM adm_usu
WHERE adm_usu.usu_nomunix = usuario

							  --Idener

IF cnt <> arr[pos].permiso THEN
CASE arr[pos].permiso
WHEN 0
LET qry = "DELETE FROM dmenprog WHERE gru_id = ? AND prog_id = ?"
PREPARE prpTP4 FROM qry
EXECUTE prpTP4 USING vgru_id, arr[pos].menId
IF STATUS <> 0 THEN
LET err = FALSE
EXIT FOR
END IF
OTHERWISE
INSERT INTO dmenprog VALUES (0,vgru_id,arr[pos].MenId,vusuario,current)
IF STATUS <> 0 THEN
LET err = TRUE
EXIT FOR
END IF
END CASE
END IF
END FOR

--Segundo Detalle para Grabar
LET qry = "SELECT gru_id,prog_id FROM tmp_dmenprog",
" WHERE gru_id = ",vgru_id
PREPARE cursor_tmp FROM qry
DECLARE cursor_sel CURSOR FOR cursor_tmp

LET cnt1 = 1

FOREACH cursor_sel INTO arr2[cnt1].gru_id,
arr2[cnt1].prog_id
INSERT INTO dmenprog VALUES (0,vgru_id,arr2[cnt1].prog_id,vusuario,current)
IF STATUS <> 0 THEN
LET err = TRUE
EXIT FOREACH
END IF
LET cnt1 = cnt1 + 1
END FOREACH

IF (err) THEN
ROLLBACK WORK
CALL msg(cErr)
ELSE
COMMIT WORK
CALL msg(cPerOK)
END IF
ELSE
LET int_flag = TRUE
END IF
END INPUT
END FUNCTION

#########################################################################
## Function  : tablaPermisos2(id)
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Opciondes de Programa Permisos de Catalogos
#########################################################################
FUNCTION tablaPermisos2(id)
   DEFINE id         LIKE mprog.prog_id
   DEFINE arr        ARRAY [200] OF
      RECORD
         permiso     SMALLINT,
         prog_nom    LIKE mprog.prog_des,
			prog_id		LIKE mprog.prog_id
      END RECORD
   DEFINE qry        STRING
   DEFINE i,cnt,cnt1,pos  SMALLINT
   DEFINE err, x     SMALLINT

   LET qry = " SELECT 0, prog_nom, prog_id FROM face:mprog WHERE prog_tip IN ('P','R') AND est_id = 13",
				 " AND prog_padre = ",id,
				 " ORDER BY 2"
   PREPARE rpTP1 FROM qry
   DECLARE urTP1 CURSOR FOR rpTP1
   LET i = 1

   FOREACH urTP1 INTO arr[i].*
      LET qry = "SELECT COUNT(*) FROM tmp_dmenprog WHERE tmp_dmenprog.gru_id = ? AND tmp_dmenprog.prog_id = ?"
      PREPARE rpTP2 FROM qry
      EXECUTE rpTP2 USING vgru_id, arr[i].prog_id INTO arr[i].permiso
      LET i = i + 1
   END FOREACH
	LET i = i - 1

	IF i = 0 THEN
		CALL msg("No existen Programas para este Menu")
		RETURN
	END IF

   CALL SET_COUNT(i)

   LET int_flag = FALSE
   INPUT ARRAY arr WITHOUT DEFAULTS FROM scr2.* ATTRIBUTE(BLUE)
	
	--Verifica la Poscion de la Linea
	 BEFORE FIELD permiso2
	 	LET cnt = arr_curr()
	 	LET cnt1 = scr_line()

		IF cnt > i THEN
			CALL fgl_dialog_setcurrline(cnt1-1,cnt-1)
		END IF
	
      AFTER INPUT
         IF (NOT int_flag) THEN
            LET err = FALSE
            BEGIN WORK
            FOR pos = 1 TO i
               LET qry = "SELECT COUNT(*) FROM tmp_dmenprog  WHERE tmp_dmenprog.gru_id = ? AND tmp_dmenprog.prog_id = ?"
               PREPARE rpTP3 FROM qry
               EXECUTE rpTP3 USING vgru_id, arr[pos].prog_id INTO cnt
               IF cnt <> arr[pos].permiso THEN
                  CASE arr[pos].permiso
                     WHEN 0
                        LET qry = "DELETE FROM tmp_dmenprog WHERE gru_id = ? AND prog_id = ?"
                        PREPARE rpTP4 FROM qry
                        EXECUTE rpTP4 USING vgru_id, arr[pos].prog_id
                        IF STATUS <> 0 THEN
                           LET err = TRUE
                           EXIT FOR
                        END IF
                     OTHERWISE

                        INSERT INTO tmp_dmenprog  VALUES (vgru_id, arr[pos].prog_id)
                        IF STATUS <> 0 THEN
                           LET err = TRUE
                           EXIT FOR
                        END IF
                  END CASE
               END IF
            END FOR
            IF (err) THEN
               ROLLBACK WORK
               CALL msg(cErr)
            ELSE
               COMMIT WORK
               CALL msg(cPerOK)
            END IF
         ELSE
            LET int_flag = TRUE
         END IF
   END INPUT

	FOR i = 1 to arr.getlength()
		INITIALIZE arr[i].* TO NULL
		DISPLAY arr[i].* TO scr2[i].*
	END FOR

END FUNCTION

#########################################################################
## Function  : despliega(id)
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Despliega datos en la forma en el arreglo
#########################################################################
FUNCTION despliega(id)
   DEFINE id         LIKE adm_gru.gru_id
   DEFINE arr        DYNAMIC ARRAY OF
      RECORD
         permiso     SMALLINT,
         prog_nom    LIKE mprog.prog_nom,
			prog_id		LIKE mprog.prog_id
      END RECORD
   DEFINE qry        STRING
   DEFINE i,cnt,pos  SMALLINT
   DEFINE err, x     SMALLINT

   LET qry = " SELECT 0, prog_nom, prog_id FROM face:mprog WHERE prog_tip IN('P','R')",
				 " AND prog_padre = ",id
   PREPARE drpTP1 FROM qry
   DECLARE durTP1 CURSOR FOR drpTP1
   LET i = 1

   FOREACH durTP1 INTO arr[i].*
      LET qry = "SELECT COUNT(*) FROM tmp_dmenprog WHERE tmp_dmenprog.gru_id = ? AND tmp_dmenprog.prog_id = ?"

      PREPARE drpTP2 FROM qry
      EXECUTE drpTP2 USING vgru_id, arr[i].prog_id INTO arr[i].permiso
      LET i = i + 1
   END FOREACH
   CALL SET_COUNT(arr.getlength())

   LET int_flag = FALSE
   DISPLAY ARRAY arr TO scr2.* ATTRIBUTE(BLUE)
      AFTER DISPLAY
         IF (NOT int_flag) THEN
            LET err = FALSE
            LET int_flag = TRUE
         END IF
   END DISPLAY 

	FOR i = 1 to arr.getlength()
		INITIALIZE arr[i].* TO NULL
		DISPLAY arr[i].* TO scr2[i].*
	END FOR

END FUNCTION

#########################################################################
## Function  : carga_temp()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Crea la tabla temporal con los datos de mprog
#########################################################################

--Carga la informacion de la tabla maestra a la temporal
FUNCTION crea_temp(id)
DEFINE id INTEGER

CREATE TEMP TABLE  tmp_dmenprog
(
gru_id  INTEGER,
prog_id INTEGER
)
WITH NO LOG

INSERT INTO tmp_dmenprog
SELECT dmenprog.gru_id,dmenprog.prog_id FROM dmenprog
WHERE dmenprog.gru_id = id

END FUNCTION


#########################################################################
## Function  : elimina_temp()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Elimina la Tabla Temporal
#########################################################################

--Carga la informacion de la tabla maestra a la temporal
FUNCTION elimina_temp()

DROP TABLE tmp_dmenprog

END FUNCTION