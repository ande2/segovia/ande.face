SCHEMA face

MAIN
DEFINE
   ldoc_aut                    SMALLINT,
   ldoc_id                     INTEGER,
   estado                  SMALLINT,
	dbname						CHAR(20)

  IF NUM_ARGS() <> 3 THEN 
      DISPLAY "Ejecute el programa:  fglrun docElectronico.42r id_factura NumDocAutomatico BaseDatos"
      DISPLAY "                     id_factura       (numero serial generado al grabar en factura_e)"
      DISPLAY "                     NumDocAutomatico (1=numero automatica, 0=Enviar el numero generado internamente)"
      DISPLAY "                     NumDocAutomatico (1=numero automatica, 0=Enviar el numero generado internamente)"
      DISPLAY "                     BaseDatos 		  (Nombre de la base de datos)"
      EXIT PROGRAM
   ELSE
      LET ldoc_id    = ARG_VAL( 1 )
      LET ldoc_aut   = ARG_VAL( 2 )
		LET dbName     = ARG_VAL( 3 )
		DATABASE dbName
      CALL certDocElec ( ldoc_id, ldoc_aut ) RETURNING estado
   END IF
	
END MAIN
