SCHEMA face

MAIN
DEFINE
   ldoc_id                     INTEGER,
   estado                  SMALLINT,
	dbname						CHAR(20),
   lfirma                  VARCHAR(60), 
   lmensaje                VARCHAR(100)

  IF NUM_ARGS() <> 2 THEN 
      DISPLAY "Ejecute el programa:  fglrun docElectronico.42r id_factura dbName"
      DISPLAY "                     id_factura       (numero serial generado al grabar en factura_e)"
      DISPLAY "                     BaseDatos 		  (Nombre de la base de datos)"
      EXIT PROGRAM
   ELSE
      LET ldoc_id    = ARG_VAL( 1 )
		LET dbName     = ARG_VAL( 2)
		DATABASE dbName
      CALL getDocument ( ldoc_id ) RETURNING estado, lfirma
      IF estado THEN
         LET lmensaje = "Firma Obtenida \n\n", lfirma
         DISPLAY lmensaje  
         --CALL box_valdato( lmensaje)
      ELSE
         DISPLAY "ERROR ", SQLCA.SQLERRM 
      END IF
   END IF
	
END MAIN