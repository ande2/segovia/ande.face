################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : empr_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               empr_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "venm0011_glob.4gl"

FUNCTION empr_busca_init()
DEFINE
	select_stmt3			VARCHAR(1000)
	
LET select_stmt3 = "SELECT * FROM mcli WHERE empr_id = ? AND cli_id = ?"
PREPARE ex_stmt3 FROM select_stmt3

END FUNCTION

FUNCTION busca_empr()
DEFINE
	empr_query		CHAR(300),
	where_clause	CHAR(250),
	empr_cnt        SMALLINT,
   condicion        CHAR(300),
	empr_count		CHAR(300),
   mensaje          CHAR(100),
   respuesta        CHAR(6),
   lint_flag        SMALLINT
CLEAR FORM
--DISPLAY vempr_nomlog TO bmp1
CALL encabezado()
DISPLAY "Ingrese criterio de b�squeda y presione aceptar ",
        "para iniciar b�squeda.","" AT 22,8
DISPLAY "Solo aceptar para b�squeda general.","" AT 23,8

let gtit_enc="BUSQUEDA REGISTRO"
DISPLAY BY NAME gtit_enc                                                                                
LET int_flag = FALSE

CONSTRUCT 
   BY NAME  where_clause 
   ON       m.cli_cod,
            mcli.cli_nit,
            mcli.cli_nomcom,
            mcli.cli_idioma,
            mcli.cli_codpos,
            mcli.cli_email,
            mcli.cli_formato,
            mcli.mun_nom,
            mcli.dep_nom,
            mcli.pais_nom,
            mcli.cli_dir1,
            mcli.cli_dir2
 
	BEFORE CONSTRUCT
	CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
   	CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
	CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

    --ON KEY (CONTROL - W)
      --  CALL showhelp (2)
			
END CONSTRUCT

IF int_flag = TRUE THEN
	LET int_flag = FALSE
	CLEAR FORM
	CALL encabezado()
	ERROR "B�squeda Abortada."
	RETURN FALSE, 0
END IF	 

LET where_report = where_clause
LET empr_query = "SELECT cli_id ",
                 " FROM mcli ",
                 " WHERE empr_id =", id_empresa, 
                 " AND " , where_clause CLIPPED,
                 " ORDER BY 1 " 

LET empr_count = "SELECT count(*) FROM mcli WHERE empr_id =", id_empresa, " AND ", where_clause CLIPPED

PREPARE ex_stmt FROM empr_query
PREPARE ex_stmt2 FROM empr_count
DECLARE empr_ptr SCROLL CURSOR WITH HOLD FOR ex_stmt

OPEN empr_ptr

FETCH FIRST empr_ptr INTO gr_cli.*

IF SQLCA.SQLCODE = 100 THEN
	CALL box_valdato("No existen datos con estas condiciones.")
	MESSAGE "[Control-W] = Ayuda"
	CLOSE empr_ptr
	RETURN FALSE,0
ELSE
	DECLARE empr_all CURSOR FOR ex_stmt3	
	OPEN empr_all USING id_empresa, gr_cli.cli_id

	FETCH empr_all INTO gr_cli.*

	IF SQLCA.SQLCODE = NOTFOUND THEN
		CALL box_valdato("No existen datos con estas Condiciones.")
		CLOSE empr_ptr
		RETURN FALSE,0
	ELSE
		DECLARE contador_ptr CURSOR FOR ex_stmt2
		OPEN contador_ptr
		FETCH contador_ptr INTO empr_cnt
		CLOSE contador_ptr
		CALL empr_desp()
		MESSAGE "Existen ", empr_cnt USING "<<<<", " Registros."
		SLEEP 2
		MESSAGE ""
		RETURN TRUE, empr_cnt
	END IF				
END IF				
END FUNCTION

FUNCTION fetch_cat(fetch_flag)
DEFINE 
	fetch_flag CHAR(2)
   
   CASE fetch_flag
      WHEN "F"
         FETCH FIRST    empr_ptr INTO gr_cli.*
      WHEN "1"
         FETCH NEXT     empr_ptr INTO gr_cli.*
         IF sqlca.sqlcode  = 100 THEN
            CALL box_valdato("Usted esta posicionado en el final de la lista")
         END IF
      WHEN "-1"
         FETCH PREVIOUS empr_ptr INTO gr_cli.*
         IF sqlca.sqlcode  = 100 THEN
            CALL box_valdato("Usted esta posicionado en el inicio de la lista")
         END IF
      WHEN "L"
         FETCH LAST     empr_ptr INTO gr_cli.*
   END CASE

   OPEN empr_all USING id_empresa, gr_cli.cli_id
   FETCH empr_all INTO gr_cli.*
   IF SQLCA.SQLCODE = NOTFOUND THEN
		LET err_msg = "El registro fue eliminado de la lista"
		CALL box_valdato(err_msg)
	ELSE
		CALL empr_desp()
	END IF 
END FUNCTION

FUNCTION empr_desp()
   SELECT   a.cli_cod,
            a.cli_nit,
            a.cli_nomcom,
            a.cli_idioma,
            a.cli_codpos,
            a.cli_email,
            a.cli_formato,
            a.mun_nom,
            a.dep_nom,
            a.pais_nom,
            a.cli_dir1,
            a.cli_dir2
      INTO  gr_cli.cli_cod,
            gr_cli.cli_nit,
            gr_cli.cli_nomcom,
            gr_cli.cli_idioma,
            gr_cli.cli_codpos,
            gr_cli.cli_email,
            gr_cli.cli_formato,
            gr_cli.mun_nom,
            gr_cli.dep_nom,
            gr_cli.pais_nom,
            gr_cli.cli_dir1,
            gr_cli.cli_dir2
      FROM  mcli a
      WHERE a.cli_id= gr_cli.cli_id

   DISPLAY 
      BY NAME gr_cli.cli_cod,
            gr_cli.cli_nit,
            gr_cli.cli_nomcom,
            gr_cli.cli_idioma,
            gr_cli.cli_codpos,
            gr_cli.cli_email,
            gr_cli.cli_formato,
            gr_cli.mun_nom,
            gr_cli.dep_nom,
            gr_cli.pais_nom,
            gr_cli.cli_dir1,
            gr_cli.cli_dir2
END FUNCTION

FUNCTION limpiar() 
WHENEVER ERROR CONTINUE
--CLOSE empr_ptr
WHENEVER ERROR STOP
END FUNCTION