################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# JMonterroso Mon Dec  8 13:57:44 CST 2003 Se agrego variable global de la BD 
################################################################################

SCHEMA "face"


GLOBALS
DEFINE
   g_sccs_var                 CHAR(70), # Variable para SCCS
   gr_cli,nr_cli,ur_cli      RECORD
      cli_id                 LIKE mcli.cli_id,
      cli_cod                LIKE mcli.cli_cod,
      cli_nit                LIKE mcli.cli_nit,
      cli_nomcom             LIKE mcli.cli_nomcom,
      cli_idioma             LIKE mcli.cli_idioma,
      cli_codpos             LIKE mcli.cli_codpos,
      cli_email              LIKE mcli.cli_email,
      cli_formato            LIKE mcli.cli_formato,
      mun_nom                LIKE mcli.mun_nom,
      dep_nom                LIKE mcli.dep_nom,
      pais_nom               LIKE mcli.pais_nom,
      cli_dir1               LIKE mcli.cli_dir1,
      cli_dir2               LIKE mcli.cli_dir2
   END RECORD,
   g_empr                     RECORD LIKE mempr.*,
   dbperm,
   dbname                     CHAR(40),
   vempr_nomlog               CHAR(15),
   usuario                    CHAR(30),
   hr                         DATE,
   gtit_enc,   
   gtit_pie,
   id_empresa                 INTEGER,
   nombre_empresa             VARCHAR(40),
   nombre_departamento        VARCHAR(80),
   where_report               VARCHAR(1000),
   err_msg                    VARCHAR(50)
END GLOBALS