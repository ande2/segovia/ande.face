###############################################################################
# Funcion     : %M%
# Descripcion : Modulo para la modificacion del catalogo 
# Funciones   : modi_init()
#               empr_modi()
#               empr_anul()
#               valida_modi()
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : ea
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# HREYES      Tue Sep 13 11:44:24 GMT 2005 se elimina usuario pro10eec
# JMonterroso Mon Dec  8 14:01:11 CST 2003 Se agrego la validacion de la BD
# JMonterroso Tue Oct 21 11:14:30 CDT 2003 Se agrego al usuario pro10eec el 
#                                 poder modificar la descripcion del articulo
################################################################################

GLOBALS "venm0011_glob.4gl"

FUNCTION modi_init()
DEFINE 
	prepvar VARCHAR(1000)
   
   DECLARE lockempr CURSOR FOR
      SELECT   *
         FROM  mcli
         WHERE empr_id  = id_empresa
         AND   cli_id   = gr_cli.cli_id
         FOR UPDATE

   LET prepvar = " UPDATE  mcli " ,
                 "   SET   cli_idioma  = ?, " ,
                 "         cli_codpos  = ?, " ,
                 "         cli_email   = ?, " ,
                 "         cli_formato = ?, " ,
                 "         pais_nom    = ? " ,
                  " WHERE CURRENT OF lockempr"
   WHENEVER ERROR CONTINUE
   PREPARE upd_empr FROM prepvar
   WHENEVER ERROR STOP
   IF SQLCA.SQLCODE <> 0 THEN
      DISPLAY sqlerrmessage, " error"
      EXIT PROGRAM(1)
   END IF
END FUNCTION

FUNCTION empr_modi()
DEFINE
	mensaje CHAR(100),
	respuesta CHAR(6),
	contador	SMALLINT,
	lexiste SMALLINT

   let gtit_enc="MODIFICA REGISTRO"
   MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

   BEGIN WORK

   OPEN lockempr

   WHENEVER ERROR CONTINUE

   FETCH lockempr INTO gr_cli.*

   WHENEVER ERROR STOP
   DISPLAY "-- ",SQLCA.sqlcode, " -- ",sqlerrmessage
   IF SQLCA.SQLCODE < 0 OR SQLCA.SQLCODE = NOTFOUND THEN
      ROLLBACK WORK
      CALL box_valdato("No es posible bloquear el registro para Modificarlo.")
   ELSE
      LET contador =  0

      LET ur_cli.* = gr_cli.*
      LET int_flag = FALSE

   INPUT 
      BY NAME  gr_cli.cli_idioma,
               gr_cli.cli_codpos,
               gr_cli.cli_email,
               gr_cli.cli_formato,
               gr_cli.pais_nom
       ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
                 
         BEFORE INPUT
            CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")
            CALL fgl_dialog_setkeylabel("CONTROL-B","Buscar")
            CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")
      
        -- ON KEY (CONTROL-W)
          --  CALL showhelp (4)


         AFTER INPUT
            IF NOT int_flag THEN
               IF valida_modi() = TRUE THEN		
                  CALL box_gradato("Desea guardar la información.") RETURNING respuesta
                  IF respuesta = "Si" THEN
                     LET int_flag = FALSE
                     EXIT INPUT
                  ELSE
                     IF respuesta = "No" THEN
                        LET int_flag = TRUE
                        EXIT INPUT
                     ELSE
                        CONTINUE INPUT
                     END IF
                  END IF
               ELSE	
                  CALL box_valdato("No se efectuó ningun cambio.")
                  LET int_flag = TRUE
               END IF
            ELSE
               CALL box_inter() RETURNING int_flag
               IF int_flag = TRUE THEN
                  EXIT INPUT
               ELSE
                  CONTINUE INPUT
               END IF
            END IF
      END INPUT

      IF int_flag = TRUE THEN
         ROLLBACK WORK
         LET int_flag = FALSE
         LET gr_cli.* = ur_cli.*
         CALL empr_desp()
         CALL box_valdato("Modificación Cancelada." )
          MESSAGE "Usuario : ",usuario CLIPPED,"     Fecha : ", TODAY  ATTRIBUTE (BLUE)

      ELSE
         WHENEVER ERROR CONTINUE
         EXECUTE     upd_empr 
            USING    gr_cli.cli_idioma,
                     gr_cli.cli_codpos,
                     gr_cli.cli_email,
                     gr_cli.cli_formato,
                     gr_cli.pais_nom

         WHENEVER ERROR STOP 	
         IF SQLCA.SQLCODE < 0 THEN
            ROLLBACK WORK
            LET gr_cli.* = ur_cli.*
            CALL empr_desp()
            LET mensaje = "El error numero ", SQLCA.SQLCODE USING "-<<<<", " ha ocurrido."
            CALL box_valdato(mensaje)
            RETURN FALSE
         ELSE
            COMMIT WORK
            CALL empr_desp()
            CALL box_valdato("Registro modificado.")
            RETURN TRUE
         END IF
      END IF
      RETURN FALSE
   END IF
RETURN FALSE

END FUNCTION


FUNCTION valida_modi()
   IF gr_cli.cli_idioma  = ur_cli.cli_idioma AND
      gr_cli.cli_codpos  = ur_cli.cli_codpos AND
      gr_cli.cli_email   = ur_cli.cli_email AND
      gr_cli.cli_formato = ur_cli.cli_formato AND
      gr_cli.pais_nom    = ur_cli.pais_nom  THEN   
      RETURN FALSE
   END IF
   RETURN TRUE
END FUNCTION