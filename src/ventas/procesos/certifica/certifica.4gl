DATABASE face

MAIN
DEFINE
   ldoc_id                 INTEGER,
   estado                  SMALLINT

  IF NUM_ARGS() <> 1 THEN 
      DISPLAY "Ejecute el programa:  fglrun docElectronico.42r id_factura "
      DISPLAY "                     id_factura       (numero serial generado al grabar factura lnktra)"
      EXIT PROGRAM
   ELSE
      LET ldoc_id    = ARG_VAL( 1 )
      CALL certidoc ( ldoc_id ) RETURNING estado
      
      DISPLAY 'Estado --> ', estado
   END IF
	
END MAIN
