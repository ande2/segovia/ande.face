################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : cat_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               cat_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "venp0010_glob.4gl"

FUNCTION cat_busca_init()
DEFINE
   select_stmt3			VARCHAR(5000)





END FUNCTION

FUNCTION busca_cat()
DEFINE
	cat_query		      VARCHAR(2500),
	where_clause	      VARCHAR(1500),
	cat_cnt              SMALLINT,
   condicion            CHAR(300),
	cat_count		      CHAR(300),
   mensaje              CHAR(100),
   respuesta            CHAR(6),
   lint_flag            SMALLINT,
	x_condicion          VARCHAR(1000),
   idx                  INTEGER,
   lorigen              CHAR(3),
   r                    docdet
   
   --CALL encabezado()

   LET gtit_enc="BUSQUEDA REGISTRO"
   IF gr_reg.doc_anio IS NULL THEN
      LET gr_reg.doc_anio = YEAR(TODAY)
      LET gr_reg.doc_mes = MONTH(TODAY)
   END IF 
   CALL gr_det.clear()

   DISPLAY BY NAME gtit_enc, gr_reg.*                                                                              
   LET int_flag = FALSE

   INPUT 
   BY NAME gr_reg.doc_mes, gr_reg.doc_anio
   WITHOUT DEFAULTS 
   
      BEFORE INPUT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	


      AFTER INPUT

         IF gr_reg.doc_anio IS NULL OR gr_reg.doc_mes IS NULL THEN
            CALL box_valdato("Debe ingresar todos los datos.")
            NEXT FIELD doc_mes
         END IF
         
         IF int_flag THEN
            CALL box_inter() RETURNING int_flag 
            IF int_flag = TRUE THEN
               EXIT INPUT
            ELSE
               CONTINUE INPUT
            END IF
         END IF
   END INPUT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      ERROR "B�squeda Abortada."
      RETURN 0
   END IF

   LET cat_query = "SELECT a.lnktra                                                                doc_id, ",
                   "       CASE a.estado  WHEN  'V' THEN 'Activo'  ELSE 'Anulado'  END             estadoDocumento, ",
                   "       d.esta_cod                                                              codigoEstablecimiento, ",
                   "       a.nserie                                                                serieDocumento, ",
                   "       a.numdoc                                                                numeroDocumento, ",
                   "       a.fecemi                                                                fechaDocumento, ",
                   "       a.numnit                                                                nitComprador, ",
                   "       a.nomcli                                                                nombreComercialComprador,",
                   "       CASE a.moneda  WHEN 1    THEN 'GTQ'     ELSE 'USD'      END             codigoMoneda, ",
                   "       a.tascam                                                                tipoCambio, ",
                   "       a.totdoc - a.totiva                                                     importeBruto,", 
                   "       estado_documento( a.lnktra)                                             estado ",
                   "     from segovia@lnxseg_tcp:fac_mtransac a, dres b, mtipdoc c, mesta d, mempr e ",
                   "     where YEAR (a.fecemi) = ", gr_reg.doc_anio,
                   "     AND   MONTH(a.fecemi) =", gr_reg.doc_mes,
                   "     and   b.res_serie = a.nserie ",
                   "     and   b.tipcon_id in ( 1,2 ) ",
                   "     and   a.numdoc BETWEEN b.res_de AND b.res_a", 
                   "     and   c.tipdoc_id = b.tipdoc_id ",
                   "     and   d.esta_id = b.esta_id ",
                   "     and   e.empr_id = d.empr_id ",
                   "     ORDER BY d.esta_cod, a.nserie, a.numdoc"
                   
DISPLAY "query ", cat_query
   PREPARE ex_stmt FROM cat_query

   DECLARE cat_ptr CURSOR FOR ex_stmt

   LET idx = 1
   FOREACH cat_ptr INTO r.*
      LET gr_det[idx].* = r.*
      LET idx = idx + 1
   END FOREACH
   LET idx = idx - 1
   
   DISPLAY ARRAY gr_det TO sDet.*

   END DISPLAY
   RETURN idx
END FUNCTION