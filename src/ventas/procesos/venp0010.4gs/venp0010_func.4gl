GLOBALS "venp0010_glob.4gl"


FUNCTION TrasladaDocElec (  )
DEFINE
   QueryBuffer    VARCHAR(5000),
   lcount,
   id_empresa     INTEGER,
   ldoc_id        INTEGER,
   idx            INTEGER,
   pmax,
   pcount         INTEGER,
   select_stmt3   VARCHAR(5000),
   r              RECORD 
      doc_id                                    INTEGER,
      estadoDocumento 							LIKE mdoc.estadoDocumento ,
      codigoMoneda 								LIKE mdoc.codigoMoneda ,
      tipoDocumento 							LIKE mdoc.tipoDocumento ,
      numeroResolucion 							LIKE mdoc.numeroResolucion ,
      fechaResolucion 							VARCHAR(20) ,
      serieAutorizada 							LIKE mdoc.serieAutorizada ,
      fechaDocumento 							VARCHAR(20) ,
      tipoCambio 								LIKE mdoc.tipoCambio ,
      fechaAnulacion 							VARCHAR(20),
      numeroDocumento 							LIKE mdoc.numeroDocumento ,
      serieDocumento 							LIKE mdoc.serieDocumento ,
      regimenISR 								LIKE mdoc.regimenISR ,
      codigoEstablecimiento 					LIKE mdoc.codigoEstablecimiento ,
      nitGFACE 									LIKE mdoc.nitGFACE,
      nitVendedor 								LIKE mdoc.nitVendedor,
      municipioVendedor 						LIKE mdoc.municipioVendedor,
      departamentoVendedor 					    LIKE mdoc.departamentoVendedor ,
      direccionComercialVendedor 			    LIKE mdoc.direccionComercialVendedor,
      nombreComercialRazonSocialVendedor 	    LIKE mdoc.nombreComercialRazonSocialVendedor,
      nombreCompletoVendedor 					LIKE mdoc.nombreCompletoVendedor,
      nitComprador 								LIKE mdoc.nitComprador,
      regimen2989 								LIKE mdoc.regimen2989 ,
      telefonoComprador 						LIKE mdoc.telefonoComprador ,
      direccionComercialComprador 			    LIKE mdoc.direccionComercialComprador ,
      municipioComprador 						LIKE mdoc.municipioComprador ,
      departamentoComprador 					LIKE mdoc.departamentoComprador ,
      nombreComercialComprador 				    LIKE mdoc.nombreComercialComprador ,
      correoComprador 							LIKE mdoc.correoComprador ,
      montoTotalOperacion 						LIKE mdoc.montoTotalOperacion ,
      importeDescuento 							LIKE mdoc.importeDescuento ,
      importeTotalExento 						LIKE mdoc.importeTotalExento ,
      importeNetoGravado 						LIKE mdoc.importeNetoGravado ,
      detalleImpuestosIva 						LIKE mdoc.detalleImpuestosIva ,
      importeOtrosImpuestos 					LIKE mdoc.importeOtrosImpuestos ,
      descripcionOtroImpuesto 				    LIKE mdoc.descripcionOtroImpuesto ,
      importeBruto 								LIKE mdoc.importeBruto,
      empr_id                                   LIKE mdoc.empr_id,
      esta_id                                   LIKE mdoc.esta_id,
      res_id                                    LIKE mdoc.res_id
   END RECORD,
   rd RECORD
      doc_lin                             INTEGER,
      tipoProducto                        LIKE ddoc.tipoproducto,
      codigoProducto                      LIKE ddoc.codigoproducto,
      descripcionProducto                 LIKE ddoc.descripcionproducto,
      unidadMedida                        LIKE ddoc.unidadmedida,
      cantidad                            LIKE ddoc.cantidad,
      precioUnitario                      LIKE ddoc.preciounitario,
      importeTotalOperacion               LIKE ddoc.importetotaloperacion,
      montoBruto                          LIKE ddoc.montobruto,
      importeExento                       LIKE ddoc.importeexento,
      importeNetoGravado                  LIKE ddoc.importenetogravado,      
      montoDescuento                      LIKE ddoc.montodescuento,
      importeOtrosImpuestos               LIKE ddoc.importeotrosimpuestos,
      detalleImpuestosIva                 LIKE ddoc.detalleimpuestosiva
   END RECORD
DEFINE filename STRING 

  
   LET filename = "./", YEAR (TODAY ) USING "<<<<", MONTH (TODAY ) USING "&&", DAY (TODAY ) USING "&&", CURRENT HOUR TO HOUR,
                  CURRENT MINUTE TO MINUTE, "E"
   DISPLAY filename 
   UNLOAD TO filename 
      SELECT * FROM mdoc 
      WHERE doc_anio = gr_reg.doc_anio AND doc_mes = gr_reg.doc_mes
      
   LET filename = "./", YEAR (TODAY ) USING "<<<<", MONTH (TODAY ) USING "&&", DAY (TODAY ) USING "&&", CURRENT HOUR TO HOUR,
                  CURRENT MINUTE TO MINUTE, "D"
   DISPLAY filename 
   DISPLAY "-> ", CURRENT HOUR TO HOUR 
   UNLOAD TO filename 
      SELECT * FROM ddoc 
      WHERE doc_id IN (
         SELECT doc_id
         FROM mdoc 
         WHERE doc_anio = gr_reg.doc_anio AND doc_mes = gr_reg.doc_mes
         )
   
   DELETE FROM ddoc WHERE doc_id IN (
      SELECT doc_id
      FROM mdoc WHERE doc_anio = gr_reg.doc_anio AND doc_mes = gr_reg.doc_mes
      AND doc_firmaelectronica IS NULL 
      )

   DELETE FROM ldoc WHERE doc_id IN (
      SELECT doc_id
      FROM mdoc WHERE doc_anio = gr_reg.doc_anio AND doc_mes = gr_reg.doc_mes
      AND doc_firmaelectronica IS NULL 
      )
      
   --Eliminando del encabezado
   DELETE FROM mdoc WHERE doc_anio = gr_reg.doc_anio 
   AND doc_mes = gr_reg.doc_mes
   AND doc_firmaelectronica IS NULL


   LET select_stmt3 = "SELECT a.lnktra, ",
                      "      CASE a.estado  WHEN  'V' THEN 'Activo'  ELSE 'Anulado'  END             estadoDocumento,",
                      "      CASE a.moneda  WHEN 1    THEN 'GTQ'     ELSE 'USD'      END             codigoMoneda,",
                      "      c.tipdoc_cod                                                            tipoDocumento, ",
                      "      REPLACE(b.res_nautoriza, ", "'-',", " '')                               numeroResolucion,",
                      "      b.res_fecha                                                             fechaResolucion, ",
                      "      b.res_serie                                                             serieAutorizada, ",
                      "      a.fecemi                                                                fechaDocumento,",
                      "      a.tascam                                                                tipoCambio,",
                      "      CASE a.estado  WHEN  'V' THEN ' '  ELSE to_char(a.fecanl)    END        fechaAnulacion,",
                      "      a.numdoc                                                                numeroDocumento,",
                      "      c.tipdoc_id                                                             serieDocumento,",
                      "      e.empr_regisr                                                           regimenISR,",
                      "      d.esta_cod                                                              codigoEstablecimiento,",
                      "      '12521337'                                                              nitGFACE,",
                      "      e.empr_nit                                                              nitVendedor,", 
                      "      'GUATEMALA'                                                             municipioVendedor,",
                      "      'GUATEMALA'                                                             departamentoVendedor,", 
                      "      REPLACE(e.empr_dir1,", '"', "'", '",', '"', "`", '")',  "                direccionComercialVendedor,",
                      "      REPLACE(e.empr_nom,", '"', "'", '",', '"', "`", '")',  "                 nombreComercialRazonSocialVendedor,",
                      "      REPLACE(e.empr_nom,", '"', "'", '",', '"', "`", '")',  "                 nombreCompletoVendedor,",
                      "      a.numnit                                                                nitComprador,",
                      "      'N/A'                                                                   regimen2989,",
                      "      a.telcli                                                                telefonoComprador,",
                      "      REPLACE(a.dircli,", '"', "'", '",', '"', "`", '")',  "                   direccionComercialComprador,",
                      "      'N/A'                                                                   municipioComprador,",
                      "      'N/A'                                                                   departamentoComprador,",
                      "      REPLACE(a.nomcli,", '"', "'", '",', '"', "`", '")',  "                   nombreComercialComprador,",
                      "      'N/A'                                                                   correoComprador,",
                      "      a.totdoc                                                                montoTotalOperacion,",
                      "      0                                                                       importeDescuento,",
                      "      CASE a.exenta  WHEN  1 THEN a.totdoc  ELSE 0  END                       importeTotalExento,",
                      "      CASE a.exenta  WHEN  1 THEN 0         ELSE a.totdoc  END                importeNetoGravado,",
                      "      a.totiva                                                                detalleImpuestosIva ,",
                      "      0                                                                       importeOtroImpuesto,",
                      "      'N/A'                                                                   descripcionOtroImpuesto,",
                      "      a.totdoc - a.totiva                                                     importeBruto, ",
                      "      e.empr_id, d.esta_id, b.res_id",
                    " from segovia@lnxseg_tcp:fac_mtransac a, dres b, mtipdoc c, mesta d, mempr e",
                    " where YEAR (a.fecemi) = ", gr_reg.doc_anio,
                    " AND   MONTH(a.fecemi) = ", gr_reg.doc_mes,
                    " and   b.res_serie = a.nserie",
                    " and   b.tipcon_id IN ( 1,2 )",
                    " and   a.numdoc BETWEEN b.res_de AND b.res_a", 
                    " and   c.tipdoc_id = b.tipdoc_id",
                    " and   d.esta_id = b.esta_id",
                    " and   e.empr_id = d.empr_id",
                    " ORDER BY d.esta_cod, a.nserie, a.numdoc"

   PREPARE ex_stmt3 FROM select_stmt3
   
   SELECT Count(*)
      INTO  pMax
      FROM  segovia@lnxseg_tcp:fac_mtransac a
      where YEAR (a.fecemi) =  gr_reg.doc_anio
      AND   MONTH(a.fecemi) =  gr_reg.doc_mes
      

   LET idx = 0
   
   DECLARE docu_all CURSOR FOR ex_stmt3	
    
   FOREACH docu_all INTO r.*
      --Ejecutar el procecimiento para trasladar la factura
      DISPLAY r.doc_id
      SELECT   COUNT(*)
         INTO  lcount
         FROM  mdoc
         WHERE doc_id = r.doc_id

        IF lcount = 0 THEN

         
         LET QueryBuffer = 
            "INSERT ", 
            "   INTO        mdoc  (  doc_id, doc_anio, doc_mes,estadoDocumento, codigoMoneda, ",
            "                        tipoDocumento, numeroResolucion,  fechaResolucion,                                    serieAutorizada,", 
            "                        fechaDocumento, tipoCambio, ", 
            "                        fechaAnulacion, numeroDocumento,", 
            "                        serieDocumento, regimenISR, codigoEstablecimiento, nitGFACE, nitVendedor,", 
            "                        municipioVendedor, departamentoVendedor, direccionComercialVendedor, nombreComercialRazonSocialVendedor,", 
            "                        nombreCompletoVendedor, nitComprador, regimen2989, telefonoComprador,  direccionComercialComprador,", 
            "                        municipioComprador, nombreComercialComprador, departamentoComprador, correoComprador, montoTotalOperacion,", 
            "                        importeDescuento, importeTotalExento,  ", 
            "                        importeNetoGravado, detalleImpuestosIva, importeOtrosImpuestos, descripcionOtroImpuesto, ", 
            "                        importeBruto,  doc_certificado, empr_id, esta_id, res_id  ) ", 
            "   VALUES            (",
                                    r.doc_id, ",",
                                    gr_reg.doc_anio,",",
                                    gr_reg.doc_mes CLIPPED,",",
                                    "'", r.estadoDocumento CLIPPED,"',",
                                    "'", r.codigoMoneda,"',",
                                    "'", r.tipoDocumento CLIPPED,"',",
                                    "'", r.numeroResolucion CLIPPED,"',",
                                    formatofecha(r.fechaResolucion),",",
                                    "'", r.serieAutorizada CLIPPED,"',",
                                    formatofecha(r.fechaDocumento),",",
                                    r.tipoCambio,",",
                                    formatofecha(r.fechaAnulacion), ",",
                                    r.numeroDocumento,",",
                                    "'", r.serieDocumento CLIPPED,"',",
                                    "'", r.regimenISR CLIPPED,"',",
                                    "'", r.codigoEstablecimiento USING "<<<" ,"',",
                                    "'", r.nitGFACE CLIPPED,"',",
                                    "'", r.nitVendedor CLIPPED,"',",
                                    "'", r.municipioVendedor CLIPPED,"',",
                                    "'", r.departamentoVendedor CLIPPED,"',",
                                    "'", r.direccionComercialVendedor CLIPPED,"',",
                                    "'", r.nombreComercialRazonSocialVendedor CLIPPED,"',",
                                    "'", r.nombreCompletoVendedor CLIPPED,"',",
                                    "'", r.nitComprador CLIPPED,"',",
                                    "'", r.regimen2989 CLIPPED,"',",
                                    "'", r.telefonoComprador CLIPPED,"',",
                                    "'", r.direccionComercialComprador CLIPPED,"',",
                                    "'", r.municipioComprador CLIPPED,"',",
                                    "'", r.nombreComercialComprador CLIPPED,"',",
                                    "'", r.departamentoComprador CLIPPED,"',",
                                    "'", r.correoComprador CLIPPED,"',",
                                    r.montoTotalOperacion,",",
                                    r.importeDescuento,",",
                                    r.importeTotalExento,",",
                                    r.importeNetoGravado,",",
                                    r.detalleImpuestosIva,",",
                                    r.importeOtrosImpuestos,",",
                                    "'", r.descripcionOtroImpuesto CLIPPED,"',",
                                    r.importeBruto,",",
                                    0, ",",
                                    r.empr_id, ",",
                                    r.esta_id, ",",
                                    r.res_id,
                                    ")"
            --DISPLAY querybuffer                        
            PREPARE Q01 FROM QueryBuffer
            EXECUTE Q01

            --Grabando el detalle
            DECLARE detalleFac CURSOR FOR
               SELECT
                  a.correl,
                  'B',
                  a.cditem,
                  b.dsitem,
                  c.nomabr,
                  a.cantid,
                  a.preuni,
                  a.totpro,
                  a.totpro,
                  0,
                  1,
                  a.totpro,
                  0,
                  0,
                  a.totpro - a.totpro/1.12 
               FROM
                  segovia@lnxseg_tcp:fac_dtransac a,
                  segovia@lnxseg_tcp:inv_products b,
                  segovia@lnxseg_tcp:inv_unimedid C 
               WHERE
                      a.lnktra = r.doc_id 
                  AND b.cditem = a.cditem 
                  AND C.unimed = b.unimed  

             FOREACH detalleFac INTO rd.*
               INSERT INTO ddoc 
                           (
                           doc_id,
                           doc_lin,
                           tipoProducto, 
                           codigoProducto, 
                           descripcionProducto, 
                           unidadMedida, 
                           cantidad, 
                           precioUnitario ,
                           importeTotalOperacion ,
                           montoBruto ,
                           importeExento ,
                           importeNetoGravado ,
                           montoDescuento ,
                           importeOtrosImpuestos ,
                           detalleImpuestosIva 
                           )
                  VALUES   ( r.doc_id, rd.* )
             END FOREACH
        END IF
      LET idx = idx + 1
      LET pcount = (idx / pmax) * 100
      LET progreso = pcount
      DISPLAY BY NAME progreso
      CALL ui.Interface.refresh()
      
   END FOREACH

   CALL box_valdato("Documentos Trasladados")
   RETURN 0
END FUNCTION

FUNCTION FormatoFecha (pRecibe )
DEFINE pRecibe    VARCHAR(20),
       pFecha     DATE,
       pRetorno   VARCHAR(20)
   LET pfecha  = pRecibe CLIPPED
   IF pFecha IS NOT null THEN
      LET pRetorno = "'" || DAY(pfecha)  || "/" || MONTH(pfecha) || "/" || YEAR(pfecha) || "'"
   ELSE
      LET pRetorno = "Null"
   END IF
   RETURN pRetorno
END FUNCTION

FUNCTION FormatoTiempo (pRecibe )
DEFINE pRecibe    VARCHAR(20),
       pFecha     DATE,
       pRetorno   VARCHAR(20)
   LET pfecha  = pRecibe
   LET pRetorno = "'" || YEAR(pfecha)  || "-" || MONTH(pfecha)   || "-" || DAY(pfecha)  || " 00:00:00'"   RETURN pRetorno
   RETURN pRetorno
END FUNCTION

FUNCTION FormatoBoolean (pRecibe )
DEFINE pRecibe       SMALLINT,
       pRetorno      VARCHAR(5)
       IF pRecibe = 0 THEN
         LET pRetorno = "'f'"
      ELSE
         LET pRetorno = "'t'"
      END IF
   RETURN pRetorno   
END FUNCTION