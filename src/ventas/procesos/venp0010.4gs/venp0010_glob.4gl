################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Ricardo Ram�rez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# 
################################################################################
DATABASE face
GLOBALS
TYPE docdet RECORD
      doc_id                    LIKE mdoc.doc_id,
      estadoDocumento           LIKE mdoc.estadodocumento,
      codigoEstablecimiento     LIKE mdoc.codigoestablecimiento,
      serieDocumento            LIKE mdoc.seriedocumento,
      numeroDocumento           LIKE mdoc.numerodocumento,
      fechaDocumento            LIKE mdoc.fechadocumento,
      nitComprador              LIKE mdoc.nitcomprador,
      nombreComercialComprador  LIKE mdoc.nombrecomercialcomprador,
      codigoMoneda              LIKE mdoc.codigomoneda,
      tipoCambio                LIKE mdoc.tipocambio,
      importeBruto              LIKE mdoc.importebruto,
      cer_estado                LIKE mdoc.doc_certificado
END RECORD

DEFINE
   gr_reg  RECORD
      doc_anio             LIKE mdoc.doc_anio,
      doc_mes              LIKE mdoc.doc_mes
   END RECORD,
   gr_det                  DYNAMIC ARRAY OF docdet,
   g_empr                  RECORD LIKE mempr.*,
   dbperm,
   dbname                  VARCHAR(40),
   vempr_nomlog            CHAR(15),
   usuario                 CHAR(20),
   hr                      DATE, 
   gtit_enc, 
   gtit_pie,
   nombre_depproamento     VARCHAR(80),
   where_report            VARCHAR (1000),
   aui om.DOMnode,
   where_report            VARCHAR(1000),
   id_empresa              INTEGER,
   nombre_empresa          CHAR(50),
   progreso                SMALLINT
END GLOBALS