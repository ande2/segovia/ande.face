################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : cat_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               cat_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "venp0018_glob.4gl"

FUNCTION busca_cat()
DEFINE
   QueryBuffer          VARCHAR(1000),
	cat_query		      VARCHAR(1000),
	where_clause	      VARCHAR(1500),
	cat_cnt              SMALLINT,
   condicion            CHAR(300),
	cat_count		      CHAR(300),
   mensaje              CHAR(100),
   respuesta            CHAR(6),
   lint_flag            SMALLINT,
	x_condicion          VARCHAR(1000),
   idx                  INTEGER,
   lorigen              CHAR(3),
   r                    docdet,
   p, s                 INTEGER,
   txtXML               string
   
   --CALL encabezado()

   LET gtit_enc="BUSQUEDA REGISTRO"
   IF gr_reg.del IS NULL THEN
      LET gr_reg.del = MDY( MONTH(TODAY), 1, YEAR(TODAY))
      LET gr_reg.al = TODAY
      LET gr_reg.estado = 0
   END IF 
   CALL gr_det.clear()

   DISPLAY BY NAME gtit_enc, gr_reg.*                                                                              
   LET int_flag = FALSE

   INPUT 
      BY NAME  gr_reg.estab_id, gr_reg.serie_id, gr_reg.del, gr_reg.al, gr_reg.estado 
      WITHOUT DEFAULTS 
   
      BEFORE INPUT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

      BEFORE FIELD serie_id
         WHENEVER ERROR CONTINUE
         DROP TABLE tmp_serie
         LET QueryBuffer = "SELECT   res_id serie_id, res_serie || ' - ' || b.tipcon_nom || ' ' ||  a.res_nautoriza serie_nom ",
                           "   FROM  dres a, mtipcon b ",
                           "   WHERE a.esta_id =", gr_reg.estab_id,
                           "   AND   b.tipcon_id = a.tipcon_id ",
                           " UNION ALL ",
                           " SELECT   0, 'X' || ' - ' || 'Todas'",
                           "   FROM  dres ",
                           "   WHERE res_id = 1 ",
                           " INTO TEMP tmp_serie "
         PREPARE QuerySer FROM QueryBuffer
         EXECUTE QuerySer
         CALL combo_din2("serie_id","SELECT serie_id, serie_nom FROM tmp_serie ORDER BY serie_id ") 
         WHENEVER ERROR STOP

         
      AFTER INPUT


         
         IF gr_reg.del IS NULL OR gr_reg.al IS NULL THEN
            CALL box_valdato("Debe ingresar todos los datos.")
            NEXT FIELD del
         END IF
         IF MONTH(gr_reg.del) != MONTH(gr_reg.al ) AND
            YEAR(gr_reg.del ) != YEAR(gr_reg.al ) THEN
            CALL box_valdato("Debe ingresar fechas del mismo mes.")
            NEXT FIELD del
         END IF
         
         
         IF int_flag THEN
            CALL box_inter() RETURNING int_flag 
            IF int_flag = TRUE THEN
               EXIT INPUT
            ELSE
               CONTINUE INPUT
            END IF
         END IF
   END INPUT

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      ERROR "B�squeda Abortada."
      RETURN 0
   END IF

   LET cat_query = "SELECT ",
                   "       mdoc.doc_id ",
                   "     , mesta.esta_id ",
                   "     , mdoc.estadoDocumento", 
                   "     , mdoc.serieautorizada", 
                   "     , mdoc.numerodocumento ",
                   "     , mdoc.fechadocumento", 
                   "     , mdoc.nitcomprador ",
                   "     , mdoc.nombreComercialComprador", 
                   "     , mdoc.codigoMoneda ",
                   "     , mdoc.tipoCambio", 
                   "     , mdoc.importeBruto ", 
                   "     , estado_documento ( mdoc.doc_id) cer_estado ",
                   "  FROM",
                   "     mdoc", 
                   "     , dres ",
                   "     , mesta ",
                   "  WHERE",
                   "          DATE(mdoc.fechadocumento) BETWEEN '", gr_reg.del USING "dd/mm/yyyy", "'",
                   "     AND  '", gr_reg.al USING "dd/mm/yyyy", "'", 
                   "     AND  mesta.empr_id = mdoc.empr_id ", 
                   "     AND  mesta.esta_id = mdoc.esta_id ",
                   "     AND  dres.res_id = mdoc.res_id " 
      IF gr_reg.estab_id <>  0 THEN
         LET cat_query = cat_query CLIPPED,
                           " AND mdoc.esta_id = ", gr_reg.estab_id
      END IF
      
      IF gr_reg.serie_id <> 0 THEN
         LET cat_query = cat_query CLIPPED,
                           " AND mdoc.res_id = ", gr_reg.serie_id
      END IF

      IF gr_reg.estado > 0 THEN
         LET cat_query = cat_query CLIPPED,
                        " AND estado_documento ( mdoc.doc_id) = ", gr_reg.estado
      END IF
      
      LET cat_query = cat_query CLIPPED,
                   "  ORDER BY",
                   "     4, 6, 5"

   PREPARE ex_stmt FROM cat_query

   DECLARE cat_ptr CURSOR FOR ex_stmt

   LET idx = 1
   FOREACH cat_ptr INTO r.*
      LET gr_det[idx].* = r.*
      LET idx = idx + 1
   END FOREACH
   LET idx = idx - 1


   DIALOG

      DISPLAY ARRAY gr_det TO sDet.*

         BEFORE ROW
            LET P = ARR_CURR()
            LET s = SCR_LINE ()
            CALL LlenarLog( gr_det[p].doc_id )


      END DISPLAY

      DISPLAY ARRAY gr_log TO sLog.*

            
      
      END DISPLAY
      
      BEFORE DIALOG
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         --CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	

      
   
      ON KEY (ACCEPT )
         EXIT DIALOG         
 
   END DIALOG
   
   RETURN idx
END FUNCTION


FUNCTION llenarLog( ldoc_id )
DEFINE 
   idx,
   ldoc_id        INTEGER,
   lmensaje       VARCHAR(255),
   lfecha         DATE,
   lnum           INTEGER,
   lerr           VARCHAR(255)

   CALL gr_log.clear()

   LET idx = 1
   
   DECLARE listaLog CURSOR FOR
      SELECT   err_date, err_num, err_message
         FROM  ldoc
         WHERE doc_id = ldoc_id
         ORDER BY err_date

   FOREACH listaLog INTO lfecha, lnum, lerr   
      LET lmensaje = lfecha USING "DD/MM/YYYY", " - ",
                     lnum   USING "&&&&", " - ",
                     lerr   CLIPPED
      LET gr_log[idx].log_desc = lmensaje
      LET idx = idx + 1
   END FOREACH
   LET idx = idx - 1

   DISPLAY ARRAY gr_log TO sLog.*
      BEFORE DISPLAY
         EXIT DISPLAY

   END DISPLAY
END FUNCTION 