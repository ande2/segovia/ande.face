GLOBALS "venp0018_glob.4gl"



FUNCTION CertificaDocElec (  )
DEFINE
   QueryBuffer VARCHAR(1000),
   lcount,
   id_empresa  INTEGER,
   ldoc_id     INTEGER,
   idx         INTEGER,
   pmax,
   pcount      INTEGER,
   lmensaje    VARCHAR(100),
   lcerti,
   lnocerti    INTEGER,
   lestado     SMALLINT,
   lfirma      VARCHAR(100),
   comando     VARCHAR(100)

   LET lcerti = 0
   LET lnocerti = 0

   LET pmax = gr_det.getLength()
   
   FOR idx = 1 TO pmax

      LET lmensaje = gr_det[idx].serieDocumento   CLIPPED,
                     ' - ',
                     gr_det[idx].numeroDocumento     USING "&&&&&&&&",
                     ' ---> '

      IF gr_det[idx].cer_estado != 2 THEN

         LET lfirma = certidoc ( gr_det[idx].doc_id) 
      
         IF length(lfirma) > 0  THEN
            LET gr_det[idx].doc_firmaelectronica = lfirma
            LET gr_det[idx].cer_estado = 2
            LET lmensaje = lmensaje, 'Certificado: ', lfirma
            LET lcerti = lcerti + 1
            CALL addMessage ( lmensaje)
         ELSE
            LET lmensaje = lmensaje, 'No Certificado'
            LET lnocerti = lnocerti + 1
         END IF
      END IF
      
      LET pcount = (idx / pmax) * 100
      LET progreso = pcount
      DISPLAY BY NAME progreso
      CALL ui.Interface.refresh()
      
   END FOR

   FOR idx = 1 TO pmax
    IF gr_det[idx].cer_estado = 2 THEN
        UPDATE  mdoc
            SET mdoc.doc_firmaelectronica = gr_det[idx].doc_firmaelectronica,
                mdoc.doc_certificado = 1
            WHERE mdoc.doc_id = gr_det[idx].doc_id
    END IF
   END FOR
   
   LET lmensaje =  "Documentos Certificados  -->", lcerti USING '<<,<<&',
                   "\n",
                   "Documentos Con Problemas --> ", lnocerti USING '<<,<<&'
   CALL box_valdato(lmensaje)

   IF lnocerti > 0 THEN
         DISPLAY ARRAY gr_log TO sLog.*
      BEFORE DISPLAY
          CALL fgl_set_arr_curr(idx)
            CALL ui.Interface.refresh()
   END DISPLAY
   END IF
   RETURN 0
END FUNCTION

FUNCTION addMessage( lmensaje )
DEFINE lmensaje VARCHAR(100)
DEFINE idx INTEGER
   CALL  gr_log.appendElement()
   LET idx = gr_log.getLength()
   LET gr_log[idx].log_desc = lmensaje
   DISPLAY ARRAY gr_log TO sLog.*
      BEFORE DISPLAY
          CALL fgl_set_arr_curr(idx)
            CALL ui.Interface.refresh()
          EXIT DISPLAY
   END DISPLAY
END FUNCTION