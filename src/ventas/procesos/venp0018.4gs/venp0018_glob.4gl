################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Ricardo Ram�rez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# 
################################################################################
DATABASE face
GLOBALS
TYPE docdet RECORD
      doc_id                     LIKE mdoc.doc_id,
      esta_id                    LIKE mdoc.esta_id,
      doc_est                    VARCHAR(15),
      seriedocumento             LIKE mdoc.seriedocumento,
      numerodocumento            LIKE mdoc.numerodocumento,
      fechadocumento             DATE,
      nitcomprador               LIKE mdoc.nitcomprador,
      nombreComercialComprador   LIKE mdoc.nombreComercialComprador,
      codigoMoneda               LIKE mdoc.codigoMoneda,
      tipoCambio                 LIKE mdoc.tipoCambio,
      importeBruto               DEC(10,2),
      cer_estado                 SMALLINT,
      doc_firmaelectronica       VARCHAR(100)
END RECORD
TYPE doclog RECORD
   log_desc                VARCHAR(100)
END RECORD

DEFINE
   gr_reg  RECORD
      estab_id        INTEGER,
      serie_id        INTEGER,
      del             DATE,
      al              DATE,
      estado          SMALLINT
   END RECORD,
   gr_det                  DYNAMIC ARRAY OF docdet,
   gr_log                  DYNAMIC ARRAY OF doclog,
   g_empr                  RECORD LIKE mempr.*,
   dbperm,
   dbname                  VARCHAR(40),
   vempr_nomlog            CHAR(15),
   usuario                 CHAR(20),
   hr                      DATE, 
   gtit_enc, 
   gtit_pie,
   nombre_depproamento     VARCHAR(80),
   where_report            VARCHAR (1000),
   aui om.DOMnode,
   where_report            VARCHAR(1000),
   id_empresa              INTEGER,
   nombre_empresa          CHAR(50),
   progreso                SMALLINT
END GLOBALS