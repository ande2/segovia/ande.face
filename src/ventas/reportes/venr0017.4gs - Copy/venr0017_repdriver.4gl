SCHEMA face
GLOBALS "venr0017_glob.4gl"
TYPE report1_data RECORD
      res_id               LIKE dres.res_id,
      res_serie            LIKE dres.res_serie,
      tipcon_nom            LIKE mtipcon.tipcon_nom,
      res_neto            DEC(10,2),
      res_bruto             DEC(10,2)
END RECORD
DEFINE tot_monto           DEC(10,2)
DEFINE tot_iva             DEC(10,2)


FUNCTION run_report1_to_handler(handler)
    DEFINE
        DATA report1_data,
        handler om.SaxDocumentHandler,
        idx INTEGER

   LET tot_monto = 0
   LET tot_iva = 0
   START REPORT report1_report TO XML HANDLER HANDLER
   FOR idx = 1 TO gr_res.getLength()
      IF gr_res[idx].res_neto IS NOT NULL AND
         gr_res[idx].res_bruto IS NOT NULL THEN
         LET DATA.* =gr_res[idx].*
         OUTPUT TO REPORT report1_report(DATA.*)
      END IF 
   END FOR
   FINISH REPORT report1_report
END FUNCTION

REPORT report1_report(data)
    DEFINE
        data report1_data

    FORMAT
        ON EVERY ROW
            PRINTX data.*
            LET tot_monto = tot_monto + data.res_neto
            LET tot_iva = tot_iva + data.res_bruto
            

   ON LAST ROW
      PRINTX tot_monto, tot_iva
      
END REPORT