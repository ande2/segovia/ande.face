################################################################################
# Funcion     : %M%
# Descripcion : Definicion de variables para mantenimiento de catalogo 
# Funciones   :
#
#
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No. : %Z% %W%
# Autor       : Ricardo Ram�rez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# 
################################################################################
SCHEMA "face"
 
GLOBALS
TYPE docres RECORD
      res_id               LIKE dres.res_id,
      res_serie            LIKE dres.res_serie,
      tipcon_nom           LIKE mtipcon.tipcon_nom,
      res_neto             DEC(10,2),
      res_bruto            DEC(10,2)
END RECORD
DEFINE
   gr_reg  RECORD
      doc_anio             LIKE mdoc.doc_anio,
      doc_mes              LIKE mdoc.doc_mes
   END RECORD,
    doc_serie varchar(20),
    doc_num integer,
    doc_femision datetime year to second,
    cli_nit varchar(20),
    cli_nomcom varchar(80),
    doc_codmon char(3),
    doc_tipocambio decimal(10,5),
    doc_est varchar(20),
    doc_ing_neto_gravado decimal(10,2),
    doc_total_iva decimal(10,2),

   gr_det DYNAMIC ARRAY OF RECORD
      serieDocumento                LIKE mdoc.serieDocumento,
      numeroDocumento               LIKE mdoc.numeroDocumento,
      fechaDocumento                DATE,
      nitComprador                  LIKE mdoc.nitComprador,
      nombreComercialComprador      LIKE mdoc.nombreComercialComprador,
      codigoMoneda                  LIKE mdoc.codigoMoneda,
      tipoCambio                    LIKE mdoc.tipoCambio,
      estadoDocumento               LIKE mdoc.estadoDocumento,
      importeNetoGravado            LIKE mdoc.importeNetoGravado,
      importeBruto                  LIKE mdoc.importeBruto
   END RECORD,
   gr_detatt DYNAMIC ARRAY OF RECORD
      serieDocumento                LIKE mdoc.serieDocumento,
      numeroDocumento               LIKE mdoc.numeroDocumento,
      fechaDocumento                DATE,
      nitComprador                  LIKE mdoc.nitComprador,
      nombreComercialComprador      LIKE mdoc.nombreComercialComprador,
      codigoMoneda                  LIKE mdoc.codigoMoneda,
      tipoCambio                    LIKE mdoc.tipoCambio,
      estadoDocumento               LIKE mdoc.estadoDocumento,
      importeNetoGravado            LIKE mdoc.importeNetoGravado,
      importeBruto                  LIKE mdoc.importeBruto
   END RECORD,
   gr_sum RECORD 
        totalf          LIKE mdoc.importeNetoGravado,
        totaliva        LIKE mdoc.importeBruto,
        registros       SMALLINT 
    END RECORD,
   gr_res DYNAMIC ARRAY OF docres,
   g_empr                  RECORD LIKE mempr.*,
   dbperm,
   dbname                  VARCHAR(40),
   vempr_nomlog            CHAR(15),
   usuario                 CHAR(20),
   hr                      DATE, 
   gtit_enc, 
   gtit_pie,
   nombre_depproamento     VARCHAR(80),
   where_report            VARCHAR (1000),
   aui om.DOMnode,
   where_report            VARCHAR(1000),
   id_empresa              INTEGER,
   nombre_empresa          CHAR(50)
END GLOBALS