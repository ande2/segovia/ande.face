SCHEMA face
GLOBALS "venr0017_glob.4gl"
TYPE report1_data RECORD
      res_id               LIKE dres.res_id,
      res_serie            LIKE dres.res_serie,
      tipcon_nom            LIKE mtipcon.tipcon_nom,
      res_neto            DEC(10,2),
      res_bruto             DEC(10,2)
END RECORD

TYPE report1_data2 RECORD 
      serieDocumento                LIKE mdoc.serieDocumento,
      numeroDocumento               LIKE mdoc.numeroDocumento,
      fechaDocumento                DATE,
      nitComprador                  LIKE mdoc.nitComprador,
      nombreComercialComprador      LIKE mdoc.nombreComercialComprador,
      codigoMoneda                  LIKE mdoc.codigoMoneda,
      tipoCambio                    LIKE mdoc.tipoCambio,
      estadoDocumento               LIKE mdoc.estadoDocumento,
      importeNetoGravado            LIKE mdoc.importeNetoGravado,
      importeBruto                  LIKE mdoc.importeBruto
END RECORD 

DEFINE tot_bruto           DEC(10,2)
DEFINE tot_iva             DEC(10,2)
DEFINE tot_neto            DEC(10,2)


FUNCTION run_report1_to_handler(handler)
    DEFINE
        data report1_data,
        data2 report1_data2,
        handler om.SaxDocumentHandler,
        idx INTEGER

   LET tot_bruto = 0
   LET tot_iva   = 0
   LET tot_neto  = 0 
   START REPORT report1_report TO XML HANDLER HANDLER
   FOR idx = 1 TO gr_det.getLength()
      --IF gr_res[idx].res_neto IS NOT NULL AND
         --gr_res[idx].res_bruto IS NOT NULL THEN
         LET data2.* = gr_det[idx].*
         OUTPUT TO REPORT report1_report(data2.*)
      --END IF 
   END FOR
   FINISH REPORT report1_report
END FUNCTION

REPORT report1_report(data2)
    DEFINE
        data2 report1_data2,
        vIva DECIMAL (10,2)

    FORMAT
        ON EVERY ROW
            LET vIva = (data2.importeBruto - data2.importeNetoGravado)*-1
            PRINTX vIva
            PRINTX data2.*
            IF data2.importeBruto IS NOT NULL THEN 
               IF data2.estadoDocumento == "Activo" THEN 
                  LET tot_bruto = tot_bruto + data2.importeBruto
                  LET tot_iva   = tot_iva   + vIva
                  LET tot_neto  = tot_neto  + data2.importeNetoGravado
               END IF    
            END IF    
            

   ON LAST ROW
      PRINTX tot_bruto, tot_iva, tot_neto
      
END REPORT