################################################################################
# Funcion     : %M%
# Descripcion : Funcion para consulta de catalogo 
# Funciones   : cat_busca_init() 
#               busca_cat()
#               fetch_cat(fetch_flag) 
#               cat_desp()
#               limpiar() 
#
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : erickalvarez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################


GLOBALS "venr0020_glob.4gl"

FUNCTION cat_busca_init()
DEFINE
   select_stmt3			VARCHAR(1000)

   LET select_stmt3 = "SELECT * ", 
                      " from  mdoc a "
   DISPLAY select_stmt3
   PREPARE ex_stmt3 FROM select_stmt3

END FUNCTION

FUNCTION busca_cat()
DEFINE
	cat_query		VARCHAR(1000),
	where_clause	VARCHAR(1500),
	cat_cnt        SMALLINT,
   condicion      CHAR(300),
	cat_count		CHAR(300),
   mensaje        CHAR(100),
   respuesta   CHAR(6),
   lint_flag   smallint,
	x_condicion VARCHAR(1000),
   idx         INTEGER,
   lorigen              CHAR(3)



   --CALL encabezado()

   LET gtit_enc="BUSQUEDA REGISTRO"
   INITIALIZE gr_reg.*  TO NULL
   --INITIALIZE gr_sum.*  TO NULL
   CALL gr_det.clear()
   LET int_flag = FALSE

 INPUT BY NAME gr_reg.doc_mes, gr_reg.doc_anio WITHOUT DEFAULTS 
   
      BEFORE INPUT
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
         --CALL fgl_dialog_setkeylabel("CONTROL-B", "Buscar")
         CALL fgl_dialog_setkeylabel("INTERRUPT","Cancelar")	
            LET gr_reg.doc_anio=year(today)
            LET gr_reg.doc_mes =month(today)
            DISPLAY gr_reg.doc_mes,gr_reg.doc_anio
      AFTER INPUT

         IF gr_reg.doc_anio IS NULL OR gr_reg.doc_mes IS NULL THEN
            CALL box_valdato("Debe ingresar todos los datos.")
            NEXT FIELD doc_mes
         END IF
         
         IF int_flag THEN
            CALL box_inter() RETURNING int_flag 
            IF int_flag = TRUE THEN
               EXIT INPUT
            ELSE
               CONTINUE INPUT
            END IF
         END IF
   END INPUT

   

   IF int_flag = TRUE THEN
      LET int_flag = FALSE
      CLEAR FORM
      ERROR "B�squeda Abortada."
      RETURN 0
   END IF
   LET gr_sum.totalf=NULL
   LET gr_sum.totaliva=NULL
   
   LET cat_query = "SELECT    mdoc.serieAutorizada, mdoc.numeroDocumento, mdoc.fechaDocumento, mdoc.nitComprador, mdoc.nombreComercialComprador,",
                   "          mdoc.codigoMoneda, mdoc.tipoCambio, mdoc.estadoDocumento, mdoc.importenetogravado+mdoc.importetotalexento,  mdoc.importeBruto",
                   "  FROM    mdoc, mesta, dres, mtipcon",
                   "  WHERE   mdoc.doc_anio     = ", gr_reg.doc_anio,     
                   "  AND     mdoc.doc_mes      = ", gr_reg.doc_mes,
                   "  AND     mesta.esta_id     = mdoc.esta_id ",
                   "  AND     dres.esta_id      = mesta.esta_id",
                   "  AND     dres.res_serie    = mdoc.serieautorizada",
                   "  AND     mtipcon.tipcon_id = dres.tipcon_id",
                   "  ORDER BY  mdoc.serieAutorizada, mdoc.numerodocumento"
   
   PREPARE ex_stmt FROM cat_query
   

   DECLARE cat_ptr CURSOR FOR ex_stmt
   LET idx = 1
   
   FOREACH cat_ptr INTO gr_det[idx].*

      LET idx = idx + 1
   END FOREACH

   --Llenando el grid de resumen
   LET cat_Query = "SELECT  dres.res_id, dres.res_serie, mtipcon.tipcon_nom,",
                   "            sum( (",
                   "            case  ",
                   "                when mdoc.estadoDocumento='Anulado'",
                   "                    THEN      0.00 ",
                   "                    ELSE  nvl(mdoc.importeNetoGravado+mdoc.importetotalexento,0)", 
                   "                 END) * mtipcon.tipcon_operacion  ), ",
                   "            sum( (CASE  when mdoc.estadoDocumento='Anulado' THEN  0.00",
                   "                ELSE    mdoc.detalleimpuestosiva   END) * mtipcon.tipcon_operacion   )",
                   "    FROM  mdoc, mesta, dres, mtipcon ",
                   "  WHERE mdoc.doc_anio  = ",  gr_reg.doc_anio,     
                   "  AND   mdoc.doc_mes   = ", gr_reg.doc_mes,
                   "    AND   mesta.esta_id = mdoc.esta_id",
                   "    AND   dres.esta_id    = mesta.esta_id",
                   "    AND   dres.res_serie = mdoc.serieautorizada",
                   "   and   dres.tipcon_id = mtipcon.tipcon_id",
                   "    GROUP BY  1,2,3",
                   "    ORDER BY 2"
                   
   PREPARE ex_stmt2 FROM cat_query

   DECLARE cat_ptr2 CURSOR FOR ex_stmt2
   LET idx = 1
   
   FOREACH cat_ptr2 INTO gr_res[idx].*
      LET idx = idx + 1
   END FOREACH

   DIALOG

      DISPLAY ARRAY gr_det TO sDet.*
      END display
   
      DISPLAY ARRAY gr_res TO sRes.*

      END DISPLAY

      BEFORE DIALOG
         CALL fgl_dialog_setkeylabel("ACCEPT","Aceptar")	
   
      ON KEY (ACCEPT )
         EXIT DIALOG  
   END DIALOG
   
   RETURN idx
END FUNCTION