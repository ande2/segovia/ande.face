################################################################################
# Funcion     : %M%
# Descripcion : Modulo principal para cliente
# Funciones   : main
#					 cat_init()
#               cat_menu()
#               encabezado()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Ricardo Ramirez
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador     Fecha                    Descripcion de la modificacion
#
################################################################################

GLOBALS "venr0020_glob.4gl"

MAIN
DEFINE
   n_param SMALLINT

DEFER INTERRUPT

OPTIONS
   INPUT WRAP,
   HELP FILE "venr0020_help.ex",
   HELP KEY CONTROL-W,
   COMMENT 	LINE OFF,
   PROMPT 	LINE LAST - 2,
   MESSAGE 	LINE LAST - 1,
   ERROR 	LINE LAST
                                                                                
	LET n_param = num_args()
	IF n_param = 0 THEN
		RETURN
	ELSE
		LET dbname = arg_val(1)
	END IF
   LET dbperm = dbname
--   LET dbname = "face"
   DATABASE dbname
   SELECT   a.empr_id, a.empr_nom      
      INTO  id_empresa, nombre_empresa
      FROM  face:mempr a
      WHERE a.empr_db = dbperm
   CALL busca_empresa(dbperm) RETURNING g_empr.*
   CALL cat_init(dbname)
   
   CALL startlog("venr0020.log")
   CALL cat_init(dbname)
   
   CALL cat_busca_init()
   --CLOSE WINDOW SCREEN

   CALL main_menu()
END MAIN

FUNCTION cat_init(dbname)
DEFINE
   nombre_departamento, 
   nombre_empresa             CHAR(50),
   cmd                        CHAR(40),
   ip_usu, dbname             CHAR(40),
   fecha_actual               DATE,
   x_condicion,
   dir_destino                CHAR(100)
   LET usuario = fgl_getenv("LOGNAME")
   LET fecha_actual = today
   let hr =fecha_actual
   
   SELECT   a.empr_id, a.empr_nom      
      INTO  id_empresa, nombre_empresa
      FROM  face:mempr a
      WHERE a.empr_db = dbname
      
   OPEN FORM cat_form FROM "venr0020_form"

   DISPLAY FORM cat_form
   CLEAR FORM
   CALL fgl_settitle("DOCUMENTOS DE TRASLADO PARA CERTIFICAR")

END FUNCTION

FUNCTION main_menu()
DEFINE 
   cuantos		SMALLINT,
   usuario CHAR(8),
   vopciones CHAR(255), --cadena de ceros y unos para control de permisos
   ord_opc SMALLINT,    --orden de las opciones en el menu
   cnt     SMALLINT,    --contdor
   tol ARRAY[13] OF RECORD
      opcion   CHAR(15),
      acc      CHAR(15),
      desc_opc CHAR(60),
      des_cod  SMALLINT,
      imagen    CHAR(20),
      linkprop INTEGER
   END RECORD,
   cpo ARRAY[13] OF RECORD
      opcion   CHAR(15),
      desc_opc CHAR(60),
      des_cod  SMALLINT,
      imagen    CHAR(20),
      linkprop INTEGER
   END RECORD,

   tb om.DomNode,
   tbi om.DomNode,
   tbs om.DomNode,
   tm  om.DomNode,
   tmg om.DomNode,
   tmi om.DomNode,
   tms om.DomNode,
   f  om.DomNode

   LET cnt = 1
   LET ord_opc = NULL

   -- captura nombre de usuario
   LET usuario = fgl_getenv("LOGNAME")
    DISPLAY usuario
    DISPLAY dbperm
   -- captura permisos del usuario sobre las opciones del menu
   CALL men_opc(usuario,"venr0017", dbperm) RETURNING vopciones
   --LET vopciones='111'
   -- carga opciones de menu
   DECLARE opciones_menu CURSOR FOR
      SELECT   b.prog_ord,c.des_desc_md,b.prop_desc,
               c.des_cod,c.des_desc_ct
         FROM  face:mprog a,
               face:dprogopc b, 
               face:sd_des c
         WHERE a.prog_nom = "venr0017"
         AND   b.prog_id = a.prog_id
         AND   c.des_tipo = 22
         AND   c.des_cod = b.des_cod
         ORDER BY b.prog_ord
         
	LET cnt = 1
   
   FOREACH opciones_menu INTO ord_opc,cpo[cnt].*
          LET cnt = cnt + 1
   END FOREACH
   FREE opciones_menu

   FOR cnt = 1 TO LENGTH(vopciones)
          LET tol[cnt].acc     =DOWNSHIFT(cpo[cnt].opcion CLIPPED)
          LET tol[cnt].opcion  =cpo[cnt].opcion CLIPPED
          LET tol[cnt].desc_opc = cpo[cnt].desc_opc CLIPPED
          LET tol[cnt].des_cod  = cpo[cnt].des_cod
          LET tol[cnt].imagen   = DOWNSHIFT(cpo[cnt].imagen CLIPPED)
          LET tol[cnt].linkprop = cpo[cnt].linkprop
   END FOR

   LET aui=ui.Interface.getRootNode()
   LET tb =aui.createChild("ToolBar")
   LET tbi=createToolBarItem(tb,tol[1].acc,tol[1].opcion,tol[1].desc_opc,tol[1].imagen) --Buscar
   LET tbi=createToolBarItem(tb,tol[2].acc,tol[2].opcion,tol[2].desc_opc,tol[2].imagen) --Generar
   LET tbi=createToolBarItem(tb,tol[3].acc,tol[3].opcion,tol[3].desc_opc,tol[3].imagen) --Salir

   MENU ""
      BEFORE MENU

         LET cuantos = 0
      -- despliega opciones a las que tiene acceso el usuario
         HIDE OPTION ALL
         FOR cnt = 1 TO LENGTH(vopciones)
            IF vopciones[cnt] = 1 AND
               (tol[cnt].des_cod = 1 OR
               tol[cnt].des_cod = 6 OR
               tol[cnt].des_cod = 9) THEN
               DISPLAY tol[cnt].opcion
               SHOW OPTION tol[cnt].opcion
               LET cuantos = cuantos + 1
            END IF
         END FOR

         IF cuantos = 0 THEN
            CALL box_error("Lo siento, no tiene acceso a este programa")
            EXIT MENU
         END IF

-- opcion busqueda
	COMMAND KEY("B") tol[1].opcion tol[1].desc_opc
		HELP 2
		CALL busca_cat() RETURNING cuantos
        IF cuantos > 0 THEN
         SHOW OPTION tol[2].opcion
        END IF 

      
 -- opcion imprimir
   COMMAND KEY ("G") tol[2].opcion tol[2].desc_opc
		HELP 8
      IF cuantos > 0 THEN 
        CALL PrintReport ( )
      ELSE
       CALL box_error("No hay datos")
      END if

      
-- opcion salir
   COMMAND key("Q") tol[3].opcion tol[3].desc_opc
		HELP 8
		EXIT MENU
END MENU
END FUNCTION


FUNCTION usu_destino()
DEFINE
   ip_usu CHAR(20),
   i SMALLINT
   LET ip_usu = fgl_getenv("FGLSERVER")
   FOR i = 1 TO 20
      IF ip_usu[i] = ":" THEN
         EXIT FOR
      END IF
   END FOR
   LET ip_usu = ip_usu[1,i-1] CLIPPED
   RETURN ip_usu
END FUNCTION