SCHEMA face
GLOBALS "venr0020_glob.4gl"
TYPE report1_data RECORD
      res_id               LIKE dres.res_id,
      res_serie            LIKE dres.res_serie,
      tipcon_nom            LIKE mtipcon.tipcon_nom,
      res_neto            DEC(10,2),
      res_bruto             DEC(10,2)
END RECORD

TYPE report1_data2 RECORD 
      serieDocumento                LIKE mdoc.serieDocumento,
      numeroDocumento               LIKE mdoc.numeroDocumento,
      fechaDocumento                DATE,
      nitComprador                  LIKE mdoc.nitComprador,
      nombreComercialComprador      LIKE mdoc.nombreComercialComprador,
      codigoMoneda                  LIKE mdoc.codigoMoneda,
      tipoCambio                    LIKE mdoc.tipoCambio,
      estadoDocumento               LIKE mdoc.estadoDocumento,
      importeNetoGravado            LIKE mdoc.importeNetoGravado,
      importeBruto                  LIKE mdoc.importeBruto
END RECORD 

DEFINE tot_monto           DEC(10,2)
DEFINE tot_iva             DEC(10,2)


FUNCTION run_report1_to_handler(handler)
    DEFINE
        data report1_data,
        data2 report1_data2,
        handler om.SaxDocumentHandler,
        idx INTEGER

   LET tot_monto = 0
   LET tot_iva = 0
   START REPORT report1_report TO XML HANDLER HANDLER
   FOR idx = 1 TO gr_det.getLength()
      --IF gr_res[idx].res_neto IS NOT NULL AND
         --gr_res[idx].res_bruto IS NOT NULL THEN
         LET data2.* = gr_det[idx].*
         OUTPUT TO REPORT report1_report(data2.*)
      --END IF 
   END FOR
   FINISH REPORT report1_report
END FUNCTION

REPORT report1_report(data2)
    DEFINE
        data2 report1_data2,
        vIva DECIMAL (10,2)

    FORMAT
        ON EVERY ROW
            LET vIva = data2.importeBruto - data2.importeNetoGravado
            PRINTX vIva
            PRINTX data2.*
            --LET tot_monto = tot_monto + data.res_neto
            --LET tot_iva = tot_iva + data.res_bruto
            

   --ON LAST ROW
     -- PRINTX tot_monto, tot_iva
      
END REPORT