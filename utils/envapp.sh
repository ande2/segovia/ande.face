unset LANG

export PATH=$BASEDIR/utils:$PATH
export DBPATH=$BASEDIR/Resources:$BASEDIR/bin
export REMOTEHOST=`who -m | cut -d '(' -f2 | cut -d ')' -f1 | cut -d ':' -f1`
export FGLSERVER=$REMOTEHOST
export FGLDBPATH=$BASEDIR/sql2:$BASEDIR/Resources
export FGLLDPATH=$BASEDIR/src/lib:$BASEDIR/src/bin:$GENERODIR/gre/lib
export FGLDBSCHEMA=$BASEDIR/sql2
export FGLSOURCEPATH=$GENERODIR/gre/src/api:$BASEDIR/sql:$BASEDIR/bin
