# Eliminar la configuracion de idioma
unset LANG
export JAVA_HOME=/opt/java

# Variables de Informix
. /opt/informix/ifmx.sh

# Variables de Genero
. /opt/4js/gst4/envgenero
. /opt/4js/gst4/gre/envgre

export BASEDIR=/app/ande

. $BASEDIR/utils/envapp.sh
