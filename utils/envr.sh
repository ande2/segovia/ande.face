# Eliminar la configuracion de idioma
unset LANG
export JAVA_HOME=/opt/java

# Variables de Informix
. /opt/informix/ifmx.sh

# Variables de Genero
. /opt/fjs/gst1/envgenero
. /opt/fjs/gst1/gre/envgre

export BASEDIR=/app/ande

. $BASEDIR/utils/envapp.sh
